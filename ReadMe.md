# IntroWeb

Progetto di "Introduzione alla programmazione Web", 3 anno, Informatica, Università di Trento

## Studenti

|     #    |                          |                 @                   |
|:--------:|:------------------------:|:-----------------------------------:|
| `193868` | Leonardo Berrighi        | leonardo.berrighi@studenti.unitn.it |
| `192449` | Elia Semprebon           | elia.semprebon@studenti.unitn.it    |
| `192681` | Leonardo Luzi Crivellini | l.luzicrivellini@studenti.unitn.it  |
|          |                          |                                     |

## Scelte tecniche  

<div style="text-align: justify">
Medicare è un sito web per la gestione dei servizi sanitari e risorse ad essi collegati.  
Abbiamo usato Maven, per gestire la complessità, strutturazione ed efficacia del progetto. Rispecchia fedelmente le caratteristiche del MVC.  
L'applicativo viene correttamente visualizzato sia su chrome e mozilla, che da mobile, con grafica responsive per un'ottima visione d'insieme. Come reparto grafico, abbiamo i Bootstrap component, per una migliore armonizzazione e compatibilità.  
La base di dati è gestita localmente. Abbiamo scelto MySql, e il file sql risiede nella cartella "db", insieme a un file python per l'hashing delle password, e un file txt per le mail usate come prova dal mailserver.  
Le varie pagine del progetto sono delle Jsp, per la grafica vediamo il CSS, mentre tutta la parte di controllo e persistenza è gestita da Java.  
Abbiamo utilizzato dei JavaBeans per modellare le istanze del database, e i Dao per andare a popolarle (gli unici che interagiscono col database). La scelta dei JavaBeans ricade sul fatto che sono componenti software riusabili ed indipendenti dalla piattaforma, oltre essere molto utili in fase di scambio di informazioni, anche con le varie pagine.  
Il sistema vede 3 profilazioni utente differenti, con funzionalità diverse. Tutte le notifiche sono gestite anche via mail.
Per quanto riguarda ricette e ticket da pagare, è fornito un pdf con QRcode integrato.
</div>

## Funzionalità

Qui andremo a vedere, passo a passo, ciò che l'utente può fare in ogni pagina.

Nella prima pagina vediamo il bottone login, che ci porta alla sezione apposita, dove è possibile entrare nel sistema con il proprio profilo. E' implementato inoltre il "ricordami". Le funzionalità seguenti sono divise per tipo di login, ricordando che anche i medici di base, e quelli specialistici, visualizzano le informazioni proprie come fossero anche pazienti, semplicemente col pulsante nella home.

PAZIENTE  
Nella home vediamo subito se il sistema presenta notifiche per il paziente (che potrebbero essere XXX), oltre alla lista completa degli esami, per i quali il paziente può andare a ricercare autonomamente per parole chiave o per qualsivoglia elemento.  
La toolBar presenta i seguenti campi:  
* Home -> riporta alla home da qualsiasi pagina ci si trovi  
* Appuntamenti -> è possibile prenotare una visita medica inserendo la richiesta di appuntamento in un google calendar (aggiornamenti richiesta anche via mail)  
* Servizi -> visualizzazione listata di visite mediche, medicinali, esami e anamnesi. I medicinali possono anche essere esportati via PDF, in modo da porterli andare a prendere in farmacia.    
* I miei ticket -> da qui è possibile visualizzare i propri ticket, pagarli ed eventualmente esportarli  
* Profilo -> Logout ed "il mio account", che contiene tutte le informazioni sul paziente e del medico di base (con foto), e la possibilità di cambiare il secondo, oltre che di modificare la propria password. E' possibile anche aggiungere nuove foto profilo.  

MEDICO DI BASE  
Nella home vediamo subito se il sistema presenta notifiche per il medico di base (che potrebbero essere XXX), poi il parco pazienti, se ci sono delle visite da effettuare in quel tal giorno e la lista esami.
La toolBar presenta i seguenti campi:  
* Home -> analogo a quanto sopra  
* I miei pazienti -> una ricerca per informazioni paziente  
* Visite mediche -> visuale sulle proprie visite   
* Profilo -> analogo a quanto sopra  

SPECIALISTA  
Nella home vediamo subito se il sistema presenta notifiche per il medico specialista (che potrebbero essere XXX), poi la lista dei pazienti, se ci sono delle visite specialistiche da effettuare in quel tal giorno e la lista esami.
La toolBar presenta i seguenti campi:  
* Home -> analogo a quanto sopra  
* I miei pazienti -> una ricerca per informazioni paziente  
* Esami -> visuale sugli esami prenotati   
* Profilo -> analogo a quanto sopra  