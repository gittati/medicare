<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
		
		<title>MediCare</title>
		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/fonts/simple-line-icons.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/smoothproducts.css">
		
		<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
		<script src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
		<script src="${pageContext.request.contextPath}/assets/js/smoothproducts.min.js"></script>
		<script src="${pageContext.request.contextPath}/assets/js/theme.js"></script>
		<script src="https://use.fontawesome.com/50f3e3804d.js"></script>
		
	</head>

	<body>
		<nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
			<div class="container"><a class="navbar-brand logo" href="#">MediCare</a><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
				<div class="collapse navbar-collapse" id="navcol-1">
					<ul class="nav navbar-nav ml-auto">
						<li class="nav-item" role="presentation"><a class="nav-link active" href="#">Home</a></li>
						<li class="nav-item" role="presentation"><a class="nav-link" href="#">About</a></li>
						<li class="nav-item" role="presentation"><a class="nav-link" href="#">Features</a></li>
						<li class="nav-item" role="presentation"><a class="nav-link" href="#">Contact us</a></li>
						<li class="nav-item dropdown" role="presentation">
							<a class="nav-link dropdown-toggle" id="dropdownmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
								<i class="fa fa-user fa-1x"></i> Profile 
							</a>
							<div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="dropdownmenu">
								<a class="dropdown-item" href="#">My account</a>
								<a class="dropdown-item" href="#">Log out</a>
							</div>
						</li>						
					</ul>
				</div>
			</div>
		</nav>
		<main class="page landing-page">
		
		</main>
		<footer class="page-footer dark">
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<h5>Get started</h5>
						<ul>
							<li><a href="#">Home</a></li>
							<li><a href="#">Sign up</a></li>
							<li><a href="#">Downloads</a></li>
						</ul>
					</div>
					<div class="col-sm-3">
						<h5>About us</h5>
						<ul>
							<li><a href="#">Company Information</a></li>
							<li><a href="#">Contact us</a></li>
							<li><a href="#">Reviews</a></li>
						</ul>
					</div>
					<div class="col-sm-3">
						<h5>Support</h5>
						<ul>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">Help desk</a></li>
							<li><a href="#">Forums</a></li>
						</ul>
					</div>
					<div class="col-sm-3">
						<h5>Legal</h5>
						<ul>
							<li><a href="#">Terms of Service</a></li>
							<li><a href="#">Terms of Use</a></li>
							<li><a href="#">Privacy Policy</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="footer-copyright">
				<p>© 2018 Copyright Text</p>
			</div>
		</footer>

	</body>

</html>