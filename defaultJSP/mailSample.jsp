<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>MediCare</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
	</head>
	
	<body style="margin: 0; padding: 0;">
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
			<table align="center" border="0" cellpadding="0" cellspacing="0" width="35%" style="border-collapse: collapse; background-color: #f8f9fa; border-radius:15px;">
				<tr>
					<td style="padding: 3% 0 3% 0;">
						<div align="center">
							<h1 style="text-shadow: 2px 2px 2px rgb(25, 255, 180);">
								<img align="center" width="75" height="60" src="${pageContext.request.contextPath}\\assets\logo\logoMail.png"/> <font face="Montserrat" color="#007bff" style="font-size: 34px;">MediCare</font>
							</h1>
							
						</div>
					</td>					
				</tr>
				<tr>					
					<td style="padding: 15% 5% 15% 5%;">						
						<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
						
							<tr>
								<td align="center" style="padding: 0% 0 5% 0;">
									<hr color="#007bff" width="75%">
									<font face="Montserrat">Title</font>
								</td>
							</tr>
							<tr>
								<td align="center" style="padding: 10% 0 10% 0;">
									<font face="Montserrat">Text</font>
								</td>
							</tr>
							<tr>
								<td align="center" style="padding: 5% 0 0% 0;">
									<font face="Montserrat">Elementi vari</font> 
									<hr color="#007bff" width="75%">
								</td>
							</tr>

						</table>
					</td>
				</tr>
				<tr>
					<td align="center" style="padding: 15% 5% 5% 5%;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
							<tr>
								<td align="center">
									<font face="Montserrat">Say hello to the footer</font>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</table>
	</body>

</html>