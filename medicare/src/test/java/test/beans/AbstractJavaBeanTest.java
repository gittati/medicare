package test.beans;

import static org.junit.Assert.assertEquals;
import java.io.Serializable;
import java.time.LocalDateTime;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.apache.commons.lang3.SerializationUtils;
import org.junit.Test;
import org.meanbean.lang.Factory;
import org.meanbean.test.BeanTester;
import persistence.beans.LoginBean;

/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public abstract class AbstractJavaBeanTest {
    protected String[] propertiesToBeIgnored;
 
    @Test
    public void beanIsSerializable() throws Exception {
        final LoginBean myBean = getBeanInstance();
        final byte[] serializedMyBean = SerializationUtils.serialize((Serializable) myBean);
        @SuppressWarnings("unchecked")
        final LoginBean deserializedMyBean = (LoginBean) SerializationUtils.deserialize(serializedMyBean);
        assertEquals(myBean, deserializedMyBean);
    }
    
    /*
    @Test
    public void equalsAndHashCodeContract() {
        EqualsVerifier.forClass(getBeanInstance().getClass()).suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS).verify();
    }*/
 
    @Test
    public void getterAndSetterCorrectness() throws Exception {
        final BeanTester beanTester = new BeanTester();
        beanTester.getFactoryCollection().addFactory(LocalDateTime.class, new LocalDateTimeFactory());
        beanTester.testBean(getBeanInstance().getClass());
    }
 
    protected abstract LoginBean getBeanInstance();
 
        /**
        * Concrete Factory that creates a LocalDateTime.
        */
        class LocalDateTimeFactory implements Factory {
 
        @Override
        public LocalDateTime create() {
            return LocalDateTime.now();
        }
 
    }
}