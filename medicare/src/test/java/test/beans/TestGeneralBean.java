package test.beans;

import static org.junit.Assert.*;
import org.junit.Test;
import persistence.beans.LoginBean;

/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class TestGeneralBean extends AbstractJavaBeanTest {
    
    @Override
    protected LoginBean getBeanInstance() {
        return new LoginBean();
    }
}