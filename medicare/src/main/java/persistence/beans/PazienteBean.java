package persistence.beans;

import java.io.Serializable;

/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class PazienteBean  implements Serializable {
    private static final long serialVersionUID = 1L;
    private String cf;
    private String nome;
    private String cognome;
    private String dataNascita;
    private String luogoNascita;
    private String sesso;
    private String email;
    private String medicoBase;
    private String residenza;
    
    public PazienteBean() {
        super();
    }

    public String getCf() {
        return cf;
    }

    public void setCf(String cf) {
        this.cf = cf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(String dataNascita) {
        this.dataNascita = dataNascita;
    }

    public String getLuogoNascita() {
        return luogoNascita;
    }

    public void setLuogoNascita(String luogoNascita) {
        this.luogoNascita = luogoNascita;
    }

    public String getSesso() {
        return sesso;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMedicoBase() {
        return medicoBase;
    }

    public void setMedicoBase(String medicoBase) {
        this.medicoBase = medicoBase;
    }
    
    
    /**
     * @return the residenza
     */
    public String getResidenza() {
        return residenza;
    }

    /**
     * @param residenza the residenza to set
     */
    public void setResidenza(String residenza) {
        this.residenza = residenza;
    }
    
    @Override
    public int hashCode() {
        //Implement if necessary
        return 1;
    }
 
    @Override
    public boolean equals(final Object obj) {
        //Implement if necessary
        return true;
    }
    
    @Override
    public String toString() {
        return "PazienteBean [param1=" + cf + ", param2=" + nome + "param3=" + cognome + ", param4=" + dataNascita + "param5=" + luogoNascita + "param6=" + sesso + " param7=" + email + "param8=" + medicoBase + "]";
    }
}
