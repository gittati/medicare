package persistence.beans;

import java.io.Serializable;
import java.io.Serializable;
import lombok.Data;


/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
@Data
public class AnamnesiBean  implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String descrizione;
    private String dataRilascio;
    private String visitaSpecialistica;
    private String ticket;
    
    /*public AnamnesiBean() {
        super();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getDataRilascio() {
        return dataRilascio;
    }

    public void setDataRilascio(String dataRilascio) {
        this.dataRilascio = dataRilascio;
    }

    public String getVisitaSpecialistica() {
        return visitaSpecialistica;
    }

    public void setVisitaSpecialistica(String visitaSpecialistica) {
        this.visitaSpecialistica = visitaSpecialistica;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }
    
    @Override
    public int hashCode() {
        //Implement if necessary
        return 1;
    }
 
    @Override
    public boolean equals(final Object obj) {
        //Implement if necessary
        return true;
    }
    
    @Override
    public String toString() {
        return "AnamnesiBean [param1=" + id + ", param2=" + descrizione + "param3=" + dataRilascio + ", param4=" + visitaSpecialistica + "param5=" + ticket + "]";
    }*/
}
