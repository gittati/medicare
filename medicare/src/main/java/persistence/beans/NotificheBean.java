package persistence.beans;


import java.io.Serializable;


/**
 *
 * @author utente
 */
public class NotificheBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String paziente;
    private String message;
    private String id;
    
    
    public NotificheBean() {
        
    }

    /**
     * @return the paziente
     */
    public String getPaziente() {
        return paziente;
    }

    /**
     * @param paziente the paziente to set
     */
    public void setPaziente(String paziente) {
        this.paziente = paziente;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
    @Override
    public int hashCode() {
        //Implement if necessary
        return 1;
    }
 
    @Override
    public boolean equals(final Object obj) {
        //Implement if necessary
        return true;
    }
    
    @Override
    public String toString() {
        return "NotificheBean [param1=" + paziente + ", param2=" + message + "]";
    }
}
