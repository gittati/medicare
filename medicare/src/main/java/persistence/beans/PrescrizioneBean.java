package persistence.beans;

import java.io.Serializable;

/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class PrescrizioneBean  implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String visitaMedica;
    
    public PrescrizioneBean() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVisitaMedica() {
        return visitaMedica;
    }

    public void setVisitaMedica(String visitaMedica) {
        this.visitaMedica = visitaMedica;
    }
    
    @Override
    public int hashCode() {
        //Implement if necessary
        return 1;
    }
 
    @Override
    public boolean equals(final Object obj) {
        //Implement if necessary
        return true;
    }
    
    @Override
    public String toString() {
        return "PrescrizioneBean [param1=" + id + ", param2=" + visitaMedica + "]";
    }
}
