package persistence.beans;

import java.io.Serializable;

/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class VisitaMedicaBean  implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String paziente;
    private String medicoBase;
    private String dataIscrizione;
    private String terminata;
    
    public VisitaMedicaBean() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPaziente() {
        return paziente;
    }

    public void setPaziente(String paziente) {
        this.paziente = paziente;
    }

    public String getMedicoBase() {
        return medicoBase;
    }

    public void setMedicoBase(String medicoBase) {
        this.medicoBase = medicoBase;
    }

    public String getDataIscrizione() {
        return dataIscrizione;
    }

    public void setDataIscrizione(String dataIscrizione) {
        this.dataIscrizione = dataIscrizione;
    }

    public String getTerminata() {
        return terminata;
    }

    public void setTerminata(String terminata) {
        this.terminata = terminata;
    }
    
    @Override
    public int hashCode() {
        //Implement if necessary
        return 1;
    }
 
    @Override
    public boolean equals(final Object obj) {
        //Implement if necessary
        return true;
    }
    
    @Override
    public String toString() {
        return "VisitaMedicaBean [param1=" + id + ", param2=" + paziente + "param3=" + medicoBase + ", param4=" + dataIscrizione + "param5=" + terminata + "]";
    }
}
