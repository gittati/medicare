package persistence.beans;

import java.io.Serializable;

/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class MedicinalePrescrittoBean  implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String prescrizione;
    private String medicinale;
    private String dataRilascio;
    
    public MedicinalePrescrittoBean() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrescrizione() {
        return prescrizione;
    }

    public void setPrescrizione(String prescrizione) {
        this.prescrizione = prescrizione;
    }

    public String getMedicinale() {
        return medicinale;
    }

    public void setMedicinale(String medicinale) {
        this.medicinale = medicinale;
    }
    
    /**
     * @return the dataRilascio
     */
    public String getDataRilascio() {
        return dataRilascio;
    }

    /**
     * @param dataRilascio the dataRilascio to set
     */
    public void setDataRilascio(String dataRilascio) {
        this.dataRilascio = dataRilascio;
    }
    
    @Override
    public int hashCode() {
        //Implement if necessary
        return 1;
    }
 
    @Override
    public boolean equals(final Object obj) {
        //Implement if necessary
        return true;
    }
    
    @Override
    public String toString() {
        return "MedicinaleBean [param1=" + id + ", param2=" + prescrizione + ", param3=" + medicinale + ", param4=" + this.getDataRilascio()+ "]";
    }
}
