package persistence.beans;

import java.io.Serializable;

/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class FotoBean  implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String cf;
    private String dataInserimento;
    private String fotografia;
    
    public FotoBean() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCf() {
        return cf;
    }

    public void setCf(String cf) {
        this.cf = cf;
    }

    public String getDataInserimento() {
        return dataInserimento;
    }

    public void setDataInserimento(String dataInserimento) {
        this.dataInserimento = dataInserimento;
    }

    public String getFotografia() {
        return fotografia;
    }

    public void setFotografia(String fotografia) {
        this.fotografia = fotografia;
    }
    
    @Override
    public int hashCode() {
        //Implement if necessary
        return 1;
    }
 
    @Override
    public boolean equals(final Object obj) {
        //Implement if necessary
        return true;
    }
    
    @Override
    public String toString() {
        return "FotoBean [param1=" + id + ", param2=" + cf + "param3=" + dataInserimento + ", param4=" + fotografia + "]";
    }
}
