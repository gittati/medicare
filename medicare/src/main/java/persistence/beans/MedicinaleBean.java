package persistence.beans;

import java.io.Serializable;

/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class MedicinaleBean  implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String tipoMedicinale;
    
    public MedicinaleBean() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTipoMedicinale() {
        return tipoMedicinale;
    }

    public void setTipoMedicinale(String tipoMedicinale) {
        this.tipoMedicinale = tipoMedicinale;
    }
    
    @Override
    public int hashCode() {
        //Implement if necessary
        return 1;
    }
 
    @Override
    public boolean equals(final Object obj) {
        //Implement if necessary
        return true;
    }
    
    @Override
    public String toString() {
        return "MedicinaleBean [param1=" + id + ", param2=" + tipoMedicinale + "]";
    }
}
