package persistence.beans;

import java.io.Serializable;

/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class EsamePrescrittoBean  implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String esame;
    private String prescrizione;
    private String dataPrenotazione;
    
    public EsamePrescrittoBean() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEsame() {
        return esame;
    }

    public void setEsame(String esame) {
        this.esame = esame;
    }

    public String getPrescrizione() {
        return prescrizione;
    }

    public void setPrescrizione(String prescrizione) {
        this.prescrizione = prescrizione;
    }

    public String getDataPrenotazione() {
        return dataPrenotazione;
    }

    public void setDataPrenotazione(String dataPrenotazione) {
        this.dataPrenotazione = dataPrenotazione;
    }
    
    @Override
    public int hashCode() {
        //Implement if necessary
        return 1;
    }
 
    @Override
    public boolean equals(final Object obj) {
        //Implement if necessary
        return true;
    }
    
    @Override
    public String toString() {
        return "EsamePrescrittoBean [param1=" + id + ", param2=" + esame + "param3=" + prescrizione + ", param4=" + dataPrenotazione + "]";
    }
}
