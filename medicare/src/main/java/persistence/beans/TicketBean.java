package persistence.beans;

import java.io.Serializable;

/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class TicketBean  implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String costo;
    private String pagato;
    
    public TicketBean() {
        super();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCosto() {
        return costo;
    }

    public void setCosto(String costo) {
        this.costo = costo;
    }
    
    public String getPagato() {
        return pagato;
    }
    
    public void setPagato(String pagato) {
        this.pagato = pagato;
    }
    
    @Override
    public int hashCode() {
        //Implement if necessary
        return 1;
    }
 
    @Override
    public boolean equals(final Object obj) {
        //Implement if necessary
        return true;
    }
    
    @Override
    public String toString() {
        return "TicketBean [param1=" + id + ", param2=" + costo + "]";
    }
}
