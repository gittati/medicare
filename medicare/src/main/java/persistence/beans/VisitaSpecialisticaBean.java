package persistence.beans;

import java.io.Serializable;

/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class VisitaSpecialisticaBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String medicoSpecialista;
    private String esamePrescritto;
    private String terminata;
    
    public VisitaSpecialisticaBean() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMedicoSpecialista() {
        return medicoSpecialista;
    }

    public void setMedicoSpecialista(String medicoSpecialista) {
        this.medicoSpecialista = medicoSpecialista;
    }

    public String getEsamePrescritto() {
        return esamePrescritto;
    }

    public void setEsamePrescritto(String esamePrescritto) {
        this.esamePrescritto = esamePrescritto;
    }
    
    
    /**
     * @return the terminata
     */
    public String getTerminata() {
        return terminata;
    }

    /**
     * @param terminata the terminata to set
     */
    public void setTerminata(String terminata) {
        this.terminata = terminata;
    }
    
    @Override
    public int hashCode() {
        //Implement if necessary
        return 1;
    }
 
    @Override
    public boolean equals(final Object obj) {
        //Implement if necessary
        return true;
    }
    
    @Override
    public String toString() {
        return "VisitaSpecialisticaBean [param1=" + id + ", param2=" + medicoSpecialista + "param3=" + esamePrescritto + "]";
    }
}
