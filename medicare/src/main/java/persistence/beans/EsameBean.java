package persistence.beans;


import java.io.Serializable;


/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class EsameBean  implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String tipoEsame;
    
    public EsameBean() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTipoEsame() {
        return tipoEsame;
    }

    public void setTipoEsame(String tipoEsame) {
        this.tipoEsame = tipoEsame;
    }
    
    @Override
    public int hashCode() {
        //Implement if necessary
        return 1;
    }
 
    @Override
    public boolean equals(final Object obj) {
        //Implement if necessary
        return true;
    }
    
    @Override
    public String toString() {
        return "EsameBean [param1=" + id + ", param2=" + tipoEsame + "]";
    }
}
