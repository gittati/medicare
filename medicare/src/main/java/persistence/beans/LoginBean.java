package persistence.beans;


import java.io.Serializable;
import servlet.UserType;


/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class LoginBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String persona;
    private String username;
    private String password;
    private UserType type;
    
    public LoginBean() {
        super();
    }

 
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPersona() {
        return persona;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * @return the type
     */
    public UserType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(UserType type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        //Implement if necessary
        return 1;
    }
 
    @Override
    public boolean equals(final Object obj) {
        //Implement if necessary
        return true;
    }
    
    @Override
    public String toString() {
        return "LoginBean [param1=" + id + ", param2=" + persona + "param3=" + username + ", param4=" + password + "]";
    }
}
