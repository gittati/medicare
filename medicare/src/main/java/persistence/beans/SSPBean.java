/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.beans;

import java.io.Serializable;

/**
 *
 * @author utente
 */
public class SSPBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String provincia;
    private String password;
    private String salt;
    
    public SSPBean() {
        
    }

    /**
     * @return the provincia
     */
    public String getProvincia() {
        return provincia;
    }

    /**
     * @param provincia the provincia to set
     */
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }
    
    
    @Override
    public int hashCode() {
        //Implement if necessary
        return 1;
    }
 
    @Override
    public boolean equals(final Object obj) {
        //Implement if necessary
        return true;
    }
    
    @Override
    public String toString() {
        return "EsameBean [param1=" + provincia + "]";
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the salt
     */
    public String getSalt() {
        return salt;
    }

    /**
     * @param salt the salt to set
     */
    public void setSalt(String salt) {
        this.salt = salt;
    }
}
