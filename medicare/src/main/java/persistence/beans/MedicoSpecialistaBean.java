package persistence.beans;

import java.io.Serializable;

/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class MedicoSpecialistaBean  implements Serializable {
    private static final long serialVersionUID = 1L;
    private String cf;
    
    public MedicoSpecialistaBean() {
        super();
    }

    public String getCf() {
        return cf;
    }

    public void setCf(String cf) {
        this.cf = cf;
    }
    
    @Override
    public int hashCode() {
        //Implement if necessary
        return 1;
    }
 
    @Override
    public boolean equals(final Object obj) {
        //Implement if necessary
        return true;
    }
    
    @Override
    public String toString() {
        return "MedicoSpecialistaBean [param1=" + cf + "]";
    }
}
