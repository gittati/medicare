package persistence.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import persistence.beans.RegioneBean;


/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class RegioneDAO {
    private final Connection con;
    
    public RegioneDAO(Connection con) {
        this.con = con;
    }
    
    //this will trasform into a method, and we will create others one to do differents query on this entity
    public boolean validate(RegioneBean regioneBean) throws ClassNotFoundException {
        boolean status;

        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from regione where nome = ?");
            preparedStatement.setString(1, regioneBean.getNome());
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return status;
    }
    
    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
