package persistence.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import persistence.beans.EsameBean;


/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class EsameDAO {
    private final Connection con;
    
    
    public EsameDAO(Connection con) {
        this.con = con;
    }
    
    
    //this will trasform into a method, and we will create others one to do differents query on this entity
    public boolean validate(EsameBean esameBean) throws ClassNotFoundException {
        boolean status;

        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from esame where id = ?;");
            preparedStatement.setString(1, esameBean.getId());
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return status;
    }
    
    
    public EsameBean getEsameById(String id) {
        EsameBean exam;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from esame where id = ?;");
            preparedStatement.setString(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            exam = new EsameBean();
            exam.setId(id);
            exam.setTipoEsame(rs.getString("tipoEsame"));
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return exam;
    }
    
    
    public ArrayList<EsameBean> getEsameBySpecialista(String specialista) {
        ArrayList<EsameBean> list = new ArrayList<>();
        EsameBean eb;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select esame.tipoEsame from esame INNER JOIN esamePrescritto ON esamePrescritto.esame = esame.id  INNER JOIN visitaSpecialistica ON visitaSpecialistica.esamePrescritto = esamePrescritto.id where visitaSpecialistica.medicoSpecialista = ?;");
            preparedStatement.setString(1, specialista);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                eb = new EsameBean();
                eb.setTipoEsame(rs.getString("tipoEsame")); //gli altri non mi interessano
                list.add(eb);
            }
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        
        return list;
    }
    
    
    public ArrayList<EsameBean> list() {
        ArrayList<EsameBean> list = new ArrayList<>();
        EsameBean esame;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from esame;");
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                esame = new EsameBean();
                esame.setId(rs.getString("id"));
                esame.setTipoEsame(rs.getString("tipoEsame"));
                list.add(esame);
            }
        } catch(SQLException e) {
            return null;
        }
        
        return list;
    }
    
    
    public boolean getEsameInfo(EsameBean esame) {
        boolean status;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from esame where id = ?;");
            preparedStatement.setString(1, esame.getId());
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
            if(!status) return false;
            //complete the bean
            esame.setTipoEsame(rs.getString("tipoEsame"));
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return status;
    }
    
    
    public String getEsameNome(int id) {
        String result;
        boolean status;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select tipoEsame from esame where id = ?;");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
            if(!status) return null;
            result = rs.getString("tipoEsame");
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return result;
    }
    
    public String getTipoEsameByTicket(int ticketId) {
        String tipoEsame = "";
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select tipoEsame from Esame INNER JOIN EsamePrescritto ON EsamePrescritto.esame = Esame.id INNER JOIN VisitaSpecialistica ON EsamePrescritto.id = VisitaSpecialistica.esamePrescritto INNER JOIN Anamnesi ON VisitaSpecialistica.id = Anamnesi.visitaSpecialistica INNER JOIN Ticket ON Anamnesi.ticket = Ticket.id where Ticket.id = ?;");
            preparedStatement.setInt(1, ticketId);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            tipoEsame = rs.getString("tipoEsame");
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }    
        return tipoEsame;
    }
    
    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
