package persistence.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import persistence.beans.NotificheBean;


/**
 *
 * @author utente
 */
public class NotificheDAO {
    private final Connection con;
    
    public NotificheDAO(Connection con) {
        this.con = con;
    }
    
    
    public boolean insert(NotificheBean bean) {
        boolean status;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("INSERT INTO Notifiche(paziente, messaggio) VALUES (?, ?);");
            preparedStatement.setString(1, bean.getPaziente());
            preparedStatement.setString(2, bean.getMessage());
            int row = preparedStatement.executeUpdate();
            status = (row == 1);
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return status;
    }
    
    
    public ArrayList<NotificheBean> getNotificheForUser(String cf) {
        ArrayList<NotificheBean> notifiche = new ArrayList<>();
        NotificheBean not;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from Notifiche where paziente = ?");
            preparedStatement.setString(1, cf);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                not = new NotificheBean();
                not.setMessage(rs.getString("messaggio"));
                not.setPaziente(rs.getString("paziente"));
                not.setId(rs.getString("id"));
                
                //cancello la notifica
                preparedStatement = con.prepareStatement("delete from Notifiche where id = ?");
                preparedStatement.setInt(1, Integer.parseInt(not.getId()));
                preparedStatement.executeUpdate();
                notifiche.add(not);
            }
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return notifiche;
    }
    
    
    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
