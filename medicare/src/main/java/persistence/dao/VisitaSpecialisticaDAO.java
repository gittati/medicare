package persistence.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import persistence.beans.VisitaSpecialisticaBean;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class VisitaSpecialisticaDAO {
    private final Connection con;
    
    
    public VisitaSpecialisticaDAO(Connection con) {
        this.con = con;
    }
    
    
    //this will trasform into a method, and we will create others one to do differents query on this entity
    public boolean validate(VisitaSpecialisticaBean visitaSpecialisticaBean) throws ClassNotFoundException {
        boolean status;

        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from visitaSpecialistica where id = ?");
            preparedStatement.setString(1, visitaSpecialisticaBean.getId());
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return status;
    }
    
    
    public boolean insert(VisitaSpecialisticaBean bean) {
        boolean status;
        PreparedStatement preparedStatement;
        
        try {
            preparedStatement = con.prepareStatement("insert into visitaSpecialistica(medicoSpecialista, esamePrescritto, terminata) values(?, ?, ?);");
            preparedStatement.setString(1, bean.getMedicoSpecialista());
            preparedStatement.setInt(2, Integer.parseInt(bean.getEsamePrescritto()));
            preparedStatement.setInt(3, 0);
            int row = preparedStatement.executeUpdate();
            status = (row == 1);
            
            if(status) {
                preparedStatement = con.prepareStatement("select id from visitaSpecialistica where medicoSpecialista = ? and esamePrescritto = ?;");
                preparedStatement.setString(1, bean.getMedicoSpecialista());
                preparedStatement.setInt(2, Integer.parseInt(bean.getEsamePrescritto()));
                ResultSet rs = preparedStatement.executeQuery();
                status = rs.next();
                bean.setId(rs.getString("id"));
            }
        } catch (SQLException ex) {
            printSQLException(ex);
            return false;
        }
        return status;
    }
    
    
    public ArrayList<VisitaSpecialisticaBean> getVisitaSpecialisticaBySpecialista(String cfSpecialista) {
        ArrayList<VisitaSpecialisticaBean> list = new ArrayList<>();
        VisitaSpecialisticaBean vmb;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from visitaSpecialistica where medicoSpecialista = ?;");
            preparedStatement.setString(1, cfSpecialista);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                vmb = new VisitaSpecialisticaBean();
                vmb.setId(rs.getString("id"));
                vmb.setMedicoSpecialista(rs.getString("medicoSpecialista"));
                vmb.setEsamePrescritto(rs.getString("esamePrescritto"));
                vmb.setTerminata(rs.getString("terminata"));
                list.add(vmb);
            }
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        return list;
    }
    
    
    //NT means non terminata
    public ArrayList<VisitaSpecialisticaBean> getVisitaSpecialisticaBySpecialistaNT(String cfSpecialista) {
        ArrayList<VisitaSpecialisticaBean> list = new ArrayList<>();
        VisitaSpecialisticaBean vmb;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from visitaspecialistica where medicoSpecialista = ? and terminata = 0;");
            preparedStatement.setString(1, cfSpecialista);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                vmb = new VisitaSpecialisticaBean();
                vmb.setId(rs.getString("id"));
                vmb.setMedicoSpecialista(rs.getString("medicoSpecialista"));
                vmb.setEsamePrescritto(rs.getString("esamePrescritto"));
                vmb.setTerminata(rs.getString("terminata"));
                list.add(vmb);
            }
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        return list;
    }
    
    
    //NT means non terminata
    public ArrayList<VisitaSpecialisticaBean> getTodayVisitaSpecialisticaBySpecialistaNT(String cfSpecialista) {
        ArrayList<VisitaSpecialisticaBean> list = new ArrayList<>();
        VisitaSpecialisticaBean vmb;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from visitaspecialistica inner join esamePrescritto on visitaSpecialistica.esamePrescritto = esamePrescritto.id where medicoSpecialista = ? and terminata = 0 and dataPrenotazione = ?;");
            preparedStatement.setString(1, cfSpecialista);
            try {
                preparedStatement.setString(2, formatTodayDate().toString());
            } catch (ParseException ex) {
                Logger.getLogger(VisitaSpecialisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                vmb = new VisitaSpecialisticaBean();
                vmb.setId(rs.getString("id"));
                vmb.setMedicoSpecialista(rs.getString("medicoSpecialista"));
                vmb.setEsamePrescritto(rs.getString("esamePrescritto"));
                vmb.setTerminata(rs.getString("terminata"));
                list.add(vmb);
            }
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        return list;
    }
    
    
    public ArrayList<VisitaSpecialisticaBean> getTodayVisitaSpecialisticaBySpecialista(String cfSpecialista) {
        ArrayList<VisitaSpecialisticaBean> list = new ArrayList<>();
        VisitaSpecialisticaBean vmb;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from visitaspecialistica inner join esamePrescritto on visitaSpecialistica.esamePrescritto = esamePrescritto.id where medicoSpecialista = ? and dataPrenotazione = ?;");
            preparedStatement.setString(1, cfSpecialista);
            try {
                preparedStatement.setString(2, formatTodayDate().toString());
            } catch (ParseException ex) {
                Logger.getLogger(VisitaSpecialisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                vmb = new VisitaSpecialisticaBean();
                vmb.setId(rs.getString("id"));
                vmb.setMedicoSpecialista(rs.getString("medicoSpecialista"));
                vmb.setEsamePrescritto(rs.getString("esamePrescritto"));
                vmb.setTerminata(rs.getString("terminata"));
                list.add(vmb);
            }
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        return list;
    }
    
    public String getMedicoSpecialistaByTicket(int ticketId) {
        String medicoSpecialista = "";
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select nome, cognome FROM Paziente INNER JOIN MedicoSpecialista ON Paziente.cf = MedicoSpecialista.cf INNER JOIN VisitaSpecialistica ON MedicoSpecialista.cf = VisitaSpecialistica.medicoSpecialista INNER JOIN Anamnesi ON VisitaSpecialistica.id = Anamnesi.visitaSpecialistica INNER JOIN Ticket ON Anamnesi.ticket = Ticket.id where Ticket.id = ?;");
            preparedStatement.setInt(1, ticketId);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            medicoSpecialista = rs.getString("nome")+ " " +rs.getString("cognome");
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }        
        return medicoSpecialista;
    }
    
    public int getNumberTodayVisitaSpecialisticaBySpecialista(String cfSpecialista) {
        ArrayList<VisitaSpecialisticaBean> list = new ArrayList<>();
        VisitaSpecialisticaBean vmb;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from visitaspecialistica inner join esamePrescritto on visitaSpecialistica.esamePrescritto = esamePrescritto.id where medicoSpecialista = ? and dataPrenotazione = ?;");
            preparedStatement.setString(1, cfSpecialista);
            try {
                preparedStatement.setString(2, formatTodayDate().toString());
            } catch (ParseException ex) {
                Logger.getLogger(VisitaSpecialisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                vmb = new VisitaSpecialisticaBean();
                vmb.setId(rs.getString("id"));
                vmb.setMedicoSpecialista(rs.getString("medicoSpecialista"));
                vmb.setEsamePrescritto(rs.getString("esamePrescritto"));
                vmb.setTerminata(rs.getString("terminata"));
                list.add(vmb);
            }
        } catch (SQLException e) {
            System.err.println(e);
            return 0;
        }
        return list.size();
    }
    
    
    public VisitaSpecialisticaBean getVisitaSByEsameP(int idEsameP) {
        VisitaSpecialisticaBean visita = new VisitaSpecialisticaBean();
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from visitaspecialistica where esamePrescritto = ?;");
            preparedStatement.setInt(1, idEsameP);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            
            visita.setId(rs.getString("id"));
            visita.setMedicoSpecialista(rs.getString("medicoSpecialista"));
            visita.setEsamePrescritto(rs.getString("esamePrescritto"));
            visita.setTerminata(rs.getString("terminata"));
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        return visita;
    }
    
    
    public boolean terminaVisita(VisitaSpecialisticaBean bean, int terminata) {
        boolean status;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("update visitaspecialistica set terminata = ? where id = ?;");
            preparedStatement.setInt(2, Integer.parseInt(bean.getId()));
            preparedStatement.setInt(1, terminata);
            status = (preparedStatement.executeUpdate() == 1);
            bean.setTerminata(String.valueOf(terminata));
        } catch (SQLException e) {
            printSQLException(e);
            return false;
        }
        return status;
    }
    
    
    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
    
    
    public static Date formatTodayDate() throws ParseException {
        java.util.Date utilDate = new java.util.Date();
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        return sqlDate;
    }
}
