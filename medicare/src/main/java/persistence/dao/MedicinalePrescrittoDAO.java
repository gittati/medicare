package persistence.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import persistence.beans.MedicinalePrescrittoBean;


/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class MedicinalePrescrittoDAO {
    private final Connection con;
    
    
    public MedicinalePrescrittoDAO(Connection con) {
        this.con = con;
    }
    
    
    //this will trasform into a method, and we will create others one to do differents query on this entity
    public boolean validate(MedicinalePrescrittoBean medicinalePrescrittoBean) throws ClassNotFoundException {
        boolean status;

        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from meidicinalePrescritto where id = ?;");
            preparedStatement.setString(1, medicinalePrescrittoBean.getId());
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return status;
    }
    
    
    public boolean insert(MedicinalePrescrittoBean mpb) {
        boolean status;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("insert into medicinalePrescritto(prescrizione, medicinale, dataRilascio) values(?, ?, ?);");
            preparedStatement.setInt(1, Integer.parseInt(mpb.getPrescrizione()));
            preparedStatement.setInt(2, Integer.parseInt(mpb.getMedicinale()));
            try {
                preparedStatement.setString(3, formatTodayDate().toString());
            } catch (ParseException ex) {
                Logger.getLogger(MedicinalePrescrittoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            int row = preparedStatement.executeUpdate();
            status = (row == 1);
            
            if(status) {
                preparedStatement = con.prepareStatement("select id from medicinalePrescritto where prescrizione = ? and medicinale = ?;");
                preparedStatement.setInt(1, Integer.parseInt(mpb.getPrescrizione()));
                preparedStatement.setInt(2, Integer.parseInt(mpb.getMedicinale()));
                ResultSet rs = preparedStatement.executeQuery();
                status = rs.next();
                mpb.setId(rs.getString("id"));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
        return status;
    }
    
    
    public MedicinalePrescrittoBean getLastMedicinaleForVisita(String idVisita) {
        MedicinalePrescrittoBean medp;
        
        try {
            //collego a prescrizione
            PreparedStatement preparedStatement = con.prepareStatement("select * from prescrizione where visitaMedica = ?;");
            preparedStatement.setInt(1, Integer.parseInt(idVisita));
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            String idPresc = rs.getString("id");
            //collego a medicinale prescritto
            preparedStatement = con.prepareStatement("select * from medicinaleprescritto where prescrizione = ? order by dataRilascio DESC;");
            preparedStatement.setInt(1, Integer.parseInt(idPresc));
            ResultSet rs2 = preparedStatement.executeQuery();
            rs2.next();
            medp = new MedicinalePrescrittoBean();
            medp.setId(rs2.getString("id"));
            medp.setMedicinale(rs2.getString("medicinale"));
            medp.setPrescrizione(rs2.getString("prescrizione"));
            medp.setDataRilascio(rs2.getString("dataRilascio"));
        } catch(SQLException ex) {
            System.out.println(ex);
            return null;
        }
        return medp;
    }
    
    
    public ArrayList<MedicinalePrescrittoBean> getMedicinaliForPrescrizione(int idPresc) {
        ArrayList<MedicinalePrescrittoBean> medicinali = new ArrayList<>();
        MedicinalePrescrittoBean med;
        
        try {
            //collego a prescrizione
            PreparedStatement preparedStatement = con.prepareStatement("select * from medicinaleprescritto where prescrizione = ?;");
            preparedStatement.setInt(1, idPresc);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                med = new MedicinalePrescrittoBean();
                med.setId(rs.getString("id"));
                med.setMedicinale(rs.getString("medicinale"));
                med.setPrescrizione(rs.getString("prescrizione"));
                med.setDataRilascio(rs.getString("dataRilascio"));
                medicinali.add(med);
            } 
        } catch(SQLException ex) {
            return null;
        }
        return medicinali;
    }
    
    
    public ArrayList<MedicinalePrescrittoBean> getMedicinaliForUser(String cf) {
        ArrayList<MedicinalePrescrittoBean> listMed = new ArrayList<>();
        MedicinalePrescrittoBean med;
        
        try {
            //collego a prescrizione
            PreparedStatement preparedStatement = con.prepareStatement("select * from medicinaleprescritto as MP inner join prescrizione on MP.prescrizione = prescrizione.id inner join visitamedica ON prescrizione.visitaMedica = visitamedica.id where visitamedica.paziente = ?;");
            preparedStatement.setString(1, cf);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                med = new MedicinalePrescrittoBean();
                med.setId(rs.getString("id"));
                med.setMedicinale(rs.getString("medicinale"));
                med.setPrescrizione(rs.getString("prescrizione"));
                med.setDataRilascio(rs.getString("dataRilascio"));
                listMed.add(med);
            } 
        } catch(SQLException ex) {
            System.out.println(ex);
            return null;
        }
        return listMed;
    }
    
    
    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
    
    public static Date formatTodayDate() throws ParseException {
        java.util.Date utilDate = new java.util.Date();
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        return sqlDate;
    }
}
