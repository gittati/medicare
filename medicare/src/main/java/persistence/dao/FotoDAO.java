package persistence.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import persistence.beans.FotoBean;


/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class FotoDAO {
    private final Connection con;
    
    public FotoDAO(Connection con) {
        this.con = con;
    }
    
    //this will trasform into a method, and we will create others one to do differents query on this entity
    public boolean validate(FotoBean fotoBean) throws ClassNotFoundException {
        boolean status;

        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from foto where id = ?");
            preparedStatement.setString(1, fotoBean.getId());
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return status;
    }
    
    
    public boolean insert(String cf, String nomeFile) {
        boolean status;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("insert into foto(cf, dataInserimento, fotografia) values (?, ?, ?)");
            preparedStatement.setString(1, cf);
            try {
                preparedStatement.setString(2, formatTodayDate().toString());
            } catch(ParseException e) {
                return false;
            }
            preparedStatement.setString(3, nomeFile);
            int ok = preparedStatement.executeUpdate();
            status = (ok == 1);
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return status;
    }
    
    
    public ArrayList<FotoBean> getFotoUtente(String cf){
        ArrayList<FotoBean> listaFoto = new ArrayList<>();
        FotoBean fotoBean;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from foto where cf = ?");
            preparedStatement.setString(1, cf);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                fotoBean = new FotoBean();
                fotoBean.setCf(cf);
                fotoBean.setId(rs.getString("id"));
                fotoBean.setDataInserimento(rs.getString("dataInserimento"));
                fotoBean.setFotografia(rs.getString("fotografia"));
                listaFoto.add(fotoBean);
            }
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }      
        return listaFoto;
    }
    
    
    //foto scheda paziente
    public FotoBean getLastFotoUtente(String cf) {
        FotoBean fotoBean = new FotoBean();
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from foto where cf = ? order by dataInserimento desc;");
            preparedStatement.setString(1, cf);
            ResultSet rs = preparedStatement.executeQuery();
            
            rs.next();
            fotoBean.setCf(cf);
            fotoBean.setId(rs.getString("id"));
            fotoBean.setDataInserimento(rs.getString("dataInserimento"));
            fotoBean.setFotografia(rs.getString("fotografia"));
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return fotoBean;
    }
    
    
    public int getMaxFotoId() {
        int r;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select max(id) as massimone from foto;");
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            r = Integer.parseInt(rs.getString("massimone"));
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        }
        return r;
    }
    
    
    private Date formatTodayDate() throws ParseException {
        java.util.Date utilDate = new java.util.Date();
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        return sqlDate;
    }
    
    
    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}