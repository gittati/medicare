package persistence.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import persistence.beans.MedicinaleBean;


/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class MedicinaleDAO {
    private final Connection con;
    
    
    public MedicinaleDAO(Connection con) {
        this.con = con;
    }
    
    
    //this will trasform into a method, and we will create others one to do differents query on this entity
    public boolean validate(MedicinaleBean medicinaleBean) throws ClassNotFoundException {
        boolean status;

        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from medicinale where id = ?;");
            preparedStatement.setString(1, medicinaleBean.getId());
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return status;
    }
    
    
    public boolean getMedicinale(MedicinaleBean bean) {
        boolean status;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from medicinale where id = ?;");
            preparedStatement.setString(1, bean.getId());
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
            bean.setTipoMedicinale(rs.getString("tipoMedicinale"));
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return status;
    }
    
    public String getTipoMedicinaleByMedicinalePrescritto(String medicinalePrescrittoId) {
        String tipoMedicinale = "";
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("SELECT tipoMedicinale FROM Medicinale INNER JOIN MedicinalePrescritto ON MedicinalePrescritto.medicinale = Medicinale.id WHERE MedicinalePrescritto.id = ?;");
            preparedStatement.setInt(1, Integer.parseInt(medicinalePrescrittoId));
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            tipoMedicinale = rs.getString("tipoMedicinale");
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        
        return tipoMedicinale;
    }
    
    public ArrayList<MedicinaleBean> listMedicinali() {
        ArrayList<MedicinaleBean> list = new ArrayList<>();
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from medicinale;");
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                MedicinaleBean mb = new MedicinaleBean();
                mb.setId(rs.getString("id"));
                mb.setTipoMedicinale(rs.getString("tipoMedicinale"));
                list.add(mb);
            }
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return list;
    }
    
    
    public String getNomeMedicinale(String idMed) {
        String result;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from medicinale where id = ?;");
            preparedStatement.setInt(1, Integer.parseInt(idMed));
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            result = rs.getString("tipoMedicinale");
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return result;
    }
    
    
    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
