/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import persistence.beans.EsamePrescrittoBean;
import persistence.beans.PazienteBean;
import persistence.beans.SSPBean;
import persistence.beans.VisitaMedicaBean;

/**
 *
 * @author utente
 */
public class SSPDAO {
    private final Connection con;
    
    public SSPDAO(Connection con) {
        this.con = con;
    }
    
    
    public ArrayList<PazienteBean> getPazientiIscritti(String abbr) {
        ArrayList<PazienteBean> list = new ArrayList<>();
        PazienteBean paz;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from paziente as P inner join citta on P.residenza = citta.nome inner join provincia as PR on citta.provincia = PR.abbr where PR.abbr = ?");
            preparedStatement.setString(1, abbr);
            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next()) {
                paz = new PazienteBean();
                paz.setCf(rs.getString("cf"));
                paz.setNome(rs.getString("nome"));
                paz.setCognome(rs.getString("cognome"));
                paz.setDataNascita(rs.getString("dataNascita"));
                paz.setLuogoNascita(rs.getString("luogoNascita"));
                paz.setSesso(rs.getString("sesso"));
                paz.setEmail(rs.getString("email"));
                paz.setMedicoBase(rs.getString("medicoBase"));
                paz.setResidenza(rs.getString("residenza"));
                list.add(paz);
            }
            
        } catch (SQLException e) {
            printSQLException(e);
            return null;
        }
        return list;
    }
    
    
    public ArrayList<PazienteBean> getMediciIscritti(String abbr) {
        ArrayList<PazienteBean> list = new ArrayList<>();
        PazienteBean paz;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from paziente as P inner join citta on P.residenza = citta.nome inner join provincia as PR on citta.provincia = PR.abbr inner join medicobase as M on m.cf = P.cf where PR.abbr = ?");
            preparedStatement.setString(1, abbr);
            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next()) {
                paz = new PazienteBean();
                paz.setCf(rs.getString("cf"));
                paz.setNome(rs.getString("nome"));
                paz.setCognome(rs.getString("cognome"));
                paz.setDataNascita(rs.getString("dataNascita"));
                paz.setLuogoNascita(rs.getString("luogoNascita"));
                paz.setSesso(rs.getString("sesso"));
                paz.setEmail(rs.getString("email"));
                paz.setMedicoBase(rs.getString("medicoBase"));
                paz.setResidenza(rs.getString("residenza"));
                list.add(paz);
            }
            
        } catch (SQLException e) {
            printSQLException(e);
            return null;
        }
        return list;
    }
    
    
    public ArrayList<PazienteBean> getSpecialistiIscritti(String abbr) {
        ArrayList<PazienteBean> list = new ArrayList<>();
        PazienteBean paz;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from paziente as P inner join citta on P.residenza = citta.nome inner join provincia as PR on citta.provincia = PR.abbr inner join medicospecialista as M on m.cf = P.cf where PR.abbr = ?");
            preparedStatement.setString(1, abbr);
            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next()) {
                paz = new PazienteBean();
                paz.setCf(rs.getString("cf"));
                paz.setNome(rs.getString("nome"));
                paz.setCognome(rs.getString("cognome"));
                paz.setDataNascita(rs.getString("dataNascita"));
                paz.setLuogoNascita(rs.getString("luogoNascita"));
                paz.setSesso(rs.getString("sesso"));
                paz.setEmail(rs.getString("email"));
                paz.setMedicoBase(rs.getString("medicoBase"));
                paz.setResidenza(rs.getString("residenza"));
                list.add(paz);
            }
            
        } catch (SQLException e) {
            printSQLException(e);
            return null;
        }
        return list;
    }
    
    
    public ArrayList<VisitaMedicaBean> getVisiteInDataWithMedicinale(String abbr, String dataScelta) {
        ArrayList<VisitaMedicaBean> visite = new ArrayList<>();
        VisitaMedicaBean bean;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select distinct V.id, V.paziente, V.MedicoBase, V.dataIscrizione, V.terminata from visitaMedica as V inner join prescrizione as PRE on PRE.visitaMedica = V.id inner join MedicinalePrescritto as MP on MP.prescrizione = PRE.id inner join Paziente as P on V.medicoBase = P.cf inner join citta on P.residenza = citta.nome inner join provincia as PR on citta.provincia = PR.abbr inner join medicobase as M on m.cf = P.cf where PR.abbr = ? and V.dataIscrizione = ?;");
            preparedStatement.setString(1, abbr);
            preparedStatement.setString(2, dataScelta);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                bean = new VisitaMedicaBean();
                bean.setId(rs.getString("id"));
                bean.setPaziente(rs.getString("paziente"));
                bean.setMedicoBase(rs.getString("medicoBase"));
                bean.setDataIscrizione(rs.getString("dataIscrizione"));
                bean.setTerminata(rs.getString("terminata"));
                visite.add(bean);
            }
        } catch (SQLException e) {
            printSQLException(e);
            return null;
        }
        return visite;
    }
    
    
    public ArrayList<EsamePrescrittoBean> getEsamiInData(String abbr, String dataScelta) {
        ArrayList<EsamePrescrittoBean> esami = new ArrayList<>();
        EsamePrescrittoBean bean;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from esamePrescritto as E inner join visitaSpecialistica as VS on VS.esamePrescritto = E.id inner join Paziente as P on VS.medicoSpecialista = P.cf inner join citta on P.residenza = citta.nome inner join provincia as PR on citta.provincia = PR.abbr where PR.abbr = ? and E.dataPrenotazione = ?;");
            preparedStatement.setString(1, abbr);
            preparedStatement.setString(2, dataScelta);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                bean = new EsamePrescrittoBean();
                bean.setId(rs.getString("id"));
                bean.setEsame(rs.getString("esame"));
                bean.setPrescrizione(rs.getString("prescrizione"));
                bean.setDataPrenotazione(rs.getString("dataPrenotazione"));
                esami.add(bean);
            }
        } catch (SQLException e) {
            printSQLException(e);
            return null;
        }
        return esami;
    }
    
    
    public boolean validate(SSPBean ssp) {
        boolean status;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from ssp where provincia = ? and password = ?");
            preparedStatement.setString(1, ssp.getProvincia());
            preparedStatement.setString(2, ssp.getPassword());
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return status;
    }
    
    
    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
