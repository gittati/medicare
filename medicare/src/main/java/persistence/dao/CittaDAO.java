/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import persistence.beans.CittaBean;

/**
 *
 * @author utente
 */
public class CittaDAO {
    private final Connection con;
    
    public CittaDAO(Connection con) {
        this.con = con;
    }
    
    
    public ArrayList<CittaBean> getCityForProvincia(String provincia) {
        ArrayList<CittaBean> city = new ArrayList<>();
        CittaBean citta;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from citta where provincia = ?");
            preparedStatement.setString(1, provincia);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                citta = new CittaBean();
                citta.setNome(rs.getString("nome"));
                citta.setProvincia(rs.getString("provincia"));
                city.add(citta);
            }
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        
        return city;
    }
    
    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
