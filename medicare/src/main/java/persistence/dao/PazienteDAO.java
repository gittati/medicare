package persistence.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import persistence.beans.PazienteBean;


/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class PazienteDAO {
    private final Connection con;
    
    public PazienteDAO(Connection con) {
        this.con = con;
    }
    
    //this will trasform into a method, and we will create others one to do differents query on this entity
    public boolean getPazienteByCf(PazienteBean pazienteBean) {
        boolean status;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from paziente where cf = ?;");
            preparedStatement.setString(1, pazienteBean.getCf());
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
            pazienteBean.setCf(rs.getString("cf"));
            pazienteBean.setNome(rs.getString("nome"));
            pazienteBean.setCognome(rs.getString("cognome"));
            pazienteBean.setDataNascita(rs.getString("dataNascita"));
            pazienteBean.setLuogoNascita(rs.getString("luogoNascita"));
            pazienteBean.setSesso(rs.getString("sesso"));
            pazienteBean.setEmail(rs.getString("email"));
            pazienteBean.setMedicoBase(rs.getString("medicoBase"));
            pazienteBean.setResidenza(rs.getString("residenza"));
        } catch (SQLException e) {
            printSQLException(e);
            return false;
        }
        return status;
    }
    
    
    public String getNomePaziente(String paziente) {
        String r;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from paziente where cf = ?;");
            preparedStatement.setString(1, paziente);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            r = rs.getString("nome") + " " + rs.getString("cognome");
        } catch (SQLException e) {
            printSQLException(e);
            return null;
        }
        return r;
    }
    
    
    public String getNomeUtenteByPresc(String prescrizione) {
        String r;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from paziente as p inner join visitamedica as v on v.paziente = p.cf inner join Prescrizione as PR on PR.visitaMedica = V.id where PR.id = ?;");
            preparedStatement.setInt(1, Integer.parseInt(prescrizione));
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            r = rs.getString("nome") + " " + rs.getString("cognome");
        } catch (SQLException e) {
            printSQLException(e);
            return null;
        }
        return r;
    }
    
    public String getNomePazienteBySpecialista(String specialista) {
        String cf;
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select visitaMedica.paziente from visitaMedica inner join prescrizione on prescrizione.visitamedica = visitaMedica.id INNER JOIN esamePrescritto ON esamePrescritto.prescrizione = prescrizione.id INNER JOIN visitaSpecialistica ON visitaSpecialistica.esamePrescritto = esamePrescritto.id where visitaSpecialistica.medicoSpecialista = ?;");
            preparedStatement.setString(1, specialista);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            cf = rs.getString("paziente");
        } catch (SQLException e) {
            printSQLException(e);
            return null;
        }
        String r;
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from paziente where cf = ?;");
            preparedStatement.setString(1, cf);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            r = rs.getString("nome") + " " + rs.getString("cognome");
        } catch (SQLException e) {
            printSQLException(e);
            return null;
        }
        return r;
    }
    
    public String getNomePazienteByTicket(int ticketId) {
        String nomePaziente = "";
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select nome, cognome FROM Paziente INNER JOIN VisitaMedica ON Paziente.cf = VisitaMedica.paziente INNER JOIN Prescrizione ON VisitaMedica.id = Prescrizione.visitaMedica INNER JOIN EsamePrescritto ON EsamePrescritto.prescrizione = Prescrizione.id INNER JOIN VisitaSpecialistica ON EsamePrescritto.id = VisitaSpecialistica.esamePrescritto INNER JOIN Anamnesi ON VisitaSpecialistica.id = Anamnesi.visitaSpecialistica INNER JOIN Ticket ON Anamnesi.ticket = Ticket.id where Ticket.id = ?;");
            preparedStatement.setInt(1, ticketId);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            nomePaziente = rs.getString("nome")+" "+rs.getString("cognome");
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }  
        return nomePaziente;
    }
    
    public String getNomeByMedicinalePrescritto(String medicinalePrescrittoId) {
        String nomePaziente="";
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("SELECT nome, cognome FROM Paziente INNER JOIN VisitaMedica ON VisitaMedica.paziente = Paziente.cf INNER JOIN Prescrizione ON Prescrizione.visitaMedica = VisitaMedica.id INNER JOIN MedicinalePrescritto ON MedicinalePrescritto.prescrizione = Prescrizione.id WHERE MedicinalePrescritto.id = ?;");
            preparedStatement.setInt(1, Integer.parseInt(medicinalePrescrittoId));
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            nomePaziente = rs.getString("nome")+" "+rs.getString("cognome");
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        
        return nomePaziente;
    }
    
    public boolean setMedBase(PazienteBean pazienteBean, String cf) {
        boolean status;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("update paziente set medicoBase = ? where cf = ?;");
            preparedStatement.setString(1, cf);
            preparedStatement.setString(2, pazienteBean.getCf());
            status = (preparedStatement.executeUpdate() == 1);
        } catch (SQLException e) {
            printSQLException(e);
            return false;
        }
        return status;
    }
    
    
    public String getMailFromCf(String cf) {
        String r;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from paziente where cf = ?;");
            preparedStatement.setString(1, cf);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            r = rs.getString("email");
        } catch (SQLException e) {
            printSQLException(e);
            return null;
        }
        return r;
    }
    
    
    public String getCfFromUser(String username) {
        String r;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from login where username = ?;");
            preparedStatement.setString(1, username);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            r = rs.getString("persona");
        } catch (SQLException e) {
            printSQLException(e);
            return null;
        }
        return r;
    }
    
    
    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
