package persistence.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import persistence.beans.EsamePrescrittoBean;
import java.util.ArrayList;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class EsamePrescrittoDAO {
    private final Connection con;
    
    
    public EsamePrescrittoDAO(Connection con) {
        this.con = con;
    }
    
    
    //this will trasform into a method, and we will create others one to do differents query on this entity
    public boolean validate(EsamePrescrittoBean esamePrescrittoBean) {
        boolean status;

        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from esamePrescritto where id = ?");
            preparedStatement.setString(1, esamePrescrittoBean.getId());
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return status;
    }
    
    
    public EsamePrescrittoBean getEsamePrescrittoById(String id) {
        EsamePrescrittoBean epb;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from esamePrescritto where id = ?");
            preparedStatement.setString(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            epb = new EsamePrescrittoBean();
            epb.setEsame(rs.getString("esame"));
            epb.setDataPrenotazione(rs.getString("dataPrenotazione"));
            epb.setId(id);
            epb.setPrescrizione(rs.getString("prescrizione"));
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return epb;
    }
    
    
    public ArrayList<EsamePrescrittoBean> getEsamePrescrittoByPaziente(String cfPaziente) {
        ArrayList<EsamePrescrittoBean> list = new ArrayList<>();
        EsamePrescrittoBean epb;
        
        try {
            //SELECT EsamePrescritto.id, EsamePrescritto.esame, EsamePrescritto.prescrizione, EsamePrescritto.dataPrescrizione FROM EsamePrescritto INNER JOIN Prescrizione ON EsamePrescritto.prescrizione = Prescrizione.id INNER JOIN VisitaMedica ON Prescrizione.visitaMedica = VisitaMedica.id WHERE VisitaMedica.paziente = "CGLMRC38D08L378S"
            PreparedStatement preparedStatement = con.prepareStatement("SELECT EsamePrescritto.id, EsamePrescritto.esame, EsamePrescritto.prescrizione, EsamePrescritto.dataPrenotazione FROM EsamePrescritto INNER JOIN Prescrizione ON EsamePrescritto.prescrizione = Prescrizione.id INNER JOIN VisitaMedica ON Prescrizione.visitaMedica = VisitaMedica.id WHERE VisitaMedica.paziente = ?;");
            preparedStatement.setString(1, cfPaziente);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                epb = new EsamePrescrittoBean();
                epb.setId(rs.getString("id"));
                epb.setEsame(rs.getString("esame"));
                epb.setPrescrizione(rs.getString("prescrizione"));
                epb.setDataPrenotazione(rs.getString("dataPrenotazione"));
                list.add(epb);
            }
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        
        return list;
    }
    
    
    public ArrayList<EsamePrescrittoBean> getEsamePrescrittoByPrescrizione(int prescId) {
        ArrayList<EsamePrescrittoBean> list = new ArrayList<>();
        EsamePrescrittoBean epb;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("SELECT * FROM EsamePrescritto where prescrizione = ?;");
            preparedStatement.setInt(1, prescId);
            System.err.println(""+preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                epb = new EsamePrescrittoBean();
                epb.setId(rs.getString("id"));
                epb.setEsame(rs.getString("esame"));
                epb.setPrescrizione(rs.getString("prescrizione"));
                epb.setDataPrenotazione(rs.getString("dataPrenotazione"));
                list.add(epb);
            }
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        
        return list;
    }
    
    
    public ArrayList<EsamePrescrittoBean> getEsamePrescrittoBySpecialista(String specialista) {
        ArrayList<EsamePrescrittoBean> list = new ArrayList<>();
        EsamePrescrittoBean epb;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select esamePrescritto.dataPrenotazione from esamePrescritto INNER JOIN visitaSpecialistica ON visitaSpecialistica.esamePrescritto = esamePrescritto.id where visitaSpecialistica.medicoSpecialista = ?;");
            preparedStatement.setString(1, specialista);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                epb = new EsamePrescrittoBean();
                epb.setDataPrenotazione(rs.getString("dataPrenotazione")); //non mi interessano gli altri
                list.add(epb);
            }
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        
        return list;
    }
    
    public boolean insert(EsamePrescrittoBean epb) {
        boolean status;
        PreparedStatement preparedStatement;
        
        try {
            preparedStatement = con.prepareStatement("insert into esamePrescritto(esame, prescrizione, dataPrenotazione) values(?, ?, ?);");
            preparedStatement.setInt(1, Integer.parseInt(epb.getEsame()));
            preparedStatement.setInt(2, Integer.parseInt(epb.getPrescrizione()));
            preparedStatement.setString(3, epb.getDataPrenotazione());
            try {
                Date t;
                t = formatDate(epb.getDataPrenotazione());
                preparedStatement.setDate(3, t);
                int row = preparedStatement.executeUpdate();
                status = (row == 1);

                if(status) {
                    preparedStatement = con.prepareStatement("select id from esamePrescritto where esame = ? and prescrizione = ? and dataPrenotazione = ?;");
                    preparedStatement.setInt(1, Integer.parseInt(epb.getEsame()));
                    preparedStatement.setInt(2, Integer.parseInt(epb.getPrescrizione()));
                    preparedStatement.setDate(3, t);
                    ResultSet rs = preparedStatement.executeQuery();
                    status = rs.next();
                    epb.setId(rs.getString("id"));
                }
            } catch (NumberFormatException | SQLException | ParseException e) { 
               System.err.println("Unparseable date: " + e);
               return false;
            }
        } catch (SQLException ex) {
            printSQLException(ex);
            return false;
        }
        return status;
    }
    
    
    public boolean checkDateDuplicate(String dataEsame, int prescrizione) {
        boolean status;
        
        try { 
            PreparedStatement preparedStatement = con.prepareStatement("select id from esamePrescritto where prescrizione = ? and dataPrenotazione = ?;");
            preparedStatement.setInt(1, prescrizione);
            Date t;
            t = formatDate(dataEsame);
            preparedStatement.setDate(2, t);
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
        } catch (SQLException | ParseException ex) {
            Logger.getLogger(EsamePrescrittoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return status;
    }
    
    
    public String getStatoEsame(int id) {
        String result;
        boolean status;
        
        try { 
            PreparedStatement preparedStatement = con.prepareStatement("select terminata FROM visitaspecialistica AS VS inner join esameprescritto AS EP on EP.id = VS.esameprescritto where EP.id = ?;");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
            
            if(status) {
                result = rs.getString("terminata");
            } else {
                return null;
            }
        } catch (SQLException  ex) {
            Logger.getLogger(EsamePrescrittoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return result;
    }
    
    
    private static Date formatDate(String date) throws ParseException {
        DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        java.util.Date utilDate = format.parse(date);
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        return sqlDate;
    }
    
    
    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
