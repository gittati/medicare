package persistence.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import persistence.beans.PrescrizioneBean;
import java.util.ArrayList;


/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class PrescrizioneDAO {
    private final Connection con;
    
    
    public PrescrizioneDAO(Connection con) {
        this.con = con;
    }
    
    
    //this will trasform into a method, and we will create others one to do differents query on this entity
    public boolean validate(PrescrizioneBean prescrizioneBean) throws ClassNotFoundException {
        boolean status;

        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from prescrizione where id = ?");
            preparedStatement.setString(1, prescrizioneBean.getId());
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return status;
    }
    
    
    public PrescrizioneBean getPrescrizioneById(String id) {
        PrescrizioneBean pb;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from prescrizione where id = ?");
            preparedStatement.setString(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            pb = new PrescrizioneBean();
            pb.setId(id);
            pb.setVisitaMedica(rs.getString("visitaMedica"));
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return pb;
    }
    
    public String getIdByMedicinalePrescritto(String medicinalePrescrittoId) {
        String id;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("SELECT Prescrizione.id FROM Prescrizione INNER JOIN MedicinalePrescritto ON Prescrizione.id = MedicinalePrescritto.prescrizione WHERE MedicinalePrescritto.id = ?;");
            preparedStatement.setInt(1, Integer.parseInt(medicinalePrescrittoId));
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            id = rs.getString("id");
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        
        return id;
    }
    
    ///LEO MODIFICA QUA PER PIACERE
    public ArrayList<PrescrizioneBean> getPrescrizioneByPaziente(String cfPaziente) {
        ArrayList<PrescrizioneBean> list = new ArrayList<>();
        PrescrizioneBean prb;
        
        try {
            //SELECT Prescrizione.id, Prescrizione.visitamedica, Prescrizione.esameprescritto, Prescrizione.medicinale FROM Prescrizione INNER JOIN VisitaMedica ON Prescrizione.visitamedica = VisitaMedica.id WHERE VisitaMedica.paziente = "CGLMRC38D08L378S"
            PreparedStatement preparedStatement = con.prepareStatement("SELECT Prescrizione.id, Prescrizione.visitamedica FROM Prescrizione INNER JOIN VisitaMedica ON Prescrizione.visitamedica = VisitaMedica.id WHERE VisitaMedica.paziente = ?;");
            preparedStatement.setString(1, cfPaziente);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                prb = new PrescrizioneBean();
                prb.setId(rs.getString("id"));
                prb.setVisitaMedica(rs.getString("visitaMedica"));
                list.add(prb);
            }
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        
        return list;
    }
    
    
    public boolean createPrescrizione(PrescrizioneBean presc) throws SQLException {
        boolean status;
        PreparedStatement preparedStatement = con.prepareStatement("select * from prescrizione where visitaMedica = ?");
        preparedStatement.setString(1, presc.getVisitaMedica());
        ResultSet rs = preparedStatement.executeQuery();
        status = rs.next();
        
        if(!status) {
            //non presente
            preparedStatement = con.prepareStatement("insert into prescrizione(visitaMedica) values(?);");
            preparedStatement.setString(1, presc.getVisitaMedica());
            int row = preparedStatement.executeUpdate();
            //aggiorno id
            preparedStatement = con.prepareStatement("select * from prescrizione where visitaMedica = ?");
            preparedStatement.setString(1, presc.getVisitaMedica());
            rs = preparedStatement.executeQuery();
            status = rs.next();
            presc.setId(rs.getString("id"));
            return status;
        }
        presc.setId(rs.getString("id"));
        presc.setVisitaMedica(rs.getString("visitaMedica"));
        return status;
    }
    
    
    public PrescrizioneBean getPrescrizioneByVisita(String idVisita) {
        PrescrizioneBean prb = new PrescrizioneBean(); 
        
        try {
            //SELECT Prescrizione.id, Prescrizione.visitamedica, Prescrizione.esameprescritto, Prescrizione.medicinale FROM Prescrizione INNER JOIN VisitaMedica ON Prescrizione.visitamedica = VisitaMedica.id WHERE VisitaMedica.paziente = "CGLMRC38D08L378S"
            PreparedStatement preparedStatement = con.prepareStatement("SELECT Prescrizione.id, Prescrizione.visitamedica FROM Prescrizione INNER JOIN VisitaMedica ON Prescrizione.visitamedica = VisitaMedica.id WHERE VisitaMedica.id = ?;");
            preparedStatement.setInt(1, Integer.parseInt(idVisita));
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            prb.setId(rs.getString("id"));
            prb.setVisitaMedica(rs.getString("visitaMedica"));
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return prb;
    }
    
    
    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
