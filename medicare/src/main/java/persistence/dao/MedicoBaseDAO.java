package persistence.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import persistence.beans.MedicoBaseBean;
import persistence.beans.PazienteBean;


/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class MedicoBaseDAO {
    private final Connection con;
    
    public MedicoBaseDAO(Connection con) {
        this.con = con;
    }
    
    //this will trasform into a method, and we will create others one to do differents query on this entity
    public boolean validate(MedicoBaseBean medicoBaseBean) throws ClassNotFoundException {
        boolean status;

        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from medicoBase where cf = ?");
            preparedStatement.setString(1, medicoBaseBean.getCf());
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return status;
    }
    
    
    public ArrayList<PazienteBean> list(String myCf, String exCf, String residenza) {
        ArrayList<PazienteBean> list = new ArrayList<>();
        PazienteBean pbean;
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from paziente inner join medicobase ON paziente.cf = medicobase.cf"
                    + " where paziente.cf != ? and paziente.cf != ? and paziente.residenza = ?;");
            preparedStatement.setString(1, myCf);
            preparedStatement.setString(2, exCf);
            preparedStatement.setString(3, residenza);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                pbean = new PazienteBean();
                pbean.setCf(rs.getString("cf"));
                pbean.setNome(rs.getString("nome"));
                pbean.setCognome(rs.getString("cognome"));
                pbean.setDataNascita(rs.getString("dataNascita"));
                pbean.setLuogoNascita(rs.getString("luogoNascita"));
                pbean.setSesso(rs.getString("sesso"));
                pbean.setEmail(rs.getString("email"));
                pbean.setMedicoBase(rs.getString("medicoBase"));
                pbean.setResidenza(rs.getString("residenza"));
                list.add(pbean);
            }
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return list;
    }
    
    
    public ArrayList<PazienteBean> parcoPazienti(String cf) {
        ArrayList<PazienteBean> list = new ArrayList<>();
        PazienteBean pbean;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from paziente where medicoBase = ?;");
            preparedStatement.setString(1, cf);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                pbean = new PazienteBean();
                pbean.setCf(rs.getString("cf"));
                pbean.setNome(rs.getString("nome"));
                pbean.setCognome(rs.getString("cognome"));
                pbean.setDataNascita(rs.getString("dataNascita"));
                pbean.setLuogoNascita(rs.getString("luogoNascita"));
                pbean.setSesso(rs.getString("sesso"));
                pbean.setEmail(rs.getString("email"));
                pbean.setMedicoBase(rs.getString("medicoBase"));
                pbean.setResidenza(rs.getString("residenza"));
                list.add(pbean);
            }
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return list;
    }
    
    
    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
