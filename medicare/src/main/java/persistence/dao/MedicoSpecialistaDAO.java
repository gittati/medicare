package persistence.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import persistence.beans.MedicoSpecialistaBean;
import persistence.beans.PazienteBean;


/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class MedicoSpecialistaDAO {
    private final Connection con;
    
    
    public MedicoSpecialistaDAO(Connection con) {
        this.con = con;
    }
    
    
    //this will trasform into a method, and we will create others one to do differents query on this entity
    public boolean validate(MedicoSpecialistaBean medicoSpecialistaBean) throws ClassNotFoundException {
        boolean status;

        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from medicoSpecialista where cf = ?");
            preparedStatement.setString(1, medicoSpecialistaBean.getCf());
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return status;
    }
    
    
    public ArrayList<PazienteBean> list() {
        ArrayList<PazienteBean> list = new ArrayList<>();
        PazienteBean msb;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from paziente inner join medicospecialista ON paziente.cf = medicospecialista.cf;");
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                msb = new PazienteBean();
                msb.setCf(rs.getString("cf"));
                msb.setNome(rs.getString("nome"));
                msb.setCognome(rs.getString("cognome"));
                msb.setDataNascita(rs.getString("dataNascita"));
                msb.setLuogoNascita(rs.getString("luogoNascita"));
                msb.setSesso(rs.getString("sesso"));
                msb.setEmail(rs.getString("email"));
                msb.setMedicoBase(rs.getString("medicoBase"));
                msb.setResidenza(rs.getString("residenza"));
                list.add(msb);
            }
        } catch(SQLException e) {
            return null;
        }
        return list;
    }
    
    public ArrayList<PazienteBean> parcoPazienti() {
        ArrayList<PazienteBean> list = new ArrayList<>();
        PazienteBean pbean;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from paziente");
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                pbean = new PazienteBean();
                pbean.setCf(rs.getString("cf"));
                pbean.setNome(rs.getString("nome"));
                pbean.setCognome(rs.getString("cognome"));
                pbean.setDataNascita(rs.getString("dataNascita"));
                pbean.setLuogoNascita(rs.getString("luogoNascita"));
                pbean.setSesso(rs.getString("sesso"));
                pbean.setEmail(rs.getString("email"));
                pbean.setMedicoBase(rs.getString("medicoBase"));
                pbean.setResidenza(rs.getString("residenza"));
                list.add(pbean);
            }
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return list;
    }
    
    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}