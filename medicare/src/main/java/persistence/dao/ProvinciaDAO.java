package persistence.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import persistence.beans.ProvinciaBean;


/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class ProvinciaDAO {
    private final Connection con;
    
    public ProvinciaDAO(Connection con) {
        this.con = con;
    }
    
    //this will trasform into a method, and we will create others one to do differents query on this entity
    public boolean validate(ProvinciaBean provinciaBean) throws ClassNotFoundException {
        boolean status;

        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from provincia where abbr = ?");
            preparedStatement.setString(1, provinciaBean.getAbbr());
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return status;
    }
    
    
    public ProvinciaBean getProvinciaByNome(String nome) {
        ProvinciaBean prov = new ProvinciaBean();
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from provincia where nome = ?");
            preparedStatement.setString(1, nome);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            prov.setNome(nome);
            prov.setAbbr(rs.getString("abbr"));
            prov.setRegione(rs.getString("regione"));
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return prov;
    }
    
    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
