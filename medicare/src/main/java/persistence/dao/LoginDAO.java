package persistence.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import persistence.beans.LoginBean;
import servlet.UserType;


/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class LoginDAO {
    private final Connection con;
    
    public LoginDAO(Connection con) {
        this.con = con;
    }
    
    //this will trasform into a method, and we will create others one to do differents query on this entity
    public boolean validate(LoginBean loginBean) throws ClassNotFoundException {
        boolean status;
        String cf, id;

        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from login where username = ? and password = ?");
            preparedStatement.setString(1, loginBean.getUsername());
            preparedStatement.setString(2, loginBean.getPassword());
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
            if(!status) return false;
            
            //complete the bean
            cf = rs.getString("persona");
            loginBean.setPersona(cf);
            id = rs.getString("id");
            loginBean.setId(id);
            
            //test if it is a medician
            preparedStatement = con.prepareStatement("select * from medicobase where cf = ?");
            preparedStatement.setString(1, cf);
            rs = preparedStatement.executeQuery();
            status = rs.next();
            
            if(status) {
                loginBean.setType(UserType.BASE);
                return true;
            }
            
            //test if it is a specialist
            preparedStatement = con.prepareStatement("select * from medicospecialista where cf = ?");
            preparedStatement.setString(1, cf);
            rs = preparedStatement.executeQuery();
            status = rs.next();
            
            if(status) {
                loginBean.setType(UserType.SPECIALISTA);
                return true;
            }
            
            //executed only if user in simply a patient
            loginBean.setType(UserType.PAZIENTE);
            return true;
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
    }
    
    
    public String getSaltForUser(String username) {
        String salt;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select salt from login where username = ?;");
            preparedStatement.setString(1, username);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            salt = rs.getString("salt");
        } catch (SQLException e) {
            printSQLException(e);
            return null;
        }
        return salt;
    }
    
    
    public String getUsernameByCf(String cf) {
        String username;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select username from login where persona = ?");
            preparedStatement.setString(1, cf);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            username = rs.getString("username");
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return username;
    }
    
    
    public boolean setPasswordForUser(String password, String username) {
        boolean status;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("update login set password = ? where username = ?;");
            preparedStatement.setString(2, username);
            preparedStatement.setString(1, password);
            status = (preparedStatement.executeUpdate() == 1);
        } catch (SQLException e) {
            printSQLException(e);
            return false;
        }
        return status;
    }

    
    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
