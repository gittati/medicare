package persistence.dao;


import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import persistence.beans.VisitaMedicaBean;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class VisitaMedicaDAO {
    private final Connection con;
    
    
    public VisitaMedicaDAO(Connection con) {
        this.con = con;
    }
    
    
    //this will trasform into a method, and we will create others one to do differents query on this entity
    public boolean insert(VisitaMedicaBean visitaMedicaBean) throws ClassNotFoundException, ParseException {
        boolean status;

        try {
            PreparedStatement preparedStatement = con.prepareStatement("INSERT INTO visitaMedica(paziente, medicoBase, dataIscrizione, terminata) VALUES (?, ?, ?, ?);");
            preparedStatement.setString(1, visitaMedicaBean.getPaziente());
            preparedStatement.setString(2, visitaMedicaBean.getMedicoBase());
            
            try {
               Date t;
               t = formatDate(visitaMedicaBean.getDataIscrizione());
               preparedStatement.setDate(3, t);
            } catch (ParseException e) { 
               System.err.println("Unparseable date");
               return false;
            }
            preparedStatement.setBoolean(4, Boolean.parseBoolean(visitaMedicaBean.getMedicoBase()));
            int row = preparedStatement.executeUpdate();
            status = (row == 1);
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return status;
    }
    
    
    public ArrayList<VisitaMedicaBean> getVisitaMedicaByPaziente(String cfPaziente) {
        ArrayList<VisitaMedicaBean> list = new ArrayList<>();
        VisitaMedicaBean vmb;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from visitaMedica where paziente = ?;");
            preparedStatement.setString(1, cfPaziente);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                vmb = new VisitaMedicaBean();
                vmb.setId(rs.getString("id"));
                vmb.setPaziente(rs.getString("paziente"));
                vmb.setMedicoBase(rs.getString("medicoBase"));
                vmb.setDataIscrizione(rs.getString("dataIscrizione"));
                vmb.setTerminata(rs.getString("terminata"));
                list.add(vmb);
            }
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        
        return list;
    }
    
    public VisitaMedicaBean getVisitaMedicaByMedicinalePrescritto(String medicinalePrescrittoId) {
        VisitaMedicaBean vmb = new VisitaMedicaBean();
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("SELECT * FROM VisitaMedica INNER JOIN Prescrizione ON Prescrizione.visitaMedica = VisitaMedica.id INNER JOIN MedicinalePrescritto ON MedicinalePrescritto.prescrizione = Prescrizione.id WHERE MedicinalePrescritto.id = ?;");
            preparedStatement.setInt(1, Integer.parseInt(medicinalePrescrittoId));
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                vmb.setId(rs.getString("id"));
                vmb.setPaziente(rs.getString("paziente"));
                vmb.setMedicoBase(rs.getString("medicoBase"));
                vmb.setDataIscrizione(rs.getString("dataIscrizione"));
                vmb.setTerminata(rs.getString("terminata"));
            }
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        
        return vmb;
    }
    
    public ArrayList<VisitaMedicaBean> getVisiteMedicheByMedico(String cfMedico) {
        ArrayList<VisitaMedicaBean> list = new ArrayList<>();
        VisitaMedicaBean vmb;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from visitaMedica where medicoBase = ?;");
            preparedStatement.setString(1, cfMedico);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                vmb = new VisitaMedicaBean();
                vmb.setId(rs.getString("id"));
                vmb.setPaziente(rs.getString("paziente"));
                vmb.setMedicoBase(rs.getString("medicoBase"));
                vmb.setDataIscrizione(rs.getString("dataIscrizione"));
                vmb.setTerminata(rs.getString("terminata"));
                list.add(vmb);
            }
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        return list;
    }
    
    
    public ArrayList<VisitaMedicaBean> getTodayVisiteMedicheByMedico(String cfMedico) {
        ArrayList<VisitaMedicaBean> list = new ArrayList<>();
        VisitaMedicaBean vmb;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from visitaMedica where medicoBase = ? and dataIscrizione = ?;");
            preparedStatement.setString(1, cfMedico);
            try {
                preparedStatement.setString(2, formatTodayDate().toString());
            } catch (ParseException ex) {
                Logger.getLogger(VisitaMedicaDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                vmb = new VisitaMedicaBean();
                vmb.setId(rs.getString("id"));
                vmb.setPaziente(rs.getString("paziente"));
                vmb.setMedicoBase(rs.getString("medicoBase"));
                vmb.setDataIscrizione(rs.getString("dataIscrizione"));
                vmb.setTerminata(rs.getString("terminata"));
                list.add(vmb);
            }
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        return list;
    }
    
    
    public ArrayList<VisitaMedicaBean> getVisiteMedicheTerminabili(String cfMedico) {
        ArrayList<VisitaMedicaBean> list = new ArrayList<>();
        VisitaMedicaBean vmb;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from visitaMedica where medicoBase = ? and terminata = ?;");
            preparedStatement.setString(1, cfMedico);
            preparedStatement.setInt(2, 2);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                vmb = new VisitaMedicaBean();
                vmb.setId(rs.getString("id"));
                vmb.setPaziente(rs.getString("paziente"));
                vmb.setMedicoBase(rs.getString("medicoBase"));
                vmb.setDataIscrizione(rs.getString("dataIscrizione"));
                vmb.setTerminata(rs.getString("terminata"));
                list.add(vmb);
            }
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        return list;
    }
    
    
    public int getNumberTodayVisiteMedicheByMedico(String cfMedico) {
        ArrayList<VisitaMedicaBean> list = new ArrayList<>();
        VisitaMedicaBean vmb;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from visitaMedica where medicoBase = ? and dataIscrizione = ?;");
            preparedStatement.setString(1, cfMedico);
            try {
                preparedStatement.setString(2, formatTodayDate().toString());
            } catch (ParseException ex) {
                Logger.getLogger(VisitaMedicaDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                vmb = new VisitaMedicaBean();
                vmb.setId(rs.getString("id"));
                vmb.setPaziente(rs.getString("paziente"));
                vmb.setMedicoBase(rs.getString("medicoBase"));
                vmb.setDataIscrizione(rs.getString("dataIscrizione"));
                vmb.setTerminata(rs.getString("terminata"));
                list.add(vmb);
            }
        } catch (SQLException e) {
            System.err.println(e);
            return 0;
        }
        return list.size();
    }
    
    
    public boolean getVisitaMedicaById(VisitaMedicaBean visita) {
        boolean valid;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from visitaMedica where id = ?;");
            preparedStatement.setInt(1, Integer.parseInt(visita.getId()));
            ResultSet rs = preparedStatement.executeQuery();
            valid = rs.next();
            
            if(valid) {
                visita.setMedicoBase(rs.getString("medicoBase"));
                visita.setPaziente(rs.getString("paziente"));
                visita.setDataIscrizione(rs.getString("dataIscrizione"));
                visita.setTerminata(rs.getString("terminata"));
            }
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        }
        return valid;
    }
    
    public ArrayList<VisitaMedicaBean> getVisitaMedicaBySpecialista(String specialista) {
        ArrayList<VisitaMedicaBean> list = new ArrayList<>();
        VisitaMedicaBean vmb;

        try {
            PreparedStatement preparedStatement = con.prepareStatement("select visitaMedica.medicoBase from visitaMedica INNER JOIN prescrizione ON prescrizione.visitaMedica = visitaMedica.id INNER JOIN esamePrescritto ON esamePrescritto.prescrizione = prescrizione.id INNER JOIN visitaSpecialistica ON visitaSpecialistica.esamePrescritto = esamePrescritto.id where visitaSpecialistica.medicoSpecialista = ?;");
            preparedStatement.setString(1, specialista);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                vmb = new VisitaMedicaBean();
                vmb.setMedicoBase(rs.getString("medicoBase")); //gli altri non mi interessano
                list.add(vmb);
            }
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        return list;
    }
    
    /*
    STATIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
    */
    public boolean setStatoVisitaMedica(String visitaMedica, int stato) {
        boolean status;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("update visitamedica set terminata = ? where id = ?;");
            preparedStatement.setInt(2, Integer.parseInt(visitaMedica));
            preparedStatement.setInt(1, stato);
            status = (preparedStatement.executeUpdate() == 1);
        } catch (SQLException e) {
            printSQLException(e);
            return false;
        }
        return status;
    }
    
    
    public boolean areVSFinished(String idVisita) {
        boolean status;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("SELECT * FROM visitamedica " +
                "inner join prescrizione on visitamedica.id = prescrizione.visitamedica " +
                "inner join esameprescritto on prescrizione.id = esameprescritto.prescrizione " +
                "inner join visitaspecialistica on visitaspecialistica.esameprescritto = esamePrescritto.id " +
                "where visitamedica.id = ? and visitaspecialistica.terminata = 0;");
            preparedStatement.setInt(1, Integer.parseInt(idVisita));
            ResultSet rs = preparedStatement.executeQuery();
            status = (!rs.next());
        } catch (SQLException e) {
            printSQLException(e);
            return true;
        }
        return status;
    }
    
    
    public String getDataVisita(String id) {
        String r;
        boolean valid;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select dataIscrizione from visitaMedica where id = ?;");
            preparedStatement.setInt(1, Integer.parseInt(id));
            ResultSet rs = preparedStatement.executeQuery();
            valid = rs.next();
            
            if(valid) {
                r = rs.getString("dataIscrizione");
            } else r = "null";
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        return r;
    }
    
    
    public VisitaMedicaBean getLastVisitaForUser(String cf) {
        boolean valid;
        VisitaMedicaBean visita = new VisitaMedicaBean();
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from visitaMedica where paziente = ? order by dataIscrizione DESC;");
            preparedStatement.setString(1, cf);
            ResultSet rs = preparedStatement.executeQuery();
            valid = rs.next();
            
            if(valid) {
                visita.setId(rs.getString("id"));
                visita.setMedicoBase(rs.getString("medicoBase"));
                visita.setPaziente(rs.getString("paziente"));
                visita.setDataIscrizione(rs.getString("dataIscrizione"));
                visita.setTerminata(rs.getString("terminata"));
            } else {
                return null;
            }
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        return visita;
    }
    
    
    public VisitaMedicaBean getLastVisitaForUserWithM(String cf) {
        boolean valid;
        VisitaMedicaBean visita = null;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from visitaMedica where paziente = ? order by dataIscrizione DESC;");
            preparedStatement.setString(1, cf);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                //collego la prescrizione
                preparedStatement = con.prepareStatement("select * from prescrizione where visitaMedica = ?;");
                preparedStatement.setInt(1, Integer.parseInt(rs.getString("id")));
                ResultSet rs2 = preparedStatement.executeQuery();
                valid = rs2.next();
                
                if(valid) {
                    String idPresc = rs2.getString("id");
                    //collego se esistono medicinali prescritti
                    preparedStatement = con.prepareStatement("select * from medicinaleprescritto where prescrizione = ?;");
                    preparedStatement.setInt(1, Integer.parseInt(idPresc));
                    ResultSet rs3 = preparedStatement.executeQuery();
                    valid = rs3.next();

                    if(valid) {
                        visita = new VisitaMedicaBean();
                        visita.setId(rs.getString("id"));
                        visita.setMedicoBase(rs.getString("medicoBase"));
                        visita.setPaziente(rs.getString("paziente"));
                        visita.setDataIscrizione(rs.getString("dataIscrizione"));
                        visita.setTerminata(rs.getString("terminata"));
                        return visita;
                    }
                }
            }
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        return visita;
    }
    
    
    public String getDataForMedicinale(int idMed) {
        String result;
        boolean valid;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select dataIscrizione from visitamedica inner join prescrizione on prescrizione.visitaMedica = visitamedica.id inner join medicinaleprescritto on medicinaleprescritto.prescrizione = prescrizione.id where medicinaleprescritto.id = ?;");
            preparedStatement.setInt(1, idMed);
            ResultSet rs = preparedStatement.executeQuery();
            valid = rs.next();
            
            if(valid) {
                result = rs.getString("dataIscrizione");
            } else {
                return null;
            }
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        return result;
    }
    
    
    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
    
    
    public static Date formatDate(String date) throws ParseException {
        DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        java.util.Date utilDate = format.parse(date);
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        return sqlDate;
    }
    
    
    public static Date formatTodayDate() throws ParseException {
        java.util.Date utilDate = new java.util.Date();
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        return sqlDate;
    }
}
