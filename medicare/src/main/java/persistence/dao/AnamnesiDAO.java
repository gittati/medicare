package persistence.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;
import persistence.beans.AnamnesiBean;


/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class AnamnesiDAO {
    private final Connection con;
    
    
    public AnamnesiDAO(Connection con) {
        this.con = con;
    }
    
    
    //this will trasform into a method, and we will create others one to do differents query on this entity
    public boolean validate(AnamnesiBean anamnesiBean) throws ClassNotFoundException {
        boolean status;

        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from anamnesi where id = ?");
            preparedStatement.setString(1, anamnesiBean.getId());
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return status;
    }
    
    
    public boolean create(AnamnesiBean anamnesi) {
        boolean status;
        PreparedStatement preparedStatement;
        
        try {
            preparedStatement = con.prepareStatement("insert into anamnesi(descrizione, dataRilascio, visitaSpecialistica, ticket) values(?, ?, ?, ?);");
            preparedStatement.setString(1, anamnesi.getDescrizione());
            preparedStatement.setInt(3, Integer.parseInt(anamnesi.getVisitaSpecialistica()));
            preparedStatement.setInt(4, Integer.parseInt(anamnesi.getTicket()));
            
            try {
                Date t;
                DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                t = formatDate(dateFormat.format(new java.util.Date()));
                preparedStatement.setDate(2, t);
                int row = preparedStatement.executeUpdate();
                status = (row == 1);

                if(status) {
                    preparedStatement = con.prepareStatement("select id from anamnesi where descrizione = ? and dataRilascio = ? and visitaSpecialistica = ? and ticket = ?;");
                    preparedStatement.setString(1, anamnesi.getDescrizione());
                    preparedStatement.setDate(2, t);
                    preparedStatement.setInt(3, Integer.parseInt(anamnesi.getVisitaSpecialistica()));
                    preparedStatement.setInt(4, Integer.parseInt(anamnesi.getTicket()));
                    ResultSet rs = preparedStatement.executeQuery();
                    status = rs.next();
                    anamnesi.setId(rs.getString("id"));
                }
            } catch (NumberFormatException | SQLException | ParseException e) { 
               System.err.println("Unparseable date: " + e);
               return false;
            }
        } catch (SQLException ex) {
            printSQLException(ex);
            return false;
        }
        return status;
    }
    
    
    public ArrayList<AnamnesiBean> getAnamnesiForUser(String cf) {
        ArrayList<AnamnesiBean> anamnesiList = new ArrayList<>();
        AnamnesiBean theA;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from anamnesi AS A inner join visitaspecialistica AS VS on A.visitaSpecialistica = VS.id inner join esameprescritto AS EP on VS.esamePrescritto = EP.id inner join prescrizione AS P on EP.prescrizione = P.id inner join visitamedica AS VM on P.visitaMedica = VM.id where VM.paziente = ?;");
            preparedStatement.setString(1, cf);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                theA = new AnamnesiBean();
                theA.setId(rs.getString("id"));
                theA.setDataRilascio(rs.getString("dataRilascio"));
                theA.setVisitaSpecialistica(rs.getString("visitaSpecialistica"));
                theA.setTicket(rs.getString("ticket"));
                theA.setDescrizione(rs.getString("descrizione"));
                anamnesiList.add(theA);
            }
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return anamnesiList;
    }
    
    public AnamnesiBean getAnamnesiByTicket(int ticketId) {
        AnamnesiBean anamnesi = new AnamnesiBean();
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from Anamnesi INNER JOIN Ticket ON Anamnesi.ticket = Ticket.id where Ticket.id = ?;");
            preparedStatement.setInt(1, ticketId);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            anamnesi.setId(rs.getString("id"));
            anamnesi.setDataRilascio(rs.getString("dataRilascio"));
            anamnesi.setVisitaSpecialistica(rs.getString("visitaSpecialistica"));
            anamnesi.setTicket(rs.getString("ticket"));
            anamnesi.setDescrizione(rs.getString("descrizione"));
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return anamnesi;
    }
    
    public AnamnesiBean getAnamnesiByVisitaS(int visitaId) {
        AnamnesiBean anamnesi = new AnamnesiBean();
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from anamnesi where visitaSpecialistica = ?;");
            preparedStatement.setInt(1, visitaId);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            anamnesi.setId(rs.getString("id"));
            anamnesi.setDataRilascio(rs.getString("dataRilascio"));
            anamnesi.setVisitaSpecialistica(rs.getString("visitaSpecialistica"));
            anamnesi.setTicket(rs.getString("ticket"));
            anamnesi.setDescrizione(rs.getString("descrizione"));
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return anamnesi;
    }
    
    
    private static Date formatDate(String date) throws ParseException {
        /*Date initDate = (Date) new SimpleDateFormat(initDateFormat).parse(date);
        SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
        String parsedDate = formatter.format(initDate);
        return parsedDate;*/
        DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));

        java.util.Date utilDate = format.parse(date);

        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        return sqlDate;
    }
    
    
    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
