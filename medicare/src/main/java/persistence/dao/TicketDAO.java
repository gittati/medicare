package persistence.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import persistence.beans.TicketBean;


/**
 *
 * @author (leonardo.berrighi@gmail.com)
 */
public class TicketDAO {
    private final Connection con;
    
    
    public TicketDAO(Connection con) {
        this.con = con;
    }
    
    
    //this will trasform into a method, and we will create others one to do differents query on this entity
    public boolean validate(TicketBean ticketBean) throws ClassNotFoundException {
        boolean status;

        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from ticket where id = ?");
            preparedStatement.setString(1, ticketBean.getId());
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return status;
    }
    
    
    public TicketBean getTicketById(int id) {
        TicketBean t = new TicketBean();
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from ticket where id = ?;");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            t.setId(rs.getString("id"));
            t.setCosto(rs.getString("costo"));
            t.setPagato(rs.getString("pagato"));
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return t;
    }
    
    
    public ArrayList<TicketBean> getTicketsForUser(String cf) {
        ArrayList<TicketBean> ticketList = new ArrayList<>();
        TicketBean ticket;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select ticket.id, ticket.costo, ticket.pagato from ticket inner join anamnesi on anamnesi.ticket = ticket.id inner join visitaspecialistica AS VS on anamnesi.visitaSpecialistica = VS.id inner join esameprescritto AS EP on VS.esamePrescritto = EP.id inner join prescrizione AS P on EP.prescrizione = P.id inner join visitamedica AS VM on P.visitaMedica = VM.id where VM.paziente = ?;");
            preparedStatement.setString(1, cf);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()) {
                ticket = new TicketBean();
                ticket.setId(rs.getString("id"));
                ticket.setCosto(rs.getString("costo"));
                ticket.setPagato(rs.getString("pagato"));
                ticketList.add(ticket);
            }
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return ticketList;
    }
    
    
    public String getDateForTicket(int id) {
        String result;
        boolean status;
        
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select dataRilascio from ticket inner join anamnesi on anamnesi.ticket = ticket.id where ticket.id = ?;");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
            
            if(status) {
                result = rs.getString("dataRilascio");
            } else return null;
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
        return result;
    }
    
    
    public boolean addTicket(TicketBean ticket) {
        boolean status;
        PreparedStatement preparedStatement;
        
        try {
            preparedStatement = con.prepareStatement("select max(id) as idMax from ticket;");
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            int id;
            
            if(rs.getString("idMax") != null) {
                id = Integer.parseInt(rs.getString("idMax")) + 1;
            } else {
                id = 1;
            }
            ticket.setId(String.valueOf(id));
            preparedStatement = con.prepareStatement("insert into ticket(id, costo) values(?, ?);");
            preparedStatement.setInt(2, Integer.parseInt(ticket.getCosto()));
            preparedStatement.setInt(1, id);
            int row = preparedStatement.executeUpdate();
            status = (row == 1);
        } catch (SQLException ex) {
            printSQLException(ex);
            return false;
        }
        return status;
    }
    
    public boolean pagamentoEffettuato(String id) {
        boolean status;
        try {
            PreparedStatement preparedStatement = con.prepareStatement("UPDATE Ticket SET pagato = 1 WHERE id = ?;");
            preparedStatement.setString(1, id);
            int row = preparedStatement.executeUpdate();
            status = (row == 1);
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        
        return status;
    }
    
    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
