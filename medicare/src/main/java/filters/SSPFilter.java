package filters;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import servlet.UserType;
 

/**
 * This Java filter demonstrates how to intercept the request
 * and transform the response to implement authentication feature.
 * for the website's back-end.
 *
 * @author 
 */
public class SSPFilter implements Filter {
    private FilterConfig filterConfig = null;
    
    
    public SSPFilter() {
    }
 
    
    @Override
    public void init(FilterConfig fConfig) throws ServletException {
        this.filterConfig = fConfig;
        if (filterConfig != null) {            
            log("SSPFilter:Initializing filter");
        }
    }
    
     
    @Override
    public void destroy() {
    }
    
    
    /**
     *
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        RequestDispatcher rd;
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession(false);
        boolean isLoggedIn = (session != null && session.getAttribute("SSPInstance") != null);
        String loginURI = "/medicare/Servlet/loginSSP";
        String indexURI = "/medicare/index.jsp";
        boolean isIndexPage = httpRequest.getRequestURI().equals(indexURI);
        boolean isLoginRequest = httpRequest.getRequestURI().equals(loginURI);
        boolean isLoginPage = httpRequest.getRequestURI().endsWith("loginSSP.jsp");
        
        if(!isIndexPage) {
            if (isLoggedIn && (isLoginRequest || isLoginPage)) {
                // the user is already logged in and he's trying to login again
                // then forwards to the homepage
                rd = request.getRequestDispatcher("/sspPage.jsp");
                rd.forward(request, response);
            } else if (isLoggedIn || isLoginRequest) {
                // continues the filter chain
                // allows the request to reach the destination
                chain.doFilter(request, response);
            } else {
                // the user is not logged in, so authentication is required
                // forwards to the Login page
                rd = request.getRequestDispatcher("/loginSSP.jsp");
                rd.forward(request, response);
            }
        }
    }
    
    
    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (IOException ex) {
        }
        return stackTrace;
    }
    
    
    public void log(String msg) {
        filterConfig.getServletContext().log(msg);    
    }
    
    
    /**
     * Return the filter configuration object for this filter.
     * @return the filter configuration
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    
    /**
     * Set the filter configuration object for this filter.
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    
    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("SSPFilter()");
        }
        StringBuilder sb = new StringBuilder("SSPFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }
}