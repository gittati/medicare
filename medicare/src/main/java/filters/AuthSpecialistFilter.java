package filters;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import servlet.UserType;

/**
 *
 * @author utente
 */
public class AuthSpecialistFilter implements Filter { 
    private FilterConfig filterConfig = null;
    
    
    public AuthSpecialistFilter() {
    }
    

    /**
     * Init method for this filter
     * @param filterConfig the filter configuration
     */
    @Override
    public void init(FilterConfig filterConfig) {        
        this.filterConfig = filterConfig;
        if (filterConfig != null) {               
            log("AuthSpecialistFilter:Initializing filter");
        }
    }
    
    
    /**
     * Destroy method for this filter
     */
    @Override
    public void destroy() {        
    }


    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession(false);
        UserType type;
        RequestDispatcher rd;
        boolean isLoggedIn = (session != null && session.getAttribute("pazienteBeanInstance") != null && session.getAttribute("userType") != null);
        
        if (isLoggedIn) {
            // continues the filter chain
            // allows the request to reach the destination if user is medician
            type = (UserType)session.getAttribute("userType");
            
            if(type == UserType.SPECIALISTA) {
                chain.doFilter(request, response);
            } else  {
                switch(type) {
                    case PAZIENTE:
                        rd = request.getRequestDispatcher("/home.jsp");
                        rd.forward(request, response);
                        break;
                    case BASE:
                        rd = request.getRequestDispatcher("/homeMedician.jsp");
                        rd.forward(request, response);
                        break;
                    default:
                        rd = request.getRequestDispatcher("/login.jsp");
                        rd.forward(request, response);
                }
            }
        } else {
            // the user is not logged in, so authentication is required
            // forwards to the Login page
            rd = request.getRequestDispatcher("/login.jsp");
            rd.forward(request, response);
        }
    }

    
    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (IOException ex) {
        }
        return stackTrace;
    }
    
    
    public void log(String msg) {
        filterConfig.getServletContext().log(msg);        
    }
    
    
    /**
     * Return the filter configuration object for this filter.
     * @return the filter configuration
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    
    /**
     * Set the filter configuration object for this filter.
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }


    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("AuthSpecialistFilter()");
        }
        StringBuilder sb = new StringBuilder("AuthSpecialistFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }
}
