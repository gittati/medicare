package servlet;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 *
 * @author utente
 */
public class ConnectionManager {
    private Connection con;
    
    
    public ConnectionManager() {
        try {
            // Initialize all the information regarding 
            // Database Connection 
            String dbDriver = "com.mysql.cj.jdbc.Driver";
            String dbURL = "jdbc:mysql://localhost:3306/";
            // Database name to access 
            String dbName = "ospedale";
            String dbUsername = "root";
            String dbPassword = "admin";
            Class.forName(dbDriver);
                
            try {            	
                con = DriverManager.getConnection(dbURL + dbName, dbUsername,  dbPassword);                 
            } catch (SQLException ex) {
                System.out.println(ex);
            }
        } catch(ClassNotFoundException e) {
            System.out.println(e);
        }
    }
    
    
    public Connection getConnection() {
        return con;
    }
}
