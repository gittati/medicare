package servlet;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import persistence.beans.LoginBean;
import persistence.beans.PazienteBean;
import persistence.beans.VisitaMedicaBean;
import persistence.dao.LoginDAO;
import persistence.dao.MedicoBaseDAO;
import persistence.dao.PazienteDAO;
import java.util.ArrayList;
import persistence.beans.AnamnesiBean;
import persistence.beans.EsameBean;
import persistence.dao.VisitaMedicaDAO;
import persistence.dao.PrescrizioneDAO;
import persistence.beans.PrescrizioneBean;
import persistence.dao.EsamePrescrittoDAO;
import persistence.beans.EsamePrescrittoBean;
import persistence.beans.FotoBean;
import persistence.beans.MedicinaleBean;
import persistence.beans.MedicinalePrescrittoBean;
import persistence.beans.NotificheBean;
import persistence.beans.TicketBean;
import persistence.beans.VisitaSpecialisticaBean;
import persistence.dao.AnamnesiDAO;
import persistence.dao.EsameDAO;
import persistence.dao.FotoDAO;
import persistence.dao.MedicinaleDAO;
import persistence.dao.MedicinalePrescrittoDAO;
import persistence.dao.MedicoSpecialistaDAO;
import persistence.dao.NotificheDAO;
import persistence.dao.TicketDAO;
import persistence.dao.VisitaSpecialisticaDAO;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import static servlet.UserType.BASE;
import static servlet.UserType.SPECIALISTA;
import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import persistence.beans.CittaBean;
import persistence.beans.ProvinciaBean;
import persistence.beans.SSPBean;
import persistence.dao.CittaDAO;
import persistence.dao.ProvinciaDAO;
import persistence.dao.SSPDAO;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


/**
 *
 * @author utente
 */
public class Servlet extends HttpServlet {
    private LoginDAO loginDao;
    private PazienteDAO pazienteDao;
    private VisitaMedicaDAO visitaMedicaDao;
    private MedicoBaseDAO medicoBaseDao;
    private PrescrizioneDAO prescrizioneDao;
    private EsamePrescrittoDAO esamePrescrittoDao;
    private MedicinaleDAO medicinaleDao;
    private ConnectionManager connectionManager;
    private MedicinalePrescrittoDAO medicinalePrescrittoDao;
    private MedicoSpecialistaDAO medicoSpecialistaDao;
    private VisitaSpecialisticaDAO visitaSpecialisticaDao;
    private EsameDAO esameDao;
    private TicketDAO ticketDao;
    private FotoDAO fotoDao;
    private AnamnesiDAO anamnesiDao;
    private NotificheDAO notificheDao;
    private ProvinciaDAO provinciaDao;
    private CittaDAO cittaDao;
    private SSPDAO SSPDao;
    //pdf gestion
    private GregorianCalendar gc;
    private int mese;
    private Date today;
    private BarcodeQRCode my_code;
    private Image qr_image;
    
    
    @Override
    public void init() {
        connectionManager = new ConnectionManager();
        loginDao = new LoginDAO(connectionManager.getConnection());
        pazienteDao = new PazienteDAO(connectionManager.getConnection());
        visitaMedicaDao = new VisitaMedicaDAO(connectionManager.getConnection());
        medicoBaseDao = new MedicoBaseDAO(connectionManager.getConnection());
        prescrizioneDao = new PrescrizioneDAO(connectionManager.getConnection());
        esamePrescrittoDao = new EsamePrescrittoDAO(connectionManager.getConnection());
        medicinaleDao = new MedicinaleDAO(connectionManager.getConnection());
        medicinalePrescrittoDao = new MedicinalePrescrittoDAO(connectionManager.getConnection());
        medicoSpecialistaDao = new MedicoSpecialistaDAO(connectionManager.getConnection());
        esameDao = new EsameDAO(connectionManager.getConnection());
        visitaSpecialisticaDao = new VisitaSpecialisticaDAO(connectionManager.getConnection());
        fotoDao = new FotoDAO(connectionManager.getConnection());
        ticketDao = new TicketDAO(connectionManager.getConnection());
        anamnesiDao = new AnamnesiDAO(connectionManager.getConnection());
        notificheDao = new NotificheDAO(connectionManager.getConnection());
        provinciaDao = new ProvinciaDAO(connectionManager.getConnection());
        cittaDao = new CittaDAO(connectionManager.getConnection());
        SSPDao = new SSPDAO(connectionManager.getConnection());
    }

    
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean valid;
        HttpSession session;
        
        switch(request.getRequestURI()) {
            case "/medicare/Servlet/logout":
                //ELIMINO LA SESSIONE
                session = request.getSession();
                session.setAttribute("pazienteBeanInstance", null);
                session.invalidate();
                //REINDIRIZZO
                response.sendRedirect("/medicare/login.jsp?out=1");
                break;
                
            case "/medicare/Servlet/logoutSSN":
                //ELIMINO LA SESSIONE
                session = request.getSession();
                session.invalidate();
                //REINDIRIZZO
                response.sendRedirect("/medicare/loginSSP.jsp?out=1");
                break;
                
            case "/medicare/Servlet/pazienteData":
                //CARICA LE FOTO
                ArrayList<FotoBean> fotoUtente;
                session = request.getSession();
                PazienteBean pbean = (PazienteBean)session.getAttribute("pazienteBeanInstance");
                fotoUtente = fotoDao.getFotoUtente(pbean.getCf());                
                //CARICA INFO SU MED BASE
                FotoBean fotoMedBase;
                PazienteBean mbean = new PazienteBean();
                mbean.setCf(pbean.getMedicoBase());
                fotoMedBase = fotoDao.getLastFotoUtente(mbean.getCf());
                valid = doMedicoData(mbean);
                //carica la prima foto
                FotoBean theFoto = fotoDao.getLastFotoUtente(pbean.getCf());
                session.setAttribute("lastFoto", theFoto);
                //carico lista dei medici base
                ArrayList<PazienteBean> list;
                String myCf = pbean.getCf();
                String exCf = mbean.getCf();
                String residenza = pbean.getResidenza();
                list = medicoBaseDao.list(myCf, exCf, residenza);
                //REINDIRIZZO
                if(valid) {
                    session.setAttribute("listMedBase", list);
                    session.setAttribute("fotoUtente", fotoUtente);
                    session.setAttribute("fotoMedBase", fotoMedBase);
                    session.setAttribute("medicoDataInstance", mbean);
                    //session.setAttribute("isHidden", false);
                    response.sendRedirect("/medicare/pazienteData.jsp");
                }
                else {
                    response.sendRedirect("/medicare/pazienteData.jsp?error=1");
                }
                break;
                
            case "/medicare/Servlet/parcoPazienti":
                ArrayList<PazienteBean> parco;
                ArrayList<VisitaMedicaBean> visite;
                ArrayList<VisitaMedicaBean> visiteTerminabili;
                session = request.getSession();
                PazienteBean medico = (PazienteBean)session.getAttribute("pazienteBeanInstance");
                parco = medicoBaseDao.parcoPazienti(medico.getCf());
                
                if(parco != null) {
                    session.setAttribute("parcoPazienti", parco);
                    //carico visite mediche
                    visite = visitaMedicaDao.getTodayVisiteMedicheByMedico(medico.getCf());
                    visiteTerminabili = visitaMedicaDao.getVisiteMedicheTerminabili(medico.getCf());
                    session.setAttribute("visiteMedicheTerminabili", visiteTerminabili);
                    ArrayList<String> nomiPazientiT = new ArrayList<>();
                    
                    for(VisitaMedicaBean visita : visiteTerminabili) {
                        nomiPazientiT.add(pazienteDao.getNomePaziente(visita.getPaziente()));
                    }
                    session.setAttribute("nomiPazientiT", nomiPazientiT);
                    
                    if(visite != null) {
                        session.setAttribute("visiteMediche", visite);
                        ArrayList<String> nomiPazienti = new ArrayList<>();
                        
                        for(VisitaMedicaBean visita : visite) {
                            nomiPazienti.add(pazienteDao.getNomePaziente(visita.getPaziente()));
                        }
                        session.setAttribute("nomiPazientiVisite", nomiPazienti);
                        response.sendRedirect("/medicare/homeMedician.jsp");
                    } else {
                        response.sendRedirect("/medicare/homeMedician.jsp?error=1");
                    }
                } else {
                    response.sendRedirect("/medicare/homeMedician.jsp?error=1");
                }
                break;
            
            case "/medicare/Servlet/pazienti":
                ArrayList<PazienteBean> pazienti;
                pazienti = medicoSpecialistaDao.parcoPazienti();
                ArrayList<VisitaSpecialisticaBean> visiteSpecialistiche;
                ArrayList<EsamePrescrittoBean> esamiPrescritti = new ArrayList<>();
                ArrayList<EsameBean> esami = new ArrayList<>();
                ArrayList<PazienteBean> pazientiVS = new ArrayList<>();
                ArrayList<PrescrizioneBean> prescrizioni = new ArrayList<>();
                ArrayList<VisitaMedicaBean> visiteMediche = new ArrayList<>();
                ArrayList<String> pazientiVSNomi = new ArrayList<>();
                ArrayList<String> mediciBaseVSNomi = new ArrayList<>();
                
                session = request.getSession();
                PazienteBean specialista = (PazienteBean)session.getAttribute("pazienteBeanInstance");
                
                if(pazienti != null) {
                    session.setAttribute("pazienti", pazienti);
                    visiteSpecialistiche = visitaSpecialisticaDao.getTodayVisitaSpecialisticaBySpecialistaNT(specialista.getCf());
                    
                    if(visiteSpecialistiche != null) {
                        session.setAttribute("visiteSpecialistiche", visiteSpecialistiche);
                        
                        //per ogni visita SP carico tutte le info che servono
                        for(int i = 0; i < visiteSpecialistiche.size(); i++) {
                            //carico esami prescritti
                            EsamePrescrittoBean epb;
                            epb = esamePrescrittoDao.getEsamePrescrittoById(visiteSpecialistiche.get(i).getEsamePrescritto());
                            esamiPrescritti.add(epb);
                            //carico esami
                            EsameBean examB;
                            examB = esameDao.getEsameById(epb.getEsame());
                            esami.add(examB);
                            //carico prescrizioni
                            PrescrizioneBean prescrizioneB;
                            prescrizioneB = prescrizioneDao.getPrescrizioneById(epb.getPrescrizione());
                            prescrizioni.add(prescrizioneB);
                            //carico visite mediche
                            VisitaMedicaBean vmb = new VisitaMedicaBean();
                            vmb.setId(prescrizioneB.getVisitaMedica());
                            visitaMedicaDao.getVisitaMedicaById(vmb);
                            visiteMediche.add(vmb);
                            //carico i pazienti delle visite specialistiche
                            PazienteBean paz = new PazienteBean();
                            paz.setCf(vmb.getPaziente());
                            pazienteDao.getPazienteByCf(paz);
                            pazientiVS.add(paz);
                            //carico nomi paziente
                            String nomeP = pazienteDao.getNomePaziente(vmb.getPaziente());
                            pazientiVSNomi.add(nomeP);
                            //carico nomi medici base
                            String nomeMB = pazienteDao.getNomePaziente(vmb.getMedicoBase());
                            mediciBaseVSNomi.add(nomeMB);
                        }
                        //aggiungo tutto quello che serve alla sessione
                        session.setAttribute("esamiPrescritti", esamiPrescritti);
                        session.setAttribute("esamiSP", esami);
                        session.setAttribute("pazientiVSNomi", pazientiVSNomi);
                        session.setAttribute("mediciBaseVSNomi", mediciBaseVSNomi);
                        session.setAttribute("pazientiVS", pazientiVS);
                        session.setAttribute("prescrizioniVS", prescrizioni);
                        response.sendRedirect("/medicare/homeSpecialist.jsp");
                    } else {
                        response.sendRedirect("/medicare/homeSpecialist.jsp?error=2");
                    }
                } else {
                    response.sendRedirect("/medicare/homeSpecialist.jsp?error=1");
                }
                break;
                
            /*case "/medicare/Servlet/listMedBase":
                ArrayList<PazienteBean> list;
                session = request.getSession();
                PazienteBean pazienteSession = (PazienteBean)session.getAttribute("pazienteBeanInstance");
                String myCf = pazienteSession.getCf();
                String exCf = pazienteSession.getMedicoBase();
                list = medicoBaseDao.list(myCf, exCf);
                
                if(list != null) {
                    session.setAttribute("listMedBase", list);
                    //session.setAttribute("isHidden", true);
                    response.sendRedirect("/medicare/pazienteData.jsp");
                } else {
                    response.sendRedirect("/medicare/pazienteData.jsp?error=1");
                }
                break;
            */    
            case "/medicare/Servlet/visualizzaPazienteData":
                session = request.getSession();
                PazienteBean pb = (PazienteBean)session.getAttribute("pazienteBeanInstance");
                //carico le visite mediche
                ArrayList<VisitaMedicaBean> visiteM;
                ArrayList<String> nomiMV = new ArrayList<>();
                String nome;
                visiteM = visitaMedicaDao.getVisitaMedicaByPaziente(pb.getCf());
                
                for(int i = 0; i < visiteM.size(); i++) {
                    nome = pazienteDao.getNomePaziente(visiteM.get(i).getMedicoBase());
                    nomiMV.add(i, nome);
                }
                //carico i medicinali prescritti
                ArrayList<MedicinalePrescrittoBean> medicinaliPresc;
                ArrayList<String> nomiMedicinaliP = new ArrayList<>();
                medicinaliPresc = medicinalePrescrittoDao.getMedicinaliForUser(pb.getCf());
                
                for(int i = 0; i < medicinaliPresc.size(); i++) {
                    nome = medicinaleDao.getNomeMedicinale(medicinaliPresc.get(i).getMedicinale());
                    nomiMedicinaliP.add(i, nome);
                }
                //carico gli esami specialistici
                ArrayList<EsamePrescrittoBean> esamiPresc;
                ArrayList<VisitaSpecialisticaBean> visiteSpec = new ArrayList<>();
                ArrayList<String> nomiEsamiPresc = new ArrayList<>();
                ArrayList<String> nomiSpecialisti = new ArrayList<>();
                ArrayList<AnamnesiBean> anamnesiList = new ArrayList<>();
                ArrayList<TicketBean> ticketList = new ArrayList<>();
                esamiPresc = esamePrescrittoDao.getEsamePrescrittoByPaziente(pb.getCf());
                
                for(int i = 0; i < esamiPresc.size(); i++) {
                    visiteSpec.add(i, visitaSpecialisticaDao.getVisitaSByEsameP(Integer.parseInt(esamiPresc.get(i).getId())));
                    nome = esameDao.getEsameNome(Integer.parseInt(esamiPresc.get(i).getEsame()));
                    nomiEsamiPresc.add(i, nome);
                    nome = pazienteDao.getNomePaziente(visiteSpec.get(i).getMedicoSpecialista());
                    nomiSpecialisti.add(i, nome);

                    //se presente, carico l'anamnesi corrispondente
                    if(visiteSpec.get(i).getTerminata().equals("1")) {
                        anamnesiList.add(i, anamnesiDao.getAnamnesiByVisitaS(Integer.parseInt(visiteSpec.get(i).getId())));
                        ticketList.add(i, ticketDao.getTicketById(Integer.parseInt(anamnesiList.get(i).getTicket())));
                    }
                }
                session.setAttribute("visiteM", visiteM);
                session.setAttribute("nomiMV", nomiMV);
                session.setAttribute("medicinaliPresc", medicinaliPresc);
                session.setAttribute("nomiMedicinaliP", nomiMedicinaliP);
                session.setAttribute("esamiPresc", esamiPresc);
                session.setAttribute("visiteSpec", visiteSpec);
                session.setAttribute("nomiEsamiPresc", nomiEsamiPresc);
                session.setAttribute("nomiSpecialisti", nomiSpecialisti);
                session.setAttribute("anamnesiList", anamnesiList);
                session.setAttribute("ticketList", ticketList);
                System.err.println("diiiiiiiiiM: " + anamnesiList.size());
                response.sendRedirect("/medicare/visualizzaPazienteData.jsp");
                break;
                
            case "/medicare/Servlet/visualizzaTicket":
                int idTicket;
                session = request.getSession();
                pb = (PazienteBean)session.getAttribute("pazienteBeanInstance");
                //CREO BEAN
                ArrayList<TicketBean> theLista;
                theLista = ticketDao.getTicketsForUser(pb.getCf());
                ArrayList<String> lista1 = new ArrayList<>();
                
                for(int i = 0; i < theLista.size(); i++) {
                    idTicket = Integer.parseInt(theLista.get(i).getId());
                    lista1.add(i, ticketDao.getDateForTicket(idTicket));
                }
                
                //INSERISCO IL BEAN NELLA SESSIONE E REINDIRIZZO
                if(!theLista.isEmpty() && !lista1.isEmpty()) {
                    session = request.getSession();
                    session.setAttribute("listTickets", theLista);
                    session.setAttribute("listDateTickets", lista1);
                    response.sendRedirect("/medicare/visualizzaTicket.jsp");
                } else {
                    response.sendRedirect("/medicare/visualizzaTicket.jsp"); //con error
                    System.err.println("lista ticket vuota");
                }
                break;
                
            /*case "/medicare/Servlet/annullaSetMedBase":
                session = request.getSession();
                //session.setAttribute("isHidden", false);
                response.sendRedirect("/medicare/pazienteData.jsp");
                break;*/
                
            case "/medicare/Servlet/confermaVisitaMedica":
                session = request.getSession();
                ArrayList<EsamePrescrittoBean> tempEsamiPrescritti = (ArrayList<EsamePrescrittoBean>)session.getAttribute("tempEsamiPrescritti");
                ArrayList<VisitaSpecialisticaBean> tempVisiteSpecialistiche = (ArrayList<VisitaSpecialisticaBean>)session.getAttribute("tempVisiteSpecialistiche");
                ArrayList<MedicinalePrescrittoBean> tempMPB = (ArrayList<MedicinalePrescrittoBean>)session.getAttribute("tempMedicinaliPrescritti");
                
                //carico esami prescritti
                for(EsamePrescrittoBean exam : tempEsamiPrescritti) {
                    esamePrescrittoDao.insert(exam);
                }
                //CAMBIA visitaspecialistica.esamePrescritto con id di esamePrescritto
                for(int i = 0; i < tempEsamiPrescritti.size(); i++) {
                    tempVisiteSpecialistiche.get(i).setEsamePrescritto(tempEsamiPrescritti.get(i).getId());
                }
                //carico visite specialistiche
                for(VisitaSpecialisticaBean vsb: tempVisiteSpecialistiche) {
                    visitaSpecialisticaDao.insert(vsb);
                }
                //carico medicinali prescritti
                for(MedicinalePrescrittoBean mpb : tempMPB) {
                    medicinalePrescrittoDao.insert(mpb);
                }
                PrescrizioneBean prescBean = (PrescrizioneBean) session.getAttribute("prescrizioneInstance");
                
                if(tempEsamiPrescritti.size() > 0) {
                    visitaMedicaDao.setStatoVisitaMedica(prescBean.getVisitaMedica(), 1);
                } else {
                    visitaMedicaDao.setStatoVisitaMedica(prescBean.getVisitaMedica(), 3);
                }
                //preparo tutto per sendMail
                if(tempMPB.size() > 0) session.setAttribute("MisMedicinale", true);
                else session.setAttribute("MisMedicinale", false);
                if(tempVisiteSpecialistiche.size() > 0) session.setAttribute("MisEsame", true);
                else session.setAttribute("MisEsame", false);
                session.setAttribute("MpageTo", "Servlet/parcoPazienti");
                String data = visitaMedicaDao.getDataVisita(prescBean.getVisitaMedica());
                session.setAttribute("MdataVisita", data);
                response.sendRedirect("/medicare/MailServlet/sendMailVisitaEseguita");
                break;
                
            case "/medicare/Servlet/annullaPrenotazioneEsame":
                session = request.getSession();
                session.setAttribute("isDataEsameHidden", false);
                session.setAttribute("isListMSHidden", true);
                response.sendRedirect("/medicare/doVisita.jsp");
                break;
                
            case "/medicare/Servlet/annullaPrenotazioneEsameWA":
                session = request.getSession();
                session.setAttribute("isDataEsameHidden", false);
                session.setAttribute("isListMSHidden", true);
                response.sendRedirect("/medicare/doVisitaWithAnamnesi.jsp");
                break;
                
            case "/medicare/Servlet/annullaVisitaMedica":
                session = request.getSession();
                session.setAttribute("tempMedicinaliPrescritti", null);
                session.setAttribute("tempMedicinali", null);
                session.setAttribute("tempEsami", null);
                session.setAttribute("tempMS", null);
                session.setAttribute("tempEsamiPrescritti", null);
                session.setAttribute("tempVisiteSpecialistiche", null);
                session.setAttribute("isDataEsameHidden", false);
                session.setAttribute("isListMSHidden", true);
                response.sendRedirect("/medicare/homeMedician.jsp");
                break;
                
            case "/medicare/Servlet/doVisitaSpecialistica":
                session = request.getSession();
                int index = Integer.parseInt(request.getParameter("index"));
                ArrayList<EsamePrescrittoBean> esamiPrescrittiA = (ArrayList<EsamePrescrittoBean>)session.getAttribute("esamiPrescritti");
                ArrayList<EsameBean> esamiA = (ArrayList<EsameBean>) session.getAttribute("esamiSP");
                ArrayList<PazienteBean> pazientiVSA = (ArrayList<PazienteBean>) session.getAttribute("pazientiVS");
                ArrayList<String> mediciBaseVSNomiA = (ArrayList<String>) session.getAttribute("mediciBaseVSNomi");
                ArrayList<VisitaSpecialisticaBean> visiteSpecialisticheA = (ArrayList<VisitaSpecialisticaBean>) session.getAttribute("visiteSpecialistiche");
                ArrayList<PrescrizioneBean> prescrizioniVSA = (ArrayList<PrescrizioneBean>) session.getAttribute("prescrizioniVS");
                EsamePrescrittoBean exprAttuale = esamiPrescrittiA.get(index);
                EsameBean exAttuale = esamiA.get(index);
                PazienteBean pazAttuale = pazientiVSA.get(index);
                VisitaSpecialisticaBean VSAttuale = visiteSpecialisticheA.get(index);
                PrescrizioneBean prescAttuale = prescrizioniVSA.get(index);
                session.setAttribute("exprAttuale", exprAttuale);
                session.setAttribute("exAttuale", exAttuale);
                session.setAttribute("pazAttuale", pazAttuale);
                session.setAttribute("VSAttuale", VSAttuale);
                session.setAttribute("prescAttuale", prescAttuale);
                session.setAttribute("medbaseVSAttuale", mediciBaseVSNomiA.get(index));
                //preparo info per email send
                session.setAttribute("MPaziente", pazAttuale.getCf());
                session.setAttribute("MtipoEsame", exAttuale.getTipoEsame());
                session.setAttribute("MdataEsame", exprAttuale.getDataPrenotazione());
                response.sendRedirect("/medicare/doVisitaSpec.jsp");
                break;
                
            case "/medicare/Servlet/caricaVisite":
                session = request.getSession();
                medico = (PazienteBean)session.getAttribute("pazienteBeanInstance");
                ArrayList<VisitaMedicaBean> allVisite;
                allVisite = visitaMedicaDao.getVisiteMedicheByMedico(medico.getCf());
                session.setAttribute("allVisiteMediche", allVisite);
                ArrayList<String> allNomiPazienti = new ArrayList<>();
                        
                for(VisitaMedicaBean visita : allVisite) {
                    allNomiPazienti.add(pazienteDao.getNomePaziente(visita.getPaziente()));
                }
                session.setAttribute("allNomiPazientiVisite", allNomiPazienti);
                //REINDIRIZZO
                response.sendRedirect("/medicare/visualizzaVisita.jsp");
                break;
                
            case "/medicare/Servlet/caricaEsami":
                session = request.getSession();
                specialista = (PazienteBean)session.getAttribute("pazienteBeanInstance");
                visiteSpecialistiche = visitaSpecialisticaDao.getVisitaSpecialisticaBySpecialista(specialista.getCf());
                    
                if(visiteSpecialistiche != null) {
                    session.setAttribute("allVisiteSpecialistiche", visiteSpecialistiche);
                    esamiPrescritti = new ArrayList<>();
                    esami = new ArrayList<>();
                    pazientiVS = new ArrayList<>();
                    prescrizioni = new ArrayList<>();
                    visiteMediche = new ArrayList<>();
                    pazientiVSNomi = new ArrayList<>();
                    mediciBaseVSNomi = new ArrayList<>();
                
                    //per ogni visita SP carico tutte le info che servono
                    for(int i = 0; i < visiteSpecialistiche.size(); i++) {
                        //carico esami prescritti
                        EsamePrescrittoBean epb;
                        epb = esamePrescrittoDao.getEsamePrescrittoById(visiteSpecialistiche.get(i).getEsamePrescritto());
                        esamiPrescritti.add(epb);
                        //carico esami
                        EsameBean examB;
                        examB = esameDao.getEsameById(epb.getEsame());
                        esami.add(examB);
                        //carico prescrizioni
                        PrescrizioneBean prescrizioneB;
                        prescrizioneB = prescrizioneDao.getPrescrizioneById(epb.getPrescrizione());
                        prescrizioni.add(prescrizioneB);
                        //carico visite mediche
                        VisitaMedicaBean vmb = new VisitaMedicaBean();
                        vmb.setId(prescrizioneB.getVisitaMedica());
                        visitaMedicaDao.getVisitaMedicaById(vmb);
                        visiteMediche.add(vmb);
                        //carico i pazienti delle visite specialistiche
                        PazienteBean paz = new PazienteBean();
                        paz.setCf(vmb.getPaziente());
                        pazienteDao.getPazienteByCf(paz);
                        pazientiVS.add(paz);
                        //carico nomi paziente
                        String nomeP = pazienteDao.getNomePaziente(vmb.getPaziente());
                        pazientiVSNomi.add(nomeP);
                        //carico nomi medici base
                        String nomeMB = pazienteDao.getNomePaziente(vmb.getMedicoBase());
                        mediciBaseVSNomi.add(nomeMB);
                    }
                    //aggiungo tutto quello che serve alla sessione
                    session.setAttribute("allEsamiPrescritti", esamiPrescritti);
                    session.setAttribute("allEsamiSP", esami);
                    session.setAttribute("allPazientiVSNomi", pazientiVSNomi);
                    session.setAttribute("allMediciBaseVSNomi", mediciBaseVSNomi);
                    session.setAttribute("allPazientiVS", pazientiVS);
                    session.setAttribute("allPrescrizioniVS", prescrizioni);
                }
                //REINDIRIZZO
                response.sendRedirect("/medicare/visualizzaEsame.jsp");
                break;
                
            case "/medicare/Servlet/switchState":
                session = request.getSession();
                int switchState = (int)session.getAttribute("switchState");
                UserType userType = (UserType) session.getAttribute("userType");
                
                if(switchState == 0) {
                    session.setAttribute("switchState", 1);
                    //REINDIRIZZO
                    response.sendRedirect("/medicare/home.jsp");
                } else {
                    session.setAttribute("switchState", 0);
                    
                    if(userType == BASE) {
                        //REINDIRIZZO
                        response.sendRedirect("/medicare/homeMedician.jsp");
                    } else if(userType == SPECIALISTA) {
                        //REINDIRIZZO
                        response.sendRedirect("/medicare/homeSpecialist.jsp");
                    }
                }
                break;
                
            case "/medicare/Servlet/closeSchedaIntera":
                session = request.getSession();
                session.setAttribute("isAllInfoDisabled", true);
                UserType type = (UserType) session.getAttribute("userType");
                
                if(type == UserType.BASE) {
                    response.sendRedirect("/medicare/pazientiMInfo.jsp");
                } else {
                    response.sendRedirect("/medicare/pazientiSInfo.jsp");
                    session.setAttribute("pazienteInfo", null);
                }
                break;
                
            case "/medicare/Servlet/closeEsameScelto":
                session = request.getSession();
                session.setAttribute("theEsameP", null);
                session.setAttribute("theTipoEsame", null);
                session.setAttribute("theVisitaM", null);
                session.setAttribute("thePaz", null);
                session.setAttribute("theMed", null);
                session.setAttribute("theVisitaS", null);
                session.setAttribute("isConclusa", null);
                session.setAttribute("theAnamnesi", null);
                session.setAttribute("theTicket", null);
                response.sendRedirect("/medicare/visualizzaEsame.jsp");
                break;
                
            case "/medicare/Servlet/closeVisitaScelta":
                session = request.getSession();
                session.setAttribute("theVisitaSelected", null);
                session.setAttribute("thePazienteVisita", null);
                session.setAttribute("listEsamiPrescrittiV", null);
                session.setAttribute("listVisiteSpecialisticheV", null);
                session.setAttribute("listAnamnesiV", null);
                session.setAttribute("listTicketsV", null);
                session.setAttribute("listNomiEsamiV", null);
                session.setAttribute("listNomiSpecialistV", null);
                session.setAttribute("listMedicinaliPV", null);
                session.setAttribute("listNomiMedicinaliV", null);
                response.sendRedirect("/medicare/visualizzaVisita.jsp");
                break;
                
            case "/medicare/Servlet/caricaXLSInfo":
                session = request.getSession();
                ArrayList<MedicinalePrescrittoBean> medicinaliS = (ArrayList<MedicinalePrescrittoBean>) session.getAttribute("medicinaliS");
                ArrayList<String> nomiMedicinaliS = (ArrayList<String>) session.getAttribute("nomiMedicinaliS");
                ArrayList<String> nomiPazS = (ArrayList<String>) session.getAttribute("nomiPazS");
                ArrayList<String> nomiMedS = (ArrayList<String>) session.getAttribute("nomiMedS");
                XSSFWorkbook workbook = new XSSFWorkbook();
                XSSFSheet sheet = workbook.createSheet("Datatypes in Java");
                int rowNum = 0;
                
                //intestazione file
                Row row = sheet.createRow(rowNum++);
                int colNum = 0;
                //id
                Cell cell = row.createCell(colNum++);
                cell.setCellValue((String) "Id");
                //data
                cell = row.createCell(colNum++);
                cell.setCellValue((String) "Data rilascio");
                //paziente
                cell = row.createCell(colNum++);
                cell.setCellValue((String) "Paziente");
                //medico
                cell = row.createCell(colNum++);
                cell.setCellValue((String) "Medico di base");
                //medicinale
                cell = row.createCell(colNum++);
                cell.setCellValue((String) "Medicinale");
                    
                for (int i = 0; i < medicinaliS.size(); i++) {
                    row = sheet.createRow(rowNum++);
                    colNum = 0;
                    //id
                    cell = row.createCell(colNum++);
                    cell.setCellValue((String) medicinaliS.get(i).getId());
                    //data
                    cell = row.createCell(colNum++);
                    cell.setCellValue((String) medicinaliS.get(i).getDataRilascio());
                    //paziente
                    cell = row.createCell(colNum++);
                    cell.setCellValue((String) nomiPazS.get(i));
                    //medico
                    cell = row.createCell(colNum++);
                    cell.setCellValue((String) nomiMedS.get(i));
                    //medicinale
                    cell = row.createCell(colNum++);
                    cell.setCellValue((String) nomiMedicinaliS.get(i));
                }
                try {
                    java.sql.Date dataSceltaJ = (java.sql.Date) session.getAttribute("dataScelta");
                    String dataScelta = dataSceltaJ.toString();
                    String filename = "report_"+dataScelta+".xls";
                    response.setContentType("application/vnd.ms-excel");
                    //response.setHeader("Content-Disposition", "attachment; filename=filename.xls");
                    response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", filename));
                    workbook.write(response.getOutputStream()); // Write workbook to response.
                    workbook.close();
                } catch (FileNotFoundException e) {
                    System.out.println(""+e);
                } catch (IOException e) {
                    System.out.println(""+e);
                }
                break;
        }
    }

    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean valid;
        HttpSession session;
        PazienteBean pazienteSession;
        
        switch(request.getRequestURI()) {
            case "/medicare/Servlet/login":
                LoginBean loginBean = new LoginBean();
                String remember = request.getParameter("remember");
                boolean rememberOn;     
                int numVisite, numEsami;
                if(remember == null) rememberOn = false;
                else rememberOn = (remember.equals("on"));
                String cryptedPassword = hashPassword(request.getParameter("password"), request.getParameter("username"));
                valid = doLogin(request.getParameter("username"), cryptedPassword, loginBean);
                
                if(valid) {
                    //SESSIONE
                    PazienteBean pb = new PazienteBean();
                    pb.setCf(loginBean.getPersona());
                    session = request.getSession();
                    doPazienteData(pb);
                    session.setAttribute("pazienteBeanInstance", pb);
                    session.setAttribute("userType", loginBean.getType());
                    session.setAttribute("switchState", 0);
                    //COOKIE PER IL REMEMBER ME
                    if(rememberOn) {
                        Cookie c1 = new Cookie("cookuser", request.getParameter("username"));
                        Cookie c2 = new Cookie("cookpass", request.getParameter("password"));
                        c1.setMaxAge(60 * 60 * 24 * 365 * 10); //10 YEARS
                        c2.setMaxAge(60 * 60 * 24 * 365 * 10); //10 YEARS
                        response.addCookie(c1);
                        response.addCookie(c2);
                    }
                    //CARICO GLI ESAMI
                    ArrayList<EsameBean> listEsami;
                    listEsami = esameDao.list();
                    session.setAttribute("listEsami", listEsami);
                    //CARICO LE NOTIFICHE
                    ArrayList<NotificheBean> notifiche;
                    notifiche = notificheDao.getNotificheForUser(pb.getCf());
                    session.setAttribute("notifiche", notifiche);
                    //REINDIRIZZO
                    switch(loginBean.getType()) {
                        case PAZIENTE:
                            response.sendRedirect("/medicare/home.jsp");
                            break;
                        case BASE:
                            numVisite = visitaMedicaDao.getNumberTodayVisiteMedicheByMedico(pb.getCf());
                            session.setAttribute("numVisiteOggi", numVisite);
                            response.sendRedirect("/medicare/Servlet/parcoPazienti");
                            break;
                        case SPECIALISTA:
                            numEsami = visitaSpecialisticaDao.getNumberTodayVisitaSpecialisticaBySpecialista(pb.getCf());
                            session.setAttribute("numEsamiOggi", numEsami);
                            response.sendRedirect("/medicare/Servlet/pazienti");
                            break;
                    }
                } else {
                    response.sendRedirect("/medicare/login.jsp?error=1");
                }
                break;
                
            case "/medicare/Servlet/visitaMedicaInsData":
                VisitaMedicaBean visitaMedicaBean = new VisitaMedicaBean();
                session = request.getSession();
                pazienteSession = (PazienteBean)session.getAttribute("pazienteBeanInstance");
                
                try {
                    valid = insertVisitaMedica(request.getParameter("date"), pazienteSession.getCf(), pazienteSession.getMedicoBase(), visitaMedicaBean);
                } catch (ParseException ex) {
                    Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
                    valid = false;
                }
                if(valid) {
                    //REINDIRIZZO ALLA MAIL SERVLET
                    session.setAttribute("MdataVisita", request.getParameter("date"));
                    session.setAttribute("MpageTo", "visitaMedicaInsData.jsp?works=1");
                    response.sendRedirect("/medicare/MailServlet/sendMailPrenotazioneVisita");
                } else {
                    //REINDIRIZZO
                    response.sendRedirect("/medicare/visitaMedicaInsData.jsp?error=1");
                }
                break;
                
            case "/medicare/Servlet/setMedBase":
                String cf = (String)request.getParameter("cf");
                session = request.getSession();
                pazienteSession = (PazienteBean)session.getAttribute("pazienteBeanInstance");
                valid = pazienteDao.setMedBase(pazienteSession, cf);
                if(valid) {
                    pazienteSession.setMedicoBase(cf);
                    //CARICA INFO SU MED BASE
                    PazienteBean mbean = new PazienteBean();
                    mbean.setCf(pazienteSession.getMedicoBase());
                    doMedicoData(mbean);
                    session.setAttribute("medicoDataInstance", mbean);
                    //session.setAttribute("isHidden", false);
                    //REINDIRIZZO
                    session.setAttribute("MpageTo", "Servlet/pazienteData");
                    response.sendRedirect("/medicare/MailServlet/sendMailCambioMedBase");
                } else {
                    //REINDIRIZZO
                    //session.setAttribute("isHidden", false);
                    response.sendRedirect("/medicare/pazienteData.jsp?error=2");
                }
                break;
                
            case "/medicare/Servlet/getVisitaInfo":
                session = request.getSession();
                String id = request.getParameter("id");
                VisitaMedicaBean visita = new VisitaMedicaBean();
                PrescrizioneBean prescrizione = new PrescrizioneBean();
                prescrizione.setVisitaMedica(id);
                visita.setId(id);
                
                try {
                    valid = (visitaMedicaDao.getVisitaMedicaById(visita) && prescrizioneDao.createPrescrizione(prescrizione));
                } catch (SQLException ex) {
                    System.out.println(ex);
                    valid = false;
                }
                
                if(valid) {
                    PazienteBean paziente = new PazienteBean();
                    paziente.setCf(visita.getPaziente());
                    session.setAttribute("MPaziente", visita.getPaziente());
                    valid = doPazienteData(paziente);
                    
                    if(valid) {
                        session = request.getSession();
                        ArrayList<MedicinaleBean> medicinali = medicinaleDao.listMedicinali();
                        //lista medicinali
                        session.setAttribute("listMedicinali", medicinali);
                        //la visita
                        session.setAttribute("visitaInstance", visita);
                        //il paziente della visita
                        session.setAttribute("pazienteVisitaInstance", paziente);
                        //la prescrizione collegata alla visita
                        session.setAttribute("prescrizioneInstance", prescrizione);
                        //lista medici specialisti
                        ArrayList<PazienteBean> specialisti;
                        specialisti = medicoSpecialistaDao.list();
                        session.setAttribute("listMedSpecialisti", specialisti);
                        //lista esami specialistici
                        ArrayList<EsameBean> esami;
                        esami = esameDao.list();
                        session.setAttribute("listEsamiSpecialistici", esami);
                        session.setAttribute("isListMSHidden", true);
                        session.setAttribute("isDataEsameHidden", false);
                        //vettori temporanei
                        session.setAttribute("tempMedicinaliPrescritti", new ArrayList<MedicinalePrescrittoBean>());
                        session.setAttribute("tempMedicinali", new ArrayList<MedicinaleBean>());
                        session.setAttribute("tempEsami", new ArrayList<EsameBean>());
                        session.setAttribute("tempMS", new ArrayList<PazienteBean>());
                        session.setAttribute("tempEsamiPrescritti", new ArrayList<EsamePrescrittoBean>());
                        session.setAttribute("tempVisiteSpecialistiche", new ArrayList<VisitaSpecialisticaBean>());
                        //reindirizzo
                        response.sendRedirect("/medicare/doVisita.jsp");
                    } else {
                        //errore: paziente non esistente
                        response.sendRedirect("/medicare/doVisita.jsp?error=2");
                    }
                } else {
                    //errore: visita non presente
                    response.sendRedirect("/medicare/doVisita.jsp?error=1");
                }
                break;
                
            case "/medicare/Servlet/getVisitaInfoWithAnamnesi":
                session = request.getSession();
                id = request.getParameter("id");
                visita = new VisitaMedicaBean();
                prescrizione = new PrescrizioneBean();
                prescrizione.setVisitaMedica(id);
                visita.setId(id);
                
                try {
                    valid = (visitaMedicaDao.getVisitaMedicaById(visita) && prescrizioneDao.createPrescrizione(prescrizione));
                } catch (SQLException ex) {
                    System.out.println(ex);
                    valid = false;
                }

                if(valid) {
                    PazienteBean paziente = new PazienteBean();
                    paziente.setCf(visita.getPaziente());
                    session.setAttribute("MPaziente", visita.getPaziente());
                    valid = doPazienteData(paziente);
                    
                    if(valid) {
                        session = request.getSession();
                        ArrayList<MedicinaleBean> medicinali = medicinaleDao.listMedicinali();
                        //lista medicinali
                        session.setAttribute("listMedicinali", medicinali);
                        //la visita
                        session.setAttribute("visitaInstance", visita);
                        //il paziente della visita
                        session.setAttribute("pazienteVisitaInstance", paziente);
                        //la prescrizione collegata alla visita
                        session.setAttribute("prescrizioneInstance", prescrizione);
                        //lista medici specialisti
                        ArrayList<PazienteBean> specialisti;
                        specialisti = medicoSpecialistaDao.list();
                        session.setAttribute("listMedSpecialisti", specialisti);
                        //lista esami specialistici
                        ArrayList<EsameBean> esami;
                        esami = esameDao.list();
                        session.setAttribute("listEsamiSpecialistici", esami);
                        session.setAttribute("isListMSHidden", true);
                        session.setAttribute("isDataEsameHidden", false);
                        
                        //lista esami fatti in passato per questa visita
                        //preparo variabili e vettori
                        VisitaSpecialisticaBean theVisitaS;
                        AnamnesiBean theAnamnesi;
                        TicketBean theTicket;
                        String theEsame;
                        String theSpecialist;
                        EsamePrescrittoBean theEsameP;
                        //carico esami prescritti
                        ArrayList<EsamePrescrittoBean> esamiPrescritti = esamePrescrittoDao.getEsamePrescrittoByPrescrizione(Integer.parseInt(prescrizione.getId()));
                        ArrayList<VisitaSpecialisticaBean> visiteSpecialistiche = new ArrayList<>();
                        ArrayList<AnamnesiBean> anamnesi = new ArrayList<>();
                        ArrayList<TicketBean> tickets = new ArrayList<>();
                        ArrayList<String> nomiEsami = new ArrayList<>();
                        ArrayList<String> nomiSpecialist = new ArrayList<>();
                        
                        for(int i = 0; i < esamiPrescritti.size(); i++) {
                            theEsameP = esamiPrescritti.get(i);
                            //visite specialistiche
                            theVisitaS = visitaSpecialisticaDao.getVisitaSByEsameP(Integer.parseInt(theEsameP.getId()));
                            visiteSpecialistiche.add(i, theVisitaS);
                            //nome esame
                            theEsame = esameDao.getEsameNome(Integer.parseInt(theEsameP.getEsame()));
                            nomiEsami.add(i, theEsame);
                            //nome specialista
                            theSpecialist = pazienteDao.getNomePaziente(theVisitaS.getMedicoSpecialista());
                            nomiSpecialist.add(i, theSpecialist);
                            
                            if(theVisitaS.getTerminata().equals("1")) {
                                //allora c'e anamnesi e ticket
                                theAnamnesi = anamnesiDao.getAnamnesiByVisitaS(Integer.parseInt(theVisitaS.getId()));
                                anamnesi.add(i, theAnamnesi);
                                theTicket = ticketDao.getTicketById(Integer.parseInt(theAnamnesi.getId()));
                                tickets.add(i, theTicket);
                            } else {
                                anamnesi.add(i, null);
                                tickets.add(i, null);
                            }
                        }
                        session.setAttribute("listEsamiPrescritti", esamiPrescritti);
                        session.setAttribute("listVisiteSpecialistiche", visiteSpecialistiche);
                        session.setAttribute("listAnamnesi", anamnesi);
                        session.setAttribute("listTickets", tickets);
                        session.setAttribute("listNomiEsami", nomiEsami);
                        session.setAttribute("listNomiSpecialist", nomiSpecialist);
                        
                        //lista medicinali prescritti in passato per questa visita
                        ArrayList<MedicinalePrescrittoBean> medicinaliP;
                        ArrayList<String> nomiMedicinali = new ArrayList<>();
                        MedicinalePrescrittoBean theMedicinaleP;
                        String theNomeM;
                        medicinaliP = medicinalePrescrittoDao.getMedicinaliForPrescrizione(Integer.parseInt(prescrizione.getId()));
                        
                        for(int i = 0; i < medicinaliP.size(); i++) {
                            theMedicinaleP = medicinaliP.get(i);
                            theNomeM = medicinaleDao.getNomeMedicinale(theMedicinaleP.getMedicinale());
                            nomiMedicinali.add(i, theNomeM);
                        }
                        session.setAttribute("listMedicinaliP", medicinaliP);
                        session.setAttribute("listNomiMedicinali", nomiMedicinali);
                        //vettori temporanei
                        session.setAttribute("tempMedicinaliPrescritti", new ArrayList<MedicinalePrescrittoBean>());
                        session.setAttribute("tempMedicinali", new ArrayList<MedicinaleBean>());
                        session.setAttribute("tempEsami", new ArrayList<EsameBean>());
                        session.setAttribute("tempMS", new ArrayList<PazienteBean>());
                        session.setAttribute("tempEsamiPrescritti", new ArrayList<EsamePrescrittoBean>());
                        session.setAttribute("tempVisiteSpecialistiche", new ArrayList<VisitaSpecialisticaBean>());
                        //reindirizzo
                        response.sendRedirect("/medicare/doVisitaWithAnamnesi.jsp");
                    } else {
                        //errore: paziente non esistente
                        response.sendRedirect("/medicare/doVisitaWithAnamnesi.jsp?error=2");
                    }
                } else {
                    //errore: visita non presente
                    response.sendRedirect("/medicare/doVisitaWithAnamnesi.jsp?error=1");
                }
                break;

            case "/medicare/Servlet/addMedicinalePrescrizione":
                session = request.getSession();
                String idMedicinale = request.getParameter("id"); 
                PrescrizioneBean presc = (PrescrizioneBean)session.getAttribute("prescrizioneInstance");
                MedicinaleBean medicinale = new MedicinaleBean();
                medicinale.setId(idMedicinale);
                medicinaleDao.getMedicinale(medicinale);
                MedicinalePrescrittoBean medPresc = new MedicinalePrescrittoBean();
                medPresc.setMedicinale(idMedicinale);
                medPresc.setPrescrizione(presc.getId());
                ArrayList<MedicinalePrescrittoBean> tempMPB = (ArrayList<MedicinalePrescrittoBean>)session.getAttribute("tempMedicinaliPrescritti");
                ArrayList<MedicinaleBean> tempMedicinali = (ArrayList<MedicinaleBean>) session.getAttribute("tempMedicinali");
                
                if(tempMedicinali.size() == 2) {
                    response.sendRedirect("/medicare/doVisita.jsp?error=7");
                } else {
                    tempMedicinali.add(medicinale);
                    tempMPB.add(medPresc);
                    response.sendRedirect("/medicare/doVisita.jsp#aggiungiMedicinale");
                }
                break;
                
            case "/medicare/Servlet/addMedicinalePrescrizioneWA":
                session = request.getSession();
                idMedicinale = request.getParameter("id"); 
                presc = (PrescrizioneBean)session.getAttribute("prescrizioneInstance");
                medicinale = new MedicinaleBean();
                medicinale.setId(idMedicinale);
                medicinaleDao.getMedicinale(medicinale);
                medPresc = new MedicinalePrescrittoBean();
                medPresc.setMedicinale(idMedicinale);
                medPresc.setPrescrizione(presc.getId());
                tempMPB = (ArrayList<MedicinalePrescrittoBean>)session.getAttribute("tempMedicinaliPrescritti");
                tempMedicinali = (ArrayList<MedicinaleBean>) session.getAttribute("tempMedicinali");
                
                if(tempMedicinali.size() == 2) {
                    response.sendRedirect("/medicare/doVisitaWithAnamnesi.jsp?error=7");
                } else {
                    tempMedicinali.add(medicinale);
                    tempMPB.add(medPresc);
                    response.sendRedirect("/medicare/doVisitaWithAnamnesi.jsp");
                }
                break;
                
            case "/medicare/Servlet/scegliEsame":
                //ricevo id esame
                String idEsame = request.getParameter("id");
                session = request.getSession();
                //salvo info esame(attuale esame)
                EsameBean esame = new EsameBean();
                esame.setId(idEsame);
                esameDao.getEsameInfo(esame);
                session.setAttribute("esameAttuale", esame);
                //creo esame prescritto e lo salvo(attuale esame prescritto)
                PrescrizioneBean pbean = (PrescrizioneBean)session.getAttribute("prescrizioneInstance");
                EsamePrescrittoBean epbean = new EsamePrescrittoBean();
                epbean.setDataPrenotazione((String)session.getAttribute("dataEsameTemp"));
                epbean.setEsame(esame.getId());
                epbean.setPrescrizione(pbean.getId());
                session.setAttribute("esamePrescrittoAttuale", epbean);
                //hidden false
                session.setAttribute("isListMSHidden", false);
                response.sendRedirect("/medicare/doVisita.jsp");
                break;
                
            case "/medicare/Servlet/scegliEsameWA":
                //ricevo id esame
                idEsame = request.getParameter("id");
                session = request.getSession();
                //salvo info esame(attuale esame)
                esame = new EsameBean();
                esame.setId(idEsame);
                esameDao.getEsameInfo(esame);
                session.setAttribute("esameAttuale", esame);
                //creo esame prescritto e lo salvo(attuale esame prescritto)
                pbean = (PrescrizioneBean)session.getAttribute("prescrizioneInstance");
                epbean = new EsamePrescrittoBean();
                epbean.setDataPrenotazione((String)session.getAttribute("dataEsameTemp"));
                epbean.setEsame(esame.getId());
                epbean.setPrescrizione(pbean.getId());
                session.setAttribute("esamePrescrittoAttuale", epbean);
                //hidden false
                session.setAttribute("isListMSHidden", false);
                response.sendRedirect("/medicare/doVisitaWithAnamnesi.jsp");
                break;
                
            case "/medicare/Servlet/scegliMedicoSpecialista":
                //ricevo cf medico specialista e salvo info
                String cfS = request.getParameter("cf");
                PazienteBean medicoSAttuale = new PazienteBean();
                medicoSAttuale.setCf(cfS);
                pazienteDao.getPazienteByCf(medicoSAttuale);
                session = request.getSession();
                //creo visitaSpecialistica e la salvo(attuale)
                VisitaSpecialisticaBean vsb = new VisitaSpecialisticaBean();
                EsamePrescrittoBean esamePAttuale = (EsamePrescrittoBean)session.getAttribute("esamePrescrittoAttuale");
                vsb.setEsamePrescritto(esamePAttuale.getId());
                vsb.setMedicoSpecialista(medicoSAttuale.getCf());
                //salvo in vett esami, esamiPrescritti, visiteSpecialistiche, mediciSpecialisti temporanei
                ArrayList<EsameBean> tempEsami = (ArrayList<EsameBean>)session.getAttribute("tempEsami");
                ArrayList<PazienteBean> tempMS = (ArrayList<PazienteBean>)session.getAttribute("tempMS");
                ArrayList<EsamePrescrittoBean> tempEsamiPrescritti = (ArrayList<EsamePrescrittoBean>)session.getAttribute("tempEsamiPrescritti");
                ArrayList<VisitaSpecialisticaBean> tempVisiteSpecialistiche = (ArrayList<VisitaSpecialisticaBean>)session.getAttribute("tempVisiteSpecialistiche");
                tempEsami.add((EsameBean)session.getAttribute("esameAttuale"));
                tempMS.add(medicoSAttuale);
                tempEsamiPrescritti.add((EsamePrescrittoBean)session.getAttribute("esamePrescrittoAttuale"));
                tempVisiteSpecialistiche.add(vsb);
                //hidden
                session.setAttribute("isListMSHidden", true);
                session.setAttribute("isDataEsameHidden", false);
                response.sendRedirect("/medicare/doVisita.jsp");
                break;
                
            case "/medicare/Servlet/scegliMedicoSpecialistaWA":
                //ricevo cf medico specialista e salvo info
                cfS = request.getParameter("cf");
                medicoSAttuale = new PazienteBean();
                medicoSAttuale.setCf(cfS);
                pazienteDao.getPazienteByCf(medicoSAttuale);
                session = request.getSession();
                //creo visitaSpecialistica e la salvo(attuale)
                vsb = new VisitaSpecialisticaBean();
                esamePAttuale = (EsamePrescrittoBean)session.getAttribute("esamePrescrittoAttuale");
                vsb.setEsamePrescritto(esamePAttuale.getId());
                vsb.setMedicoSpecialista(medicoSAttuale.getCf());
                //salvo in vett esami, esamiPrescritti, visiteSpecialistiche, mediciSpecialisti temporanei
                tempEsami = (ArrayList<EsameBean>)session.getAttribute("tempEsami");
                tempMS = (ArrayList<PazienteBean>)session.getAttribute("tempMS");
                tempEsamiPrescritti = (ArrayList<EsamePrescrittoBean>)session.getAttribute("tempEsamiPrescritti");
                tempVisiteSpecialistiche = (ArrayList<VisitaSpecialisticaBean>)session.getAttribute("tempVisiteSpecialistiche");
                tempEsami.add((EsameBean)session.getAttribute("esameAttuale"));
                tempMS.add(medicoSAttuale);
                tempEsamiPrescritti.add((EsamePrescrittoBean)session.getAttribute("esamePrescrittoAttuale"));
                tempVisiteSpecialistiche.add(vsb);
                //hidden
                session.setAttribute("isListMSHidden", true);
                session.setAttribute("isDataEsameHidden", false);
                response.sendRedirect("/medicare/doVisitaWithAnamnesi.jsp");
                break;
                
            case "/medicare/Servlet/setDataEsame":
                session = request.getSession();
                String dataEsame = request.getParameter("dataEsame");
                PrescrizioneBean prescrizioneBean = (PrescrizioneBean)session.getAttribute("prescrizioneInstance");
                valid = controlloDuplicatoEsame(dataEsame, Integer.parseInt(prescrizioneBean.getId()));
                
                if(valid) {
                    response.sendRedirect("/medicare/doVisita.jsp?error=5");
                    return;
                }
                tempEsamiPrescritti = (ArrayList<EsamePrescrittoBean>)session.getAttribute("tempEsamiPrescritti");
                
                if(tempEsamiPrescritti.size() > 0) {
                    for(EsamePrescrittoBean epb : tempEsamiPrescritti) {
                        if(epb.getDataPrenotazione().equals(dataEsame)) {
                            response.sendRedirect("/medicare/doVisita.jsp?error=5");
                            return;
                        }
                    }
                }
                session.setAttribute("dataEsameTemp", dataEsame);
                session.setAttribute("isDataEsameHidden", true);
                response.sendRedirect("/medicare/doVisita.jsp#prenotaEsameSpec");
                break;
                
            case "/medicare/Servlet/setDataEsameWA":
                session = request.getSession();
                dataEsame = request.getParameter("dataEsame");
                prescrizioneBean = (PrescrizioneBean)session.getAttribute("prescrizioneInstance");
                valid = controlloDuplicatoEsame(dataEsame, Integer.parseInt(prescrizioneBean.getId()));
                
                if(valid) {
                    response.sendRedirect("/medicare/doVisitaWithAnamnesi.jsp?error=5");
                    return;
                }
                tempEsamiPrescritti = (ArrayList<EsamePrescrittoBean>)session.getAttribute("tempEsamiPrescritti");
                
                if(tempEsamiPrescritti.size() > 0) {
                    for(EsamePrescrittoBean epb : tempEsamiPrescritti) {
                        if(epb.getDataPrenotazione().equals(dataEsame)) {
                            response.sendRedirect("/medicare/doVisitaWithAnamnesi.jsp?error=5");
                            return;
                        }
                    }
                }
                session.setAttribute("dataEsameTemp", dataEsame);
                session.setAttribute("isDataEsameHidden", true);
                response.sendRedirect("/medicare/doVisita.jsp");
                break;
                
            case "/medicare/Servlet/confermaVisitaSpecialistica":
                String descrizione = (String)request.getParameter("descrizione");
                session = request.getSession();
                VisitaSpecialisticaBean theVS = (VisitaSpecialisticaBean) session.getAttribute("VSAttuale");
                PrescrizioneBean thePresc = (PrescrizioneBean) session.getAttribute("prescAttuale");
                TicketBean ticket = new TicketBean();
                ticket.setCosto("50");
                valid = ticketDao.addTicket(ticket);
                
                if(valid) {
                    AnamnesiBean anamnesi = new AnamnesiBean();
                    anamnesi.setTicket(ticket.getId());
                    anamnesi.setDescrizione(descrizione);
                    anamnesi.setVisitaSpecialistica(theVS.getId());
                    anamnesiDao.create(anamnesi);
                    //set visita specialistica terminata 1
                    visitaSpecialisticaDao.terminaVisita(theVS, 1);
                    //set visitamedica stato
                    boolean isVisitaClean = visitaMedicaDao.areVSFinished(thePresc.getVisitaMedica());
                    
                    if(isVisitaClean) {
                        //set stato visita medica
                        visitaMedicaDao.setStatoVisitaMedica(thePresc.getVisitaMedica(), 2);
                    }
                    //preparo tutto per la mail
                    session.setAttribute("MpageTo", "Servlet/pazienti");
                    response.sendRedirect("/medicare/MailServlet/sendMailEsameEseguito");
                } else {
                    response.sendRedirect("/medicare/doVisitaSpec.jsp?error=1");
                }
                break;
                
            case "/medicare/Servlet/changePassword":
                session = request.getSession();
                String oldPassword, newPassword, newPasswordR;
                oldPassword = (String)request.getParameter("oldPassword");
                newPassword = (String)request.getParameter("newPassword");
                newPasswordR = (String)request.getParameter("newPasswordR");
                
                //check equals new pwd
                if(newPassword.equals(newPasswordR)) {
                    PazienteBean thePaziente = (PazienteBean) session.getAttribute("pazienteBeanInstance");
                    String username = loginDao.getUsernameByCf(thePaziente.getCf());
                    LoginBean loginB = new LoginBean();
                    String cryptedOldPassword = hashPassword(oldPassword, username);
                    valid = doLogin(username, cryptedOldPassword, loginB);
                    
                    if(valid) {
                        String cryptedNewPassword = hashPassword(newPassword, username);
                        valid = loginDao.setPasswordForUser(cryptedNewPassword, username);
                        
                        if(valid) {
                            session.setAttribute("MpageTo", "pazienteData.jsp");
                            response.sendRedirect("/medicare/MailServlet/sendMailCambioPassword");
                        } else {
                            response.sendRedirect("/medicare/changePassword.jsp?error=3");
                        }
                    } else {
                        response.sendRedirect("/medicare/changePassword.jsp?error=1");
                    }
                } else {
                    response.sendRedirect("/medicare/changePassword.jsp?error=2");
                }
                break;
                
            case "/medicare/Servlet/forgotPassword":
                session = request.getSession();
                cf = pazienteDao.getCfFromUser(request.getParameter("username"));
                String newPsw;
                int n = 8;
                String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"+"0123456789"+"abcdefghijklmnopqrstuvxyz";

                // create StringBuffer size of AlphaNumericString 
                StringBuilder sb = new StringBuilder(n);

                for (int i = 0; i < n; i++) {

                    // generate a random number between 
                    // 0 to AlphaNumericString variable length 
                    int index = (int) (AlphaNumericString.length() * Math.random());

                    // add Character one by one in end of sb 
                    sb.append(AlphaNumericString.charAt(index));
                }
                //random password generated
                newPsw = sb.toString();
                //hash psw
                String cryptedForgotPassword = hashPassword(newPsw, request.getParameter("username"));
                valid = loginDao.setPasswordForUser(cryptedForgotPassword, request.getParameter("username"));
                
                if(valid) {
                    session.setAttribute("newPsw", newPsw);
                    session.setAttribute("cf", cf);
                    session.setAttribute("MpageTo", "login.jsp?error=11");
                    response.sendRedirect("/medicare/MailServlet/sendMailForgotPassword");
                } else {
                    response.sendRedirect("/medicare/login.jsp?error=10");
                }
                
                break;
                
            case "/medicare/Servlet/getLastVisitaAndMedicinale":
                session = request.getSession();
                cf = request.getParameter("cf");
                //info paziente
                PazienteBean paz = new PazienteBean();
                paz.setCf(cf);
                pazienteDao.getPazienteByCf(paz);
                //foto paziente
                FotoBean foto = fotoDao.getLastFotoUtente(paz.getCf());
                session.setAttribute("fotoSchedaPaziente", foto);
                //parco pazienti
                PazienteBean medico = (PazienteBean)session.getAttribute("pazienteBeanInstance");
                ArrayList<PazienteBean> parco = medicoBaseDao.parcoPazienti(medico.getCf());
                session.setAttribute("parcoPazienti", parco);
                VisitaMedicaBean lastVisita = visitaMedicaDao.getLastVisitaForUser(cf); //data - stato visita
                
                if(lastVisita != null) {
                    VisitaMedicaBean lastVisitaWithMedicinale = visitaMedicaDao.getLastVisitaForUserWithM(cf); //data medicinale
                    
                    if(lastVisitaWithMedicinale != null) {
                        MedicinalePrescrittoBean lastMedicinale = medicinalePrescrittoDao.getLastMedicinaleForVisita(lastVisitaWithMedicinale.getId());
                        String nomeMedicinale = medicinaleDao.getNomeMedicinale(lastMedicinale.getMedicinale()); //nome medicinale

                        session.setAttribute("dataLastVisita", lastVisita.getDataIscrizione());
                        session.setAttribute("statoLastVisita", lastVisita.getTerminata());
                        session.setAttribute("dataLastMedicinale", lastMedicinale.getDataRilascio());
                        session.setAttribute("nomeLastMedicinale", nomeMedicinale);
                    } else {
                        session.setAttribute("dataLastVisita", lastVisita.getDataIscrizione());
                        session.setAttribute("statoLastVisita", lastVisita.getTerminata());
                        session.setAttribute("dataLastMedicinale", null);
                        session.setAttribute("nomeLastMedicinale", null);
                    }
                } else {
                    session.setAttribute("dataLastVisita", null);
                    session.setAttribute("statoLastVisita", null);
                    session.setAttribute("dataLastMedicinale", null);
                    session.setAttribute("nomeLastMedicinale", null);
                }
                session.setAttribute("isAllInfoDisabled", true);
                session.setAttribute("pazienteInfo", paz);
                response.sendRedirect("/medicare/pazientiMInfo.jsp");
                break;
            
            case "/medicare/Servlet/pagaTicket":
                session = request.getSession();
                id = request.getParameter("id");
                //aggiorna valore da 0 a 1, se va a buon fine, aggiorna la pagina passandogli il nuovo array list
                if(ticketDao.pagamentoEffettuato(id)) { //se aggiorna il valore del ticket
                    //torna i nuovi arraylist modificati e reinderizza sulla pagina modificata
                    int idTicket;
                    PazienteBean pb = (PazienteBean)session.getAttribute("pazienteBeanInstance");
                    //CREO BEAN
                    ArrayList<TicketBean> lista;
                    lista = ticketDao.getTicketsForUser(pb.getCf());
                    ArrayList<String> lista1 = new ArrayList<>();

                    for(int i = 0; i < lista.size(); i++) {
                        idTicket = Integer.parseInt(lista.get(i).getId());
                        lista1.add(i, ticketDao.getDateForTicket(idTicket));
                    }

                    //INSERISCO IL BEAN NELLA SESSIONE E REINDIRIZZO
                    if(!lista.isEmpty() && !lista1.isEmpty()) {
                        session = request.getSession();
                        session.setAttribute("listTickets", lista);
                        session.setAttribute("listDateTickets", lista1);
                        response.sendRedirect("/medicare/visualizzaTicket.jsp");
                    } else {
                        response.sendRedirect("/medicare/visualizzaTicket.jsp"); //con error
                        System.err.println("lista ticket vuota");
                    }
                } else {
                    response.sendRedirect("/medicare/visualizzaTicket.jsp?error=1");
                }
                break;
                
            case "/medicare/Servlet/visualizzaVisitaInfo":
                session = request.getSession();
                id = request.getParameter("id");
                //lista esami fatti per questa visita
                //preparo variabili e vettori
                VisitaSpecialisticaBean theVisitaS;
                AnamnesiBean theAnamnesi;
                TicketBean theTicket;
                String theEsame;
                String theSpecialist;
                EsamePrescrittoBean theEsameP;
                //carico la visita e la prescrizione
                VisitaMedicaBean theVisita = new VisitaMedicaBean();
                theVisita.setId(id);
                visitaMedicaDao.getVisitaMedicaById(theVisita);
                session.setAttribute("theVisitaSelected", theVisita);
                thePresc = prescrizioneDao.getPrescrizioneByVisita(theVisita.getId());
                //carico info sul paziente
                String thePazCf = theVisita.getPaziente();
                PazienteBean thePazienteVisita = new PazienteBean();
                thePazienteVisita.setCf(thePazCf);
                pazienteDao.getPazienteByCf(thePazienteVisita);
                session.setAttribute("thePazienteVisita", thePazienteVisita);
                //pulisco le variabili
                ArrayList<EsamePrescrittoBean> esamiPrescritti;
                ArrayList<VisitaSpecialisticaBean> visiteSpecialistiche = new ArrayList<>();
                ArrayList<AnamnesiBean> anamnesi = new ArrayList<>();
                ArrayList<TicketBean> tickets = new ArrayList<>();
                ArrayList<String> nomiEsami = new ArrayList<>();
                ArrayList<String> nomiSpecialist = new ArrayList<>();
                
                if(thePresc != null) {
                    //carico esami prescritti
                    esamiPrescritti = esamePrescrittoDao.getEsamePrescrittoByPrescrizione(Integer.parseInt(thePresc.getId()));

                    for(int i = 0; i < esamiPrescritti.size(); i++) {
                        theEsameP = esamiPrescritti.get(i);
                        //visite specialistiche
                        theVisitaS = visitaSpecialisticaDao.getVisitaSByEsameP(Integer.parseInt(theEsameP.getId()));
                        visiteSpecialistiche.add(i, theVisitaS);
                        //nome esame
                        theEsame = esameDao.getEsameNome(Integer.parseInt(theEsameP.getEsame()));
                        nomiEsami.add(i, theEsame);
                        //nome specialista
                        theSpecialist = pazienteDao.getNomePaziente(theVisitaS.getMedicoSpecialista());
                        nomiSpecialist.add(i, theSpecialist);

                        if(theVisitaS.getTerminata().equals("1")) {
                            //allora c'e anamnesi e ticket
                            theAnamnesi = anamnesiDao.getAnamnesiByVisitaS(Integer.parseInt(theVisitaS.getId()));
                            anamnesi.add(i, theAnamnesi);
                            theTicket = ticketDao.getTicketById(Integer.parseInt(theAnamnesi.getId()));
                            tickets.add(i, theTicket);
                         } else {
                            anamnesi.add(i, null);
                            tickets.add(i, null);
                        }
                        
                        session.setAttribute("listEsamiPrescrittiV", esamiPrescritti);
                        session.setAttribute("listVisiteSpecialisticheV", visiteSpecialistiche);
                        session.setAttribute("listAnamnesiV", anamnesi);
                        session.setAttribute("listTicketsV", tickets);
                        session.setAttribute("listNomiEsamiV", nomiEsami);
                        session.setAttribute("listNomiSpecialistV", nomiSpecialist);
                    }
                    //lista medicinali prescritti per questa visita
                    ArrayList<MedicinalePrescrittoBean> medicinaliP = new ArrayList<>();
                    ArrayList<String> nomiMedicinali = new ArrayList<>();
                    MedicinalePrescrittoBean theMedicinaleP;
                    String theNomeM;
                    session.setAttribute("listMedicinaliPV", new ArrayList<>());
                    session.setAttribute("listNomiMedicinaliV", new ArrayList<>());
                    medicinaliP.clear();
                    medicinaliP = medicinalePrescrittoDao.getMedicinaliForPrescrizione(Integer.parseInt(thePresc.getId()));

                    if(medicinaliP != null && medicinaliP.size() > 0) {
                        for(int i = 0; i < medicinaliP.size(); i++) {
                            theMedicinaleP = medicinaliP.get(i);
                            theNomeM = medicinaleDao.getNomeMedicinale(theMedicinaleP.getMedicinale());
                            nomiMedicinali.add(i, theNomeM);
                        }
                        session.setAttribute("listMedicinaliPV", medicinaliP);
                        session.setAttribute("listNomiMedicinaliV", nomiMedicinali);
                    }
                } else {
                    session.setAttribute("listMedicinaliPV", new ArrayList<>());
                    session.setAttribute("listNomiMedicinaliV", new ArrayList<>());
                    session.setAttribute("listEsamiPrescrittiV", null);
                    session.setAttribute("listVisiteSpecialisticheV", null);
                    session.setAttribute("listAnamnesiV", null);
                    session.setAttribute("listTicketsV", null);
                    session.setAttribute("listNomiEsamiV", null);
                    session.setAttribute("listNomiSpecialistV", null);
                }
                response.sendRedirect("/medicare/Servlet/caricaVisite");
                break;
                
            case "/medicare/Servlet/scaricaPdf":
                String ticketId = request.getParameter("id");
                TicketBean ticketPdf = ticketDao.getTicketById(Integer.parseInt(ticketId));
                AnamnesiBean anamnesiPdf = anamnesiDao.getAnamnesiByTicket(Integer.parseInt(ticketId));
                String medicoSpecialistaPdf = visitaSpecialisticaDao.getMedicoSpecialistaByTicket(Integer.parseInt(ticketId));
                String tipoEsamePdf = esameDao.getTipoEsameByTicket(Integer.parseInt(ticketId));
                String nomePazientePdf = pazienteDao.getNomePazienteByTicket(Integer.parseInt(ticketId));
                
                gc = new GregorianCalendar(); //data odierna
                mese = gc.get(Calendar.MONTH) + 1;
                today = Date.valueOf(gc.get(Calendar.YEAR) + "-" + mese + "-" + gc.get(Calendar.DAY_OF_MONTH));
                
                try {
                    Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
                    Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
                    
                    Document document = new Document();
                    
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    
                    PdfWriter.getInstance(document, baos);
                    
                    document.open();
                    
                    Paragraph preface = new Paragraph();
                    
                    String testoQR = "Id ticket: " + ticketPdf.getId() + "\n Costo: "
                    + ticketPdf.getCosto() + "\n Data: " + anamnesiPdf.getDataRilascio() + "\n Medico specialista: " + medicoSpecialistaPdf + "\n Tipo esame:"
                    + tipoEsamePdf + "\n Descrizione anamnesi: " + anamnesiPdf.getDescrizione();
                    my_code = new BarcodeQRCode(testoQR, 1, 1, null);
                    qr_image = my_code.getImage();
                    qr_image.scaleAbsolute(100, 100);
                    
                    // Setting table's cells horizontal alignment
                    PdfPTable tableTop = new PdfPTable(3);
                    PdfPCell cell1 = new PdfPCell(qr_image);
                    cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell1.setBorder(Rectangle.NO_BORDER);
                    tableTop.addCell(cell1);
                    PdfPCell cell2 = new PdfPCell(new Phrase("TICKET", catFont));
                    cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell2.setBorder(Rectangle.NO_BORDER);
                    tableTop.addCell(cell2);
                    PdfPCell cell3 = new PdfPCell(new Phrase(nomePazientePdf));
                    cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell3.setBorder(Rectangle.NO_BORDER);
                    tableTop.addCell(cell3);
                    tableTop.completeRow();
                    document.add(tableTop);
                    
                    for (int i = 0; i < 8; i++) {
                        preface.add(new Paragraph(" "));
                    }
                    
                    PdfPTable table = new PdfPTable(2);
                    table.addCell("Id ticket:");
                    table.addCell(""+ticketPdf.getId());
                    table.addCell("Costo: ");
                    table.addCell(""+ticketPdf.getCosto());
                    table.addCell("Data: ");
                    table.addCell(""+anamnesiPdf.getDataRilascio());
                    table.addCell("Medico specialista: ");
                    table.addCell(medicoSpecialistaPdf);
                    table.addCell("Tipo esame: ");
                    table.addCell(tipoEsamePdf);
                    table.addCell("Descrizione anamnesi: ");
                    table.addCell(""+anamnesiPdf.getDescrizione());
                    preface.add(table);
                    
                    preface.add(new Paragraph(" "));
                    preface.add(new Paragraph(" "));
                    preface.add(new Paragraph("Data stampa: "+today));
                    
                    for (int i = 0; i < 8; i++) {
                        preface.add(new Paragraph(" "));
                    }
                    
                    // footer
                    preface.add(new Paragraph("This document is protected by copyright. - Medicare",redFont));
                    
                    document.add(preface);
                    document.close();

                    // setting some response headers
                    response.setHeader("Expires", "0");
                    response.setHeader("Cache-Control",
                        "must-revalidate, post-check=0, pre-check=0");
                    response.setHeader("Pragma", "public");
                    response.setHeader("Content-Disposition", "attachment; filename=ticket.pdf");
                    // setting the content type
                    response.setContentType("application/pdf");
                    // the contentlength
                    response.setContentLength(baos.size());
                    // write ByteArrayOutputStream to the ServletOutputStream
                    OutputStream os = response.getOutputStream();
                    baos.writeTo(os);
                    os.flush();
                    os.close();
                }
                catch(DocumentException e) {
                    throw new IOException(e.getMessage());
                }
                
                break;
                
            case "/medicare/Servlet/ricettaFarmaceuticaPdf":
                String medicinalePrescrittoId = request.getParameter("medicinalePrescrittoId");
                VisitaMedicaBean visitaMedica = visitaMedicaDao.getVisitaMedicaByMedicinalePrescritto(medicinalePrescrittoId);
                String tipoMedicinale = medicinaleDao.getTipoMedicinaleByMedicinalePrescritto(medicinalePrescrittoId);
                String idPrescrizione = prescrizioneDao.getIdByMedicinalePrescritto(medicinalePrescrittoId);
                String nomePaziente = pazienteDao.getNomeByMedicinalePrescritto(medicinalePrescrittoId);
                
                gc = new GregorianCalendar(); //data odierna
                mese = gc.get(Calendar.MONTH) + 1;
                today = Date.valueOf(gc.get(Calendar.YEAR) + "-" + mese + "-" + gc.get(Calendar.DAY_OF_MONTH));
                
                try {
                    Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
                    Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
                    
                    Document document = new Document();
                    
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    
                    PdfWriter.getInstance(document, baos);
                    
                    document.open();
                    
                    Paragraph preface = new Paragraph();
                    
                    String testoQRRicetta = "Id medicinale prescritto: " + medicinalePrescrittoId + "\n Identificativo medico: "
                    + visitaMedica.getMedicoBase() + "\n Codice fiscale paziente: " + visitaMedica.getPaziente() + "\n Timestamp prescrizione: " + visitaMedica.getDataIscrizione() + "\n Identificativo prescrizione:"
                    + idPrescrizione + "\n Descrizione farmaco: " + tipoMedicinale;
                    my_code = new BarcodeQRCode(testoQRRicetta, 1, 1, null);
                    qr_image = my_code.getImage();
                    qr_image.scaleAbsolute(100, 100);
                    
                    // Setting table's cells horizontal alignment
                    PdfPTable tableTop = new PdfPTable(3);
                    PdfPCell cell1 = new PdfPCell(qr_image);
                    cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell1.setBorder(Rectangle.NO_BORDER);
                    tableTop.addCell(cell1);
                    PdfPCell cell2 = new PdfPCell(new Phrase("RICETTA", catFont));
                    cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell2.setBorder(Rectangle.NO_BORDER);
                    tableTop.addCell(cell2);
                    PdfPCell cell3 = new PdfPCell(new Phrase(nomePaziente));
                    cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell3.setBorder(Rectangle.NO_BORDER);
                    tableTop.addCell(cell3);
                    tableTop.completeRow();
                    document.add(tableTop);
                    
                    for (int i = 0; i < 8; i++) {
                        preface.add(new Paragraph(" "));
                    }
                    
                    PdfPTable table = new PdfPTable(2);
                    table.addCell("Identificativo medico:");
                    table.addCell(""+visitaMedica.getMedicoBase());
                    table.addCell("Codice fiscale paziente: ");
                    table.addCell(""+visitaMedica.getPaziente());
                    table.addCell("Timestamp prescrizione: ");
                    table.addCell(""+visitaMedica.getDataIscrizione());
                    table.addCell("Identificativo prescrizione: ");
                    table.addCell(idPrescrizione);
                    table.addCell("Descrizione farmaco: ");
                    table.addCell(tipoMedicinale);
                    preface.add(table);
                    
                    preface.add(new Paragraph(" "));
                    preface.add(new Paragraph(" "));
                    preface.add(new Paragraph("Data stampa: "+today));
                    
                    for (int i = 0; i < 8; i++) {
                        preface.add(new Paragraph(" "));
                    }
                    
                    // footer
                    preface.add(new Paragraph("This document is protected by copyright. - Medicare",redFont));
                    
                    document.add(preface);
                    document.close();

                    // setting some response headers
                    response.setHeader("Expires", "0");
                    response.setHeader("Cache-Control",
                        "must-revalidate, post-check=0, pre-check=0");
                    response.setHeader("Pragma", "public");
                    response.setHeader("Content-Disposition", "attachment; filename=ricetta.pdf");
                    // setting the content type
                    response.setContentType("application/pdf");
                    // the contentlength
                    response.setContentLength(baos.size());
                    // write ByteArrayOutputStream to the ServletOutputStream
                    OutputStream os = response.getOutputStream();
                    baos.writeTo(os);
                    os.flush();
                    os.close();
                }
                catch(DocumentException e) {
                    throw new IOException(e.getMessage());
                }
                break;
                
            case "/medicare/Servlet/getInfoPaziente":
                //carica tutte le info che il medico vede di un paziente
                int exId, theId, idTicket;
                session = request.getSession();
                cf = request.getParameter("cf");
                //info paziente
                paz = new PazienteBean();
                paz.setCf(cf);
                pazienteDao.getPazienteByCf(paz);
                session.setAttribute("isAllInfoDisabled", false);
                session.setAttribute("pazienteInfo", paz);
                //foto paziente
                foto = fotoDao.getLastFotoUtente(paz.getCf());
                session.setAttribute("fotoSchedaPaziente", foto);
                //visite
                ArrayList<VisitaMedicaBean> visite;
                visite = visitaMedicaDao.getVisitaMedicaByPaziente(cf);
                session.setAttribute("visiteUtente", visite);
                //esami
                ArrayList<EsamePrescrittoBean> esami;
                ArrayList<String> tipiEsame = new ArrayList<>();
                ArrayList<String> statiEsami = new ArrayList<>();
                esami = esamePrescrittoDao.getEsamePrescrittoByPaziente(cf);
                
                for(int i = 0; i < esami.size(); i++) {
                    exId = Integer.parseInt(esami.get(i).getEsame());
                    tipiEsame.add(i, esameDao.getEsameNome(exId));
                    statiEsami.add(i, esamePrescrittoDao.getStatoEsame(Integer.parseInt(esami.get(i).getId())));
                }
                session.setAttribute("esamiUtente", esami);
                session.setAttribute("nomiEsamiUtente", tipiEsame);
                session.setAttribute("statiEsamiUtente", statiEsami);
                //medicinali
                ArrayList<MedicinalePrescrittoBean> medicinali;
                ArrayList<String> dateMedicinali = new ArrayList<>();
                ArrayList<String> nomiMedicinali = new ArrayList<>();
                medicinali = medicinalePrescrittoDao.getMedicinaliForUser(cf);
                
                for(int i = 0; i < medicinali.size(); i++) {
                    theId = Integer.parseInt(medicinali.get(i).getId());
                    dateMedicinali.add(i, visitaMedicaDao.getDataForMedicinale(theId));
                    nomiMedicinali.add(i, medicinaleDao.getNomeMedicinale(medicinali.get(i).getMedicinale()));
                }
                session.setAttribute("medicinaliUtente", medicinali);
                session.setAttribute("dateMedicinaliUtente", dateMedicinali);
                session.setAttribute("nomiMedicinaliUtente", nomiMedicinali);
                //anamnesi
                anamnesi = anamnesiDao.getAnamnesiForUser(cf);
                session.setAttribute("anamnesiUtente", anamnesi);
                //ticket
                ArrayList<String> ticketsDate = new ArrayList<>();
                tickets = ticketDao.getTicketsForUser(cf);
                
                for(int i = 0; i < tickets.size(); i++) {
                    idTicket = Integer.parseInt(tickets.get(i).getId());
                    ticketsDate.add(i, ticketDao.getDateForTicket(idTicket));
                }
                session.setAttribute("ticketsUtente", tickets);
                session.setAttribute("dateTicketsUtente", ticketsDate);
                response.sendRedirect("/medicare/pazientiMInfo.jsp");
                break;
                
            case "/medicare/Servlet/getInfoPazienteForS":
                //carica tutte le info che lo specialista vede di un paziente
                session = request.getSession();
                cf = request.getParameter("cf");
                //info paziente
                paz = new PazienteBean();
                paz.setCf(cf);
                pazienteDao.getPazienteByCf(paz);
                session.setAttribute("pazienteInfo", paz);
                //visite e relativi nomi dei medici
                visite = visitaMedicaDao.getVisitaMedicaByPaziente(cf);
                String nomeM;
                ArrayList<String> nomiMVisite = new ArrayList<>();
                
                for(int i = 0; i < visite.size(); i++) {
                    nomeM = pazienteDao.getNomePaziente(visite.get(i).getMedicoBase());
                    nomiMVisite.add(i, nomeM);
                }
                session.setAttribute("visiteUtente", visite);
                session.setAttribute("nomiMVisite", nomiMVisite);
                //esami
                tipiEsame = new ArrayList<>();
                statiEsami = new ArrayList<>();
                esami = esamePrescrittoDao.getEsamePrescrittoByPaziente(cf);
                
                for(int i = 0; i < esami.size(); i++) {
                    exId = Integer.parseInt(esami.get(i).getEsame());
                    tipiEsame.add(i, esameDao.getEsameNome(exId));
                    statiEsami.add(i, esamePrescrittoDao.getStatoEsame(Integer.parseInt(esami.get(i).getId())));
                }
                session.setAttribute("esamiUtente", esami);
                session.setAttribute("nomiEsamiUtente", tipiEsame);
                session.setAttribute("statiEsamiUtente", statiEsami);
                //medicinali
                nomiMedicinali = new ArrayList<>();
                medicinali = medicinalePrescrittoDao.getMedicinaliForUser(cf);
                
                for(int i = 0; i < medicinali.size(); i++) {
                    nomiMedicinali.add(i, medicinaleDao.getNomeMedicinale(medicinali.get(i).getMedicinale()));
                }
                session.setAttribute("medicinaliUtente", medicinali);
                session.setAttribute("nomiMedicinaliUtente", nomiMedicinali);
                //anamnesi
                anamnesi = anamnesiDao.getAnamnesiForUser(cf);
                session.setAttribute("anamnesiUtente", anamnesi);
                //ticket
                ticketsDate = new ArrayList<>();
                tickets = ticketDao.getTicketsForUser(cf);
                
                for(int i = 0; i < tickets.size(); i++) {
                    idTicket = Integer.parseInt(tickets.get(i).getId());
                    ticketsDate.add(i, ticketDao.getDateForTicket(idTicket));
                }
                session.setAttribute("ticketsUtente", tickets);
                session.setAttribute("dateTicketsUtente", ticketsDate);
                response.sendRedirect("/medicare/pazientiSInfo.jsp");
                break;
                
            case "/medicare/Servlet/visualizzaEsameInfo":
                //carica tutte le info che lo specialista vede di un esame
                session = request.getSession();
                id = request.getParameter("id");
                //prendo l'esame prescritto
                theEsameP = esamePrescrittoDao.getEsamePrescrittoById(id);
                //prendo prescrizione e tipo di esame
                thePresc = prescrizioneDao.getPrescrizioneById(theEsameP.getPrescrizione());
                String theTipoEsame = esameDao.getEsameNome(Integer.parseInt(theEsameP.getEsame()));
                //prendo visita medica e info su paziente, medico base
                VisitaMedicaBean theVisitaM = new VisitaMedicaBean();
                theVisitaM.setId(thePresc.getId());
                visitaMedicaDao.getVisitaMedicaById(theVisitaM);
                PazienteBean thePaz = new PazienteBean();
                thePaz.setCf(theVisitaM.getPaziente());
                pazienteDao.getPazienteByCf(thePaz);
                PazienteBean theMed = new PazienteBean();
                theMed.setCf(theVisitaM.getMedicoBase());
                pazienteDao.getPazienteByCf(theMed);
                //prendo visita specialistica
                theVisitaS = visitaSpecialisticaDao.getVisitaSByEsameP(Integer.parseInt(id));
                
                if(theVisitaS.getTerminata().equals("1")) {
                    //carico anamnesi e ticket
                    theAnamnesi = anamnesiDao.getAnamnesiByVisitaS(Integer.parseInt(theVisitaS.getId()));
                    theTicket = ticketDao.getTicketById(Integer.parseInt(theAnamnesi.getTicket()));
                    session.setAttribute("theEsameP", theEsameP);
                    session.setAttribute("theTipoEsame", theTipoEsame);
                    session.setAttribute("theVisitaM", theVisitaM);
                    session.setAttribute("thePaz", thePaz);
                    session.setAttribute("theMed", theMed);
                    session.setAttribute("theVisitaS", theVisitaS);
                    session.setAttribute("isConclusa", true);
                    session.setAttribute("theAnamnesi", theAnamnesi);
                    session.setAttribute("theTicket", theTicket);
                    response.sendRedirect("/medicare/Servlet/caricaEsami");
                } else {
                    session.setAttribute("theEsameP", theEsameP);
                    session.setAttribute("theTipoEsame", theTipoEsame);
                    session.setAttribute("theVisitaM", theVisitaM);
                    session.setAttribute("thePaz", thePaz);
                    session.setAttribute("theMed", theMed);
                    session.setAttribute("theVisitaS", theVisitaS);
                    session.setAttribute("isConclusa", false);
                    response.sendRedirect("/medicare/Servlet/caricaEsami");
                }
                break;
                
            case "/medicare/Servlet/addFoto":
                session = request.getSession();
                thePaz = (PazienteBean)session.getAttribute("pazienteBeanInstance");
                String routePath, filePath;
                boolean isMultiPart = ServletFileUpload.isMultipartContent(request);
                File file;
                if(!isMultiPart) response.sendRedirect("/medicare/pazienteData.jsp?error=10");
                DiskFileItemFactory factory = new DiskFileItemFactory();
                //factory.setSizeThreshold(boooooooooo);
                routePath = Servlet.class.getClassLoader().getResource(File.separator).getPath();
                filePath = routePath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "src" + File.separator + "main" + File.separator + "webapp" + File.separator + "fotoUtenti" + File.separator;
                factory.setRepository(new File(filePath));
                //create a new file upload handle
                ServletFileUpload upload = new ServletFileUpload(factory);
                //upload.setFileSizeMax(boooooo);
                
                String nomeFile = (fotoDao.getMaxFotoId()+1) + ".png";
                
                try {
                    java.util.List fileItems = upload.parseRequest(request);
                    Iterator i = fileItems.iterator();
                    i.hasNext();
                    FileItem fi = (FileItem)i.next();
                    
                    if(!fi.isFormField()) {
                        file = new File(filePath + nomeFile);
                        fi.write(file);
                        fotoDao.insert(thePaz.getCf(), nomeFile);
                        Thread.sleep(2000);
                    }
                    response.sendRedirect("/medicare/Servlet/pazienteData");
                } catch(Exception e) {
                    System.out.println(""+e);
                    response.sendRedirect("/medicare/pazienteData.jsp?error=11");
                }
                break;
                
            case "/medicare/Servlet/loginSSP":
                session = request.getSession();
                String provincia = request.getParameter("nomeProvincia");
                String pwd = request.getParameter("password");
                //valido l'SSP
                ProvinciaBean provinciaB = provinciaDao.getProvinciaByNome(provincia);
                SSPBean ssp = new SSPBean();
                ssp.setProvincia(provinciaB.getAbbr());
                ssp.setPassword(pwd);
                valid = SSPDao.validate(ssp);
                
                if(!valid) {
                    response.sendRedirect("/medicare/loginSSP.jsp?error=1");
                } else {
                    //info sul SSP
                    session.setAttribute("SSPInstance", ssp);
                    ArrayList<CittaBean> city = cittaDao.getCityForProvincia(provinciaB.getAbbr());
                    session.setAttribute("provincia", provinciaB);
                    session.setAttribute("city", city);
                    //utenti e medici registrati
                    ArrayList<PazienteBean> listIscritti = SSPDao.getPazientiIscritti(provinciaB.getAbbr());
                    ArrayList<PazienteBean> listMedici = SSPDao.getMediciIscritti(provinciaB.getAbbr());
                    ArrayList<PazienteBean> listSpecialisti = SSPDao.getSpecialistiIscritti(provinciaB.getAbbr());
                    session.setAttribute("listIscritti", listIscritti);
                    session.setAttribute("listMedici", listMedici);
                    session.setAttribute("listSpecialisti", listSpecialisti);
                    //hidden e reindirizzo
                    session.setAttribute("isAbilitato", false);
                    response.sendRedirect("/medicare/sspPage.jsp");
                }
                break;
                
            case "/medicare/Servlet/getInfoByDay":
                session = request.getSession();
                String dataScelta = request.getParameter("dataScelta");
                ssp = (SSPBean)session.getAttribute("SSPInstance");
                String nome;
                
                try {
                    Date dataBuona = VisitaMedicaDAO.formatDate(dataScelta);
                    session.setAttribute("isAbilitato", true);
                    session.setAttribute("dataScelta", dataBuona);
                    //lista visite oggi con med in questa provincia
                    ArrayList<VisitaMedicaBean> visiteServizio;
                    ArrayList<String> nomiPazS = new ArrayList<>();
                    ArrayList<String> nomiMedS = new ArrayList<>();
                    ArrayList<MedicinalePrescrittoBean> medicinaliS = new ArrayList<>();
                    ArrayList<String> nomiMedicinaliS = new ArrayList<>();
                    visiteServizio = SSPDao.getVisiteInDataWithMedicinale(ssp.getProvincia(), dataBuona.toString());
                    PrescrizioneBean prescS;
                    
                    for(int i = 0; i < visiteServizio.size(); i++) {
                        //carico i medicinali
                        prescS = prescrizioneDao.getPrescrizioneByVisita(visiteServizio.get(i).getId());
                        ArrayList<MedicinalePrescrittoBean> medicinaliTemp = medicinalePrescrittoDao.getMedicinaliForPrescrizione(Integer.parseInt(prescS.getId()));
                        
                        for(int k = 0; k < medicinaliTemp.size(); k++) {
                            medicinaliS.add(medicinaliTemp.get(k));
                            nomiMedicinaliS.add(medicinaleDao.getNomeMedicinale(medicinaliTemp.get(k).getMedicinale()));
                            nome = pazienteDao.getNomePaziente(visiteServizio.get(i).getMedicoBase());
                            nomiMedS.add(nome);
                            nome = pazienteDao.getNomePaziente(visiteServizio.get(i).getPaziente());
                            nomiPazS.add(nome);
                        }
                        
                    }
                    session.setAttribute("medicinaliS", medicinaliS);
                    session.setAttribute("nomiMedicinaliS", nomiMedicinaliS);
                    session.setAttribute("nomiPazS", nomiPazS);
                    session.setAttribute("nomiMedS", nomiMedS);
                    //lista esami oggi con spec in questa provincia
                    ArrayList<EsamePrescrittoBean> esamiPrescrittiS;
                    ArrayList<String> nomiEsamiS = new ArrayList<>();
                    ArrayList<VisitaSpecialisticaBean> visiteSpecS = new ArrayList<>();
                    ArrayList<String> nomiPazS2 = new ArrayList<>();
                    ArrayList<String> nomiSPecS = new ArrayList<>();
                    esamiPrescrittiS = SSPDao.getEsamiInData(ssp.getProvincia(), dataBuona.toString());
                    
                    for(int i = 0; i < esamiPrescrittiS.size(); i++) {
                        visiteSpecS.add(i, visitaSpecialisticaDao.getVisitaSByEsameP(Integer.parseInt(esamiPrescrittiS.get(i).getId())));
                        nome = esameDao.getEsameNome(Integer.parseInt(esamiPrescrittiS.get(i).getEsame()));
                        nomiEsamiS.add(i, nome);
                        nome = pazienteDao.getNomePaziente(visiteSpecS.get(i).getMedicoSpecialista());
                        nomiSPecS.add(i, nome);
                        nome = pazienteDao.getNomeUtenteByPresc(esamiPrescrittiS.get(i).getPrescrizione());
                        nomiPazS2.add(i, nome);
                    }
                    session.setAttribute("esamiPrescrittiS", esamiPrescrittiS);
                    session.setAttribute("nomiEsamiS", nomiEsamiS);
                    session.setAttribute("visiteSpecS", visiteSpecS);
                    session.setAttribute("nomiPazS2", nomiPazS2);
                    session.setAttribute("nomiSPecS", nomiSPecS);
                    response.sendRedirect("/medicare/sspPage.jsp");
                } catch (ParseException ex) {
                    Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
        }
    }
    
    
    /**
     * Handles the receive of personal data of the pazient
     *
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private boolean doPazienteData(PazienteBean pazienteBeanInstance) throws ServletException, IOException {
        boolean valid = false;
        
        if (pazienteDao.getPazienteByCf(pazienteBeanInstance)) { 
            valid = true;
        }
        return valid;
    }
    
    
    private boolean insertVisitaMedica(String data, String paziente, String medicoBase, VisitaMedicaBean visitaMedicaBean) throws ParseException {
        boolean valid = false;
        visitaMedicaBean.setPaziente(paziente);
        visitaMedicaBean.setMedicoBase(medicoBase);
        visitaMedicaBean.setDataIscrizione(data);
        visitaMedicaBean.setTerminata("false");
        
        try {
            if(visitaMedicaDao.insert(visitaMedicaBean)) {
                valid = true;
            }
        } catch (ClassNotFoundException e) {
            return false;
        }
        
        return valid;
    }
    
    
    /**
     * Handles the login
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private boolean doLogin(String username, String password, LoginBean loginBean) throws ServletException, IOException {
        boolean valid = false;
        loginBean.setUsername(username);
        loginBean.setPassword(password);
        
        try {
            if (loginDao.validate(loginBean)) {
                valid = true;
            }
        } catch (ClassNotFoundException e) {
            return false;
        }
        
        return valid;
    }
    
    
    private boolean doMedicoData(PazienteBean mbean) {
        boolean status = false;

        if (pazienteDao.getPazienteByCf(mbean)) {
            status = true;
        }
        return status;
    }
    
    
    private boolean controlloDuplicatoEsame(String date, int prescrizione) {
        boolean status;
        status = esamePrescrittoDao.checkDateDuplicate(date, prescrizione);
        return status;
    }
    
    
    private String hashPassword(String password, String username) {
        String generatedPassword;
        String salt = loginDao.getSaltForUser(username);
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            String saltedPassword = password + salt;
            md.update(saltedPassword.getBytes());
            //Get the hash's bytes 
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            
            for(int i=0; i< bytes.length ;i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        } 
        catch (NoSuchAlgorithmException e) {
            System.out.println(e);
            return null;
        }
        return generatedPassword;
    }
    
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "The servlet for doing nothing works!";
    }
}
