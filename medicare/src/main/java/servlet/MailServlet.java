/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;  
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.*;  
import javax.mail.internet.*;  
import javax.servlet.http.HttpSession;
import persistence.beans.NotificheBean;
import persistence.beans.PazienteBean;
import persistence.dao.NotificheDAO;
import persistence.dao.PazienteDAO;


/**
 *
 * @author utente
 */
public class MailServlet extends HttpServlet {
    ConnectionManager connectionManager;
    Session mailSession;
    PazienteDAO pazienteDao;
    NotificheDAO notificheDao;
    
    
    @Override
    public void init() {
        connectionManager = new ConnectionManager();
        pazienteDao = new PazienteDAO(connectionManager.getConnection());
        notificheDao = new NotificheDAO(connectionManager.getConnection());
        setMailServerProperties();
    }

    
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session;
        PazienteBean paziente;
        String mailTo, object, date, text, title, pageTo, pazienteCf;
        NotificheBean notifica;
        
        switch(request.getRequestURI()) {
            case "/medicare/MailServlet/sendMailPrenotazioneVisita":
                session = request.getSession();
                paziente = (PazienteBean) session.getAttribute("pazienteBeanInstance");
                mailTo = pazienteDao.getMailFromCf(paziente.getCf());
                object = "Prenotazione visita medica";
                date = (String) session.getAttribute("MdataVisita");
                text = "La sua prenotazione per la visita medica in data " + date + " è stata confermata.";
                title = "Prenotazione visita medica";
                pageTo = "/medicare/" + (String) session.getAttribute("MpageTo");
                
                try {
                    MimeMessage mex = creteEmailMessage(mailTo, object, text, title);
                    sendEmail(mex);
                } catch (MessagingException ex) {
                    Logger.getLogger(MailServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
                //aggiungo la notifica
                notifica = new NotificheBean();
                notifica.setMessage(text);
                notifica.setPaziente(paziente.getCf());
                notificheDao.insert(notifica);
                response.sendRedirect(pageTo);
                break;
                
            case "/medicare/MailServlet/sendMailCambioMedBase":
                session = request.getSession();
                paziente = (PazienteBean) session.getAttribute("pazienteBeanInstance");
                mailTo = pazienteDao.getMailFromCf(paziente.getCf());
                object = "Cambio medico di base";
                text = "Il tuo medico di base è stato cambiato con successo";
                title = "Cambio medico di base";
                pageTo = "/medicare/" + (String) session.getAttribute("MpageTo");
                
                try {
                    MimeMessage mex = creteEmailMessage(mailTo, object, text, title);
                    sendEmail(mex);
                } catch (MessagingException ex) {
                    Logger.getLogger(MailServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
                response.sendRedirect(pageTo);
                break;
                
            case "/medicare/MailServlet/sendMailCambioPassword":
                session = request.getSession();
                paziente = (PazienteBean) session.getAttribute("pazienteBeanInstance");
                mailTo = pazienteDao.getMailFromCf(paziente.getCf());
                object = "Cambio password";
                text = "La tua password è stata cambiata con successo";
                title = "Cambio password";
                pageTo = "/medicare/" + (String) session.getAttribute("MpageTo");
                
                try {
                    MimeMessage mex = creteEmailMessage(mailTo, object, text, title);
                    sendEmail(mex);
                } catch (MessagingException ex) {
                    Logger.getLogger(MailServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
                response.sendRedirect(pageTo);
                break;
            
            case "/medicare/MailServlet/sendMailForgotPassword":
                session = request.getSession();
                String cf = (String)session.getAttribute("cf");
                String newPsw = (String) session.getAttribute("newPsw");
                mailTo = pazienteDao.getMailFromCf(cf);
                object = "Assegnazione nuova password";
                text = "La tua password è stata cambiata in: " + newPsw + ". Accedi e vai su profilo per scegliere una nuova password.";
                title = "Password dimenticata";
                pageTo = "/medicare/" + (String) session.getAttribute("MpageTo");
                
                try {
                    MimeMessage mex = creteEmailMessage(mailTo, object, text, title);
                    sendEmail(mex);
                } catch (MessagingException ex) {
                    Logger.getLogger(MailServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
                response.sendRedirect(pageTo);
                break;
                
            case "/medicare/MailServlet/sendMailVisitaEseguita":
                session = request.getSession();
                pazienteCf = (String) session.getAttribute("MPaziente");
                mailTo = pazienteDao.getMailFromCf(pazienteCf);
                object = "Visita Medica eseguita";
                date = (String) session.getAttribute("MdataVisita");
                text = "La tua visita medica in data " + date + " è stata compilata.\n";
                title = "Visita Medica eseguita";
                pageTo = "/medicare/" + (String) session.getAttribute("MpageTo");
                boolean isMedicinale, isEsame;
                isMedicinale = (boolean) session.getAttribute("MisMedicinale");
                isEsame = (boolean) session.getAttribute("MisEsame");
                
                if(isMedicinale) {
                    text += "Hai nuove ricette da visualizzare.\n";
                }
                if(isEsame) {
                    text += "Hai nuovi esami prenotati dal tuo medico di base.\n";
                }
                
                try {
                    MimeMessage mex = creteEmailMessage(mailTo, object, text, title);
                    sendEmail(mex);
                } catch (MessagingException ex) {
                    Logger.getLogger(MailServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
                //aggiungo la notifica
                notifica = new NotificheBean();
                notifica.setMessage(text);
                notifica.setPaziente(pazienteCf);
                notificheDao.insert(notifica);
                response.sendRedirect(pageTo);
                break;
                
            case "/medicare/MailServlet/sendMailEsameEseguito":
                session = request.getSession();
                pazienteCf = (String) session.getAttribute("MPaziente");
                mailTo = pazienteDao.getMailFromCf(pazienteCf);
                object = "Esame eseguito";
                date = (String) session.getAttribute("MdataEsame");
                String tipoEsame = (String) session.getAttribute("MtipoEsame");
                text = "I tuo esame " + tipoEsame + " in data " + date + " è stato compilato. Hai una nuova anamnesi da visualizare.";
                title = "Esame eseguito";
                pageTo = "/medicare/" + (String) session.getAttribute("MpageTo");
                
                try {
                    MimeMessage mex = creteEmailMessage(mailTo, object, text, title);
                    sendEmail(mex);
                } catch (MessagingException ex) {
                    Logger.getLogger(MailServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
                //aggiungo la notifica
                notifica = new NotificheBean();
                notifica.setMessage(text);
                notifica.setPaziente(pazienteCf);
                notificheDao.insert(notifica);
                //aggiungo la notifica per il ticket
                notifica = new NotificheBean();
                notifica.setMessage("Hai un nuovo ticket da pagare.");
                notifica.setPaziente(pazienteCf);
                notificheDao.insert(notifica);
                response.sendRedirect(pageTo);
                break;
        }
    }
    

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
    }
    
    
    private void setMailServerProperties() {
        Properties emailProperties = System.getProperties();
        emailProperties.put("mail.smtp.port", "587");
        emailProperties.put("mail.smtp.auth", "true");
        emailProperties.put("mail.smtp.starttls.enable", "true");
        mailSession = Session.getDefaultInstance(emailProperties, null);
    }
    
    
    private MimeMessage creteEmailMessage(String mailTo, String object, String text, String title) throws AddressException, MessagingException {
        String emailBody = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                            "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
                            "	<head>\n" +
                            "		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" +
                            "		<title>MediCare</title>\n" +
                            "		<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n" +
                            "		<link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i\">\n" +
                            "	</head>\n" +
                            "	\n" +
                            "	<body style=\"margin: 0; padding: 0;\">\n" +
                            "		<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" +
                            "			<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"35%\" style=\"border-collapse: collapse; background-color: #f8f9fa; border-radius:15px;\">\n" +
                            "				<tr>\n" +
                            "					<td style=\"padding: 3% 0 3% 0;\">\n" +
                            "						<div align=\"center\">\n" +
                            "							<h1 style=\"text-shadow: 2px 2px 2px rgb(25, 255, 180);\">\n" +
                            "								<img align=\"center\" width=\"75\" height=\"60\" src=\"C:\\Users\\utente\\Desktop\\progettoIntroWeb\\medicare\\src\\main\\webapp\\assets\\logo\\logoMail.png\"/> <font face=\"Montserrat\" color=\"#007bff\" style=\"font-size: 34px;\">MediCare</font>\n" +
                            "							</h1>\n" +
                            "							\n" +
                            "						</div>\n" +
                            "					</td>					\n" +
                            "				</tr>\n" +
                            "				<tr>					\n" +
                            "					<td style=\"padding: 15% 5% 15% 5%;\">						\n" +
                            "						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;\">\n" +
                            "						\n" +
                            "							<tr>\n" +
                            "								<td align=\"center\" style=\"padding: 0% 0 5% 0;\">\n" +
                            "									<hr color=\"#007bff\" width=\"75%\">\n" +
                            "									<font face=\"Montserrat\">" + title + "</font>\n" +
                            "								</td>\n" +
                            "							</tr>\n" +
                            "							<tr>\n" +
                            "								<td align=\"center\" style=\"padding: 10% 0 10% 0;\">\n" +
                            "									<font face=\"Montserrat\">" + text + "</font>\n" +
                            "								</td>\n" +
                            "							</tr>\n" +
                            "							<tr>\n" +
                            "								<td align=\"center\" style=\"padding: 5% 0 0% 0;\">\n" +
                            "									<font face=\"Montserrat\">Elementi vari</font> \n" +
                            "									<hr color=\"#007bff\" width=\"75%\">\n" +
                            "								</td>\n" +
                            "							</tr>\n" +
                            "\n" +
                            "						</table>\n" +
                            "					</td>\n" +
                            "				</tr>\n" +
                            "				<tr>\n" +
                            "					<td align=\"center\" style=\"padding: 15% 5% 5% 5%;\">\n" +
                            "						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;\">\n" +
                            "							<tr>\n" +
                            "								<td align=\"center\">\n" +
                            "									<font face=\"Montserrat\">Say hello to the footer</font>\n" +
                            "								</td>\n" +
                            "							</tr>\n" +
                            "						</table>\n" +
                            "					</td>\n" +
                            "				</tr>\n" +
                            "			</table>\n" +
                            "		</table>\n" +
                            "	</body>\n" +
                            "\n" +
                            "</html>";
        MimeMessage emailMessage = new MimeMessage(mailSession);
        /**
         * Set the mail recipients
         * */
        emailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(mailTo));
        emailMessage.setSubject(object);
        emailMessage.setContent(emailBody, "text/html");
        return emailMessage;
    }
    
    
    private void sendEmail(MimeMessage emailMessage) throws AddressException, MessagingException {
        /**
         * Sender's credentials
         * */
        String fromUser = "medicareservicesipw19@gmail.com";
        String fromUserEmailPassword = "Ciao123!";

        String emailHost = "smtp.gmail.com";
        Transport transport = mailSession.getTransport("smtp");
        transport.connect(emailHost, fromUser, fromUserEmailPassword);
        /**
         * Send the mail
         * */
        transport.sendMessage(emailMessage, emailMessage.getAllRecipients());
        transport.close();
        System.out.println("Email sent successfully.");
    }

    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Mail Servlet running";
    }

}
