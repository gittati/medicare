<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>MediCare</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/fonts/simple-line-icons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/smoothproducts.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">


        <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/smoothproducts.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/theme.js"></script>
        <script src="https://use.fontawesome.com/50f3e3804d.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    </head>

    <body>
        <nav class="navbar navbar-light bg-white fixed-top clean-navbar">
            <div class="container-fluid">
                <a id="logoIndex" class="navbar-brand logo" href="#">
                    <h3 class="font-weight-bolder text-primary" style="text-shadow: 2px 2px 2px rgb(25, 255, 180);"> 
                        <img class="" width="40" height="30" src="${pageContext.request.contextPath}/assets/logo/logo.png"/>MediCare
                    </h3>
                </a>
            </div>
        </nav>

        <main class="page landing-page">
            <section class="clean-block clean-hero" style="color: rgba( 255 , 255 , 255 , 0.000 ); background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${pageContext.request.contextPath}/assets/img/bg_3.jpg); ">
                <div class="text text-center">
                    <h2 style='color: #ffffff;'>Benvenuto su MediCare</h2>
                    <div class="container-fluid m-4 p-3 border border-light rounded-lg mx-auto" style="text-align: left">
                        <ul class="list-group list-group-flush">
                            <tl class='p-2 text-light'><i class='fa fa-angle-right fa-lg' style='color:#007bff'></i> Scegli il tuo medico di base </tl>
                            <tl class='p-2 text-light'><i class='fa fa-angle-right fa-lg' style='color:#007bff'></i> Tieni traccia delle tue visite e dei tuoi esami </tl>
                            <tl class='p-2 text-light'><i class='fa fa-angle-right fa-lg' style='color:#007bff'></i> Ricevi notifiche e email in tempo reale </tl>
                            <tl class='p-2 text-light'><i class='fa fa-angle-right fa-lg' style='color:#007bff'></i> Prenota le tue visite mediche online </tl>
                        </ul>

                    </div>
                    <a class="btn btn-outline-light btn-lg" type="button" href="${pageContext.request.contextPath}/login.jsp">
                        Login 
                    </a>
                    <a class="btn btn-outline-light btn-lg" type="button" href="${pageContext.request.contextPath}/loginSSP.jsp">
                        SSP
                    </a>                        
                </div>
            </section>
        </main>

        <hr/>
        <footer class="page-footer light">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h5>About Us</h5>
                        <ul>
                            <li><a href="#">Gruppo gitLab MediCare</a></li>
                            <li><a href="#">UniTN project</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Capacità</h5>
                        <ul>
                            <li><a href="#">150+ esami prenotabili</a></li>
                            <li><a href="#">800+ medicinali prescrivibili</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Caratteristiche</h5>
                        <ul>
                            <li><a href="#">Veloce</a></li>
                            <li><a href="#">Affidabile</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Sicurezze</h5>
                        <ul>
                            <li><a href="#">Solo medici preparati</a></li>
                            <li><a href="#">Solo gli specialisti migliori</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <p>© 2020 Tutti i diritti sono riservati al gruppo MediCare e a UNITN</p>
            </div>
        </footer>
    </body>
</html>