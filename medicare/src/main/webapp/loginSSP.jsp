<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<title>Login page</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/fonts/simple-line-icons.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/smoothproducts.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

		
	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/smoothproducts.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/theme.js"></script>
	<script src="https://use.fontawesome.com/50f3e3804d.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    </head>

    <body>
        <%
	    Cookie[] cookies=request.getCookies();
	    String username = "", password = "";
            
	    if (cookies != null) {
	        for (Cookie cookie : cookies) {
                    if(cookie.getName().equals("cookuser")) {
                        username = cookie.getValue();
                    } else if(cookie.getName().equals("cookpass")){
                        password = cookie.getValue();
	           }
	        }
	    }
	%>
        
	<main class="page login-page">
            <section class="clean-block clean-form dark">            
		<div class="container-fluid">
                    <a class="btn btn-outline-dark" href="${pageContext.request.contextPath}/index.jsp" style="position: absolute; top: 2%; right: 2%">
                        <i class="fa fa-home fa-2x" style="margin: auto"></i>
                    </a>
                    <div class="block-heading">
			<h2 class="text-primary">Login</h2>
                    </div>
                    
			<% 
                            String error = String.valueOf(request.getParameter("error"));
                            String isOut = String.valueOf(request.getParameter("out"));
                            String phrase = "";
					
                            if(error.equals("1")) {
				phrase = "<div id='loginAlert' class='alert alert-danger text-center' style='margin: auto; width: 50%;'>Password errata!</div>";
                            } else if(isOut.equals("1")) {
                                phrase = "<div id='loginAlert' class='alert alert-success text-center' style='margin: auto; width: 50%;'>Logout avvenuto con successo!</div>";
                            } else {
				phrase = "<div id='loginAlert' class='alert alert-info text-center' style='margin: auto; width: 50%;'>Esegui il login per utilizzare il sistema.</div>";
                            }
                            out.println(phrase);
			%>
                    
					
                    <br>

                    <form method="post" action="/medicare/Servlet/loginSSP">
			<div class="form-group">
                            <label for="username">Provincia</label>
                            <select class="form-control" name="nomeProvincia">
                                <option>Agrigento</option>
<option>Alessandria	</option>
<option>Ancona	</option>
<option>Aosta	</option>
<option>L'Aquila	</option>
<option>Arezzo	</option>
<option>Ascoli-Piceno	</option>
<option>Asti	</option>
<option>Avellino	</option>
<option>Bari	</option>
<option>Barletta-Andria-Trani	</option>
<option>Belluno	</option>
<option>Benevento	</option>
<option>Bergamo	</option>
<option>Biella	</option>
<option>Bologna	</option>
<option>Bolzano	</option>
<option>Brescia	</option>
<option>Brindisi	</option>
<option>Cagliari	</option>
<option>Caltanissetta	</option>
<option>Campobasso	</option>
<option>Carbonia Iglesias	</option>
<option>Caserta	</option>
<option>Catania	</option>
<option>Catanzaro	</option>
<option>Chieti	</option>
<option>Como	</option>
<option>Cosenza	</option>
<option>Cremona	</option>
<option>Crotone	</option>
<option>Cuneo	</option>
<option>Enna	</option>
<option>Fermo	</option>
<option>Ferrara	</option>
<option>Firenze	</option>
<option>Foggia	</option>
<option>Forli-Cesena	</option>
<option>Frosinone	</option>
<option>Genova	</option>
<option>Gorizia	</option>
<option>Grosseto	</option>
<option>Imperia	</option>
<option>Isernia	</option>
<option>La-Spezia	</option>
<option>Latina	</option>
<option>Lecce	</option>
<option>Lecco	</option>
<option>Livorno	</option>
<option>Lodi	</option>
<option>Lucca	</option>
<option>Macerata	</option>
<option>Mantova	</option>
<option>Massa-Carrara	</option>
<option>Matera	</option>
<option>Medio Campidano	</option>
<option>Messina	</option>
<option>Milano	</option>
<option>Modena	</option>
<option>Monza-Brianza	</option>
<option>Napoli	</option>
<option>Novara	</option>
<option>Nuoro	</option>
<option>Ogliastra	</option>
<option>Olbia Tempio	</option>
<option>Oristano	</option>
<option>Padova	</option>
<option>Palermo	</option>
<option>Parma	</option>
<option>Pavia	</option>
<option>Perugia	</option>
<option>Pesaro-Urbino	</option>
<option>Pescara	</option>
<option>Piacenza	</option>
<option>Pisa	</option>
<option>Pistoia	</option>
<option>Pordenone	</option>
<option>Potenza	</option>
<option>Prato	</option>
<option>Ragusa	</option>
<option>Ravenna	</option>
<option>Reggio-</option>	</option>
<option>Reggio-Emilia	</option>
<option>Rieti	</option>
<option>Rimini	</option>
<option>Roma	</option>
<option>Rovigo	</option>
<option>Salerno	</option>
<option>Sassari	</option>
<option>Savona	</option>
<option>Siena	</option>
<option>Siracusa	</option>
<option>Sondrio	</option>
<option>Taranto	</option>
<option>Teramo	</option>
<option>Terni	</option>
<option>Torino	</option>
<option>Trapani	</option>
<option>Trento	</option>
<option>Treviso	</option>
<option>Trieste	</option>
<option>Udine	</option>
<option>Varese	</option>
<option>Venezia	</option>
<option>Verbania</option>
<option>Vercelli</option>
<option>Verona</option>
<option>Vibo-Valentia</option>
<option>Vicenza</option>
<option>Viterbo</option>

                            </select>
                        </div>
			<div class="form-group">
                            <label for="password">Password</label>
                            <input class="form-control" name="password" type="password" id="password" value="<%=password%>">
			</div>
			<div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember">
				<label class="form-check-label" for="checkbox">Remember me</label>
                            </div>
			</div>
			<button class="btn btn-primary btn-block" type="submit">Login</button>
                    </form>    
		</div>
            </section>
	</main>
		
	<hr/>
        <footer class="page-footer light">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h5>About Us</h5>
                        <ul>
                            <li><a href="#">Gruppo gitLab MediCare</a></li>
                            <li><a href="#">UniTN project</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Capacità</h5>
                        <ul>
                            <li><a href="#">150+ esami prenotabili</a></li>
                            <li><a href="#">800+ medicinali prescrivibili</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Caratteristiche</h5>
                        <ul>
                            <li><a href="#">Veloce</a></li>
                            <li><a href="#">Affidabile</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Sicurezze</h5>
                        <ul>
                            <li><a href="#">Solo medici preparati</a></li>
                            <li><a href="#">Solo gli specialisti migliori</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
		<p>© 2020 Tutti i diritti sono riservati al gruppo MediCare e a UNITN</p>
            </div>
	</footer>
    </body>
</html>