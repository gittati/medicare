<%@page import="servlet.UserType"%>
<%@page import="persistence.beans.FotoBean"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "persistence.beans.PazienteBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<% PazienteBean pb = (PazienteBean) session.getAttribute("pazienteBeanInstance");%>
<% PazienteBean mb = (PazienteBean) session.getAttribute("medicoDataInstance");%>
<% ArrayList<PazienteBean> listMedBase = (ArrayList<PazienteBean>) session.getAttribute("listMedBase");%>
<% ArrayList<FotoBean> fotoUtente = (ArrayList<FotoBean>) session.getAttribute("fotoUtente"); %>
<% FotoBean fotoMedBase = (FotoBean) session.getAttribute("fotoMedBase"); %>
<%
    UserType userType = (UserType) session.getAttribute("userType");
    int switchState = (int) session.getAttribute("switchState");
    FotoBean lastFoto = (FotoBean) session.getAttribute("lastFoto");
%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>MediCare</title>

        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/fonts/simple-line-icons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/smoothproducts.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">


        <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/smoothproducts.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/theme.js"></script>
        <script src="https://use.fontawesome.com/50f3e3804d.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

    </head>

    <body>    
        <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
            <div class="container-fluid">
                <a class="navbar-brand logo" href="#">
                    <h3 class="font-weight-bolder text-primary" style="text-shadow: 2px 2px 2px rgb(25, 255, 180);"> 
                        <img class="" width="40" height="30" src="${pageContext.request.contextPath}/assets/logo/logo.png"/>MediCare
                    </h3>
                </a>
                <button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav ml-auto">
                        <c:choose>
                            <c:when test="${userType == UserType.PAZIENTE}">
                                <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/home.jsp">Home</a></li>
                                <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/visitaMedicaInsData.jsp">Appuntamenti</a></li>
                                <li class="nav-item" role="presentation"><a class ="nav-link" href="/medicare/Servlet/visualizzaPazienteData">Servizi</a></li>
                                <li class="nav-item" role="presentation"><a class="nav-link" href="/medicare/Servlet/visualizzaTicket">I miei ticket</a></li>
                                <li class="nav-item dropdown" role="presentation">
                                    <a class="nav-link dropdown-toggle  active" id="dropdownmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                        <i class="fa fa-user fa-1x"></i> Profilo 
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="dropdownmenu">
                                        <a class="dropdown-item" href="/medicare/Servlet/pazienteData">Il mio account</a>
                                        <a class="dropdown-item" href="/medicare/Servlet/logout">Log out</a>
                                    </div>
                                </li>
                            </c:when>
                            <c:when test="${userType == BASE}">
                                <c:choose>
                                    <c:when test="${switchState == 0}">
                                        <li class="nav-item" role="presentation"><a class="nav-link" href="/medicare/Servlet/parcoPazienti">Home</a></li>
                                        <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/pazientiMInfo.jsp">I miei Pazienti</a></li>
                                        <li class="nav-item" role="presentation"><a class="nav-link" href="/medicare/Servlet/caricaVisite">Visite mediche</a></li>
                                        <li class="nav-item dropdown" role="presentation">
                                            <a class="nav-link dropdown-toggle active" id="dropdownmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                                <i class="fa fa-user fa-1x"></i> Profilo 
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="dropdownmenu">
                                                <a class="dropdown-item" href="/medicare/Servlet/pazienteData">Il mio account</a>
                                                <a class="dropdown-item" href="/medicare/Servlet/logout">Log out</a>
                                            </div>
                                        </li>

                                        <c:choose>
                                            <c:when test="${switchState == 0}">
                                                <li class="nav-item" role="presentation">
                                                    <form action="/medicare/Servlet/switchState" method="get">
                                                        <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Paziente</a>
                                                    </form>
                                                </li>
                                            </c:when>
                                            <c:otherwise>
                                                <li class="nav-item" role="presentation">
                                                    <form action="/medicare/Servlet/switchState" method="get">
                                                        <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Medico</a>
                                                    </form>
                                                </li>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/home.jsp">Home</a></li>
                                        <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/visitaMedicaInsData.jsp">Appuntamenti</a></li>
                                        <li class="nav-item" role="presentation"><a class ="nav-link" href="/medicare/Servlet/visualizzaPazienteData">Servizi</a></li>
                                        <li class="nav-item" role="presentation"><a class="nav-link" href="/medicare/Servlet/visualizzaTicket">I miei ticket</a></li>
                                        <li class="nav-item dropdown" role="presentation">
                                            <a class="nav-link dropdown-toggle active" id="dropdownmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                                <i class="fa fa-user fa-1x"></i> Profilo 
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="dropdownmenu">
                                                <a class="dropdown-item" href="/medicare/Servlet/pazienteData">Il mio account</a>
                                                <a class="dropdown-item" href="/medicare/Servlet/logout">Log out</a>
                                            </div>
                                        </li>
                                        <c:choose>
                                            <c:when test="${userType != PAZIENTE}">
                                                <c:choose>
                                                    <c:when test="${switchState == 0}">
                                                        <li class="nav-item" role="presentation">
                                                            <form action="/medicare/Servlet/switchState" method="get">
                                                                <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Paziente</a>
                                                            </form>
                                                        </li>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <li class="nav-item" role="presentation">

                                                            <form action="/medicare/Servlet/switchState" method="get">
                                                                <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Medico</a>
                                                            </form>
                                                        </li>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:when>
                                        </c:choose>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <c:choose>
                                    <c:when test="${switchState == 0}">
                                        <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/homeSpecialist.jsp">Home</a></li>
                                        <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/pazientiSInfo.jsp">I miei Pazienti</a></li>
                                        <li class="nav-item" role="presentation"><a class="nav-link" href="/medicare/Servlet/caricaEsami">Esami</a></li>
                                        <li class="nav-item dropdown" role="presentation">
                                            <a class="nav-link dropdown-toggle active" id="dropdownmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                                <i class="fa fa-user fa-1x"></i> Profilo 
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="dropdownmenu">
                                                <a class="dropdown-item" href="/medicare/Servlet/pazienteData">Il mio account</a>
                                                <a class="dropdown-item" href="/medicare/Servlet/logout">Log out</a>
                                            </div>
                                        </li>
                                        <c:choose>
                                            <c:when test="${switchState == 0}">
                                                <li class="nav-item" role="presentation">
                                                    <form action="/medicare/Servlet/switchState" method="get">
                                                        <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Paziente</a>
                                                    </form>
                                                </li>
                                            </c:when>
                                            <c:otherwise>
                                                <li class="nav-item" role="presentation">
                                                    <form action="/medicare/Servlet/switchState" method="get">
                                                        <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Specialista</a>
                                                    </form>
                                                </li>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/home.jsp">Home</a></li>

                                        <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/visitaMedicaInsData.jsp">Appuntamenti</a></li>
                                        <li class="nav-item" role="presentation"><a class ="nav-link" href="/medicare/Servlet/visualizzaPazienteData">Servizi</a></li>
                                        <li class="nav-item" role="presentation"><a class="nav-link" href="/medicare/Servlet/visualizzaTicket">I miei ticket</a></li>
                                        <li class="nav-item dropdown" role="presentation">
                                            <a class="nav-link dropdown-toggle active" id="dropdownmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                                <i class="fa fa-user fa-1x"></i> Profilo 
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="dropdownmenu">
                                                <a class="dropdown-item" href="/medicare/Servlet/pazienteData">Il mio account</a>
                                                <a class="dropdown-item" href="/medicare/Servlet/logout">Log out</a>
                                            </div>
                                        </li>
                                        <c:choose>
                                            <c:when test="${userType != PAZIENTE}">
                                                <c:choose>
                                                    <c:when test="${switchState == 0}">
                                                        <li class="nav-item" role="presentation">
                                                            <form action="/medicare/Servlet/switchState" method="get">
                                                                <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Paziente</a>
                                                            </form>
                                                        </li>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <li class="nav-item" role="presentation">
                                                            <form action="/medicare/Servlet/switchState" method="get">
                                                                <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Medico</a>
                                                            </form>
                                                        </li>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:when>
                                        </c:choose>
                                    </c:otherwise>
                                </c:choose>
                            </c:otherwise>
                        </c:choose>            
                    </ul>
                </div>
            </div>
        </nav>

        <main class="page landing-page">            
            <section class="clean-block clean-info">                
                <div class="container-fluid">
                    <div id="DataPazienteDiv">
                        <%
                            String error = String.valueOf(request.getParameter("error"));
                            String phrase = "";

                            if (error.equals("1")) {
                                phrase = "<p>Errore nel caricamento!</p>";
                                out.println(phrase);
                            } else if (error.equals("2")) {
                                phrase = "<p>Impossibile cambiare medico di base!</p>";
                                out.println(phrase);
                            }
                        %>
                        <div class="block-heading text-center">
                            <h2 class="text-info">I miei dati</h2>
                        </div>  
                        <div class="row"> 
                            <div class="col-sm-4">
                                <c:choose>
                                    <c:when test="${lastFoto != null}">
                                        <img class="img-thumbnail rounded-circle mx-auto d-block w-auto" src="${pageContext.request.contextPath}/fotoUtenti/${lastFoto.getFotografia()}"/>
                                    </c:when>
                                    <c:otherwise>
                                        <img class="img-thumbnail rounded-circle mx-auto d-block w-auto" src="${pageContext.request.contextPath}/assets/img/avatars/blankProfile.png"/>
                                    </c:otherwise>       
                                </c:choose>                               
                            </div>
                            <div class="col-sm-8">
                                <ul class="list-group list-group-flush text-center">
                                    <li class="list-group-item"><%="CF:" + pb.getCf()%></li>
                                    <li class="list-group-item"><%="Nome: " + pb.getNome()%></li>
                                    <li class="list-group-item"><%="Cognome: " + pb.getCognome()%></li>
                                    <li class="list-group-item"><%="Sesso: " + pb.getSesso()%></li>
                                    <li class="list-group-item"><%="Data nascita: " + pb.getDataNascita()%></li>
                                    <li class="list-group-item"><%="Luogo nascita: " + pb.getLuogoNascita()%></li>
                                    <li class="list-group-item"><%="Mail: " + pb.getEmail()%></li>
                                    <li class="list-group-item"><%="Residenza: " + pb.getResidenza()%></li>
                                    <li class="list-group-item"><a id="buttonToggle" class="btn btn-primary" href="${pageContext.request.contextPath}/changePassword.jsp">Cambia password</a></li>
                                </ul>                                      
                            </div>
                        </div>
                    </div>                
                </div>
            </section>

            <section class="clean-block clean-info" id="dataMedicoSection">
                <div class="container-fluid">   
                    <div>
                        <div class="block-heading text-center">
                            <h2 class="text-light">Il mio medico di base</h2>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <c:choose>
                                    <c:when test="${fotoMedBase != null}">
                                        <img class="img-thumbnail rounded-circle mx-auto d-block w-auto" src="${pageContext.request.contextPath}/fotoUtenti/${fotoMedBase.getFotografia()}" />
                                    </c:when>
                                    <c:otherwise>
                                        <img class="img-thumbnail rounded-circle mx-auto d-block w-auto" src="${pageContext.request.contextPath}/assets/img/avatars/blankProfile.png"/>
                                    </c:otherwise>       
                                </c:choose>     
                            </div>
                            <div class="col-sm-8" >
                                <ul class="list-group list-group-flush text-center">
                                    <li class="list-group-item" id="listaDatiMedBase"><%="CF:" + mb.getCf()%></li>
                                    <li class="list-group-item" id="listaDatiMedBase"><%="Nome: " + mb.getNome()%></li>
                                    <li class="list-group-item" id="listaDatiMedBase"><%="Cognome: " + mb.getCognome()%></li>
                                    <li class="list-group-item" id="listaDatiMedBase"><%="Sesso: " + mb.getSesso()%></li>
                                    <li class="list-group-item" id="listaDatiMedBase"><%="Data nascita: " + mb.getDataNascita()%></li>
                                    <li class="list-group-item" id="listaDatiMedBase"><%="Luogo nascita: " + mb.getLuogoNascita()%></li>
                                    <li class="list-group-item" id="listaDatiMedBase"><%="Mail: " + mb.getEmail()%></li>
                                    <li class="list-group-item" id="listaDatiMedBase"><%="Residenza: " + mb.getResidenza()%></li>
                                    <li class="list-group-item" id="listaDatiMedBase">
                                        <button id="buttonToggle" type="button" class="btn btn-light" data-toggle="modal" data-target="#myModal">Cambia medico di base</button>
                                    </li>
                                </ul>                                      
                            </div>
                        </div>
                    </div>
                </div>
                <!-- The Modal -->
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable">
                        <div class="modal-content">

                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h2 class="modal-title text-info">Seleziona il nuovo medico di base</h2>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="container">
                                    <div class="table-responsive">
                                        <!--Table-->

                                        <table id="selezionaMedicoBase" class="table table-bordered table-hover"  cellspacing="0" width="100%">
                                            <!--Table head-->
                                            <thead>
                                                <tr>
                                                    <th class="th">Cf</th>
                                                    <th class="th">Nome</th>
                                                    <th class="th">Cognome</th>
                                                    <th class="th">Email</th>
                                                    <th class="th">Residenza</th>
                                                    <th class="th"></th>
                                                </tr>
                                            </thead>
                                            <!--Table head-->
                                            <!--Table body-->
                                            <tbody>
                                                <c:forEach var="med" items="${listMedBase}">
                                                    <tr>
                                                        <td><c:out value="${med.getCf()}" /></td>
                                                        <td><c:out value="${med.getNome()}" /></td>
                                                        <td><c:out value="${med.getCognome()}" /></td>
                                                        <td><c:out value="${med.getEmail()}" /></td>
                                                        <td><c:out value="${med.getResidenza()}" /></td>
                                                        <td>
                                                            <form action="/medicare/Servlet/setMedBase" method="post">
                                                                <a id="selezionaMB" class="btn btn-info" href="#" onclick="parentNode.submit();">Seleziona</a>
                                                                <input type="hidden" name="cf" value="${med.getCf()}" />
                                                            </form>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                            <!--Table body -->
                                        </table>
                                    </div>
                                </div>
                                <!--Table-->
                            </div>

                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button id="annullaSelMB" type="button" class="btn btn-danger btn-block" data-dismiss="modal">Annulla</button>
                            </div>
                        </div>
                    </div>
                </div>                                       
            </section>

            <section class="clean-block slider dark">
                <div class="container">
                    <div class="block-heading">
                        <h2 class="text-info">Le mie foto</h2>
                    </div>


                    <c:choose>
                        <c:when test="${fotoUtente.size() > 0}">
                            <div class="carousel slide" data-ride="carousel" id="carousel-1">
                                <div class="carousel-inner text-center" role="listbox">
                                    <div class="carousel-item active">
                                        <c:forEach var="counter" begin="0" end="${fotoUtente.size()-1}">
                                            <img class="w-50 center-block img-fluid rounded-circle" src="${pageContext.request.contextPath}/fotoUtenti/${fotoUtente.get(counter).getFotografia()}" alt="Slide Image">
                                        </div>

                                        <c:if test="${counter < fotoUtente.size()-1}">
                                            <div class="carousel-item">
                                            </c:if>    
                                        </c:forEach>
                                    </div>
                                    <div>
                                        <a class="carousel-control-prev" href="#carousel-1" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carousel-1" role="button"data-slide="next">
                                            <span class="carousel-control-next-icon"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                    <ol class="carousel-indicators">
                                        <li data-target="#carousel-1" data-slide-to="0" class="active"></li>
                                            <c:forEach var="counter" begin="1" end="${fotoUtente.size()-1}">
                                            <li data-target="#carousel-1" data-slide-to="${counter}"></li>
                                            </c:forEach>
                                    </ol>
                                </div>
                            </div>
                            <br>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class ="col-12 col-sm-12 text-center">
                                        <form action="/medicare/Servlet/addFoto" method="post" enctype="multipart/form-data" style="display: inline-block">
                                            <label class="btn btn-outline-info" for="upload-photo" style="cursor: pointer; border-radius: 15px" data-toggle="tooltip" data-html="true" title="Carica foto" data-placement="bottom"><i class="fa fa-plus fa-2x" style="margin: auto; vertical-align: middle;"></i></label>
                                            <input type="file" name="file" id="upload-photo" onchange="this.form.submit()" style="opacity: 0; position: absolute; z-index: -1;"/>
                                        </form>                                      
                            </div>
                        </c:when>
                        <c:otherwise>
                            <br>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class ="col-12 col-sm-12 text-center">
                                        <form action="/medicare/Servlet/addFoto" method="post" enctype="multipart/form-data">
                                            <label class="btn btn-outline-info" for="upload-photo" style="cursor: pointer;"><span data-toggle="tooltip" data-html="true" title="Carica foto" data-placement="bottom"><i class="fa fa-plus fa-2x" style="margin: auto; vertical-align: middle;"></i></span></label>
                                            <input type="file" name="file" onchange="this.form.submit()" id="upload-photo" style="opacity: 0; position: absolute; z-index: -1;"/>
                                        </form>
                                        
                                    </div>
                                </div>
                            </div>
                        </c:otherwise>       
                    </c:choose>                                        
                </div>
            </section>

        </main>

        <footer class="page-footer light">
            <hr/>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h5>About Us</h5>
                        <ul>
                            <li><a href="#">Gruppo gitLab MediCare</a></li>
                            <li><a href="#">UniTN project</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Capacità</h5>
                        <ul>
                            <li><a href="#">150+ esami prenotabili</a></li>
                            <li><a href="#">800+ medicinali prescrivibili</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Caratteristiche</h5>
                        <ul>
                            <li><a href="#">Veloce</a></li>
                            <li><a href="#">Affidabile</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Sicurezze</h5>
                        <ul>
                            <li><a href="#">Solo medici preparati</a></li>
                            <li><a href="#">Solo gli specialisti migliori</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <p>© 2020 Tutti i diritti sono riservati al gruppo MediCare e a UNITN</p>
            </div>
        </footer>
    </body>


</html>