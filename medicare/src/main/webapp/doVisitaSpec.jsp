<%-- 
    Document   : doVisita
    Created on : 27 nov 2019, 10:10:54
    Author     : utente
--%>

<%@page import="persistence.beans.PazienteBean"%>
<%@page import="persistence.beans.EsamePrescrittoBean"%>
<%@page import="persistence.beans.EsameBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    EsamePrescrittoBean exprAttuale = (EsamePrescrittoBean) session.getAttribute("exprAttuale");
    EsameBean exAttuale = (EsameBean) session.getAttribute("exAttuale");
    PazienteBean pazAttuale = (PazienteBean) session.getAttribute("pazAttuale");
    String medbaseVSAttuale = (String) session.getAttribute("medbaseVSAttuale");
    int switchState = (int) session.getAttribute("switchState");
%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>MediCare</title>

        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/fonts/simple-line-icons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/smoothproducts.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">


        <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/smoothproducts.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/theme.js"></script>
        <script src="https://use.fontawesome.com/50f3e3804d.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    </head>

    <body>
        <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
            <div class="container-fluid">
                <a class="navbar-brand logo" href="#">
                    <h3 class="font-weight-bolder text-primary" style="text-shadow: 2px 2px 2px rgb(25, 255, 180);"> 
                        <img class="" width="40" height="30" src="${pageContext.request.contextPath}/assets/logo/logo.png"/>MediCare
                    </h3>
                </a>
                <button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/medicare/Servlet/parcoPazienti">Home</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/pazientiSInfo.jsp"">I miei pazienti</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/medicare/Servlet/caricaEsami">Esami</a></li>
                        <li class="nav-item dropdown" role="presentation">
                            <a class="nav-link dropdown-toggle" id="dropdownmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                <i class="fa fa-user fa-1x"></i> Profilo
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="dropdownmenu">
                                <a class="dropdown-item" href="/medicare/Servlet/pazienteData">Il mio account</a>
                                <a class="dropdown-item" href="/medicare/Servlet/logout">Log out</a>
                            </div>
                        </li>
                        <c:choose>
                            <c:when test="${switchState == 0}">
                                <li class="nav-item" role="presentation">
                                    <form action="/medicare/Servlet/switchState" method="get">
                                        <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Paziente</a>
                                    </form>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="nav-item" role="presentation">
                                    <form action="/medicare/Servlet/switchState" method="get">
                                        <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Specialista</a>
                                    </form>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </ul>
                </div>
            </div>
        </nav>


        <main class="page landing-page">
            <section class='clean-block'>
                <div class='container-fluid'>
                    <div class='block-heading'>
                        <h2 class='text-primary'>Visita Specialistica</h2>
                    </div>
                    <div class="container-fluid mx-auto">
                        <div class='block-heading'>
                            <h3 class='text-primary'>Informazioni sul paziente</h3>
                        </div>
                        <div class="row justify-content-center text-center mx-auto">
                            <div class='col-12 col-sm-12 '>
                                <div class="w-50 p-3 border border-info rounded-lg shadow mx-auto">
                                    <ul class='list-group'>
                                        <li class='list-group-item'><%="<p><b>CF:</b> " + pazAttuale.getCf() + "</p>"%></li>
                                        <li class='list-group-item'><%="<p><b>Nome:</b> " + pazAttuale.getNome() + "</p>"%></li>
                                        <li class='list-group-item'><%="<p><b>Cognome:</b> " + pazAttuale.getCognome() + "</p>"%></li>
                                        <li class='list-group-item'><%="<p><b>Data nascita:</b> " + pazAttuale.getDataNascita() + "</p>"%></li>
                                        <li class='list-group-item'><%="<p><b>Luogo nascita:</b> " + pazAttuale.getLuogoNascita() + "</p>"%></li>
                                        <li class='list-group-item'><%="<p><b>Sesso:</b> " + pazAttuale.getSesso() + "</p>"%></li>
                                        <li class='list-group-item'><%="<p><b>Mail:</b> " + pazAttuale.getEmail() + "</p>"%></li>
                                        <li class='list-group-item'><%="<p><b>Residenza:</b> " + pazAttuale.getResidenza() + "</p>"%></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid mx-auto">
                        <div class='block-heading'>
                            <h3 class='text-primary'>Informazioni sulla visita</h3>
                        </div>
                        <div class="row m-1 align-items-center  text-center">
                            <div class='col-12 col-sm-12 '>
                                <div class="w-50 p-3 border border-info rounded-lg shadow text-center mx-auto">
                                    <ul class='list-group'>
                                        <li class='list-group-item'><%="<p><b>Tipo visita:</b> " + exAttuale.getTipoEsame() + "</p>"%></li>
                                        <li class='list-group-item'><%="<p><b>Data visita:</b> " + exprAttuale.getDataPrenotazione() + "</p>"%></li>
                                        <li class='list-group-item'><%="<p><b>Prescritta da:</b> " + medbaseVSAttuale + "</p>"%></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid mx-auto">
                        <div class='block-heading'>
                            <h3 class='text-primary'>Anamnesi</h3>
                        </div>
                        <div class="row m-1 align-items-center  text-center">
                            <div class='col-12 col-sm-12'>
                                <div class="w-50 p-3 border border-info rounded-lg shadow text-center mx-auto">
                                    <form action="/medicare/Servlet/confermaVisitaSpecialistica" method="post">
                                        <div class='form-group p-2'>
                                            <textarea type="text" name="descrizione" style="width: 75%; height: 120px"></textarea>
                                        </div>
                                        <div class='form-group p-2'>
                                            <input type="submit" class="btn btn-primary" name="Conferma" value="Conferma" />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>      
            </section>
        </main>


        <hr/>
        <footer class="page-footer light">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h5>About Us</h5>
                        <ul>
                            <li><a href="#">Gruppo gitLab MediCare</a></li>
                            <li><a href="#">UniTN project</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Capacit�</h5>
                        <ul>
                            <li><a href="#">150+ esami prenotabili</a></li>
                            <li><a href="#">800+ medicinali prescrivibili</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Caratteristiche</h5>
                        <ul>
                            <li><a href="#">Veloce</a></li>
                            <li><a href="#">Affidabile</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Sicurezze</h5>
                        <ul>
                            <li><a href="#">Solo medici preparati</a></li>
                            <li><a href="#">Solo gli specialisti migliori</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <p>� 2020 Tutti i diritti sono riservati al gruppo MediCare e a UNITN</p>
            </div>
        </footer>
    </body>
</html>

