<%-- 
    Document   : visualizzaPazienteData
    Created on : 23 gen 2020, 10:57:01
    Author     : (leoberri243@gmail.com)
--%>

<%@page import="servlet.UserType"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import = "persistence.beans.TicketBean"%>
<% ArrayList<TicketBean> listTickets = (ArrayList<TicketBean>) session.getAttribute("listTickets");%>
<% ArrayList<String> listDateTickets = (ArrayList<String>) session.getAttribute("listDateTickets");%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%
    UserType userType = (UserType) session.getAttribute("userType");
    int switchState = (int) session.getAttribute("switchState");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Medicare</title>

        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/fonts/simple-line-icons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/smoothproducts.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">


        <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/smoothproducts.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/theme.js"></script>
        <script src="https://use.fontawesome.com/50f3e3804d.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
            <div class="container-fluid">
                <a class="navbar-brand logo" href="#">
                    <h3 class="font-weight-bolder text-primary" style="text-shadow: 2px 2px 2px rgb(25, 255, 180);"> 
                        <img class="" width="40" height="30" src="${pageContext.request.contextPath}/assets/logo/logo.png"/>MediCare
                    </h3>
                </a>
                <button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/home.jsp">Home</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/visitaMedicaInsData.jsp">Appuntamenti</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/medicare/Servlet/visualizzaPazienteData">Servizi</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link active" href="/medicare/Servlet/visualizzaTicket">I miei ticket</a></li>
                        <li class="nav-item dropdown" role="presentation">
                            <a class="nav-link dropdown-toggle" id="dropdownmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                <i class="fa fa-user fa-1x"></i> Profilo 
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="dropdownmenu">
                                <a class="dropdown-item" href="/medicare/Servlet/pazienteData">Il mio account</a>
                                <a class="dropdown-item" href="/medicare/Servlet/logout">Log out</a>
                            </div>
                        </li>
                        <c:choose>
                            <c:when test="${userType != UserType.PAZIENTE}">
                                <c:choose>
                                    <c:when test="${switchState == 0}">
                                        <li class="nav-item" role="presentation">
                                            <form action="/medicare/Servlet/switchState" method="get">
                                                <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Paziente</a>
                                            </form>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="nav-item" role="presentation">
                                            <form action="/medicare/Servlet/switchState" method="get">
                                                <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Medico</a>
                                            </form>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                        </c:choose>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="page landing-page">
            <section class="clean-block clean-info" style="background-color: #007bff">
                <div class="wrapper">
                    <%
                        String error = String.valueOf(request.getParameter("error"));
                        String phrase = "";

                        if (error.equals("1")) {
                            phrase = "<div class='container-fluid'><div class='row'><div class='col- col-sm-12'><div class='w-100 alert alert-danger alert-dismissible fade show' style='position: absolute; left: 0; right: 0;'><button type='button' class='close' data-dismiss='alert'>&times;</button>Errore nel pagamento!</div></div></div></div>";
                            out.println(phrase);
                        }
                    %>

                    <div class="container-fluid">
                        <div class="block-heading text-center">
                            <h2 class="text" style="color: #fff">Tickets utente</h2>
                        </div>
                        <div class="container-fluid bg-white p-3 border border-primary rounded-lg">
                            <div class="row justify-content-center m-1">
                                <div class="table-responsive">
                                    <!--Table-->
                                    <div class="container">
                                        <div class="row align-items-center text-center">
                                            <div class="table-responsive">
                                                <!--Table-->
                                                <table id="listTickets" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                                    <!--Table head-->
                                                    <thead>
                                                        <tr>
                                                            <th class="th">Id</th>
                                                            <th class="th">Data</th>
                                                            <th class="th">Costo</th>
                                                            <th class="th">Pagato</th>
                                                            <th class="th"></th>
                                                        </tr>
                                                    </thead>
                                                    <!--Table head-->
                                                    <tbody>
                                                        <c:choose>
                                                            <c:when test="${listTickets.size() == 0 || listTickets == null}">

                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:forEach var="i" begin="0" end="${listTickets.size()-1}" step="1">
                                                                    <tr>
                                                                        <td><c:out value="${listTickets.get(i).getId()}" /></td>
                                                                        <td><c:out value="${listDateTickets.get(i)}" /></td>
                                                                        <td><c:out value="${listTickets.get(i).getCosto()}" /></td>
                                                                        <td><c:out value="${listTickets.get(i).getPagato()}" /></td>
                                                                        <td>
                                                                            <c:choose>
                                                                                <c:when test="${listTickets.get(i).getPagato() != 0}">
                                                                                    <form action="/medicare/Servlet/scaricaPdf" method="post">
                                                                                        <a id="pdfVT" class="btn btn-info" href="javascript:;" onclick="parentNode.submit();" target='_blank'>PDF</a>
                                                                                        <input type="hidden" name="id" value="${listTickets.get(i).getId()}" />
                                                                                    </form>
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <form action="/medicare/Servlet/pagaTicket" method="post">
                                                                                        <a id="ticketVT" class="btn btn-info" href="javascript:;" onclick="parentNode.submit();">Paga</a>
                                                                                        <input type="hidden" name="id" value="${listTickets.get(i).getId()}" />
                                                                                    </form>
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </td>
                                                                    </tr>
                                                                </c:forEach>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tbody>
                                                    <!--Table body-->
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Table-->  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>            
        </main>
        <hr/>
        <footer class="page-footer light">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h5>About Us</h5>
                        <ul>
                            <li><a href="#">Gruppo gitLab MediCare</a></li>
                            <li><a href="#">UniTN project</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Capacità</h5>
                        <ul>
                            <li><a href="#">150+ esami prenotabili</a></li>
                            <li><a href="#">800+ medicinali prescrivibili</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Caratteristiche</h5>
                        <ul>
                            <li><a href="#">Veloce</a></li>
                            <li><a href="#">Affidabile</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Sicurezze</h5>
                        <ul>
                            <li><a href="#">Solo medici preparati</a></li>
                            <li><a href="#">Solo gli specialisti migliori</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <p>© 2020 Tutti i diritti sono riservati al gruppo MediCare e a UNITN</p>
            </div>
        </footer>
    </body>
</html>
