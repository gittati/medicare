<%@page import="servlet.UserType"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import= "persistence.beans.NotificheBean"%>
<%@page import= "java.util.ArrayList"%>
<%@page import= "persistence.beans.EsameBean"%>
<%@page import = "persistence.beans.PazienteBean"%>

<% ArrayList<EsameBean> listEsami = (ArrayList<EsameBean>) session.getAttribute("listEsami");%>
<% PazienteBean pb = (PazienteBean) session.getAttribute("pazienteBeanInstance");%>
<%
    ArrayList<NotificheBean> notifiche = (ArrayList<NotificheBean>)session.getAttribute("notifiche");
    UserType userType = (UserType) session.getAttribute("userType");
    int switchState = (int)session.getAttribute("switchState");
%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>MediCare</title>

        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/fonts/simple-line-icons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/smoothproducts.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">


        <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/smoothproducts.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/theme.js"></script>
        <script src="https://use.fontawesome.com/50f3e3804d.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
        <!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->
        <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
        <!-- Bootstrap Date-Picker Plugin -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    </head>

    <body>
        <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar" id="myNav">
            <div class="container-fluid">
                <a class="navbar-brand logo" href="#">
                    <h3 class="font-weight-bolder text-primary" style="text-shadow: 2px 2px 2px rgb(25, 255, 180);"> 
                        <img class="" width="40" height="30" src="${pageContext.request.contextPath}/assets/logo/logo.png"/>MediCare
                    </h3>
                </a>
                <button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item" role="presentation"><a class="nav-link active" href="${pageContext.request.contextPath}/home.jsp">Home</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/visitaMedicaInsData.jsp">Appuntamenti</a></li>
                        <li class="nav-item" role="presentation"><a class ="nav-link" href="/medicare/Servlet/visualizzaPazienteData">Servizi</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/medicare/Servlet/visualizzaTicket">I miei ticket</a></li>
                        <li class="nav-item dropdown" role="presentation">
                            <a class="nav-link dropdown-toggle" id="dropdownmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                <i class="fa fa-user fa-1x"></i> Profilo 
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-cyan" id="myDropdownMenu" aria-labelledby="dropdownmenu">
                                <a class="dropdown-item" href="/medicare/Servlet/pazienteData">Il mio account</a>
                                <a class="dropdown-item" href="/medicare/Servlet/logout">Log out</a>
                            </div>
                        </li>
                        <c:choose>
                            <c:when test="${userType != UserType.PAZIENTE}">
                                <c:choose>
                                    <c:when test="${switchState == 0}">
                                        <li class="nav-item" role="presentation">
                                            <form action="/medicare/Servlet/switchState" method="get">
                                                <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Paziente</a>
                                            </form>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="nav-item" role="presentation">
                                            <form action="/medicare/Servlet/switchState" method="get">
                                                <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Medico</a>
                                            </form>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                        </c:choose>
                    </ul>
                </div>
            </div>
        </nav>

        <div class='w-100 alert alert-info alert-dismissible fade show' style='position: fixed; left: 0; right: 0; bottom: 5px; z-index: 4;'><button type='button' class='btn btn-default close' data-dismiss='alert'>OK</button>Questo sito usa i coockies, <a href='#' class='alert-link'>leggi di più a riguardo.</a></div>
        
        <main class="page landing-page">
            
            <section class="clean-block clean-hero" style="color: rgba( 255 , 255 , 255 , 0.000 ); background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${pageContext.request.contextPath}/assets/img/bg_3.jpg); ">
                
                    <div class="text text-center">
                        <h3 style='color: #ffffff;'>Benvenuto su MediCare,</h3>
                        <h2 style='color: #ffffff;'><%="" + pb.getNome() +" "+ pb.getCognome()%></h2>
                        <button class="btn btn-outline-light btn-lg" type="button" data-toggle="modal" data-target="#notificheModal">
                            Notifiche 
                            <span class="badge badge-primary">
                                <%=""+ notifiche.size() +"" %>
                            </span>                           
                        </button>
                    </div> 
                    
                <!-- The Modal -->
                <div class="modal fade" id="notificheModal">
                    <div class="modal-dialog modal-lg modal-dialog-scrollable">
                        <div class="modal-content">

                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h2 class="modal-title text-info">Le tue notifiche</h2>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <!-- Modal body -->
                            <div class="modal-body" style="color: #000">
                                <div class="container">
                                    <c:choose>
                                        <c:when test="${notifiche.size() > 0}">
                                            <ul class="list-group list-group-flush align-left">
                                                <c:forEach var="notifica" items="${notifiche}">
                                                    <li class="list-group-item">
                                                        <c:out value="${notifica.getMessage()}" />
                                                    </li>
                                                </c:forEach>
                                            </ul>
                                        </c:when>
                                        <c:otherwise>
                                            <p class="align-left">Non ci sono nuove notifiche</p>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-block" data-dismiss="modal">Chiudi</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <section class='clean-block clean-info' style="background-color: #007bff">
                <div class="container-fluid">
                    <div class="block-heading text-center">
                        <h2 class="text" style="color: #fff">Lista Esami</h2>
                    </div>
                    <div class="container w-75 bg-white p-3 border border-primary rounded-lg">
                        <div class="row justify-content-center m-1">
                            <div class="col">
                                <div class="table-responsive">
                                    <!--Table-->
                                    <table id="tabellaListaEsami" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                    <!--Table head-->
                                        <thead>
                                            <tr>
                                                <th class="th">Id</th>
                                                <th class="th">Nome</th>
                                            </tr>
                                        </thead>
                                        <!--Table head-->
                                        <!--Table body-->
                                        <tbody>
                                            <c:forEach var="exam" items="${listEsami}">
                                                <tr>
                                                    <td><c:out value="${exam.getId()}" /></td>
                                                    <td><c:out value="${exam.getTipoEsame()}"/></td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                        <!--Table body-->
                                    </table>
                                </div>                            
                            </div>                    
                        </div>
                    </div>
                </div>
            </section>
        </main>
                            
        <hr/>
        <footer class="page-footer light">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h5>About Us</h5>
                        <ul>
                            <li><a href="#">Gruppo gitLab MediCare</a></li>
                            <li><a href="#">UniTN project</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Capacità</h5>
                        <ul>
                            <li><a href="#">150+ esami prenotabili</a></li>
                            <li><a href="#">800+ medicinali prescrivibili</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Caratteristiche</h5>
                        <ul>
                            <li><a href="#">Veloce</a></li>
                            <li><a href="#">Affidabile</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Sicurezze</h5>
                        <ul>
                            <li><a href="#">Solo medici preparati</a></li>
                            <li><a href="#">Solo gli specialisti migliori</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <p>© 2020 Tutti i diritti sono riservati al gruppo MediCare e a UNITN</p>
            </div>
        </footer>
    </body>
</html>