<%-- 
    Document   : homeMedician
    Created on : 20 nov 2019, 11:42:44
    Author     : utente
--%>


<%@page import="persistence.beans.NotificheBean"%>
<%@page import="persistence.beans.VisitaMedicaBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="java.util.ArrayList"%>
<%@page import="persistence.beans.PazienteBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% ArrayList<PazienteBean> parcoPazienti = (ArrayList<PazienteBean>) session.getAttribute("parcoPazienti");%>
<% ArrayList<VisitaMedicaBean> visiteMediche = (ArrayList<VisitaMedicaBean>) session.getAttribute("visiteMediche");%>
<% ArrayList<VisitaMedicaBean> visiteMedicheTerminabili = (ArrayList<VisitaMedicaBean>) session.getAttribute("visiteMedicheTerminabili");%>
<% ArrayList<String> nomiPazientiVisite = (ArrayList<String>) session.getAttribute("nomiPazientiVisite");%>
<%
    ArrayList<NotificheBean> notifiche = (ArrayList<NotificheBean>) session.getAttribute("notifiche");
    int numVisiteOggi = (int) session.getAttribute("numVisiteOggi");
    int switchState = (int) session.getAttribute("switchState");
    ArrayList<String> nomiPazientiT = (ArrayList<String>) session.getAttribute("nomiPazientiT");
%>
<% PazienteBean pb = (PazienteBean) session.getAttribute("pazienteBeanInstance");%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>MediCare</title>

        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/fonts/simple-line-icons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/smoothproducts.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">


        <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/smoothproducts.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/theme.js"></script>
        <script src="https://use.fontawesome.com/50f3e3804d.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    </head>

    <body>
        <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
            <div class="container-fluid">
                <a class="navbar-brand logo" href="#">
                    <h3 class="font-weight-bolder text-primary" style="text-shadow: 2px 2px 2px rgb(25, 255, 180);"> 
                        <img class="" width="40" height="30" src="${pageContext.request.contextPath}/assets/logo/logo.png"/>MediCare
                    </h3>
                </a>
                <button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item" role="presentation"><a class="nav-link active" href="/medicare/Servlet/parcoPazienti">Home</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/pazientiMInfo.jsp">I miei pazienti</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/medicare/Servlet/caricaVisite">Visite mediche</a></li>
                        <li class="nav-item dropdown" role="presentation">
                            <a class="nav-link dropdown-toggle" id="dropdownmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                <i class="fa fa-user fa-1x"></i> Profilo 
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="dropdownmenu">
                                <a class="dropdown-item" href="/medicare/Servlet/pazienteData">Il mio account</a>
                                <a class="dropdown-item" href="/medicare/Servlet/logout">Log out</a>
                            </div>
                        </li>
                        <c:choose>
                            <c:when test="${switchState == 0}">
                                <li class="nav-item" role="presentation">
                                    <form action="/medicare/Servlet/switchState" method="get">
                                        <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Paziente</a>
                                    </form>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="nav-item" role="presentation">
                                    <form action="/medicare/Servlet/switchState" method="get">
                                        <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Medico</a>
                                    </form>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </ul>
                </div>
            </div>
        </nav>

        <div class='w-100 alert alert-info alert-dismissible fade show' style='position: fixed; left: 0; right: 0; bottom: 5px; z-index: 4;'><button type='button' class='btn btn-default close' data-dismiss='alert'>OK</button>Questo sito usa i coockies, <a href='#' class='alert-link'>leggi di più a riguardo.</a></div>

        <main class="page landing-page">
            
            <section class="clean-block clean-hero" style="color: rgba( 255 , 255 , 255 , 0.000 ); background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${pageContext.request.contextPath}/assets/img/bg_3.jpg);" >
                <div class="text text-center">
                    <h3 style='color: #ffffff;'>Benvenuto su MediCare,</h3>
                    <h2 style='color: #ffffff;'><%="" + pb.getNome() + " " + pb.getCognome()%></h2>
                    <button class="btn btn-outline-light btn-lg" type="button" data-toggle="modal" data-target="#notificheModal">
                        Notifiche 
                        <span class="badge badge-primary">
                            <%="" + notifiche.size() + ""%>
                        </span>                           
                    </button>
                </div> 

                <!-- The Modal -->
                <div class="modal fade" id="notificheModal">
                    <div class="modal-dialog modal-lg modal-dialog-scrollable">
                        <div class="modal-content">

                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h2 class="modal-title text-info">Le tue notifiche</h2>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <!-- Modal body -->
                            <div class="modal-body" style="color: #000">
                                <div class="container">
                                    <c:choose>
                                        <c:when test="${notifiche.size() > 0}">
                                            <ul class="list-group list-group-flush align-left">
                                                <c:forEach var="notifica" items="${notifiche}">
                                                    <li class="list-group-item">
                                                        <c:out value="${notifica.getMessage()}  (Paziente)" />
                                                    </li>
                                                </c:forEach>
                                                <li class="list-group-item">
                                                    <c:out value="Hai ${numVisiteOggi} visita\e segnate per oggi." />
                                                </li>
                                            </ul>
                                        </c:when>
                                        <c:otherwise>
                                            <ul class="list-group list-group-flush align-left">
                                                <li class="list-group-item">
                                                    <c:out value="Hai ${numVisiteOggi} visita\e segnate per oggi." />
                                                </li>
                                            </ul>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-block" data-dismiss="modal">Chiudi</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="clean-block">
                <div class="container-fluid">
                    <div class="block-heading text-center">
                        <h2 class="text-primary">Visite Mediche di oggi</h2>
                    </div>
                    <div class="container-fluid bg-white p-3 border border-primary rounded-lg">
                        <div class="row justify-content-center text-center m-1">
                            <div class="col-sm-12">
                                <c:choose>
                                    <c:when test="${visiteMediche.size() > 0}"> 
                                        <div class="container">
                                            <div class="table-responsive">
                                                <!--Table-->
                                                <table id="tabellaVisiteMediche" class="table table-bordered table-hover" cellspacing="0" width="100%">
                                                    <!--Table head-->
                                                    <thead>
                                                        <tr>
                                                            <th class="th">Paziente</th>
                                                            <th class="th">Data iscrizione</th>
                                                            <th class="th">Stato</th>
                                                            <th class="th"></th>
                                                        </tr>
                                                    </thead>
                                                    <!--Table head-->
                                                    <!--Table body-->
                                                    <tbody>
                                                        <c:forEach var="i" begin="0" end="${visiteMediche.size()-1}" step="1">
                                                            <tr>
                                                                <td><c:out value="${nomiPazientiVisite.get(i)}" /></td>
                                                                <td><c:out value="${visiteMediche.get(i).getDataIscrizione()}" /></td>
                                                                <c:choose>
                                                                    <c:when test="${visiteMediche.get(i).getTerminata() == 0}">
                                                                        <td><c:out value="Da visitare" /></td>
                                                                        <td>
                                                                            <form action="/medicare/Servlet/getVisitaInfo" method="post">
                                                                                <a class="btn btn-info" href="javascript:;" onclick="parentNode.submit();">Esegui</a>
                                                                                <input type="hidden" name="id" value="${visiteMediche.get(i).getId()}" />
                                                                            </form>
                                                                        </td>
                                                                    </c:when>
                                                                    <c:when test="${visiteMediche.get(i).getTerminata() == 1}">
                                                                        <td><c:out value="In attesa di esame specialistico" /></td>
                                                                        <td>
                                                                            <form action="" method="post">
                                                                                <a class="btn btn-info" href="javascript:;" onclick="parentNode.submit();">Visualizza</a>
                                                                                <input type="hidden" name="id" value="${visiteMediche.get(i).getId()}" />
                                                                            </form>
                                                                        </td>
                                                                    </c:when>
                                                                    <c:when test="${visiteMediche.get(i).getTerminata() == 2}">
                                                                        <td><c:out value="Esami specialistici completati" /></td>
                                                                        <td>
                                                                            <form action="/medicare/Servlet/getVisitaInfoWithAnamnesi" method="post">
                                                                                <a class="btn btn-info" href="javascript:;" onclick="parentNode.submit();">Esegui</a>
                                                                                <input type="hidden" name="id" value="${visiteMediche.get(i).getId()}" />
                                                                            </form>
                                                                        </td>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <td><c:out value="Terminata" /></td>
                                                                        <td>
                                                                            <form action="/medicare/Servlet/visualizzaVisitaInfo" method="post">
                                                                                <a class="btn btn-info" href="#" onclick="parentNode.submit();">Visualizza</a>
                                                                                <input type="hidden" name="id" value="${visiteMediche.get(i).getId()}" />
                                                                            </form>
                                                                        </td>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </tr>                
                                                        </c:forEach>
                                                    </tbody>
                                                    <!--Table body-->
                                                </table>
                                            </div>
                                        </div>


                                    </c:when>
                                    <c:otherwise>
                                        <div class="alert alert-info text-center" style="margin: auto; vertical-align: middle;">
                                            Non hai nessuna visita medica oggi
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
                        
                        
            <section class="clean-block">
                <div class="container-fluid">
                    <div class="block-heading text-center">
                        <h2 class="text-primary">Visite Mediche terminabili</h2>
                    </div>
                    <div class="container-fluid bg-white p-3 border border-primary rounded-lg">
                        <div class="row justify-content-center text-center m-1">
                            <div class="col-sm-12">
                                <c:choose>
                                    <c:when test="${visiteMedicheTerminabili.size() > 0}"> 
                                        <div class="container">
                                            <div class="table-responsive">
                                                <!--Table-->
                                                <table id="tabellaVisiteMediche2" class="table table-bordered table-hover" cellspacing="0" width="100%">
                                                    <!--Table head-->
                                                    <thead>
                                                        <tr>
                                                            <th class="th">Paziente</th>
                                                            <th class="th">Data iscrizione</th>
                                                            <th class="th">Stato</th>
                                                            <th class="th"></th>
                                                        </tr>
                                                    </thead>
                                                    <!--Table head-->
                                                    <!--Table body-->
                                                    <tbody>
                                                        <c:forEach var="i" begin="0" end="${visiteMedicheTerminabili.size()-1}" step="1">
                                                            <tr>
                                                                <td><c:out value="${nomiPazientiT.get(i)}" /></td>
                                                                <td><c:out value="${visiteMedicheTerminabili.get(i).getDataIscrizione()}" /></td>
                                                                <td><c:out value="Esami specialistici completati" /></td>
                                                                <td>
                                                                    <form action="/medicare/Servlet/getVisitaInfoWithAnamnesi" method="post">
                                                                        <a class="btn btn-info" href="javascript:;" onclick="parentNode.submit();">Esegui</a>
                                                                        <input type="hidden" name="id" value="${visiteMediche.get(i).getId()}" />
                                                                    </form>
                                                                </td>
                                                            </tr>                
                                                        </c:forEach>
                                                    </tbody>
                                                    <!--Table body-->
                                                </table>
                                            </div>
                                        </div>


                                    </c:when>
                                    <c:otherwise>
                                        <div class="alert alert-info text-center" style="margin: auto; vertical-align: middle;">
                                            Non hai nessuna visita medica terminabile
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section class="clean-block" style="background-color: #007bff;">
                <div class="container-fluid">
                    <div class="block-heading text-center">
                        <h2 class="text" style="color: #fff">Parco pazienti</h2>
                    </div>
                    <div class="container-fluid bg-white p-3 border border-primary rounded-lg">
                        <div class="row justify-content-center m-1">
                            <div class="table-responsive">
                                <!--Table-->
                                <table id="tabellaParcoPazienti" class="table table-bordered table-hover" cellspacing="0" width="100%">
                                    <!--Table head-->
                                    <thead>
                                        <tr>
                                            <th class="th">Cf</th>
                                            <th class="th">Nome</th>
                                            <th class="th">Cognome</th>
                                            <th class="th">Data di nascita</th>
                                            <th class="th">Luogo di nascita</th>
                                            <th class="th">Sesso</th>
                                            <th class="th">Email</th>
                                            <th class="th"></th>
                                        </tr>
                                    </thead>
                                    <!--Table head-->
                                    <!--Table body-->
                                    <tbody>
                                        <c:forEach var="i" begin="0" end="${parcoPazienti.size()-1}" step="1" varStatus="loop">
                                            <tr>
                                                <td><c:out value="${parcoPazienti.get(i).getCf()}" /></td>
                                                <td><c:out value="${parcoPazienti.get(i).getNome()}" /></td>
                                                <td><c:out value="${parcoPazienti.get(i).getCognome()}" /></td>
                                                <td><c:out value="${parcoPazienti.get(i).getDataNascita()}" /></td>
                                                <td><c:out value="${parcoPazienti.get(i).getLuogoNascita()}" /></td>
                                                <td><c:out value="${parcoPazienti.get(i).getSesso()}" /></td>
                                                <td><c:out value="${parcoPazienti.get(i).getEmail()}" /></td>
                                                <td>
                                                    <form action="/medicare/Servlet/getLastVisitaAndMedicinale" method="post">
                                                        <a id="selezionaMB" class="btn btn-info" href="#" onclick="parentNode.submit();">Seleziona</a>
                                                        <input type="hidden" name="cf" value="${parcoPazienti.get(i).getCf()}" />
                                                    </form>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                    <!--Table body-->
                                </table>
                            </div>
                        </div>
                        <!--Table-->
                    </div>
                </div>
            </section>




            <section class='clean-block clean-info'>
                <div class="container-fluid">
                    <div class="block-heading text-center">
                        <h2 class="text-primary">Lista Esami</h2>
                    </div>
                    <div class="container w-75 bg-white p-3 border border-primary rounded-lg shadow-lg">
                        <div class="row justify-content-center m-1">
                            <div class="col">
                                <div class="table-responsive">
                                    <!--Table-->
                                    <table id="tabellaListaEsami" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                        <!--Table head-->
                                        <thead>
                                            <tr>
                                                <th class="th">Id</th>
                                                <th class="th">Nome</th>
                                            </tr>
                                        </thead>
                                        <!--Table head-->
                                        <!--Table body-->
                                        <tbody>
                                            <c:forEach var="exam" items="${listEsami}">
                                                <tr>
                                                    <td><c:out value="${exam.getId()}" /></td>
                                                    <td><c:out value="${exam.getTipoEsame()}"/></td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                        <!--Table body-->
                                    </table>
                                </div>                            
                            </div>                    
                        </div>
                    </div>
                </div>
            </section>
        </main>



        <footer class="page-footer light">
            <hr/>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h5>About Us</h5>
                        <ul>
                            <li><a href="#">Gruppo gitLab MediCare</a></li>
                            <li><a href="#">UniTN project</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Capacità</h5>
                        <ul>
                            <li><a href="#">150+ esami prenotabili</a></li>
                            <li><a href="#">800+ medicinali prescrivibili</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Caratteristiche</h5>
                        <ul>
                            <li><a href="#">Veloce</a></li>
                            <li><a href="#">Affidabile</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Sicurezze</h5>
                        <ul>
                            <li><a href="#">Solo medici preparati</a></li>
                            <li><a href="#">Solo gli specialisti migliori</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <p>© 2020 Tutti i diritti sono riservati al gruppo MediCare e a UNITN</p>
            </div>
        </footer>
    </body>
</html>
