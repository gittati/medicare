<%-- 
    Document   : homeMedician
    Created on : 20 nov 2019, 11:42:44
    Author     : utente
--%>


<%@page import="persistence.beans.PazienteBean"%>
<%@page import="persistence.beans.MedicinalePrescrittoBean"%>
<%@page import="persistence.beans.TicketBean"%>
<%@page import="persistence.beans.AnamnesiBean"%>
<%@page import="persistence.beans.VisitaSpecialisticaBean"%>
<%@page import="persistence.beans.EsamePrescrittoBean"%>
<%@page import="persistence.beans.VisitaMedicaBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% ArrayList<VisitaMedicaBean> allVisiteMediche = (ArrayList<VisitaMedicaBean>) session.getAttribute("allVisiteMediche");%>
<% ArrayList<String> allNomiPazientiVisite = (ArrayList<String>) session.getAttribute("allNomiPazientiVisite");%>
<%
    int switchState = (int) session.getAttribute("switchState");
    VisitaMedicaBean theVisitaSelected = (VisitaMedicaBean) session.getAttribute("theVisitaSelected");

    PazienteBean thePazienteVisita = (PazienteBean) session.getAttribute("thePazienteVisita");
    ArrayList<EsamePrescrittoBean> listEsamiPrescrittiV = (ArrayList<EsamePrescrittoBean>) session.getAttribute("listEsamiPrescrittiV");
    ArrayList<VisitaSpecialisticaBean> listVisiteSpecialisticheV = (ArrayList<VisitaSpecialisticaBean>) session.getAttribute("listVisiteSpecialisticheV");
    ArrayList<AnamnesiBean> listAnamnesiV = (ArrayList<AnamnesiBean>) session.getAttribute("listAnamnesiV");
    ArrayList<TicketBean> listTicketsV = (ArrayList<TicketBean>) session.getAttribute("listTicketsV");
    ArrayList<String> listNomiEsamiV = (ArrayList<String>) session.getAttribute("listNomiEsamiV");
    ArrayList<String> listNomiSpecialistV = (ArrayList<String>) session.getAttribute("listNomiSpecialistV");

    ArrayList<MedicinalePrescrittoBean> listMedicinaliPV = (ArrayList<MedicinalePrescrittoBean>) session.getAttribute("listMedicinaliPV");
    ArrayList<String> listNomiMedicinaliV = (ArrayList<String>) session.getAttribute("listNomiMedicinaliV");
%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>MediCare</title>

        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/fonts/simple-line-icons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/smoothproducts.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">


        <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/smoothproducts.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/theme.js"></script>
        <script src="https://use.fontawesome.com/50f3e3804d.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    </head>

    <body>
        <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
            <div class="container-fluid">
                <a class="navbar-brand logo" href="#">
                    <h3 class="font-weight-bolder text-primary" style="text-shadow: 2px 2px 2px rgb(25, 255, 180);"> 
                        <img class="" width="40" height="30" src="${pageContext.request.contextPath}/assets/logo/logo.png"/>MediCare
                    </h3>
                </a>
                <button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/medicare/Servlet/parcoPazienti">Home</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/pazientiMInfo.jsp">I miei pazienti</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link active" href="/medicare/Servlet/caricaVisite">Visite mediche</a></li>
                        <li class="nav-item dropdown" role="presentation">
                            <a class="nav-link dropdown-toggle" id="dropdownmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                <i class="fa fa-user fa-1x"></i> Profilo 
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="dropdownmenu">
                                <a class="dropdown-item" href="/medicare/Servlet/pazienteData">Il mio account</a>
                                <a class="dropdown-item" href="/medicare/Servlet/logout">Log out</a>
                            </div>
                        </li>
                        <c:choose>
                            <c:when test="${switchState == 0}">
                                <li class="nav-item" role="presentation">
                                    <form action="/medicare/Servlet/switchState" method="get">
                                        <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Paziente</a>
                                    </form>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="nav-item" role="presentation">
                                    <form action="/medicare/Servlet/switchState" method="get">
                                        <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Medico</a>
                                    </form>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </ul>
                </div>
            </div>
        </nav>


        <main class="page landing-page">
            <section class="clean-block">
                <div class="container-fluid">
                    <div class="block-heading">
                        <h2 class="text-primary">Informazioni sulla visita</h2>
                    </div>
                    <c:choose>
                        <c:when test="${theVisitaSelected != null}">

                            <div class="row justify-content-center">
                                <div class="w-50 p-3 border border-info rounded-lg shadow text-center" style="text-align: left; ">
                                    <%
                                        out.write("<p><b>Paziente: </b>" + thePazienteVisita.getNome() + " " + thePazienteVisita.getCognome() + "</p>");
                                        out.write("<p><b>Data inserimento:</b> " + theVisitaSelected.getDataIscrizione() + "</p>");
                                    %>
                                    <c:choose>
                                        <c:when test="${theVisitaSelected.getTerminata() == 0}">
                                            <% out.write("<p><b>Stato:</b> Da visitare</p>"); %>
                                        </c:when>
                                        <c:when test="${theVisitaSelected.getTerminata() == 1}">
                                            <% out.write("<p><b>Stato:</b> In attesa di esami specialistici</p>"); %> 
                                        </c:when>
                                        <c:when test="${theVisitaSelected.getTerminata() == 2}">
                                            <% out.write("<p><b>Stato:</b> Esami specialistici completati</p>"); %>
                                        </c:when>
                                        <c:otherwise>
                                            <% out.write("<p><b>Stato:</b> Completata</p>");%>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>           
                        </div>
                        <div class="container-fluid">
                            <div class="block-heading">
                                <h2 class="text-primary">Medicicinali prescritti per questa visita</h2>
                            </div>
                            <div class="row justify-content-center">
                                <div class="w-50 p-3 border border-info rounded-lg shadow text-center mx-auto">
                                    <c:choose>
                                        <c:when test="${listMedicinaliPV != null && listMedicinaliPV.size() > 0}">
                                            <ul class="list-group">
                                                <c:forEach var="i" begin="0" end="${listMedicinaliPV.size()-1}" step="1" varStatus="loop">
                                                    <li class="list-group-item">
                                                        <c:out value="${listNomiMedicinaliV.get(i)} in data ${listMedicinaliPV.get(i).getDataRilascio()}" />
                                                    </li>
                                                </c:forEach>
                                            </ul>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="alert alert-info text-center w-50" style="margin: auto; vertical-align: middle;">
                                                Nessun medicinale prescritto.
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid">
                            <div class="block-heading">
                                <h2 class="text-primary">Esami prescritti per questa visita</h2>
                            </div>
                            <div class="row justify-content-center">
                                <div class="w-50 p-3 border border-info rounded-lg shadow text-center mx-auto">
                                    <c:choose>
                                        <c:when test="${listEsamiPrescrittiV != null && listEsamiPrescrittiV.size() > 0}">
                                            <c:forEach var="i" begin="0" end="${listEsamiPrescrittiV.size()-1}" step="1" varStatus="loop">
                                                <div class="container-fluid">
                                                    
                                                        <c:out value="<p><b>Data esame:</b> ${listEsamiPrescrittiV.get(i).getDataPrenotazione()} </p>" escapeXml="false" />
                                                        <c:out value="<p><b>Tipo esame:</b> ${listNomiEsamiV.get(i)}</p>" escapeXml="false" />
                                                        <c:out value="<p><b>Medico specialista:</b> ${listNomiSpecialistV.get(i)}</p>" escapeXml="false" />
                                                            <c:choose>
                                                                <c:when test="${listVisiteSpecialisticheV.get(i).getTerminata().equals('0')}">                                       
                                                                
                                                                    <div class="alert alert-info text-center w-50" style="margin: auto; vertical-align: middle;">
                                                                        Esame non ancora eseguito. 
                                                                    </div>
                                                                    <br>
                                                            </c:when>
                                                            <c:otherwise>
                                                         
                                                                <div class="alert alert-info text-center w-50" style="margin: auto; vertical-align: middle;">
                                                                    Esame terminato.
                                                                </div>
                                                                <br>
                                                            <p class="m-2 text-primary" ><b>Anamnesi: </b></p>
                                                            <ul class="list-group">
                                                                <li class="list-group-item"><c:out value="<p><b>Data rilascio:</b> ${listAnamnesiV.get(i).getDataRilascio()}</p>" escapeXml="false"/></li>
                                                                <li class="list-group-item"><c:out value="<p><b>Commento:</b> ${listAnamnesiV.get(i).getDescrizione()}</p>" escapeXml="false"/></li>
                                                            </ul>
                                                            <p class="m-2" style="color: #007bff"><b>Ticket: </b></p>
                                                            <ul class="list-group">
                                                                <li class="list-group-item"><c:out value="<p><b>Costo:</b> ${listTicketsV.get(i).getCosto()}</p>" escapeXml="false"/></li>
                                                                <li class="list-group-item">
                                                                    <c:choose>
                                                                        <c:when test="${listTicketsV.get(i).getPagato().equals('0')}">
                                                                            <div class="alert alert-warning text-center w-50" style="margin: auto; vertical-align: middle;">
                                                                                Non pagato. 
                                                                            </div>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <div class="alert alert-success text-center w-50" style="margin: auto; vertical-align: middle;">
                                                                                Pagato.
                                                                            </div>

                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </li>
                                                            </ul>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </div>
                                            </c:forEach>
                                        </c:when>
                                        <c:otherwise><div class="alert alert-info text-center w-50" style="margin: auto; vertical-align: middle;">
                                                Nessun esame prescritto.
                                            </div>

                                        </c:otherwise>
                                    </c:choose> 
                                </div>
                            </div>
                            <div class="row p-5 justify-content-center">
                                <form action="/medicare/Servlet/closeVisitaScelta" method="get">
                                    <a class="btn btn-danger" href="javascript:;" onclick="parentNode.submit();">Chiudi visita </a>
                                </form>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="alert alert-info text-center w-50" style="margin: auto; vertical-align: middle;">
                                Nessuna visita selezionata.
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </section>

            <section class="clean-block" style="background-color: #007bff">
                <div class="container-fluid">
                    <div class="block-heading">
                        <h2 class="text-light">Seleziona un'altra visita</h2>
                    </div>
                    <div class="container-fluid bg-white p-3 border border-primary rounded-lg">
                        <div class="row m-1 justify-content-center text-center">
                            <div class="col-12 col-sm-12">
                                <c:choose>
                                    <c:when test="${allVisiteMediche.size() > 0}">                       
                                        <div class="table-responsive">
                                            <!--Table-->
                                            <table id="tabellaAllVisiteMediche" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                                <!--Table head-->
                                                <thead>
                                                    <tr>
                                                        <th class="th">Paziente</th>
                                                        <th class="th">Data iscrizione</th>
                                                        <th class="th">Stato</th>
                                                        <th class="th"></th>
                                                    </tr>
                                                </thead>
                                                <!--Table head-->
                                                <!--Table body-->
                                                <tbody>
                                                    <c:forEach var="i" begin="0" end="${allVisiteMediche.size()-1}" step="1">
                                                        <tr>
                                                            <td><c:out value="${allNomiPazientiVisite.get(i)}" /></td>
                                                            <td><c:out value="${allVisiteMediche.get(i).getDataIscrizione()}" /></td>
                                                            <c:choose>
                                                                <c:when test="${allVisiteMediche.get(i).getTerminata() == 0}">
                                                                    <td><c:out value="Da visitare" /></td>
                                                                    <td >
                                                                        <form action="/medicare/Servlet/visualizzaVisitaInfo" method="post">
                                                                            <a class="btn btn-info" href="javascript:;" onclick="parentNode.submit();" >Seleziona</a>
                                                                            <input type="hidden" name="id" value="${allVisiteMediche.get(i).getId()}" />
                                                                        </form>
                                                                    </td>
                                                                </c:when>
                                                                <c:when test="${allVisiteMediche.get(i).getTerminata() == 1}">
                                                                    <td><c:out value="In attesa di esame specialistico" /></td>
                                                                    <td>
                                                                        <form action="/medicare/Servlet/visualizzaVisitaInfo" method="post">
                                                                            <a class="btn btn-info" href="javascript:;" onclick="parentNode.submit();">Seleziona</a>
                                                                            <input type="hidden" name="id" value="${allVisiteMediche.get(i).getId()}" />
                                                                        </form>
                                                                    </td>
                                                                </c:when>
                                                                <c:when test="${allVisiteMediche.get(i).getTerminata() == 2}">
                                                                    <td><c:out value="Esami specialistici completati" /></td>
                                                                    <td>
                                                                        <form action="/medicare/Servlet/visualizzaVisitaInfo" method="post">
                                                                            <a class="btn btn-info" href="javascript:;" onclick="parentNode.submit();">Seleziona</a>
                                                                            <input type="hidden" name="id" value="${allVisiteMediche.get(i).getId()}" />
                                                                        </form>
                                                                    </td>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <td><c:out value="Terminata" /></td>
                                                                    <td>
                                                                        <form action="/medicare/Servlet/visualizzaVisitaInfo" method="post">
                                                                            <a class="btn btn-info" href="#" onclick="parentNode.submit();">Seleziona</a>
                                                                            <input type="hidden" name="id" value="${allVisiteMediche.get(i).getId()}" />
                                                                        </form>
                                                                    </td>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </tr>                
                                                    </c:forEach>
                                                </tbody>
                                                <!--Table body-->
                                            </table>
                                        </div>

                                    </c:when>
                                    <c:otherwise>
                                        <div class="alert alert-info text-center w-50" style="margin: auto; vertical-align: middle;">
                                            Nessuna visita disponibile.
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
            </section>
        </main>


        <hr/>
        <footer class="page-footer light">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h5>About Us</h5>
                        <ul>
                            <li><a href="#">Gruppo gitLab MediCare</a></li>
                            <li><a href="#">UniTN project</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Capacità</h5>
                        <ul>
                            <li><a href="#">150+ esami prenotabili</a></li>
                            <li><a href="#">800+ medicinali prescrivibili</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Caratteristiche</h5>
                        <ul>
                            <li><a href="#">Veloce</a></li>
                            <li><a href="#">Affidabile</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Sicurezze</h5>
                        <ul>
                            <li><a href="#">Solo medici preparati</a></li>
                            <li><a href="#">Solo gli specialisti migliori</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <p>© 2020 Tutti i diritti sono riservati al gruppo MediCare e a UNITN</p>
            </div>
        </footer>
    </body>
</html>
