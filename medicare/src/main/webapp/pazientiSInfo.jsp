<%-- 
    Document   : homeMedician
    Created on : 20 nov 2019, 11:42:44
    Author     : utente
--%>


<%@page import="persistence.beans.TicketBean"%>
<%@page import="persistence.beans.AnamnesiBean"%>
<%@page import="persistence.beans.MedicinalePrescrittoBean"%>
<%@page import="persistence.beans.EsamePrescrittoBean"%>
<%@page import="persistence.beans.VisitaMedicaBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="java.util.ArrayList"%>
<%@page import="persistence.beans.PazienteBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% PazienteBean pazienteInfo = (PazienteBean) session.getAttribute("pazienteInfo");%>
<%  
    ArrayList<VisitaMedicaBean> visiteUtente;
    ArrayList<EsamePrescrittoBean> esamiUtente;
    ArrayList<String> nomiEsamiUtente;
    ArrayList<String> statiEsamiUtente;
    ArrayList<MedicinalePrescrittoBean> medicinaliUtente;
    ArrayList<String> nomiMedicinaliUtente;
    ArrayList<AnamnesiBean> anamnesiUtente;
    ArrayList<TicketBean> ticketsUtente;
    ArrayList<String> dateTicketsUtente;
    ArrayList<String> nomiMVisite;
    ArrayList<PazienteBean> pazienti = (ArrayList<PazienteBean>) session.getAttribute("pazienti");
    int switchState = (int)session.getAttribute("switchState");
    
    if(pazienteInfo != null) {
        visiteUtente = (ArrayList<VisitaMedicaBean>) session.getAttribute("visiteUtente");
        esamiUtente = (ArrayList<EsamePrescrittoBean>) session.getAttribute("esamiUtente");
        nomiEsamiUtente = (ArrayList<String>) session.getAttribute("nomiEsamiUtente");
        statiEsamiUtente = (ArrayList<String>) session.getAttribute("statiEsamiUtente");
        medicinaliUtente = (ArrayList<MedicinalePrescrittoBean>) session.getAttribute("medicinaliUtente");
        nomiMedicinaliUtente = (ArrayList<String>) session.getAttribute("nomiMedicinaliUtente");
        anamnesiUtente = (ArrayList<AnamnesiBean>) session.getAttribute("anamnesiUtente");
        ticketsUtente = (ArrayList<TicketBean>) session.getAttribute("ticketsUtente");
        dateTicketsUtente = (ArrayList<String>) session.getAttribute("dateTicketsUtente");
        nomiMVisite = (ArrayList<String>) session.getAttribute("nomiMVisite");
    }
%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>MediCare</title>

        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/fonts/simple-line-icons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/smoothproducts.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">


        <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/smoothproducts.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/theme.js"></script>
        <script src="https://use.fontawesome.com/50f3e3804d.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    </head>

    <body>
        <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
            <div class="container-fluid">
                <a class="navbar-brand logo" href="#">
                    <h3 class="font-weight-bolder text-primary" style="text-shadow: 2px 2px 2px rgb(25, 255, 180);"> 
                        <img class="" width="40" height="30" src="${pageContext.request.contextPath}/assets/logo/logo.png"/>MediCare
                    </h3>
                </a>
                <button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon"></span>
                </button>
		<div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav ml-auto">
			<li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/homeSpecialist.jsp">Home</a></li>
			<li class="nav-item" role="presentation"><a class="nav-link active" href="${pageContext.request.contextPath}/pazientiSInfo.jsp">I miei pazienti</a></li>
			<li class="nav-item" role="presentation"><a class="nav-link" href="/medicare/Servlet/caricaEsami">Esami</a></li>
			<li class="nav-item dropdown" role="presentation">
                            <a class="nav-link dropdown-toggle" id="dropdownmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
				<i class="fa fa-user fa-1x"></i> Profilo 
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="dropdownmenu">
				<a class="dropdown-item" href="/medicare/Servlet/pazienteData">Il mio account</a>
				<a class="dropdown-item" href="/medicare/Servlet/logout">Log out</a>
                            </div>
                        </li>
                        <c:choose>
                            <c:when test="${switchState == 0}">
                                <li class="nav-item" role="presentation">
                                    <form action="/medicare/Servlet/switchState" method="get">
                                        <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Paziente</a>
                                    </form>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="nav-item" role="presentation">
                                    <form action="/medicare/Servlet/switchState" method="get">
                                        <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Specialista</a>
                                    </form>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </ul>
		</div>
            </div>
	</nav>


        <main class="page landing-page">
            <section class="clean-block">
                <div class="container-fluid">
                    <div class="block-heading align-left">
                        <h2 class="text-primary">Informazioni sul paziente</h2>
                    </div>
                    <c:choose>
                        <c:when test="${pazienteInfo != null}">
                            <div class="container-fluid">
                                <div class="row justify-content-center">
                                    <div class="w-50 p-3 border border-info rounded-lg shadow text-center" style="text-align: left; ">
                                        <%
                                            out.write("<p><b>Paziente: </b>" + pazienteInfo.getNome() + " " + pazienteInfo.getCognome() + "</p>");
                                            out.write("<p><b>Luogo nascita:</b> " + pazienteInfo.getLuogoNascita() + "</p>");
                                            out.write("<p><b>Data nascita:</b> " + pazienteInfo.getDataNascita() + "</p>");
                                            out.write("<p><b>Sesso:</b> " + pazienteInfo.getSesso() + "</p>");
                                        %>
                                    </div> 
                                </div>
                            </div>
                            <div class="container-fluid">
                                 <div class="block-heading">
                                <h2 class="text-primary">Visite mediche paziente</h2>
                            </div>         
                                    
                                    <!--Table-->
                                    <div class="container">
                                        <div class="row align-items-center">
                                            <div class="table-responsive">
                                                <!--Table-->
                                                <table id="visiteUtente" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                                    <!--Table head-->
                                                    <thead>
                                                        <tr>
                                                            <th class="th">Id</th>
                                                            <th class="th">Data</th>
                                                            <th class="th">Medico di base</th>
                                                            <th class="th">Stato</th>
                                                        </tr>
                                                    </thead>
                                                    <!--Table head-->
                                                    <!--Table body-->
                                                    <tbody>
                                                        <c:choose>
                                                            <c:when test="${visiteUtente.size() == 0}">

                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:forEach  var="i" begin="0" end="${visiteUtente.size()-1}" step="1" varStatus="loop">
                                                                    <tr>
                                                                        <td><c:out value="${visiteUtente.get(i).getId()}" /></td>
                                                                        <td><c:out value="${visiteUtente.get(i).getDataIscrizione()}" /></td>
                                                                        <td><c:out value="${nomiMVisite.get(i)}" /></td>
                                                                        <td>
                                                                            <c:choose>
                                                                                <c:when test="${visiteUtente.get(i).getTerminata().equals('0')}">
                                                                                    Da visitare
                                                                                </c:when>
                                                                                <c:when test="${visiteUtente.get(i).getTerminata().equals('1')}">
                                                                                    In attesa di esami specialistici
                                                                                </c:when>
                                                                                <c:when test="${visiteUtente.get(i).getTerminata().equals('2')}">
                                                                                    Esami specialistici completati
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    Completata
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </td>
                                                                    </tr>
                                                                </c:forEach>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tbody>
                                                    <!--Table body-->
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Table-->       
                            </div>    
                            <div class="container-fluid">
                                        <div class="block-heading">
                                            <h2 class="text-primary">Esami paziente</h2>
                                        </div>
                                    <!--Table-->
                                    <div class="container">
                                        <div class="row align-items-center">
                                            <div class="table-responsive">
                                                <!--Table-->
                                                <table id="esamiUtente" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                                    <!--Table head-->
                                                    <thead>
                                                        <tr>
                                                            <th class="th">Id</th>
                                                            <th class="th">Data</th>
                                                            <th class="th">Tipo esame</th>
                                                            <th class="th">Terminato</th>
                                                            <th class="th"></th>
                                                        </tr>
                                                    </thead>
                                                    <!--Table head-->
                                                    <!--Table body-->
                                                    <tbody>
                                                        <c:choose>
                                                            <c:when test="${esamiUtente.size() == 0}">

                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:forEach var="i" begin="0" end="${esamiUtente.size()-1}" step="1" varStatus="loop">
                                                                    <tr>
                                                                        <td><c:out value="${esamiUtente.get(i).getId()}" /></td>
                                                                        <td><c:out value="${esamiUtente.get(i).getDataPrenotazione()}" /></td>
                                                                        <td><c:out value="${nomiEsamiUtente.get(i)}" /></td>
                                                                        <td>
                                                                            <c:choose>
                                                                                <c:when test="${statiEsamiUtente.get(i).equals('0')}">
                                                                                    NO
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    SI
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </td>
                                                                        <td>
                                                                            <form action="/medicare/Servlet/visualizzaEsameInfo" method="post">
                                                                                <a class="btn btn-info" href="javascript:;" onclick="parentNode.submit();">Seleziona</a>
                                                                                <input type="hidden" name="id" value="${esamiUtente.get(i).getId()}"/>
                                                                            </form>
                                                                        </td>
                                                                    </tr>
                                                                </c:forEach>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tbody>
                                                    <!--Table body-->
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Table-->
                            </div>
                                    
                            <div class="container-fluid">
                                <div class="block-heading">
                                    <h2 class="text-primary">Medicinali prescritti</h2>
                                </div>
                                <!--Table-->
                                <div class="container">
                                    <div class="row align-items-center">
                                        <div class="table-responsive">
                                            <!--Table-->
                                            <table id="medicinaliUtente" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                                <!--Table head-->
                                                <thead>
                                                    <tr>
                                                        <th class="th">Id</th>
                                                        <th class="th">Data rilascio</th>
                                                        <th class="th">Medicinale</th>
                                                    </tr>
                                                </thead>
                                                <!--Table head-->
                                                <!--Table body-->
                                                <tbody>
                                                    <c:choose>
                                                        <c:when test="${medicinaliUtente.size() == 0}">

                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:forEach var="i" begin="0" end="${medicinaliUtente.size()-1}" step="1" varStatus="loop">
                                                                <tr>
                                                                    <td><c:out value="${medicinaliUtente.get(i).getId()}" /></td>
                                                                    <td><c:out value="${medicinaliUtente.get(i).getDataRilascio()}" /></td>
                                                                    <td><c:out value="${nomiMedicinaliUtente.get(i)}" /></td>
                                                                </tr>
                                                            </c:forEach>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </tbody>
                                                <!--Table body-->
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--Table-->
                            </div>        
                                    
                                    
                                    
                            <div class="container-fluid">
                                        <div class="block-heading">
                                            <h2 class="text-primary">Anamnesi paziente</h2>
                                        </div>
                                    <!--Table-->
                                    <div class="container">
                                        <div class="row align-items-center">
                                            <div class="table-responsive">
                                                <!--Table-->
                                                <table id="anamnesiUtente" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                                    <!--Table head-->
                                                    <thead>
                                                        <tr>
                                                            <th class="th">Id</th>
                                                            <th class="th">Data</th>
                                                            <th class="th">Referto</th>
                                                        </tr>
                                                    </thead>
                                                    <!--Table head-->
                                                    <!--Table body-->
                                                    <tbody>
                                                        <c:choose>
                                                            <c:when test="${anamnesiUtente.size() == 0}">

                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:forEach var="anamnesi" items="${anamnesiUtente}">
                                                                    <tr>
                                                                        <td><c:out value="${anamnesi.getId()}" /></td>
                                                                        <td><c:out value="${anamnesi.getDataRilascio()}" /></td>
                                                                        <td><c:out value="${anamnesi.getDescrizione()}" /></td>
                                                                    </tr>
                                                                </c:forEach>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tbody>
                                                    <!--Table body-->
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Table-->
                                    </div>
                                    <div class="container-fluid">
                                        <div class="block-heading">
                                            <h2 class="text-primary">Tickets utente</h2>
                                        </div>
                                    <!--Table-->
                                    <div class="container">
                                        <div class="row align-items-center">
                                            <div class="table-responsive">
                                                <!--Table-->
                                                <table id="ticketsUtente" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                                    <!--Table head-->
                                                    <thead>
                                                        <tr>
                                                            <th class="th">Id</th>
                                                            <th class="th">Data</th>
                                                            <th class="th">Costo</th>
                                                        </tr>
                                                    </thead>
                                                    <!--Table head-->
                                                    <!--Table body-->
                                                    <tbody>
                                                        <c:choose>
                                                            <c:when test="${ticketsUtente.size() == 0}">

                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:forEach var="i" begin="0" end="${ticketsUtente.size()-1}" step="1">
                                                                    <tr>
                                                                        <td><c:out value="${ticketsUtente.get(i).getId()}" /></td>
                                                                        <td><c:out value="${dateTicketsUtente.get(i)}" /></td>
                                                                        <td><c:out value="${ticketsUtente.get(i).getCosto()}" /></td>
                                                                    </tr>
                                                                </c:forEach>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tbody>
                                                    <!--Table body-->
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Table-->
                                    </div>
                                    <div class="row p-5 justify-content-center">
                                        <form action="/medicare/Servlet/closeSchedaIntera" method="get">
                                            <a class="btn btn-danger" href="javascript:;" onclick="parentNode.submit();">Chiudi scheda paziente </a>
                                        </form>
                                    </div>   

                        </c:when>
                        <c:otherwise>
                            <div class="alert alert-info text-center w-50" style="margin: auto; vertical-align: middle;">
                                Nessun paziente selezionato
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </section>
            
            
            <section class="clean-block" style="background-color: #007bff">
                <div class="container-fluid">
                    <div class="block-heading">
                        <h2 class="text-light">Scegli un altro paziente</h2>
                    </div>
                    <div class="container-fluid bg-white p-3 border border-primary rounded-lg">
                        <div class="row m-1 align-items-center">
                            <div class="table-responsive">
                            <!--Table-->
                            <table id="tabellaParcoPazienti" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                <!--Table head-->
                                <thead>
                                    <tr>
                                        <th class="th">Cf</th>
                                        <th class="th">Nome</th>
                                        <th class="th">Cognome</th>
                                        <th class="th">Data di nascita</th>
                                        <th class="th">Luogo di nascita</th>
                                        <th class="th">Sesso</th>
                                        <th class="th">Email</th>
                                        <th class="th"></th>
                                    </tr>
                                </thead>
                                <!--Table head-->
                                <!--Table body-->
                                <tbody>
                                    <c:forEach var="i" begin="0" end="${pazienti.size()-1}" step="1" varStatus="loop">
                                        <tr>
                                            <td><c:out value="${pazienti.get(i).getCf()}" /></td>
                                            <td><c:out value="${pazienti.get(i).getNome()}" /></td>
                                            <td><c:out value="${pazienti.get(i).getCognome()}" /></td>
                                            <td><c:out value="${pazienti.get(i).getDataNascita()}" /></td>
                                            <td><c:out value="${pazienti.get(i).getLuogoNascita()}" /></td>
                                            <td><c:out value="${pazienti.get(i).getSesso()}" /></td>
                                            <td><c:out value="${pazienti.get(i).getEmail()}" /></td>
                                            <td>
                                                <form action="/medicare/Servlet/getInfoPazienteForS" method="post">
                                                    <a id="selezionaMB" class="btn btn-info" href="#" onclick="parentNode.submit();">Seleziona</a>
                                                    <input type="hidden" name="cf" value="${pazienti.get(i).getCf()}" />
                                                </form>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                                <!--Table body-->
                            </table>
                        </div>
                    </div>
                    <!--Table-->
                    </div>
                </div>
            </section>
        </main>


        <hr/>
        <footer class="page-footer light">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h5>About Us</h5>
                        <ul>
                            <li><a href="#">Gruppo gitLab MediCare</a></li>
                            <li><a href="#">UniTN project</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Capacità</h5>
                        <ul>
                            <li><a href="#">150+ esami prenotabili</a></li>
                            <li><a href="#">800+ medicinali prescrivibili</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Caratteristiche</h5>
                        <ul>
                            <li><a href="#">Veloce</a></li>
                            <li><a href="#">Affidabile</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Sicurezze</h5>
                        <ul>
                            <li><a href="#">Solo medici preparati</a></li>
                            <li><a href="#">Solo gli specialisti migliori</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <p>© 2020 Tutti i diritti sono riservati al gruppo MediCare e a UNITN</p>
            </div>
        </footer>
    </body>
</html>
