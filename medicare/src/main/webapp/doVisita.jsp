<%-- 
    Document   : doVisita
    Created on : 27 nov 2019, 10:10:54
    Author     : utente
--%>

<%@page import="persistence.beans.VisitaSpecialisticaBean"%>
<%@page import="persistence.beans.EsamePrescrittoBean"%>
<%@page import="persistence.beans.EsameBean"%>
<%@page import="persistence.beans.MedicinalePrescrittoBean"%>
<%@page import="persistence.beans.MedicinaleBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="persistence.beans.VisitaMedicaBean"%>
<%@page import="persistence.beans.PazienteBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<% PazienteBean pazVisita = (PazienteBean) session.getAttribute("pazienteVisitaInstance");%>
<% VisitaMedicaBean visita = (VisitaMedicaBean) session.getAttribute("visitaInstance");%>
<% ArrayList<MedicinaleBean> listMedicinali = (ArrayList<MedicinaleBean>) session.getAttribute("listMedicinali");%>
<% ArrayList<MedicinalePrescrittoBean> listMedicinaliPrescTemp = (ArrayList<MedicinalePrescrittoBean>) session.getAttribute("tempMedicinaliPrescritti");%>
<% ArrayList<EsameBean> listEsamiSpecialistici = (ArrayList<EsameBean>) session.getAttribute("listEsamiSpecialistici");%>
<% ArrayList<PazienteBean> listMedSpecialisti = (ArrayList<PazienteBean>) session.getAttribute("listMedSpecialisti");%>
<% ArrayList<EsameBean> tempEsami = (ArrayList<EsameBean>) session.getAttribute("tempEsami");%>
<% ArrayList<PazienteBean> tempMS = (ArrayList<PazienteBean>) session.getAttribute("tempMS");%>
<% ArrayList<EsamePrescrittoBean> tempEsamiPrescritti = (ArrayList<EsamePrescrittoBean>) session.getAttribute("tempEsamiPrescritti");%>
<% ArrayList<MedicinaleBean> tempMedicinali = (ArrayList<MedicinaleBean>) session.getAttribute("tempMedicinali");%>
<% boolean isListMSHidden = (boolean) session.getAttribute("isListMSHidden");%>
<%
    boolean isDataEsameHidden = (boolean) session.getAttribute("isDataEsameHidden");
    int switchState = (int) session.getAttribute("switchState");
%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>MediCare</title>

        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/fonts/simple-line-icons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/smoothproducts.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">


        <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/smoothproducts.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/theme.js"></script>
        <script src="https://use.fontawesome.com/50f3e3804d.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

        <!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->
        <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
        <!-- Bootstrap Date-Picker Plugin -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
        <script>
            $(document).ready(function () {
                var date_input = $('input[name="dataEsame"]'); //our date input has the name "date"
                var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                var options = {
                    format: 'mm/dd/yyyy',
                    container: container,
                    todayHighlight: true,
                    autoclose: true
                };
                date_input.datepicker(options);
            })
        </script>
    </head>

    <body>
        <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
            <div class="container-fluid">
                <a class="navbar-brand logo" href="#">
                    <h3 class="font-weight-bolder text-primary" style="text-shadow: 2px 2px 2px rgb(25, 255, 180);"> 
                        <img class="" width="40" height="30" src="${pageContext.request.contextPath}/assets/logo/logo.png"/>MediCare
                    </h3>
                </a>
                <button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/medicare/Servlet/parcoPazienti">Home</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/pazientiMInfo.jsp">I miei pazienti</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/medicare/Servlet/caricaVisite">Visite mediche</a></li>
                        <li class="nav-item dropdown" role="presentation">
                            <a class="nav-link dropdown-toggle" id="dropdownmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                <i class="fa fa-user fa-1x"></i> Profilo 
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="dropdownmenu">
                                <a class="dropdown-item" href="/medicare/Servlet/pazienteData">Il mio account</a>
                                <a class="dropdown-item" href="/medicare/Servlet/logout">Log out</a>
                            </div>
                        </li>
                        <c:choose>
                            <c:when test="${switchState == 0}">
                                <li class="nav-item" role="presentation">
                                    <form action="/medicare/Servlet/switchState" method="get">
                                        <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Paziente</a>
                                    </form>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="nav-item" role="presentation">
                                    <form action="/medicare/Servlet/switchState" method="get">
                                        <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Medico</a>
                                    </form>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </ul>
                </div>
            </div>
        </nav>


        <main class="page landing-page">
            <section class="clean-block clean-info" id="infoVisita">
                <div class="wrapper">
                    <%
                        String error = String.valueOf(request.getParameter("error"));
                        String phrase = "";

                        if (error.equals("1")) {
                            phrase = "<div class='container-fluid'><div class='row'><div class='col- col-sm-12'><div class='w-100 alert alert-danger alert-dismissible fade show' style='position: absolute; left: 0; right: 0;'><button type='button' class='close' data-dismiss='alert'>&times;</button>Errore nel caricamento della visita!</div></div></div></div>";
                            out.println(phrase);
                        } else if (error.equals("2")) {
                            phrase = "<div class='container-fluid'><div class='row'><div class='col- col-sm-12'><div class='w-100 alert alert-danger alert-dismissible fade show' style='position: absolute; left: 0; right: 0;'><button type='button' class='close' data-dismiss='alert'>&times;</button>Errore nel caricamento del paziente!</div></div></div></div>";
                            out.println(phrase);
                        } else if (error.equals("5")) {
                            phrase = "<div class='container-fluid'><div class='row'><div class='col- col-sm-12'><div class='w-100 alert alert-danger alert-dismissible fade show' style='position: absolute; left: 0; right: 0;'><button type='button' class='close' data-dismiss='alert'>&times;</button>Il paziente ha gi� un esame in quella data!</div></div></div></div>";
                            out.println(phrase);
                        } else if (error.equals("7")) {
                            phrase = "<div class='container-fluid'><div class='row'><div class='col- col-sm-12'><div class='w-100 alert alert-danger alert-dismissible fade show' style='position: absolute; left: 0; right: 0;'><button type='button' class='close' data-dismiss='alert'>&times;</button>Non puoi prescrivere pi� di 2 medicinali!</div></div></div></div>";
                            out.println(phrase);
                        }
                    %>
                    <div class="container-fluid">
                        <div class="block-heading">
                            <h1 class="text-primary">Esegui la visita medica:</h1>
                            <h2 class="text-primary">Informazioni sulla visita</h2>
                        </div>
                        <div class="row justify-content-center">
                            <div class="w-50 p-3 border border-info rounded-lg shadow text-center">
                                <%="<p><b>Paziente:</b> " + pazVisita.getNome() + " " + pazVisita.getCognome() + "</p>"%>
                                <%="<p><b>Data inserimento:</b> " + visita.getDataIscrizione() + "</p>"%>
                                <%="<p><b>Stato:</b> da visitare" + "</p>"%>
                            </div>
                        </div>
                    </div>
            </section>

            <c:choose>
                <c:when test="${sessionScope.isDataEsameHidden == false}">
                    <section class="clean-block clean-info" id="aggiungiMedicinale">
                        <div class="container-fluid">
                            <div class="block-heading">
                                <h2 class="text-primary">Aggiungi un medicinale</h2>
                            </div>
                            <div class="container-fluid p-3 border border-primary rounded-lg">
                                <div class="row m-1 justify-content-center text-center">
                                    <div class="col-12 col-sm-12">
                                        <div class="table-responsive">
                                            <!--Table-->
                                            <table id="tabellaMedicinali" class="table table-bordered table-hover " cellspacing="0" width="100%">
                                                <!--Table head-->
                                                <thead>
                                                    <tr>
                                                        <th class="th">Id</th>
                                                        <th class="th">Nome</th>
                                                        <th class="th"></th>
                                                    </tr>
                                                </thead>
                                                <!--Table head-->
                                                <!--Table body-->
                                                <tbody>
                                                    <c:forEach var="med" items="${listMedicinali}">
                                                        <tr>
                                                            <td><c:out value="${med.getId()}" /></td>
                                                            <td><c:out value="${med.getTipoMedicinale()}" /></td>
                                                            <td align="center">
                                                                <form action="/medicare/Servlet/addMedicinalePrescrizione" method="post">
                                                                    <a class="btn btn-info" href="#" onclick="parentNode.submit();">Prescrivi</a>
                                                                    <input type="hidden" name="id" value="${med.getId()}" />
                                                                </form>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                                <!--Table body-->
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </section>
                </c:when>
            </c:choose>

            <section class="clean-block clean-info" id="prenotaEsameSpec">
                <div class="container-fluid">
                    <div class="block-heading">
                        <h2 class="text-primary">Prenota esame specialistico</h2>
                    </div>
                    <div class="container-fluid bg-white p-3 m-3 border border-primary rounded-lg mx-auto">
                        <div class="row m-1 justify-content-center text-center">
                            <div class="col-12 col-sm-12">
                                <c:choose>
                                    <c:when test="${sessionScope.isDataEsameHidden == false}">                                                        
                                        <!-- Form code begins -->

                                        <form method="post" action="/medicare/Servlet/setDataEsame">
                                            <div class="form-group text-center p-2"> <!-- Date input -->
                                                <label class="control-label" for="dataEsame"><h4 class='text-primary'>Inserisci una data per l'esame</h4></label>       
                                            </div>
                                            <div class='form-group p-2'>
                                                <input class="form-control" id="dataEsame" name="dataEsame" placeholder="MM/DD/YYY" type="text"/>
                                            </div>
                                            <div class="form-group p-2"> <!-- Submit button -->
                                                <button class="btn btn-primary btn-block" name="submit" type="submit">Submit</button>
                                            </div>
                                        </form>
                                        <!-- Form code ends -->

                                    </c:when>
                                    <c:otherwise>
                                        <c:choose>
                                            <c:when test="${sessionScope.isListMSHidden == true}">
                                                <div class="table-responsive">
                                                    <!--Table-->
                                                    <table id="tabellaEsamiSpecialistici" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                                        <!--Table head-->
                                                        <thead>
                                                            <tr>
                                                                <th class="th">Id</th>
                                                                <th class="th">Nome</th>
                                                                <th class="th"></th>
                                                            </tr>
                                                        </thead>
                                                        <!--Table head-->
                                                        <!--Table body-->
                                                        <tbody>
                                                            <c:forEach var="es" items="${listEsamiSpecialistici}">
                                                                <tr>
                                                                    <td><c:out value="${es.getId()}" /></td>
                                                                    <td><c:out value="${es.getTipoEsame()}" /></td>
                                                                    <td>
                                                                        <form action="/medicare/Servlet/scegliEsame" method="post">
                                                                            <a class="btn btn-info" href="javascript:;" onclick="parentNode.submit();">Prenota</a>
                                                                            <input type="hidden" name="id" value="${es.getId()}" />
                                                                        </form>
                                                                    </td>
                                                                </tr>
                                                            </c:forEach>
                                                        </tbody>
                                                        <!--Table body-->
                                                    </table>
                                                </div>
                                                <br>
                                                <form action="/medicare/Servlet/annullaPrenotazioneEsame" method="get">
                                                    <a class="btn btn-info" href="javascript:;" onclick="parentNode.submit();">Annulla</a>
                                                </form>
                                            </c:when>
                                            <c:otherwise>
                                                <h3 class="text-primary">Scegli Medico Specialista</h3>
                                                <div class="table-responsive">
                                                    <!--Table-->
                                                    <table id="tabellaMediciSpecialisti" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                                        <!--Table head-->
                                                        <thead>
                                                            <tr>
                                                                <th class="th">Cf</th>
                                                                <th class="th">Nome</th>
                                                                <th class="th">Cognome</th>
                                                                <th class="th">Data di nascita</th>
                                                                <th class="th">Luogo di nascita</th>
                                                                <th class="th">Sesso</th>
                                                                <th class="th">Email</th>
                                                                <th class="th">Residenza</th>
                                                                <th class="th"></th>
                                                            </tr>
                                                        </thead>
                                                        <!--Table head-->
                                                        <!--Table body-->
                                                        <tbody>
                                                            <c:forEach var="med" items="${listMedSpecialisti}">
                                                                <tr>
                                                                    <td><c:out value="${med.getCf()}" /></td>
                                                                    <td><c:out value="${med.getNome()}" /></td>
                                                                    <td><c:out value="${med.getCognome()}" /></td>
                                                                    <td><c:out value="${med.getDataNascita()}" /></td>
                                                                    <td><c:out value="${med.getLuogoNascita()}" /></td>
                                                                    <td><c:out value="${med.getSesso()}" /></td>
                                                                    <td><c:out value="${med.getEmail()}" /></td>
                                                                    <td><c:out value="${med.getResidenza()}" /></td>
                                                                    <td>
                                                                        <form action="/medicare/Servlet/scegliMedicoSpecialista" method="post">
                                                                            <a class="btn btn-info" href="javascript:;" onclick="parentNode.submit();">Seleziona</a>
                                                                            <input type="hidden" name="cf" value="${med.getCf()}" />
                                                                        </form>
                                                                    </td>
                                                                </tr>
                                                            </c:forEach>
                                                        </tbody>
                                                        <!--Table body-->
                                                    </table>
                                                </div>
                                                <form action="/medicare/Servlet/annullaPrenotazioneEsame" method="get">
                                                    <a class="btn btn-info" href="javascript:;" onclick="parentNode.submit();">Annulla</a>
                                                </form>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
            </section>


            <section class='clean-block clean-info'>
                <div class='container-fluid text-center'>
                    <div class="block-heading">
                        <h2 class="text-primary">Riassunto visita</h2>
                    </div>
                    <div class="container-fluid bg-white p-3 m-3 border border-primary rounded-lg mx-auto">
                        <div class="row m-1 p-2 justify-content-center text-center">
                            <div class="col-12 col-sm-12">
                                <%
                                    if (listMedicinaliPrescTemp.size() > 0) {
                                        out.write("<p style='color: #007bff'><b>Medicinali prescritti finora: </b></p>");
                                        out.write("<ul class='list-group'>");
                                        for (int i = 0; i < listMedicinaliPrescTemp.size(); i++) {
                                            out.write("<li class='list-group'><p><b>Nome medicinale:</b>  " + tempMedicinali.get(i).getTipoMedicinale() + "</p></li>");
                                        }
                                        out.write("</ul>");
                                    } else {
                                        out.write("<div class='alert alert-info text-center w-50' style='margin: auto; vertical-align: middle;'>Nessun medicinale prescritto finora!</div><br>");
                                    }

                                    if (tempEsamiPrescritti.size() > 0) {
                                        out.write("<p style='color: #007bff'><b>Esami prescritti finora: </b></p>");
                                        
                                        for (int i = 0; i < tempEsamiPrescritti.size(); i++) {
                                            out.write("<ul class='list-group'>");
                                            out.write("<li class='list-group'><p><b>Tipo esame:</b> " + tempEsami.get(i).getTipoEsame() + "</p></li>");
                                            out.write("<li class='list-group'><p><b>Data:</b> " + tempEsamiPrescritti.get(i).getDataPrenotazione() + "</p></li>");
                                            out.write("<li class='list-group'><p><b>Medico Specialista:</b> " + tempMS.get(i).getNome() + " " + tempMS.get(i).getCognome() + "</p></li>");
                                            out.write("</ul>");
                                            out.write("<br>");
                                        }
                                        
                                    } else {
                                        out.write("<div class='alert alert-info text-center w-50' style='margin: auto; vertical-align: middle;'>Nessun esame prescritto finora!</div>");
                                    }
                                %>
                            </div>
                        </div>
                    </div>
                    <a class="btn btn-primary" href="/medicare/Servlet/confermaVisitaMedica">
                        Conferma ed invia
                    </a>
                    <a class="btn btn-danger" href="/medicare/Servlet/annullaVisitaMedica">
                        Annulla visita
                    </a>
                </div>
            </section>
        </main>


        <footer class="page-footer light">
            <hr/>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h5>About Us</h5>
                        <ul>
                            <li><a href="#">Gruppo gitLab MediCare</a></li>
                            <li><a href="#">UniTN project</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Capacit�</h5>
                        <ul>
                            <li><a href="#">150+ esami prenotabili</a></li>
                            <li><a href="#">800+ medicinali prescrivibili</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Caratteristiche</h5>
                        <ul>
                            <li><a href="#">Veloce</a></li>
                            <li><a href="#">Affidabile</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Sicurezze</h5>
                        <ul>
                            <li><a href="#">Solo medici preparati</a></li>
                            <li><a href="#">Solo gli specialisti migliori</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <p>� 2020 Tutti i diritti sono riservati al gruppo MediCare e a UNITN</p>
            </div>
        </footer>
    </body>
</html>

