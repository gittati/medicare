<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<title>Change Password</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/fonts/simple-line-icons.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/smoothproducts.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

		
	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/smoothproducts.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/theme.js"></script>
	<script src="https://use.fontawesome.com/50f3e3804d.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    </head>

    <body>
        <main class="page login-page">
            <section class="clean-block clean-form dark">            
		<div class="container-fluid">
                    <a class="btn btn-outline-dark" href="${pageContext.request.contextPath}/pazienteData.jsp" style="position: absolute; top: 2%; right: 2%">
                        <i class="fa fa-user-circle fa-2x" style="margin: auto"></i>
                    </a>
                    <div class="block-heading">
			<h2 class="text-info">Cambia Password</h2>
                    </div>
                    <div class="alert alert-danger text-center" style="margin: auto; vertical-align: middle; width: 50%;">
			<% 
                            String error = String.valueOf(request.getParameter("error"));
                            String phrase = "";
					
                            if(error.equals("1")) {
				phrase = "<p>La vecchia password è errata!</p>";
                            } else if(error.equals("2")) {
                                phrase = "<p>La nuova password non coincide!</p>";
                            } else {
				phrase = "<p>Cambia la tua password</p>";
                            }
                            out.println(phrase);
			%>
                    </div>
					
                    <br>

                    <form method="post" action="/medicare/Servlet/changePassword">
			<div class="form-group">
                            <label for="oldPassword">Vecchia password</label>
                            <input class="form-control" name="oldPassword" type="password" id="oldPassword">
                        </div>
			<div class="form-group">
                            <label for="newPassword">Nuova password</label>
                            <input class="form-control" name="newPassword" type="password" id="newPassword">
			</div>
			<div class="form-group">
                            <label for="newPasswordR">Ripeti la nuova password</label>
                            <input class="form-control" name="newPasswordR" type="password" id="newPasswordR">
			</div>
			<button class="btn btn-primary btn-block" type="submit">Conferma</button>
                    </form>
		</div>
            </section>
	</main>
		
	<hr/>
        <footer class="page-footer light">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h5>About Us</h5>
                        <ul>
                            <li><a href="#">Gruppo gitLab MediCare</a></li>
                            <li><a href="#">UniTN project</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Capacità</h5>
                        <ul>
                            <li><a href="#">150+ esami prenotabili</a></li>
                            <li><a href="#">800+ medicinali prescrivibili</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Caratteristiche</h5>
                        <ul>
                            <li><a href="#">Veloce</a></li>
                            <li><a href="#">Affidabile</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Sicurezze</h5>
                        <ul>
                            <li><a href="#">Solo medici preparati</a></li>
                            <li><a href="#">Solo gli specialisti migliori</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
		<p>© 2020 Tutti i diritti sono riservati al gruppo MediCare e a UNITN</p>
            </div>
	</footer>
    </body>
</html>