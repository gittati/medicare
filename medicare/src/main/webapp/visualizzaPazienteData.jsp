<%-- 
    Document   : visualizzaPazienteData
    Created on : 20 nov 2019, 15:39:27
    Author     : (leoberri243@gmail.com)
--%>

<%@page import="persistence.beans.TicketBean"%>
<%@page import="persistence.beans.AnamnesiBean"%>
<%@page import="persistence.beans.VisitaSpecialisticaBean"%>
<%@page import="persistence.beans.MedicinalePrescrittoBean"%>
<%@page import="servlet.UserType"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import = "persistence.beans.VisitaMedicaBean"%>
<%@page import = "persistence.beans.PrescrizioneBean"%>
<%@page import = "persistence.beans.EsamePrescrittoBean"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%
    UserType userType = (UserType) session.getAttribute("userType");
    int switchState = (int) session.getAttribute("switchState");

    ArrayList<VisitaMedicaBean> visiteM = (ArrayList<VisitaMedicaBean>) session.getAttribute("visiteM");
    ArrayList<String> nomiMV = (ArrayList<String>) session.getAttribute("nomiMV");
    ArrayList<MedicinalePrescrittoBean> medicinaliPresc = (ArrayList<MedicinalePrescrittoBean>) session.getAttribute("medicinaliPresc");
    ArrayList<String> nomiMedicinaliP = (ArrayList<String>) session.getAttribute("nomiMedicinaliP");
    ArrayList<EsamePrescrittoBean> esamiPresc = (ArrayList<EsamePrescrittoBean>) session.getAttribute("esamiPresc");
    ArrayList<VisitaSpecialisticaBean> visiteSpec = (ArrayList<VisitaSpecialisticaBean>) session.getAttribute("visiteSpec");
    ArrayList<String> nomiEsamiPresc = (ArrayList<String>) session.getAttribute("nomiEsamiPresc");
    ArrayList<String> nomiSpecialisti = (ArrayList<String>) session.getAttribute("nomiSpecialisti");
    ArrayList<AnamnesiBean> anamnesiList = (ArrayList<AnamnesiBean>) session.getAttribute("anamnesiList");
    ArrayList<TicketBean> ticketList = (ArrayList<TicketBean>) session.getAttribute("ticketList");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Medicare</title>

        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/fonts/simple-line-icons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/smoothproducts.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">


        <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/smoothproducts.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/theme.js"></script>
        <script src="https://use.fontawesome.com/50f3e3804d.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
            <div class="container-fluid">
                <a class="navbar-brand logo" href="#">
                    <h3 class="font-weight-bolder text-primary" style="text-shadow: 2px 2px 2px rgb(25, 255, 180);"> 
                        <img class="" width="40" height="30" src="${pageContext.request.contextPath}/assets/logo/logo.png"/>MediCare
                    </h3>
                </a>
                <button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/home.jsp">Home</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/visitaMedicaInsData.jsp">Appuntamenti</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link active" href="/medicare/Servlet/visualizzaPazienteData">Servizi</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/medicare/Servlet/visualizzaTicket">I miei ticket</a></li>
                        <li class="nav-item dropdown" role="presentation">
                            <a class="nav-link dropdown-toggle" id="dropdownmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                <i class="fa fa-user fa-1x"></i> Profilo 
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="dropdownmenu">
                                <a class="dropdown-item" href="/medicare/Servlet/pazienteData">Il mio account</a>
                                <a class="dropdown-item" href="/medicare/Servlet/logout">Log out</a>
                            </div>
                        </li>
                        <c:choose>
                            <c:when test="${userType != UserType.PAZIENTE}">
                                <c:choose>
                                    <c:when test="${switchState == 0}">
                                        <li class="nav-item" role="presentation">
                                            <form action="/medicare/Servlet/switchState" method="get">
                                                <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Paziente</a>
                                            </form>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="nav-item" role="presentation">
                                            <form action="/medicare/Servlet/switchState" method="get">
                                                <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Medico</a>
                                            </form>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                        </c:choose>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="page landing-page">
            <section class="clean-block clean-info">
                <div class="container-fluid">
                    <div class="block-heading text-center">
                        <h2 class="text-primary">I miei servizi</h2>
                    </div>
                
                <div class="container-fluid bg-white border border-primary rounded-lg shadow">
                    <div class="row p-4 align-items-center">
                        <div class="col-12 col-sm-12">                    
                            <ul class="nav nav-tabs list-inline nav-justified justify-content-center">
                                <li class="nav-item active">
                                    <a class="nav-link" data-toggle="tab" href="#tab1"><b>Visite mediche</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab2"><b>Medicinali</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab3"><b>Esami</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab4"><b>Anamnesi</b></a>
                                </li>
                            </ul>
                            <div class="tab-content p-5" id="myTabContent">

                                <div class="tab-pane container-fluid fade " id="tab1" role="tabpanel" aria-labelledby="tab1-tab">
                                    <!--Table-->
                                    <div class="container-fluid">
                                        <div class="row align-items-center text-center">
                                            <div class="table-responsive">
                                                <!--Table-->
                                                <table id="visiteUtente" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                                    <!--Table head-->
                                                    <thead>
                                                        <tr>
                                                            <th class="th">Id</th>
                                                            <th class="th">Data</th>                                                            
                                                            <th class="th">Medico di base</th>
                                                            <th class="th">Stato</th>
                                                        </tr>
                                                    </thead>
                                                    <!--Table head-->
                                                    <!--Table body-->
                                                    <tbody>
                                                        <c:choose>
                                                            <c:when test="${visiteM.size() == 0}">

                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:forEach var="i" begin="0" end="${visiteM.size()-1}" step="1" varStatus="loop">
                                                                    <tr>
                                                                        <td><c:out value="${visiteM.get(i).getId()}" /></td>
                                                                        <td><c:out value="${visiteM.get(i).getDataIscrizione()}" /></td>
                                                                        <td><c:out value="${nomiMV.get(i)}" /></td>
                                                                        <td>
                                                                            <c:choose>
                                                                                <c:when test="${visiteM.get(i).getTerminata() == 0}">
                                                                                    <c:out value="Da visitare" />
                                                                                </c:when>
                                                                                <c:when test="${visiteM.get(i).getTerminata() == 1}">
                                                                                    <c:out value="In attesa di esami specialistici" />
                                                                                </c:when>
                                                                                <c:when test="${visiteM.get(i).getTerminata() == 2}">
                                                                                    <c:out value="Esami specialistici completati" />
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <c:out value="Completata" />
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </td>
                                                                    </tr>
                                                                </c:forEach>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tbody>
                                                    <!--Table body-->
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Table-->
                                </div>

                                <div class="tab-pane container-fluid fade" id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
                                    <!--Table-->
                                    <div class="container-fluid">
                                        <div class="row align-items-center text-center">
                                            <div class="table-responsive">
                                                <!--Table-->
                                                <table id="medicinaliUtente" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                                    <!--Table head-->
                                                    <thead>
                                                        <tr>
                                                            <th class="th">Id</th>
                                                            <th class="th">Data rilascio</th>
                                                            <th class="th">Medicinale</th>
                                                            <th class="th"></th>
                                                        </tr>
                                                    </thead>
                                                    <!--Table head-->
                                                    <!--Table body-->
                                                    <tbody>
                                                        <c:choose>
                                                            <c:when test="${medicinaliPresc.size() == 0}">

                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:forEach var="i" begin="0" end="${medicinaliPresc.size()-1}" step="1" varStatus="loop">
                                                                    <tr>
                                                                        <td><c:out value="${medicinaliPresc.get(i).getId()}" /></td>
                                                                        <td><c:out value="${medicinaliPresc.get(i).getDataRilascio()}" /></td>
                                                                        <td><c:out value="${nomiMedicinaliP.get(i)}" /></td>
                                                                        <td>
                                                                            <form action="/medicare/Servlet/ricettaFarmaceuticaPdf" method="post">
                                                                                <a id="pdfVT" class="btn btn-info" href="javascript:;" onclick="parentNode.submit();" target='_blank'>PDF Ricetta</a>
                                                                                <input type="hidden" name="medicinalePrescrittoId" value="${medicinaliPresc.get(i).getId()}" />
                                                                            </form>
                                                                        </td>
                                                                    </tr>
                                                                </c:forEach>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tbody>
                                                    <!--Table body-->
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Table-->
                                </div>

                                <div class="tab-pane container-fluid fade" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">
                                    <!--Table-->
                                    <div class="container-fluid">
                                        <div class="row align-items-center text-center">
                                            <div class="table-responsive">
                                                <!--Table-->
                                                <table id="esamiUtente" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                                    <!--Table head-->
                                                    <thead>
                                                        <tr>
                                                            <th class="th">Id</th>
                                                            <th class="th">Data</th>
                                                            <th class="th">Tipo esame</th>
                                                            <th class="th">Medico Specialista</th>
                                                            <th class="th">Terminato</th>
                                                        </tr>
                                                    </thead>
                                                    <!--Table head-->
                                                    <!--Table body-->
                                                    <tbody>
                                                        <c:choose>
                                                            <c:when test="${esamiPresc.size() == 0}">

                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:forEach var="i" begin="0" end="${esamiPresc.size()-1}" step="1" varStatus="loop">
                                                                    <tr>
                                                                        <td><c:out value="${esamiPresc.get(i).getId()}" /></td>
                                                                        <td><c:out value="${esamiPresc.get(i).getDataPrenotazione()}" /></td>
                                                                        <td><c:out value="${nomiEsamiPresc.get(i)}" /></td>
                                                                        <td><c:out value="${nomiSpecialisti.get(i)}" /></td>
                                                                        <td>
                                                                            <c:choose>
                                                                                <c:when test="${visiteSpec.get(i).getTerminata().equals('0')}">
                                                                                    NO
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    SI
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </td>
                                                                    </tr>
                                                                </c:forEach>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tbody>
                                                    <!--Table body-->
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Table-->
                                </div>

                                <div class="tab-pane container-fluid fade" id="tab4" role="tabpanel" aria-labelledby="tab4-tab">
                                    <!--Table-->
                                    <div class="container-fluid">
                                        <div class="row align-items-center text-center">
                                            <div class="table-responsive">
                                                <!--Table-->
                                                <table id="anamnesiUtente" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                                    <!--Table head-->
                                                    <thead>
                                                        <tr>
                                                            <th class="th">Id</th>
                                                            <th class="th">Data</th>
                                                            <th class="th">Referto</th>
                                                            <th class="th">Stato ticket</th>
                                                        </tr>
                                                    </thead>
                                                    <!--Table head-->
                                                    <!--Table body-->
                                                    <tbody>
                                                        <c:choose>
                                                            <c:when test="${anamnesiList.size() == 0}">

                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:forEach var="i" begin="0" end="${anamnesiList.size()-1}" step="1" varStatus="loop">
                                                                    <tr>
                                                                        <td><c:out value="${anamnesiList.get(i).getId()}" /></td>
                                                                        <td><c:out value="${anamnesiList.get(i).getDataRilascio()}" /></td>
                                                                        <td><c:out value="${anamnesiList.get(i).getDescrizione()}" /></td>
                                                                        <td>
                                                                            <c:choose>
                                                                                <c:when test="${ticketList.get(i).getPagato().equals('0')}">
                                                                                    Ticket da pagare
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    Ticket pagato
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </td>
                                                                    </tr>
                                                                </c:forEach>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tbody>
                                                    <!--Table body-->
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Table-->
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>
                    </div>
            </section>
        </main>

        <hr/>
        <footer class="page-footer light">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h5>About Us</h5>
                        <ul>
                            <li><a href="#">Gruppo gitLab MediCare</a></li>
                            <li><a href="#">UniTN project</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Capacità</h5>
                        <ul>
                            <li><a href="#">150+ esami prenotabili</a></li>
                            <li><a href="#">800+ medicinali prescrivibili</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Caratteristiche</h5>
                        <ul>
                            <li><a href="#">Veloce</a></li>
                            <li><a href="#">Affidabile</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Sicurezze</h5>
                        <ul>
                            <li><a href="#">Solo medici preparati</a></li>
                            <li><a href="#">Solo gli specialisti migliori</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <p>© 2020 Tutti i diritti sono riservati al gruppo MediCare e a UNITN</p>
            </div>
        </footer>
    </body>
</html>