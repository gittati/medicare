<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>MediCare</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/fonts/simple-line-icons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/smoothproducts.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">


        <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/smoothproducts.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/theme.js"></script>
        <script src="https://use.fontawesome.com/50f3e3804d.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
        <script>
            $(document).ready(function () {
                var date_input = $('input[name="data"]'); //our date input has the name "date"
                var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                var options = {
                    format: 'mm/dd/yyyy',
                    container: container,
                    todayHighlight: true,
                    autoclose: true
                };
                date_input.datepicker(options);
            })
        </script>
    </head>

    <body>
        <nav class="navbar navbar-light bg-white fixed-top clean-navbar">
            <div class="container-fluid">
                <a id="logoIndex" class="navbar-brand logo" href="#">
                    <h3 class="font-weight-bolder text-primary" style="text-shadow: 2px 2px 2px rgb(25, 255, 180);"> 
                        <img class="" width="40" height="30" src="${pageContext.request.contextPath}/assets/logo/logo.png"/>MediCare
                    </h3>
                </a>
                <span class="navbar-text text-primary">
                    Servizio Sanitario Provinciale: Provincia
                </span>
                <span class="navbar-link">
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/index.jsp">LOGOUT</a>
                </span>
            </div>
        </nav>


        <main class="page landing-page">
            <section class="clean-block clean-info">
                <div class="container-fluid">
                    <div class="block-heading">
                        <h2 class="text-primary">Scegli la data</h2>
                    </div>
                    <div class="container-fluid bg-white p-3 m-3 border border-primary rounded-lg mx-auto">
                        <div class="row m-1 justify-content-center text-center">
                            <div class="col-12 col-sm-12">
                                <c:choose>
                                    <c:when test="${sessionScope.isDataEsameHidden == false}">                                                        
                                        <!-- Form code begins -->
                                        <form method="post" action="">
                                            <div class="form-group text-center p-2"> <!-- Date input -->
                                                <label class="control-label" for="data"></label>       
                                            </div>
                                            <div class='form-group p-2'>
                                                <input class="form-control" id="data" name="data" placeholder="MM/DD/YYY" type="text"/>
                                            </div>
                                            <div class="form-group p-2"> <!-- Submit button -->
                                                <button class="btn btn-primary btn-block" name="submit" type="submit">Submit</button>
                                            </div>
                                        </form>
                                        <!-- Form code ends -->
                                    </c:when>
                                    </choose>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="clean-block clean-info">

            </section>
        </main>


        <hr/>
        <footer class="page-footer light">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h5>About Us</h5>
                        <ul>
                            <li><a href="#">Gruppo gitLab MediCare</a></li>
                            <li><a href="#">UniTN project</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Capacità</h5>
                        <ul>
                            <li><a href="#">150+ esami prenotabili</a></li>
                            <li><a href="#">800+ medicinali prescrivibili</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Caratteristiche</h5>
                        <ul>
                            <li><a href="#">Veloce</a></li>
                            <li><a href="#">Affidabile</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Sicurezze</h5>
                        <ul>
                            <li><a href="#">Solo medici preparati</a></li>
                            <li><a href="#">Solo gli specialisti migliori</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <p>© 2020 Tutti i diritti sono riservati al gruppo MediCare e a UNITN</p>
            </div>
        </footer>
    </body>
</html>