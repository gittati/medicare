<%@page import="persistence.beans.MedicinalePrescrittoBean"%>
<%@page import="persistence.beans.VisitaSpecialisticaBean"%>
<%@page import="persistence.beans.EsamePrescrittoBean"%>
<%@page import="persistence.beans.VisitaMedicaBean"%>
<%@page import="persistence.beans.CittaBean"%>
<%@page import="persistence.beans.ProvinciaBean"%>
<%@page import="servlet.UserType"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import= "java.util.ArrayList"%>
<%@page import= "persistence.beans.EsameBean"%>
<%@page import = "persistence.beans.PazienteBean"%>

<%
    ProvinciaBean provincia = (ProvinciaBean) session.getAttribute("provincia");
    ArrayList<CittaBean> city = (ArrayList<CittaBean>) session.getAttribute("city");
    ArrayList<PazienteBean> listIscritti = (ArrayList<PazienteBean>) session.getAttribute("listIscritti");
    ArrayList<PazienteBean> listMedici = (ArrayList<PazienteBean>) session.getAttribute("listMedici");
    ArrayList<PazienteBean> listSpecialisti = (ArrayList<PazienteBean>) session.getAttribute("listSpecialisti");
    boolean isAbilitato = (boolean) session.getAttribute("isAbilitato");

    ArrayList<MedicinalePrescrittoBean> medicinaliS = (ArrayList<MedicinalePrescrittoBean>) session.getAttribute("medicinaliS");
    ArrayList<String> nomiMedicinaliS = (ArrayList<String>) session.getAttribute("nomiMedicinaliS");
    ArrayList<String> nomiPazS = (ArrayList<String>) session.getAttribute("nomiPazS");
    ArrayList<String> nomiMedS = (ArrayList<String>) session.getAttribute("nomiMedS");

    ArrayList<EsamePrescrittoBean> esamiPrescrittiS = (ArrayList<EsamePrescrittoBean>) session.getAttribute("esamiPrescrittiS");
    ArrayList<String> nomiEsamiS = (ArrayList<String>) session.getAttribute("nomiEsamiS");
    ArrayList<VisitaSpecialisticaBean> visiteSpecS = (ArrayList<VisitaSpecialisticaBean>) session.getAttribute("visiteSpecS");
    ArrayList<String> nomiPazS2 = (ArrayList<String>) session.getAttribute("nomiPazS2");
    ArrayList<String> nomiSPecS = (ArrayList<String>) session.getAttribute("nomiSPecS");
%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>MediCare</title>

        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/fonts/simple-line-icons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/smoothproducts.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

        <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/smoothproducts.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/theme.js"></script>
        <script src="https://use.fontawesome.com/50f3e3804d.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

        <!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->
        <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
        <!-- Bootstrap Date-Picker Plugin -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
        <script>
            $(document).ready(function () {
                var date_input = $('input[name="dataScelta"]'); //our date input has the name "date"
                var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                var options = {
                    format: 'mm/dd/yyyy',
                    container: container,
                    todayHighlight: true,
                    autoclose: true
                };
                date_input.datepicker(options);
            });
        </script>
    </head>

    <body>
        <nav class="navbar navbar-light bg-white fixed-top clean-navbar">
            <div class="container-fluid">
                <a id="logoIndex" class="navbar-brand logo" href="#">
                    <h3 class="font-weight-bolder text-primary" style="text-shadow: 2px 2px 2px rgb(25, 255, 180);"> 
                        <img class="" width="40" height="30" src="${pageContext.request.contextPath}/assets/logo/logo.png"/>MediCare
                    </h3>
                </a>
                <span class="navbar-text text-primary">
                    Servizio Sanitario Provinciale: <c:out value="${provincia.getNome()} ( ${provincia.getAbbr()} )"/>
                </span>
                <span class="navbar-link">
                    <a class="btn btn-primary" href="/medicare/Servlet/logoutSSN">LOGOUT</a>
                </span>
            </div>
        </nav>


        <main class="page landing-page">
            <section class="clean-block clean-info">
                <div class="container-fluid">
                    <div class="block-heading">
                        <h2 class="text-primary">Città collegate alla provincia</h2>
                    </div>
                    <div class="container-fluid bg-white p-3 m-3 border border-primary rounded-lg mx-auto">
                        <div class="row m-1 justify-content-center text-center">
                            <ul class="list-group">
                                <c:forEach  var="i" begin="0" end="${city.size()-1}" step="1" varStatus="loop">
                                    <li class="list-group-item">
                                        <c:out value="${city.get(i).getNome()}"/>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>


            <section class="clean-block clean-info">
                <div class="container-fluid">
                    <div class="block-heading">
                        <h2 class="text-primary">Utenti iscritti</h2>
                    </div>
                    <div class="container-fluid bg-white p-3 m-3 border border-primary rounded-lg mx-auto">
                        <div class="row m-1 justify-content-center text-center">
                            <div class="table-responsive">
                                <!--Table-->
                                <table id="tabellaParcoPazienti1" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                    <!--Table head-->
                                    <thead>
                                        <tr>
                                            <th class="th">Cf</th>
                                            <th class="th">Nome</th>
                                            <th class="th">Cognome</th>
                                            <th class="th">Data di nascita</th>
                                            <th class="th">Luogo di nascita</th>
                                            <th class="th">Sesso</th>
                                            <th class="th">Email</th>
                                        </tr>
                                    </thead>
                                    <!--Table head-->
                                    <!--Table body-->
                                    <tbody>
                                        <c:forEach var="i" begin="0" end="${listIscritti.size()-1}" step="1" varStatus="loop">
                                            <tr>
                                                <td><c:out value="${listIscritti.get(i).getCf()}" /></td>
                                                <td><c:out value="${listIscritti.get(i).getNome()}" /></td>
                                                <td><c:out value="${listIscritti.get(i).getCognome()}" /></td>
                                                <td><c:out value="${listIscritti.get(i).getDataNascita()}" /></td>
                                                <td><c:out value="${listIscritti.get(i).getLuogoNascita()}" /></td>
                                                <td><c:out value="${listIscritti.get(i).getSesso()}" /></td>
                                                <td><c:out value="${listIscritti.get(i).getEmail()}" /></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                    <!--Table body-->
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--Table-->
                </div>
            </section>


            <section class="clean-block clean-info">
                <div class="container-fluid">
                    <div class="block-heading">
                        <h2 class="text-primary">Medici di base iscritti</h2>
                    </div>
                    <div class="container-fluid bg-white p-3 m-3 border border-primary rounded-lg mx-auto">
                        <div class="row m-1 justify-content-center text-center">                            <div class="table-responsive">
                                <!--Table-->
                                <table id="tabellaParcoPazienti2" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                    <!--Table head-->
                                    <thead>
                                        <tr>
                                            <th class="th">Cf</th>
                                            <th class="th">Nome</th>
                                            <th class="th">Cognome</th>
                                            <th class="th">Data di nascita</th>
                                            <th class="th">Luogo di nascita</th>
                                            <th class="th">Sesso</th>
                                            <th class="th">Email</th>
                                        </tr>
                                    </thead>
                                    <!--Table head-->
                                    <!--Table body-->
                                    <tbody>
                                        <c:forEach var="i" begin="0" end="${listMedici.size()-1}" step="1" varStatus="loop">
                                            <tr>
                                                <td><c:out value="${listMedici.get(i).getCf()}" /></td>
                                                <td><c:out value="${listMedici.get(i).getNome()}" /></td>
                                                <td><c:out value="${listMedici.get(i).getCognome()}" /></td>
                                                <td><c:out value="${listMedici.get(i).getDataNascita()}" /></td>
                                                <td><c:out value="${listMedici.get(i).getLuogoNascita()}" /></td>
                                                <td><c:out value="${listMedici.get(i).getSesso()}" /></td>
                                                <td><c:out value="${listMedici.get(i).getEmail()}" /></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                    <!--Table body-->
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--Table-->
                </div>
            </section>


            <section class="clean-block clean-info">
                <div class="container-fluid">
                    <div class="block-heading">
                        <h2 class="text-primary">Medici specialisti nel territorio</h2>
                    </div>
                    <div class="container-fluid bg-white p-3 m-3 border border-primary rounded-lg mx-auto">
                        <div class="row m-1 justify-content-center text-center">                            <div class="table-responsive">
                                <!--Table-->
                                <table id="tabellaParcoPazienti3" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                    <!--Table head-->
                                    <thead>
                                        <tr>
                                            <th class="th">Cf</th>
                                            <th class="th">Nome</th>
                                            <th class="th">Cognome</th>
                                            <th class="th">Data di nascita</th>
                                            <th class="th">Luogo di nascita</th>
                                            <th class="th">Sesso</th>
                                            <th class="th">Email</th>
                                        </tr>
                                    </thead>
                                    <!--Table head-->
                                    <!--Table body-->
                                    <tbody>
                                        <c:forEach var="i" begin="0" end="${listSpecialisti.size()-1}" step="1" varStatus="loop">
                                            <tr>
                                                <td><c:out value="${listSpecialisti.get(i).getCf()}" /></td>
                                                <td><c:out value="${listSpecialisti.get(i).getNome()}" /></td>
                                                <td><c:out value="${listSpecialisti.get(i).getCognome()}" /></td>
                                                <td><c:out value="${listSpecialisti.get(i).getDataNascita()}" /></td>
                                                <td><c:out value="${listSpecialisti.get(i).getLuogoNascita()}" /></td>
                                                <td><c:out value="${listSpecialisti.get(i).getSesso()}" /></td>
                                                <td><c:out value="${listSpecialisti.get(i).getEmail()}" /></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                    <!--Table body-->
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--Table-->
                </div>
            </section>


            <section class="clean-block clean-info">
                <div class="container-fluid">
                    <c:choose>
                        <c:when test="${!isAbilitato}">                     
                            <div class="block-heading">
                                <h2 class="text-primary">Scegli una data</h2>
                            </div>
                            <div class="container-fluid bg-white p-3 m-3 border border-primary rounded-lg mx-auto">
                                <div class="row m-1 justify-content-center text-center">
                                    <div class="col-12 col-sm-12">
                                        <form method="post" action="/medicare/Servlet/getInfoByDay">

                                            <div class='form-group p-2'>
                                                <input class="form-control" id="date" name="dataScelta" placeholder="MM/DD/YYY" type="text"/>
                                            </div>
                                            <div class="form-group p-2"> <!-- Submit button -->
                                                <button class="btn btn-primary btn-block" name="submit" type="submit">Invia</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </c:when>
                        <c:otherwise>
                            <div class="container-fluid">
                                <div class="block-heading">
                                    <h2 class="text-primary">Medicinali prescritti</h2>
                                </div>
                                <!--Table-->
                                <div class="container-fluid bg-white p-3 m-3 border border-primary rounded-lg mx-auto">
                                    <div class="row m-1 justify-content-center text-center">
                                        <div class="col-12 col-sm-12">
                                            <div class="table-responsive">
                                                <!--Table-->
                                                <table id="visiteUtente2" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                                    <!--Table head-->
                                                    <thead>
                                                        <tr>
                                                            <th class="th">Id</th>
                                                            <th class="th">Data</th>
                                                            <th class="th">Paziente</th>
                                                            <th class="th">Medico di base</th>
                                                            <th class="th">Medicinale</th>
                                                        </tr>
                                                    </thead>
                                                    <!--Table head-->
                                                    <!--Table body-->
                                                    <tbody>
                                                        <c:choose>
                                                            <c:when test="${medicinaliS.size() == 0}">

                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:forEach  var="i" begin="0" end="${medicinaliS.size()-1}" step="1" varStatus="loop">
                                                                    <tr>
                                                                        <td><c:out value="${medicinaliS.get(i).getId()}" /></td>
                                                                        <td><c:out value="${medicinaliS.get(i).getDataRilascio()}" /></td>
                                                                        <td><c:out value="${nomiPazS.get(i)}" /></td>
                                                                        <td><c:out value="${nomiMedS.get(i)}" /></td>
                                                                        <td><c:out value="${nomiMedicinaliS.get(i)}" /></td>
                                                                    </tr>
                                                                </c:forEach>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tbody>
                                                    <!--Table body-->
                                                </table>
                                            </div>
                                        </div>
                                        <!--Table-->
                                    </div>
                                    
                                </div>
                                <div class="row p-5 justify-content-center">
                                        <form action="/medicare/Servlet/caricaXLSInfo" method="get">
                                            <a class="btn btn-primary" href="javascript:;" onclick="parentNode.submit();">Report XLS</a>
                                        </form>
                                    </div>
                            </div>

                            <div class="container-fluid">
                                <div class="block-heading">
                                    <h2 class="text-primary">Esami specialistici</h2>
                                </div>
                                <!--Table-->
                                <div class="container-fluid bg-white p-3 m-3 border border-primary rounded-lg mx-auto">
                                    <div class="row m-1 justify-content-center text-center">
                                        <div class="col-12 col-sm-12">
                                            <div class="table-responsive">
                                                <!--Table-->
                                                <table id="esamiUtente2" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                                    <!--Table head-->
                                                    <thead>
                                                        <tr>
                                                            <th class="th">Id</th>
                                                            <th class="th">Data</th>
                                                            <th class="th">Tipo esame</th>
                                                            <th class="th">Paziente</th>
                                                            <th class="th">Medico Specialista</th>
                                                            <th class="th">Terminato</th>
                                                        </tr>
                                                    </thead>
                                                    <!--Table head-->
                                                    <!--Table body-->
                                                    <tbody>
                                                        <c:choose>
                                                            <c:when test="${esamiPrescrittiS.size() == 0}">

                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:forEach var="i" begin="0" end="${esamiPrescrittiS.size()-1}" step="1" varStatus="loop">
                                                                    <tr>
                                                                        <td><c:out value="${esamiPrescrittiS.get(i).getId()}" /></td>
                                                                        <td><c:out value="${esamiPrescrittiS.get(i).getDataPrenotazione()}" /></td>
                                                                        <td><c:out value="${nomiEsamiS.get(i)}" /></td>
                                                                        <td><c:out value="${nomiPazS2.get(i)}" /></td>
                                                                        <td><c:out value="${nomiSPecS.get(i)}" /></td>
                                                                        <td>
                                                                            <c:choose>
                                                                                <c:when test="${visiteSpecS.get(i).getTerminata().equals('0')}">
                                                                                    NO
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    SI
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </td>
                                                                    </tr>
                                                                </c:forEach>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tbody>
                                                    <!--Table body-->
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="container-fluid">
                                <div class="block-heading">
                                    <h2 class="text-primary">Scegli un'altra data</h2>
                                </div>
                                <div class="container-fluid bg-white p-3 m-3 border border-primary rounded-lg mx-auto">
                                    <div class="row m-1 justify-content-center text-center">
                                        <div class="col-12 col-sm-12">
                                            <form method="post" action="/medicare/Servlet/getInfoByDay">

                                                <div class='form-group p-2'>
                                                    <input class="form-control" id="date" name="dataScelta" placeholder="MM/DD/YYY" type="text"/>
                                                </div>
                                                <div class="form-group p-2"> <!-- Submit button -->
                                                    <button class="btn btn-primary btn-block" name="submit" type="submit">Invia</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>                   
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </section>
        </main>


        <hr/>
        <footer class="page-footer light">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h5>About Us</h5>
                        <ul>
                            <li><a href="#">Gruppo gitLab MediCare</a></li>
                            <li><a href="#">UniTN project</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Capacità</h5>
                        <ul>
                            <li><a href="#">150+ esami prenotabili</a></li>
                            <li><a href="#">800+ medicinali prescrivibili</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Caratteristiche</h5>
                        <ul>
                            <li><a href="#">Veloce</a></li>
                            <li><a href="#">Affidabile</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Sicurezze</h5>
                        <ul>
                            <li><a href="#">Solo medici preparati</a></li>
                            <li><a href="#">Solo gli specialisti migliori</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <p>© 2020 Tutti i diritti sono riservati al gruppo MediCare e a UNITN</p>
            </div>
        </footer>
    </body>
</html>