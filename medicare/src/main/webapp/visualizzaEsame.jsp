<%-- 
    Document   : homeSpecialist
    Created on : 20 nov 2019, 11:43:00
    Author     : utente
--%>

<%@page import="persistence.beans.TicketBean"%>
<%@page import="persistence.beans.AnamnesiBean"%>
<%@page import="persistence.beans.VisitaSpecialisticaBean"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="java.util.ArrayList"%>
<%@page import="persistence.beans.PazienteBean"%>
<%@page import="persistence.beans.VisitaMedicaBean"%>
<%@page import="persistence.beans.EsameBean"%>
<%@page import="persistence.beans.EsamePrescrittoBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    ArrayList<VisitaSpecialisticaBean> allVisiteSpecialistiche = (ArrayList<VisitaSpecialisticaBean>) session.getAttribute("visiteSpecialistiche");
    ArrayList<EsamePrescrittoBean> allEsamiPrescritti = (ArrayList<EsamePrescrittoBean>) session.getAttribute("esamiPrescritti");
    ArrayList<EsameBean> allEsamiSP = (ArrayList<EsameBean>) session.getAttribute("esamiSP");
    ArrayList<String> allPazientiVSNomi = (ArrayList<String>) session.getAttribute("pazientiVSNomi");
    ArrayList<String> allMediciBaseVSNomi = (ArrayList<String>) session.getAttribute("mediciBaseVSNomi");
    int switchState = (int) session.getAttribute("switchState");
    EsamePrescrittoBean theEsameP = (EsamePrescrittoBean) session.getAttribute("theEsameP");

    if (theEsameP != null) {
        String theTipoEsame = (String) session.getAttribute("theTipoEsame");
        VisitaMedicaBean theVisitaM = (VisitaMedicaBean) session.getAttribute("theVisitaM");
        PazienteBean thePaz = (PazienteBean) session.getAttribute("thePaz");
        PazienteBean theMed = (PazienteBean) session.getAttribute("theMed");
        VisitaSpecialisticaBean theVisitaS = (VisitaSpecialisticaBean) session.getAttribute("theVisitaS");
        boolean isConclusa = (boolean) session.getAttribute("isConclusa");
        AnamnesiBean theAnamnesi = (AnamnesiBean) session.getAttribute("theAnamnesi");
        TicketBean theTicket = (TicketBean) session.getAttribute("theTicket");
    }
%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>MediCare</title>

        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/fonts/simple-line-icons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/smoothproducts.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">


        <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/smoothproducts.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/theme.js"></script>
        <script src="https://use.fontawesome.com/50f3e3804d.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    </head>

    <body>
        <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
            <div class="container-fluid">
                <a class="navbar-brand logo" href="#">
                    <h3 class="font-weight-bolder text-primary" style="text-shadow: 2px 2px 2px rgb(25, 255, 180);"> 
                        <img class="" width="40" height="30" src="${pageContext.request.contextPath}/assets/logo/logo.png"/>MediCare
                    </h3>
                </a>
                <button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/homeSpecialist.jsp">Home</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/pazientiSInfo.jsp">I miei pazienti</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link active" href="/medicare/Servlet/caricaEsami">Esami</a></li>
                        <li class="nav-item dropdown" role="presentation">
                            <a class="nav-link dropdown-toggle" id="dropdownmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                <i class="fa fa-user fa-1x"></i> Profilo 
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="dropdownmenu">
                                <a class="dropdown-item" href="/medicare/Servlet/pazienteData">Il mio account</a>
                                <a class="dropdown-item" href="/medicare/Servlet/logout">Log out</a>
                            </div>
                        </li>
                        <c:choose>
                            <c:when test="${switchState == 0}">
                                <li class="nav-item" role="presentation">
                                    <form action="/medicare/Servlet/switchState" method="get">
                                        <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Paziente</a>
                                    </form>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="nav-item" role="presentation">
                                    <form action="/medicare/Servlet/switchState" method="get">
                                        <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Specialista</a>
                                    </form>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="page landing-page">            
            <section class="clean-block">
                <div class="container-fluid">
                    <div class="block-heading">
                        <h2 class="text-primary">Informazioni sull'esame specialistico</h2>
                    </div>
                    <c:choose>
                        <c:when test="${theEsameP != null}">
                            <div class="row justify-content-center">
                                <div class="w-50 p-3 border border-info rounded-lg shadow text-center">
                                    <c:out value="<p><b>Data esame:</b> ${theEsameP.getDataPrenotazione()} </p>" escapeXml="false"/>
                                    <c:out value="<p><b>Tipo esame:</b> ${theTipoEsame}</p>" escapeXml="false"/>
                                    <c:out value="<p>Visita Medica in data ${theVisitaM.getDataIscrizione()} dal medico ${theMed.getNome()} ${theMed.getCognome()} </p>" escapeXml="false"/>
                                    <c:choose>
                                        <c:when test="${isConclusa == false}">
                                            <div class="alert alert-info text-center w-50" style="margin: auto; vertical-align: middle;">
                                                Esame non ancora eseguito.
                                            </div> 
                                        </c:when>
                                        <c:otherwise>
                                            <div class="alert alert-info text-center w-50" style="margin: auto; vertical-align: middle;">
                                                Esame terminato.
                                            </div>
                                            <p class="m-2"><b>Anamnesi: </b></p>
                                            <ul class="list-group">
                                                <li class="list-group-item"><c:out value="<p><b>Data rilascio:</b> ${theAnamnesi.getDataRilascio()}</p>" escapeXml="false"/></li>
                                                <li class="list-group-item"><c:out value="<p><b>Commento:</b> ${theAnamnesi.getDescrizione()}</p>" escapeXml="false"/></li>
                                            </ul>
                                            <p class="m-2"><b>Ticket: </b></p>
                                            <ul class="list-group">
                                                <li class="list-group-item"><c:out value="<p><b>Costo:</b> ${theTicket.getCosto()}</p>" escapeXml="false"/></li>
                                                <li class="list-group-item">
                                                    <c:choose>
                                                        <c:when test="${theTicket.getPagato().equals('0')}">
                                                            <div class="alert alert-warning text-center w-50" style="margin: auto; vertical-align: middle;">
                                                                Non pagato. 
                                                            </div>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <div class="alert alert-success text-center w-50" style="margin: auto; vertical-align: middle;">
                                                                Pagato.
                                                            </div>

                                                        </c:otherwise>
                                                    </c:choose>
                                                </li>
                                            </ul>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="alert alert-info text-center w-50" style="margin: auto; vertical-align: middle;">
                                    Nessun esame specialistico selezionato
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="row p-5 justify-content-center">
                        <form action="/medicare/Servlet/closeEsameScelto" method="get">
                            <a class="btn btn-danger" href="javascript:;" onclick="parentNode.submit();">Chiudi esame</a>
                        </form>
                    </div>
                </div>
            </section>


            <section class="clean-block" style="background-color: #007bff">
                <div class="container-fluid">
                    <div class="block-heading">
                        <h2 class="text-light">Seleziona un'altro esame</h2>
                    </div>
                    <div class="container-fluid bg-white p-3 border border-primary rounded-lg">
                        <div class="row m-1 justify-content-center text-center">
                            <div class="col-12 col-sm-12">
                                <c:choose>
                                    <c:when test="${allVisiteSpecialistiche.size() > 0}">                       
                                        <div class="table-responsive">
                                            <!--Table-->
                                            <table id="tabellaVisiteSpecialistiche" class="table table-bordered table-hover" cellspacing="0" width="100%" >
                                                <!--Table head-->
                                                <thead>
                                                    <tr>
                                                        <th class="th">Paziente</th>
                                                        <th class="th">Medico di base</th>
                                                        <th class="th">Esame</th>
                                                        <th class="th">Data</th>
                                                        <th class="th">Terminato</th>
                                                        <th class="th"></th>
                                                    </tr>
                                                </thead>
                                                <!--Table head-->
                                                <!--Table body-->
                                                <tbody>
                                                    <c:forEach var="i" begin="0" end="${allEsamiPrescritti.size()-1}" step="1">
                                                        <tr>
                                                            <td><c:out value="${allPazientiVSNomi.get(i)}" /></td>
                                                            <td><c:out value="${allMediciBaseVSNomi.get(i)}" /></td>
                                                            <td><c:out value="${allEsamiSP.get(i).getTipoEsame()}" /></td>
                                                            <td><c:out value="${allEsamiPrescritti.get(i).getDataPrenotazione()}" /></td>
                                                            <td><c:out value="${allVisiteSpecialistiche.get(i).getTerminata()}" /></td>
                                                            <td>
                                                                <form action="/medicare/Servlet/visualizzaEsameInfo" method="post">
                                                                    <a class="btn btn-info" href="javascript:;" onclick="parentNode.submit();">Seleziona</a>
                                                                    <input type="hidden" name="id" value="${allEsamiPrescritti.get(i).getId()}"/>
                                                                </form>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                                <!--Table body-->
                                            </table>
                                        </div>
                                    </div>
                                    <!--Table-->
                                </c:when>
                                <c:otherwise>
                                    <div class="alert alert-info text-center w-50" style="margin: auto; vertical-align: middle;">
                                        No visite specialistiche availables
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </section>
        </main>


        <hr/>
        <footer class="page-footer light">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h5>About Us</h5>
                        <ul>
                            <li><a href="#">Gruppo gitLab MediCare</a></li>
                            <li><a href="#">UniTN project</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Capacità</h5>
                        <ul>
                            <li><a href="#">150+ esami prenotabili</a></li>
                            <li><a href="#">800+ medicinali prescrivibili</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Caratteristiche</h5>
                        <ul>
                            <li><a href="#">Veloce</a></li>
                            <li><a href="#">Affidabile</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Sicurezze</h5>
                        <ul>
                            <li><a href="#">Solo medici preparati</a></li>
                            <li><a href="#">Solo gli specialisti migliori</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <p>© 2020 Tutti i diritti sono riservati al gruppo MediCare e a UNITN</p>
            </div>
        </footer>
    </body>
</html>
