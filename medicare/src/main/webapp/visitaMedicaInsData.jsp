<%-- 
    Document   : visitaMedicaInsData
    Created on : 12 nov 2019, 17:49:54
    Author     : (leoberri243@gmail.com)
--%>

<%@page import="servlet.UserType"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    UserType userType = (UserType) session.getAttribute("userType");
    int switchState = (int) session.getAttribute("switchState");
%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>MediCare</title>

        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/fonts/simple-line-icons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/smoothproducts.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

        <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/smoothproducts.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/theme.js"></script>
        <script src="https://use.fontawesome.com/50f3e3804d.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

        <!--  jQuery -->
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
        <!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->
        <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
        <!-- Bootstrap Date-Picker Plugin -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
        <script>
            $(document).ready(function () {
                var date_input = $('input[name="date"]'); //our date input has the name "date"
                var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                var options = {
                    format: 'mm/dd/yyyy',
                    container: container,
                    todayHighlight: true,
                    autoclose: true
                };
                date_input.datepicker(options);
            });
        </script>
    </head>

    <body>
        <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
            <div class="container-fluid">
                <a class="navbar-brand logo" href="#">
                    <h3 class="font-weight-bolder text-primary" style="text-shadow: 2px 2px 2px rgb(25, 255, 180);"> 
                        <img class="" width="40" height="30" src="${pageContext.request.contextPath}/assets/logo/logo.png"/>MediCare
                    </h3>
                </a>
                <button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item" role="presentation"><a class="nav-link" href="${pageContext.request.contextPath}/home.jsp">Home</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link active" href="${pageContext.request.contextPath}/visitaMedicaInsData.jsp">Appuntamenti</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/medicare/Servlet/visualizzaPazienteData">Servizi</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/medicare/Servlet/visualizzaTicket">I miei ticket</a></li>
                        <li class="nav-item dropdown" role="presentation">
                            <a class="nav-link dropdown-toggle" id="dropdownmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                <i class="fa fa-user fa-1x"></i> Profilo 
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="dropdownmenu">
                                <a class="dropdown-item" href="/medicare/Servlet/pazienteData">Il mio account</a>
                                <a class="dropdown-item" href="/medicare/Servlet/logout">Log out</a>
                            </div>
                        </li>
                        <c:choose>
                            <c:when test="${userType != UserType.PAZIENTE}">
                                <c:choose>
                                    <c:when test="${switchState == 0}">
                                        <li class="nav-item" role="presentation">
                                            <form action="/medicare/Servlet/switchState" method="get">
                                                <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Paziente</a>
                                            </form>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="nav-item" role="presentation">
                                            <form action="/medicare/Servlet/switchState" method="get">
                                                <a id="selezionaMB" class="btn btn-primary" href="#" onclick="parentNode.submit();">Cambia a Medico</a>
                                            </form>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                        </c:choose>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="page landing-page">
            <section class="clean-block clean-info" style="background-color: #007bff">
                <div class="wrapper">
                    <%
                        String error = String.valueOf(request.getParameter("error"));
                        String works = String.valueOf(request.getParameter("works"));
                        String phrase = "";

                        if (error.equals("1")) {
                            phrase = "<div class='container-fluid'><div class='row'><div class='col- col-sm-12'><div class='w-100 alert alert-danger alert-dismissible fade show' style='position: absolute; left: 0; right: 0;'><button type='button' class='close' data-dismiss='alert'>&times;</button>Data non valida o già inserita!</div></div></div></div>";
                        } else if (works.equals("1")) {
                            phrase = "<div class='container-fluid'><div class='row'><div class='col- col-sm-12'><div class='w-100 alert alert-success alert-dismissible fade show' style='position: absolute; left: 0; right: 0;'><button type='button' class='close' data-dismiss='alert'>&times;</button>Visita prenotata con successo!</div></div></div></div>";
                        }
                        out.println(phrase);
                    %>

                    <div class="container-fluid">
                        <div class="block-heading text-center">
                            <h2 class="text" style="color: #fff">Prenota un appuntamento</h2>
                        </div> 
                        <div class="row">
                            <div class="container-fluid w-75 bg-white border border-primary rounded-lg shadow-lg">
                                <div class='row'>
                                    <div class="col-sm-6" style="margin: auto">
                                        <ul class="list-group list-group-flush align-left">
                                            <tl class='p-2'><i class='fa fa-angle-right fa-lg' style='color:#007bff'></i> Prenota una visita medica col tuo medico di base </tl>
                                            <tl class='p-2'><i class='fa fa-angle-right fa-lg' style='color:#007bff'></i> Ricevi aggiornamenti via mail </tl>
                                            <tl class='p-2'><i class='fa fa-angle-right fa-lg' style='color:#007bff'></i> Contattaci per qualsiasi evenienza </tl>
                                        </ul>
                                    </div>
                                    <div class="col-sm-6" style="margin: auto;">
                                        <div class="container-fluid border border-info rounded-lg m-3 mx-auto">
                                            <form method="post" action="/medicare/Servlet/visitaMedicaInsData">
                                                <div class="form-group text-center p-2"> <!-- Date input -->
                                                    <label class="control-label" for="date" style='margin-top: 5%'><h5 class='text' style='color: #007bff'>Scegli una data per la visita medica</h5></label>       
                                                </div>
                                                <div class='form-group p-2'>
                                                    <input class="form-control" id="date" name="date" placeholder="MM/DD/YYY" type="text"/>
                                                </div>
                                                <div class="form-group p-2"> <!-- Submit button -->
                                                    <button class="btn btn-primary btn-block" name="submit" type="submit">Invia</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>                        
                        </div>
                    </div>
                </div>
            </section>
        </main>


        <hr/>
        <footer class="page-footer light">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h5>About Us</h5>
                        <ul>
                            <li><a href="#">Gruppo gitLab MediCare</a></li>
                            <li><a href="#">UniTN project</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Capacità</h5>
                        <ul>
                            <li><a href="#">150+ esami prenotabili</a></li>
                            <li><a href="#">800+ medicinali prescrivibili</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Caratteristiche</h5>
                        <ul>
                            <li><a href="#">Veloce</a></li>
                            <li><a href="#">Affidabile</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h5>Sicurezze</h5>
                        <ul>
                            <li><a href="#">Solo medici preparati</a></li>
                            <li><a href="#">Solo gli specialisti migliori</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <p>© 2020 Tutti i diritti sono riservati al gruppo MediCare e a UNITN</p>
            </div>
        </footer>
    </body>
</html>