﻿-- db for software ingeneers's program, third years
-- team: Berrighi Leonardo, Semprebon Elia, Luzi Crivellini Leonardo

DROP DATABASE IF EXISTS ospedale;
CREATE DATABASE ospedale;
USE ospedale;

CREATE TABLE Regione(
	nome VARCHAR(24),
	PRIMARY KEY(nome)
);

CREATE TABLE Provincia(
	abbr VARCHAR(2),
	nome VARCHAR(25) NOT NULL,
	regione VARCHAR(24) NOT NULL,
	PRIMARY KEY(abbr),
	CONSTRAINT fk_r FOREIGN KEY (regione) REFERENCES Regione(nome)
);


CREATE TABLE Citta (
	nome VARCHAR(30) NOT NULL,
	provincia VARCHAR(2) NOT NULL,
	PRIMARY KEY(nome),
	CONSTRAINT fk_city FOREIGN KEY (provincia) REFERENCES Provincia(abbr)
);


CREATE TABLE SSP (
	provincia VARCHAR(2) NOT NULL,
	password VARCHAR(32) NOT NULL,
	salt CHAR(4) NOT NULL,
	primary key(provincia),
	CONSTRAINT fk_ssp FOREIGN KEY (provincia) REFERENCES Provincia(abbr)
);


CREATE TABLE Paziente(
	cf VARCHAR(16) NOT NULL,
	nome VARCHAR(32) NOT NULL,
	cognome VARCHAR(32) NOT NULL,
	dataNascita DATE NOT NULL,
	luogoNascita VARCHAR(2) NOT NULL,
	sesso CHAR(1) NOT NULL,
	email VARCHAR(64) NOT NULL,
	medicoBase VARCHAR(16), -- FOREIGN KEY fk_mb REFERENCES TO MedicoBase(cf) NOT NULL
	residenza VARCHAR(30) NOT NULL,
	PRIMARY KEY(cf),
	CONSTRAINT fk_ln FOREIGN KEY (luogoNascita) REFERENCES Provincia(abbr),
	CONSTRAINT fk_res FOREIGN KEY (residenza) REFERENCES Citta(nome)
);

CREATE TABLE Login(
	id INTEGER AUTO_INCREMENT,
	persona VARCHAR(16) NOT NULL,
	username VARCHAR(32) NOT NULL,
	password VARCHAR(32) NOT NULL,
	salt CHAR(4) NOT NULL,
	PRIMARY KEY(id),
	UNIQUE(salt),
	CONSTRAINT fk_pl FOREIGN KEY (persona) REFERENCES Paziente(cf),
	CONSTRAINT uc_login UNIQUE(username, password)
);

CREATE TABLE MedicoBase(
	cf VARCHAR(16) NOT NULL,
	PRIMARY KEY(cf),
	CONSTRAINT fk_cmb FOREIGN KEY (cf) REFERENCES Paziente(cf)
);

ALTER TABLE Paziente
ADD CONSTRAINT fk_mbp
FOREIGN KEY (medicoBase) REFERENCES MedicoBase(cf);

CREATE TABLE MedicoSpecialista(
	cf VARCHAR(16),
	PRIMARY KEY(cf),
	CONSTRAINT fk_cms FOREIGN KEY (cf) REFERENCES Paziente(cf)
);

CREATE TABLE Foto(
	id INTEGER AUTO_INCREMENT,
	cf VARCHAR(16) NOT NULL,
	dataInserimento DATE,
	fotografia VARCHAR(32) NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_cf FOREIGN KEY (cf) REFERENCES Paziente(cf),
	CONSTRAINT uc_foto UNIQUE(cf, dataInserimento)
);

CREATE TABLE VisitaMedica(
	id INTEGER AUTO_INCREMENT,
	paziente VARCHAR(16) NOT NULL,
	medicoBase VARCHAR(16) NOT NULL,
	dataIscrizione DATE,
	terminata BOOLEAN NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_pvm FOREIGN KEY (paziente) REFERENCES Paziente(cf),
	CONSTRAINT fk_mbvm FOREIGN KEY (medicoBase) REFERENCES MedicoBase(cf),
	CONSTRAINT uc_visitaMedica UNIQUE(paziente, medicoBase, dataIscrizione)
);

CREATE TABLE Esame(
	id INTEGER AUTO_INCREMENT,
	tipoEsame VARCHAR(128) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE Medicinale(
	id INTEGER AUTO_INCREMENT,
	tipoMedicinale VARCHAR(64) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE Prescrizione(
	id INTEGER AUTO_INCREMENT,
	visitaMedica INTEGER NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_vmpe FOREIGN KEY (visitaMedica) REFERENCES VisitaMedica(id)
);

CREATE TABLE EsamePrescritto(
	id INTEGER AUTO_INCREMENT,
	esame INTEGER NOT NULL,
	prescrizione INTEGER NOT NULL,
	dataPrenotazione DATE,
	PRIMARY KEY(id),
	CONSTRAINT fk_eep FOREIGN KEY (esame) REFERENCES Esame(id),
	CONSTRAINT fk_pep FOREIGN KEY (prescrizione) REFERENCES Prescrizione(id),
	CONSTRAINT uc_esamePrescritto UNIQUE(esame, prescrizione, dataPrenotazione)
);

CREATE TABLE MedicinalePrescritto(
	id INTEGER AUTO_INCREMENT,
	prescrizione INTEGER,
	medicinale INTEGER,
	dataRilascio DATE,
	PRIMARY KEY(id),
	UNIQUE(prescrizione, medicinale),
	CONSTRAINT fk_pmp FOREIGN KEY (prescrizione) REFERENCES Prescrizione(id),
	CONSTRAINT fk_mmp FOREIGN KEY (medicinale) REFERENCES Medicinale(id)
);
	
CREATE TABLE Ticket(
	id INTEGER AUTO_INCREMENT,
	costo INTEGER DEFAULT 50,
	pagato INT DEFAULT 0,
	PRIMARY KEY(id)
);

CREATE TABLE VisitaSpecialistica(
	id INT AUTO_INCREMENT,
	medicoSpecialista VARCHAR(16) NOT NULL,
	esamePrescritto INTEGER NOT NULL,
	terminata BOOLEAN NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_mdvs FOREIGN KEY (medicoSpecialista) REFERENCES MedicoSpecialista(cf),
	CONSTRAINT fk_epvs FOREIGN KEY (esamePrescritto) REFERENCES EsamePrescritto(id),
	CONSTRAINT uc_visitaSpecialistica UNIQUE(medicoSpecialista, esamePrescritto)
);

CREATE TABLE Anamnesi(
	id INTEGER AUTO_INCREMENT,
	descrizione VARCHAR(64) NOT NULL,
	dataRilascio DATE,
	visitaSpecialistica INTEGER NOT NULL,
	ticket INTEGER NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_vsa FOREIGN KEY (visitaSpecialistica) REFERENCES VisitaSpecialistica(id),
	CONSTRAINT fk_ta FOREIGN KEY (ticket) REFERENCES Ticket(id)
);

CREATE TABLE Notifiche (
	id INTEGER AUTO_INCREMENT,
	paziente VARCHAR(16) NOT NULL,
	messaggio VARCHAR(150) NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_paz FOREIGN KEY (paziente) REFERENCES Paziente(cf)
);


-- indexes 
CREATE INDEX regione_ind ON Regione (`nome`);
CREATE INDEX provincia_ind ON Provincia (`abbr`) ;
CREATE INDEX paz_ind ON Paziente (`cf`) ;
CREATE INDEX log_ind ON Login (`id`) ;
CREATE INDEX mbase_ind ON MedicoBase (`cf`) ;
CREATE INDEX mspec_ind ON MedicoSpecialista (`cf`) ;
CREATE INDEX foto_ind ON Foto (`id`) ;
CREATE INDEX visitam_ind ON VisitaMedica (`id`) ;
CREATE INDEX esame_ind ON Esame (`id`) ;
CREATE INDEX medicinale_ind ON Medicinale (`id`) ;
CREATE INDEX presc_ind ON Prescrizione (`id`) ;
CREATE INDEX esamep ON EsamePrescritto (`id`) ;
CREATE INDEX medp_ind ON MedicinalePrescritto (`id`) ;
CREATE INDEX tick_ind ON Ticket (`id`) ;
CREATE INDEX visitas_ind ON VisitaSpecialistica (`id`) ;
CREATE INDEX anamnesi_ind ON Anamnesi (`id`) ;
CREATE INDEX notif_ind ON Notifiche (`id`) ;


-- populateDB

-- Tabella regione

INSERT INTO regione VALUES("Abruzzo");
INSERT INTO regione VALUES("Basilicata");
INSERT INTO regione VALUES("Calabria");	
INSERT INTO regione VALUES("Campania");	
INSERT INTO regione VALUES("Emilia-Romagna");	
INSERT INTO regione VALUES("Friuli-Venezia Giulia");		
INSERT INTO regione VALUES("Lazio");		
INSERT INTO regione VALUES("Liguria");		
INSERT INTO regione VALUES("Lombardia");		
INSERT INTO regione VALUES("Marche");		
INSERT INTO regione VALUES("Molise");		
INSERT INTO regione VALUES("Piemonte");		
INSERT INTO regione VALUES("Puglia");		
INSERT INTO regione VALUES("Sardegna");		
INSERT INTO regione VALUES("Sicilia");		
INSERT INTO regione VALUES("Toscana");		
INSERT INTO regione VALUES("Trentino-Alto Adige");		
INSERT INTO regione VALUES("Umbria");
INSERT INTO regione VALUES("Valle d'Aosta");
INSERT INTO regione VALUES("Veneto");	

-- Tabella provincia

INSERT INTO provincia (abbr, nome, regione) VALUES ("AG", "Agrigento", "Sicilia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("AL", "Alessandria", "Piemonte");
INSERT INTO provincia (abbr, nome, regione) VALUES ("AN", "Ancona", "Marche");
INSERT INTO provincia (abbr, nome, regione) VALUES ("AO", "Aosta", "Valle d'Aosta");
INSERT INTO provincia (abbr, nome, regione) VALUES ("AQ", "Aquila", "Abruzzo");
INSERT INTO provincia (abbr, nome, regione) VALUES ("AR", "Arezzo", "Toscana");
INSERT INTO provincia (abbr, nome, regione) VALUES ("AP", "Ascoli Piceno", "Marche");
INSERT INTO provincia (abbr, nome, regione) VALUES ("AT", "Asti", "Piemonte");
INSERT INTO provincia (abbr, nome, regione) VALUES ("AV", "Avellino", "Campania");
INSERT INTO provincia (abbr, nome, regione) VALUES ("BA", "Bari", "Puglia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("BT", "Barletta", "Puglia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("BL", "Belluno", "Veneto");
INSERT INTO provincia (abbr, nome, regione) VALUES ("BN", "Benevento", "Campania");
INSERT INTO provincia (abbr, nome, regione) VALUES ("BG", "Begamo", "Lombardia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("BI", "Biella", "Piemonte");
INSERT INTO provincia (abbr, nome, regione) VALUES ("BO", "Bologna", "Emilia-Romagna");
INSERT INTO provincia (abbr, nome, regione) VALUES ("BZ", "Bolzano", "Trentino-Alto Adige");
INSERT INTO provincia (abbr, nome, regione) VALUES ("BS", "Brescia", "Lombardia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("BR", "Brindisi", "Puglia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("CA", "Cagliari", "Sardegna");
INSERT INTO provincia (abbr, nome, regione) VALUES ("CL", "Caltanisetta", "Sicilia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("CB", "Campobasso", "Molise");
INSERT INTO provincia (abbr, nome, regione) VALUES ("CI", "Carbonia", "Sardegna");
INSERT INTO provincia (abbr, nome, regione) VALUES ("CE", "Caserta", "Campania");
INSERT INTO provincia (abbr, nome, regione) VALUES ("CT", "Catania", "Sicilia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("CZ", "Catanzaro", "Calabria");
INSERT INTO provincia (abbr, nome, regione) VALUES ("CH", "Chieti", "Abruzzo");
INSERT INTO provincia (abbr, nome, regione) VALUES ("CO", "Como", "Lombardia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("CS", "Cosenza", "Calabria");
INSERT INTO provincia (abbr, nome, regione) VALUES ("CR", "Cremona", "Lombardia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("KR", "Crotone", "Calabria");
INSERT INTO provincia (abbr, nome, regione) VALUES ("CN", "Cuneo", "Piemonte");
INSERT INTO provincia (abbr, nome, regione) VALUES ("EN", "Enna", "Sicilia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("FM", "Fermo", "Marche");
INSERT INTO provincia (abbr, nome, regione) VALUES ("FE", "Ferrara", "Emilia-Romagna");
INSERT INTO provincia (abbr, nome, regione) VALUES ("FI", "Firenze", "Toscana");
INSERT INTO provincia (abbr, nome, regione) VALUES ("FG", "Foggia", "Puglia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("FC", "Forli-Cesena", "Emilia-Romagna");
INSERT INTO provincia (abbr, nome, regione) VALUES ("FR", "Frosinone", "Lazio");
INSERT INTO provincia (abbr, nome, regione) VALUES ("GE", "Genova", "Liguria");
INSERT INTO provincia (abbr, nome, regione) VALUES ("GO", "Gorizia", "Friuli-Venezia Giulia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("GR", "Grosseto", "Toscana");
INSERT INTO provincia (abbr, nome, regione) VALUES ("IM", "Imola", "Liguria");
INSERT INTO provincia (abbr, nome, regione) VALUES ("IS", "Isernia", "Molise");
INSERT INTO provincia (abbr, nome, regione) VALUES ("SP", "Spezia", "Liguria");
INSERT INTO provincia (abbr, nome, regione) VALUES ("LT", "Latina", "Lazio");
INSERT INTO provincia (abbr, nome, regione) VALUES ("LE", "Lecce", "Puglia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("LC", "Lecco", "Lombardia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("LI", "Livorno", "Toscana");
INSERT INTO provincia (abbr, nome, regione) VALUES ("LO", "Lodi", "Lombardia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("LU", "Lucca", "Toscana");
INSERT INTO provincia (abbr, nome, regione) VALUES ("MC", "Macerata", "Marche");
INSERT INTO provincia (abbr, nome, regione) VALUES ("MN", "Mantova", "Lombardia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("MS", "Massa-carrara", "Toscana");
INSERT INTO provincia (abbr, nome, regione) VALUES ("MT", "Matera", "Basilicata");
INSERT INTO provincia (abbr, nome, regione) VALUES ("VS", "Medio Campidano", "Sardegna");
INSERT INTO provincia (abbr, nome, regione) VALUES ("ME", "Messina", "Sicilia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("MI", "Milano", "Lombardia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("MO", "Modena", "Emilia-Romagna");
INSERT INTO provincia (abbr, nome, regione) VALUES ("MB", "Monza e brianza", "Lombardia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("NA", "Napoli", "Campania");
INSERT INTO provincia (abbr, nome, regione) VALUES ("NO", "Novara", "Piemonte");
INSERT INTO provincia (abbr, nome, regione) VALUES ("NU", "Nuoro", "Sardegna");
INSERT INTO provincia (abbr, nome, regione) VALUES ("OG", "Ogliastra", "Sardegna");
INSERT INTO provincia (abbr, nome, regione) VALUES ("OT", "Olbia tempio", "Sardegna");
INSERT INTO provincia (abbr, nome, regione) VALUES ("OR", "Oristano", "Sardegna");
INSERT INTO provincia (abbr, nome, regione) VALUES ("PD", "Padova", "Veneto");
INSERT INTO provincia (abbr, nome, regione) VALUES ("PA", "Palermo", "Sicilia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("PR", "Parma", "Emilia-Romagna");
INSERT INTO provincia (abbr, nome, regione) VALUES ("PV", "Pavia", "Lombardia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("PG", "Perugia", "Umbria");
INSERT INTO provincia (abbr, nome, regione) VALUES ("PU", "Pesaro-Urbino", "Marche");
INSERT INTO provincia (abbr, nome, regione) VALUES ("PE", "Pescara", "Abruzzo");
INSERT INTO provincia (abbr, nome, regione) VALUES ("PC", "Piacenza", "Emilia-Romagna");
INSERT INTO provincia (abbr, nome, regione) VALUES ("PI", "Pisa", "Toscana");
INSERT INTO provincia (abbr, nome, regione) VALUES ("PT", "Pistoia", "Toscana");
INSERT INTO provincia (abbr, nome, regione) VALUES ("PN", "Pordenone", "Friuli-Venezia Giulia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("PZ", "Potenza", "Basilicata");
INSERT INTO provincia (abbr, nome, regione) VALUES ("PO", "Prato", "Toscana");
INSERT INTO provincia (abbr, nome, regione) VALUES ("RG", "Ragusa", "Sicilia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("RA", "Ravenna", "Emilia-Romagna");
INSERT INTO provincia (abbr, nome, regione) VALUES ("RC", "Reggio Calabria", "Calabria");
INSERT INTO provincia (abbr, nome, regione) VALUES ("RE", "Reggio emilia", "Emilia-Romagna");
INSERT INTO provincia (abbr, nome, regione) VALUES ("RI", "Rieti", "Lazio");
INSERT INTO provincia (abbr, nome, regione) VALUES ("RN", "Rimini", "Emilia-Romagna");
INSERT INTO provincia (abbr, nome, regione) VALUES ("RM", "Roma", "Lazio");
INSERT INTO provincia (abbr, nome, regione) VALUES ("RO", "Rovigo", "Veneto");
INSERT INTO provincia (abbr, nome, regione) VALUES ("SA", "Salerno", "Campania");
INSERT INTO provincia (abbr, nome, regione) VALUES ("SS", "Sassari", "Sardegna");
INSERT INTO provincia (abbr, nome, regione) VALUES ("SV", "Savona", "Liguria");
INSERT INTO provincia (abbr, nome, regione) VALUES ("SI", "Siena", "Toscana");
INSERT INTO provincia (abbr, nome, regione) VALUES ("SR", "Siracusa", "Sicilia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("SO", "Sondrio", "Lombardia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("TA", "Taranto", "Puglia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("TE", "Termano", "Abruzzo");
INSERT INTO provincia (abbr, nome, regione) VALUES ("TR", "Terni", "Umbria");
INSERT INTO provincia (abbr, nome, regione) VALUES ("TO", "Torni", "Piemonte");
INSERT INTO provincia (abbr, nome, regione) VALUES ("TP", "Trapani", "Sicilia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("TN", "Trento", "Trentino-Alto Adige");
INSERT INTO provincia (abbr, nome, regione) VALUES ("TV", "Treviso", "Veneto");
INSERT INTO provincia (abbr, nome, regione) VALUES ("TS", "Trieste",  "Friuli-Venezia Giulia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("UD", "Udine", "Friuli-Venezia Giulia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("VA", "Varese", "Lombardia");
INSERT INTO provincia (abbr, nome, regione) VALUES ("VE", "Venezia", "Veneto");
INSERT INTO provincia (abbr, nome, regione) VALUES ("VB", "Verbano", "Piemonte");
INSERT INTO provincia (abbr, nome, regione) VALUES ("VC", "Vercelli", "Piemonte");
INSERT INTO provincia (abbr, nome, regione) VALUES ("VR", "Verona", "Veneto");
INSERT INTO provincia (abbr, nome, regione) VALUES ("VV", "Vibo Valentia", "Calabria");
INSERT INTO provincia (abbr, nome, regione) VALUES ("VI", "Vicenza", "Veneto");
INSERT INTO provincia (abbr, nome, regione) VALUES ("VT", "Viterbo", "Lazio");


INSERT INTO citta(nome, provincia) VALUES("Verona", "VR");
INSERT INTO citta(nome, provincia) VALUES("Trento", "TN");
INSERT INTO citta(nome, provincia) VALUES("Bolzano", "BZ");


INSERT INTO SSP(provincia, password, salt) values ("VR", "admin", "trap");
INSERT INTO SSP(provincia, password, salt) values ("TN", "admin", "suov");
INSERT INTO SSP(provincia, password, salt) values ("BZ", "admin", "cfgz");


INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("WLSTNY50L59L781F", "Tanya", "Wilson", '1950-07-19', "VR", "F", "tanyawilson@genmail.com", NULL, "Bolzano");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("DLGMRM60C42A952V", "Miriam", "Delgado", '1960-03-02', "BZ", "F", "miriamdelegado@genmail.com", NULL, "Bolzano");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("DLGJLU01S45L378L", "Julie", "Delgado", '2001-11-05', "TN", "F", "juliedelegado@genmail.com", NULL, "Bolzano");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("JNKLNE97E47L378R","Leona", "Jenkins", '1997-05-07', "TN", "F", "leonajenkins@genmail.com", NULL, "Bolzano");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("HWLMBL66C42L781H","Mabel", "Howell", '1966-03-02', "VR", "F", "mabelhowell@genmail.com", NULL, "Bolzano");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("WHTMLE03M70A952U","Emelia", "Whitehouse", '2003-08-30', "BZ", "F", "emeliawhitehouse@genmail.com", NULL, "Bolzano");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("CLRSFO75L66A952R","Sofia", "Clark", '1975-07-26', "BZ", "F", "sofiaclark@genmail.com", NULL, "Bolzano");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("CRVBBY88R53L378S","Abby", "Cervantes", '1988-10-13', "TN", "F", "abbycervantes@genmail.com", NULL, "Bolzano");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("STVLCY61S51L781U","Lacey", "Stevenson", '1961-11-11', "VR", "F", "lacystevenson@genmail.com", NULL, "Bolzano");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("WGNLCU53M63L781L","Lucia", "Wagner", '1953-08-23', "VR", "F", "luciawagner@genmail.com", NULL, "Bolzano");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("PTTNSA78B66L781N","Anisa", "Pittman", '1978-02-26', "VR", "F", "anisapittman@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("PNKJSS99C02L781D","Jessie", "Pinkman", '1999-03-02', "VR", "M", "jessiepinkman@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("CRPMBR98A56L781K","Amber", "Carpenter", '1998-01-16', "VR", "F", "ambercarpenter@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("BTLLSI69T20L781B","Lisa", "Butler", '1969-12-20', "VR", "F", "lisabutler@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("FTZSLV73D55L781X","Silvia", "Fitzgerald", '1973-04-15', "VR", "F", "silviafitzgerald@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("HMPSLL55A58L781O","Isabella", "Humphrey", '1955-01-18', "VR", "F", "isabellahumphrey@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("BYRPLA43P53L781K","Paula", "Byrne", '1943-09-13',"VR", "F", "paulabyrne@genmail.com", NULL, "Verona"); 
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("DVLKST88M48A952Q","Kristina", "Devlin", '1988-08-08', "BZ", "F", "kristinadevlin@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("WLKFNC05C54A952O","Francesca", "Wilkerson", '2005-03-14', "BZ", "F", "francescawilkerson@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("PLASRA09H47A952Q","Sara", "Paul", '2009-06-07', "BZ", "F", "sarapaul@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("PTRLVR96P05A952M","Oliver", "Peters", '1996-09-05', "BZ", "M", "oliverpeters@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("VNSRHR92C27A952I","Richard", "Evans", '1992-03-27', "BZ", "M", "richardevans@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("HYSLSO90L17A952I","Louis", "Hayes", '1990-07-17', "BZ", "M", "louishayes@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("GLLDRN93T22A952A","Dorian", "Gallagher", '1993-12-22', "BZ", "M", "doriangallagher@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("WLLDVD87M26A952P","Davide", "Williams", '1987-08-26', "BZ", "M", "davidewilliams@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("TRMTHN96L10A952G","Ethan", "Tremblake", '1996-07-10', "BZ", "M", "ethantremblake@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("PLLJKA79H19L378I","Jake", "Pollard", '1979-06-19', "TN", "M", "jakepollard@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("FSLGNN97E20L378C","Giovanni", "Fasoli", '1997-05-20', "TN", "M", "giovannifasoli@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("PTRJHN67A31L378J","John", "Petrucci", '1967-01-31', "TN", "M", "johnpetrucci@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("MNNJCB59C18L378B","Jacob", "Mann", '1959-03-18', "TN", "M", "jacobmann@genmail.com", NULL, "Trento");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("DFAWLM53S06L378I","William", "Dafoe", '1953-11-06', "TN", "M", "williamdafoe@genmail.com", NULL, "Trento");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("LRCDRD74T08L378I","Edward", "Elric", '1974-12-08', "TN", "M", "edwardelric@genmail.com", NULL, "Trento");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("CLRLCU99E23L378W","Luca", "Calearo", '1999-05-23', "TN", "M", "lucacalearo@genmail.com", NULL, "Trento");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("SNTFNK60H21L378W","Frank", "Sinatra", '1960-06-21', "TN", "M", "franksinatra@genmail.com", NULL, "Trento");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("CHVFRC90R07L378X","Federico", "Chiavico", '1990-10-07', "TN", "M", "federicochiavico@genmail.com", NULL, "Trento");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("FRRSMN97P21L378I","Simone", "Ferraro", '1997-09-21', "TN", "M", "simoneferraro0@protonmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("MNDHRY86R23L378H","Henry", "Mandez", '1986-10-23', "TN", "M", "henrymandez", NULL, "Trento");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("MCCMTH81B20L378V","Matthew", "McConnel", '1981-02-20', "TN", "M", "matthewmcconnel@genmail.com", NULL, "Trento");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("RYAMHL65S13L378B","Michael", "Ray", '1965-11-13', "TN", "M", "michaelray@genmail.com", NULL, "Trento");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("JYCJMS87R10L378T","James", "Joyce", '1987-10-10', "TN", "M", "jamesjoyce@genmail.com", NULL, "Trento");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("FRDHRS64H03L378B","Harrison", "Ford", '1964-06-03', "TN", "M", "harrisonford@genmail.com", NULL, "Trento");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("PTRSML59C07L378J","Samuel", "Peterson", '1959-03-07', "TN", "M", "samuelpeterson@genmail.com", NULL, "Trento");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("BLKDNL51T21L378R","Daniel", "Blake", '1951-12-21', "TN", "M", "danielblake@genmail.com", NULL, "Trento");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("PWRRRT66L30L378U","Robert", "Powers", '1966-07-30', "TN", "M", "robertpowers@genmail.com", NULL, "Trento");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("TRKCRS74C21L378D","Christopher", "Turk", '1974-03-21', "TN", "M", "christopherturk@genmail.com", NULL, "Bolzano");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("JFFTMS49M12A952L","Thomas", "Jefferson", '1949-08-12', "BZ", "M", "thomasjefferson@genmail.com", NULL, "Trento");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("XVRCRL40A01L781R","Charles", "Xavier", '1940-01-01', "VR", "M", "charlesxavier0@protonmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("SKYLKU37E13A952L","Luke", "Skywalker", '1937-05-13', "BZ", "M", "lukeskywalker@genmail.com", NULL, "Trento");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("SNDNHN29P12L781E","Nathan", "Sand", '1929-09-12', "VR", "M", "nathansand@genmail.com", NULL, "Trento");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("CGLMRC38D08L378S","Marco", "Caglioti", '1938-04-08', "TN", "M", "marcocaglioti0@protonmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("CSYLEI45P20L781B","Elia", "Casey", '1945-09-20', "VR", "M", "eliacasey@genmail.com", NULL, "Bolzano");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("GRLMTT48H16A952G","Matteo", "Girelli", '1948-06-16', "BZ", "M", "matteogirelli@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("JHNLCU70S51A952T","Lucia", "Jhonson", '1970-11-11', "BZ", "F", "luciajhonson@genmail.com", NULL, "Verona");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("VLPGNN53D12L781Y","Giovanni", "Volpi", '1953-04-12', "VR", "M", "giovannivolpi@genmail.com", NULL, "Trento");
INSERT INTO paziente (cf, nome, cognome, dataNascita, luogoNascita, sesso, email, medicoBase, residenza) VALUES ("MRTRCR94M17L378L","Riccardo", "Martinelli", '1994-08-17', "TN", "M", "riccardomartinelli@genmail.com", NULL, "Bolzano");


-- tabella login

INSERT INTO login(persona, username, password, salt) VALUES("WLSTNY50L59L781F", "tanya.wilson", "admin", "akjd"); 
INSERT INTO login(persona, username, password, salt) VALUES("DLGMRM60C42A952V", "miriam.delgado", "admin", "aotx"); 
INSERT INTO login(persona, username, password, salt) VALUES("DLGJLU01S45L378L", "julie.delgado", "admin", "aprt"); 
INSERT INTO login(persona, username, password, salt) VALUES("JNKLNE97E47L378R", "leona.jenkins", "admin", "abxr");  
INSERT INTO login(persona, username, password, salt) VALUES("HWLMBL66C42L781H", "mabel.howell", "admin", "aqhc"); 
INSERT INTO login(persona, username, password, salt) VALUES("WHTMLE03M70A952U", "emelia.whitehouse", "admin", "bcjr"); 
INSERT INTO login(persona, username, password, salt) VALUES("CLRSFO75L66A952R", "sofia.clark", "admin", "blaz"); 
INSERT INTO login(persona, username, password, salt) VALUES("CRVBBY88R53L378S", "abby.cervantes", "admin", "butq"); 
INSERT INTO login(persona, username, password, salt) VALUES("STVLCY61S51L781U", "lacey.stevenson", "admin", "bpla"); 
INSERT INTO login(persona, username, password, salt) VALUES("WGNLCU53M63L781L", "lucia.wagner", "admin", "bhjc"); 
INSERT INTO login(persona, username, password, salt) VALUES("PTTNSA78B66L781N", "anisa.pittman", "admin", "cfrg"); 
INSERT INTO login(persona, username, password, salt) VALUES("PNKJSS99C02L781D", "jessie.pinkman", "admin", "cjkm"); 
INSERT INTO login(persona, username, password, salt) VALUES("CRPMBR98A56L781K", "amber.carpenter", "admin", "cder"); 
INSERT INTO login(persona, username, password, salt) VALUES("BTLLSI69T20L781B", "lisa.butler", "admin", "clmo"); 
INSERT INTO login(persona, username, password, salt) VALUES("FTZSLV73D55L781X", "silvia.fitzgerald", "admin", "cdkj"); 
INSERT INTO login(persona, username, password, salt) VALUES("HMPSLL55A58L781O", "isabella.humphrey", "admin", "dlaz"); 
INSERT INTO login(persona, username, password, salt) VALUES("BYRPLA43P53L781K", "paula.byrne", "admin", "dwqp"); 
INSERT INTO login(persona, username, password, salt) VALUES("DVLKST88M48A952Q", "kristina.devlin", "admin", "duym"); 
INSERT INTO login(persona, username, password, salt) VALUES("WLKFNC05C54A952O", "francesca.wilkerson", "admin", "dpwt"); 
INSERT INTO login(persona, username, password, salt) VALUES("PLASRA09H47A952Q", "sara.paul", "admin", "dhvq"); 
INSERT INTO login(persona, username, password, salt) VALUES("PTRLVR96P05A952M", "oliver.peters", "admin", "ehjl"); 
INSERT INTO login(persona, username, password, salt) VALUES("VNSRHR92C27A952I", "richard.evans", "admin", "esxp"); 
INSERT INTO login(persona, username, password, salt) VALUES("HYSLSO90L17A952I", "louis.hayes", "admin", "erkv"); 
INSERT INTO login(persona, username, password, salt) VALUES("GLLDRN93T22A952A", "dorian.gallagher", "admin", "elfx"); 
INSERT INTO login(persona, username, password, salt) VALUES("WLLDVD87M26A952P", "davide.williams", "admin", "elmz"); 
INSERT INTO login(persona, username, password, salt) VALUES("TRMTHN96L10A952G", "ethan.tremblake", "admin", "fzyq"); 
INSERT INTO login(persona, username, password, salt) VALUES("PLLJKA79H19L378I", "jake.pollard", "admin", "fhlz"); 
INSERT INTO login(persona, username, password, salt) VALUES("FSLGNN97E20L378C", "giovanni.fasoli", "admin", "fpab"); 
INSERT INTO login(persona, username, password, salt) VALUES("PTRJHN67A31L378J", "john.petrucci", "admin", "fhjs"); 
INSERT INTO login(persona, username, password, salt) VALUES("MNNJCB59C18L378B", "jacob.mann", "admin", "fueb"); 
INSERT INTO login(persona, username, password, salt) VALUES("DFAWLM53S06L378I", "william.dafoe", "admin", "gbop"); 
INSERT INTO login(persona, username, password, salt) VALUES("LRCDRD74T08L378I", "edward.elric", "admin", "gcxz"); 
INSERT INTO login(persona, username, password, salt) VALUES("CLRLCU99E23L378W", "luca.calearo", "admin", "gpum"); 
INSERT INTO login(persona, username, password, salt) VALUES("SNTFNK60H21L378W", "frank.sinatra", "admin", "gmtq"); 
INSERT INTO login(persona, username, password, salt) VALUES("CHVFRC90R07L378X", "federico.chiavico", "admin", "gopr"); 
INSERT INTO login(persona, username, password, salt) VALUES("FRRSMN97P21L378I", "simone.ferraro", "admin", "hlxm"); 
INSERT INTO login(persona, username, password, salt) VALUES("MNDHRY86R23L378H", "henry.mandez", "admin", "hapq"); 
INSERT INTO login(persona, username, password, salt) VALUES("MCCMTH81B20L378V", "matthew.mcconnel", "admin", "hohx"); 
INSERT INTO login(persona, username, password, salt) VALUES("RYAMHL65S13L378B", "michael.ray", "admin", "hgpz"); 
INSERT INTO login(persona, username, password, salt) VALUES("JYCJMS87R10L378T", "james.joyce", "admin", "hmvi"); 
INSERT INTO login(persona, username, password, salt) VALUES("FRDHRS64H03L378B", "harrison.ford", "admin", "ifsp"); 
INSERT INTO login(persona, username, password, salt) VALUES("PTRSML59C07L378J", "samuel.peterson", "admin", "ikxq"); 
INSERT INTO login(persona, username, password, salt) VALUES("BLKDNL51T21L378R", "daniel.blake", "admin", "ilda"); 
INSERT INTO login(persona, username, password, salt) VALUES("PWRRRT66L30L378U", "robert.powers", "admin", "itrw"); 
INSERT INTO login(persona, username, password, salt) VALUES("TRKCRS74C21L378D", "christopher.turk", "admin", "iruq"); 
INSERT INTO login(persona, username, password, salt) VALUES("JFFTMS49M12A952L", "thomas.jefferson", "admin", "lopw"); 
INSERT INTO login(persona, username, password, salt) VALUES("XVRCRL40A01L781R", "charles.xavier", "admin", "leoa"); 
INSERT INTO login(persona, username, password, salt) VALUES("SKYLKU37E13A952L", "luke.skywalker", "admin", "lyxa"); 
INSERT INTO login(persona, username, password, salt) VALUES("SNDNHN29P12L781E", "nathan.sand", "admin", "ljne"); 
INSERT INTO login(persona, username, password, salt) VALUES("CGLMRC38D08L378S", "marco.caglioti", "admin", "lxnx"); 
INSERT INTO login(persona, username, password, salt) VALUES("CSYLEI45P20L781B", "elia.casey", "admin", "mopq"); 
INSERT INTO login(persona, username, password, salt) VALUES("GRLMTT48H16A952G", "matteo.girelli", "admin", "mutr");
INSERT INTO login(persona, username, password, salt) VALUES("JHNLCU70S51A952T", "lucia.jhonson", "admin", "mozz");
INSERT INTO login(persona, username, password, salt) VALUES("VLPGNN53D12L781Y", "giovanni.volpi", "admin", "migd");
INSERT INTO login(persona, username, password, salt) VALUES("MRTRCR94M17L378L", "riccardo.martinelli", "admin", "maik"); 

-- tabella medici di base
-- Christopher Turk, Daniel Blake, Lucia jhonson, Charles Xavier, Thomas Jefferson
INSERT INTO medicoBase(cf) VALUES ("TRKCRS74C21L378D");
INSERT INTO medicoBase(cf) VALUES ("JHNLCU70S51A952T");
INSERT INTO medicoBase(cf) VALUES ("XVRCRL40A01L781R");
INSERT INTO medicoBase(cf) VALUES ("BLKDNL51T21L378R");
INSERT INTO medicoBase(cf) VALUES ("JFFTMS49M12A952L");

-- update paziente
UPDATE paziente SET medicoBase = "TRKCRS74C21L378D" WHERE cf = "WLSTNY50L59L781F";
UPDATE paziente SET medicoBase = "TRKCRS74C21L378D" WHERE cf = "DLGMRM60C42A952";
UPDATE paziente SET medicoBase = "TRKCRS74C21L378D" WHERE cf = "DLGJLU01S45L378L";
UPDATE paziente SET medicoBase = "TRKCRS74C21L378D" WHERE cf = "JNKLNE97E47L378R";
UPDATE paziente SET medicoBase = "TRKCRS74C21L378D" WHERE cf = "HWLMBL66C42L781H";
UPDATE paziente SET medicoBase = "TRKCRS74C21L378D" WHERE cf = "WHTMLE03M70A952U";
UPDATE paziente SET medicoBase = "TRKCRS74C21L378D" WHERE cf = "CLRSFO75L66A952R";
UPDATE paziente SET medicoBase = "TRKCRS74C21L378D" WHERE cf = "CRVBBY88R53L378S";
UPDATE paziente SET medicoBase = "TRKCRS74C21L378D" WHERE cf = "STVLCY61S51L781U";
UPDATE paziente SET medicoBase = "TRKCRS74C21L378D" WHERE cf = "WGNLCU53M63L781L";
UPDATE paziente SET medicoBase = "JHNLCU70S51A952T" WHERE cf = "PTTNSA78B66L781N";
UPDATE paziente SET medicoBase = "JHNLCU70S51A952T" WHERE cf = "PNKJSS99C02L781D";
UPDATE paziente SET medicoBase = "JHNLCU70S51A952T" WHERE cf = "CRPMBR98A56L781K";
UPDATE paziente SET medicoBase = "JHNLCU70S51A952T" WHERE cf = "BTLLSI69T20L781B";
UPDATE paziente SET medicoBase = "JHNLCU70S51A952T" WHERE cf = "FTZSLV73D55L781X";
UPDATE paziente SET medicoBase = "JHNLCU70S51A952T" WHERE cf = "HMPSLL55A58L781O";
UPDATE paziente SET medicoBase = "JHNLCU70S51A952T" WHERE cf = "BYRPLA43P53L781K";
UPDATE paziente SET medicoBase = "JHNLCU70S51A952T" WHERE cf = "DVLKST88M48A952Q";
UPDATE paziente SET medicoBase = "JHNLCU70S51A952T" WHERE cf = "WLKFNC05C54A952O";
UPDATE paziente SET medicoBase = "XVRCRL40A01L781R" WHERE cf = "PLASRA09H47A952Q";
UPDATE paziente SET medicoBase = "XVRCRL40A01L781R" WHERE cf = "PTRLVR96P05A952M";
UPDATE paziente SET medicoBase = "XVRCRL40A01L781R" WHERE cf = "VNSRHR92C27A952I";
UPDATE paziente SET medicoBase = "XVRCRL40A01L781R" WHERE cf = "HYSLSO90L17A952I";
UPDATE paziente SET medicoBase = "XVRCRL40A01L781R" WHERE cf = "GLLDRN93T22A952A";
UPDATE paziente SET medicoBase = "XVRCRL40A01L781R" WHERE cf = "WLLDVD87M26A952P";
UPDATE paziente SET medicoBase = "XVRCRL40A01L781R" WHERE cf = "TRMTHN96L10A952G";
UPDATE paziente SET medicoBase = "XVRCRL40A01L781R" WHERE cf = "PLLJKA79H19L378I";
UPDATE paziente SET medicoBase = "XVRCRL40A01L781R" WHERE cf = "FSLGNN97E20L378C";
UPDATE paziente SET medicoBase = "XVRCRL40A01L781R" WHERE cf = "PTRJHN67A31L378J";
UPDATE paziente SET medicoBase = "BLKDNL51T21L378R" WHERE cf = "MNNJCB59C18L378B";
UPDATE paziente SET medicoBase = "BLKDNL51T21L378R" WHERE cf = "DFAWLM53S06L378I";
UPDATE paziente SET medicoBase = "BLKDNL51T21L378R" WHERE cf = "LRCDRD74T08L378I";
UPDATE paziente SET medicoBase = "BLKDNL51T21L378R" WHERE cf = "CLRLCU99E23L378W";
UPDATE paziente SET medicoBase = "BLKDNL51T21L378R" WHERE cf = "SNTFNK60H21L378W";
UPDATE paziente SET medicoBase = "BLKDNL51T21L378R" WHERE cf = "CHVFRC90R07L378X";
UPDATE paziente SET medicoBase = "BLKDNL51T21L378R" WHERE cf = "FRRSMN97P21L378I";
UPDATE paziente SET medicoBase = "BLKDNL51T21L378R" WHERE cf = "MNDHRY86R23L378H";
UPDATE paziente SET medicoBase = "BLKDNL51T21L378R" WHERE cf = "MCCMTH81B20L378V";
UPDATE paziente SET medicoBase = "BLKDNL51T21L378R" WHERE cf = "RYAMHL65S13L378B";
UPDATE paziente SET medicoBase = "JFFTMS49M12A952L" WHERE cf = "JYCJMS87R10L378T";
UPDATE paziente SET medicoBase = "JFFTMS49M12A952L" WHERE cf = "FRDHRS64H03L378B";
UPDATE paziente SET medicoBase = "JFFTMS49M12A952L" WHERE cf = "PTRSML59C07L378J";
UPDATE paziente SET medicoBase = "JFFTMS49M12A952L" WHERE cf = "BLKDNL51T21L378R";
UPDATE paziente SET medicoBase = "JFFTMS49M12A952L" WHERE cf = "DLGMRM60C42A952V";
UPDATE paziente SET medicoBase = "JFFTMS49M12A952L" WHERE cf = "PWRRRT66L30L378U";
UPDATE paziente SET medicoBase = "JFFTMS49M12A952L" WHERE cf = "TRKCRS74C21L378D";
UPDATE paziente SET medicoBase = "JFFTMS49M12A952L" WHERE cf = "JFFTMS49M12A952L";
UPDATE paziente SET medicoBase = "JHNLCU70S51A952T" WHERE cf = "XVRCRL40A01L781R";
UPDATE paziente SET medicoBase = "JFFTMS49M12A952L" WHERE cf = "SKYLKU37E13A952L";
UPDATE paziente SET medicoBase = "JHNLCU70S51A952T" WHERE cf = "SNDNHN29P12L781E";
UPDATE paziente SET medicoBase = "XVRCRL40A01L781R" WHERE cf = "CGLMRC38D08L378S";
UPDATE paziente SET medicoBase = "TRKCRS74C21L378D" WHERE cf = "CSYLEI45P20L781B";
UPDATE paziente SET medicoBase = "JHNLCU70S51A952T" WHERE cf = "GRLMTT48H16A952G";
UPDATE paziente SET medicoBase = "XVRCRL40A01L781R" WHERE cf = "JHNLCU70S51A952T";
UPDATE paziente SET medicoBase = "JFFTMS49M12A952L" WHERE cf = "VLPGNN53D12L781Y";
UPDATE paziente SET medicoBase = "TRKCRS74C21L378D" WHERE cf = "MRTRCR94M17L378L";


-- tabella medici specialisti

INSERT INTO medicoSpecialista(cf) VALUES("FRRSMN97P21L378I"); -- Simone Ferraro
INSERT INTO medicoSpecialista(cf) VALUES("STVLCY61S51L781U"); -- Lacey Stevenson
INSERT INTO medicoSpecialista(cf) VALUES("JYCJMS87R10L378T"); -- James Joyce
INSERT INTO medicoSpecialista(cf) VALUES("SNDNHN29P12L781E"); -- Nathan Sand
INSERT INTO medicoSpecialista(cf) VALUES("GRLMTT48H16A952G"); -- Matteo Girelli


-- tabella esami

INSERT INTO esame(tipoEsame) VALUES ("Estrazione di dente deciduo");
INSERT INTO esame(tipoEsame) VALUES ("Estrazione di dente permanente");
INSERT INTO esame(tipoEsame) VALUES ("Altra estrazione chirurgica dente");
INSERT INTO esame(tipoEsame) VALUES ("Ricostruzione dente con otturazione");
INSERT INTO esame(tipoEsame) VALUES ("Ricostruzione dente mediante otturazione a tre o più superfici");
INSERT INTO esame(tipoEsame) VALUES ("Ricostruzione dente mediante intarsio");
INSERT INTO esame(tipoEsame) VALUES ("Applicazione di corona");
INSERT INTO esame(tipoEsame) VALUES ("Applicazione di corona in lega aurea");
INSERT INTO esame(tipoEsame) VALUES ("Altra applicazione corona");
INSERT INTO esame(tipoEsame) VALUES ("Applicazione corona e perno");
INSERT INTO esame(tipoEsame) VALUES ("Altra applicazione corona e perno");
INSERT INTO esame(tipoEsame) VALUES ("Inserzione di ponte fisso");
INSERT INTO esame(tipoEsame) VALUES ("Inserzione di protesi rimovibile");
INSERT INTO esame(tipoEsame) VALUES ("Altra inserzione di protesi");
INSERT INTO esame(tipoEsame) VALUES ("Inserzione di protesi provvisoria");
INSERT INTO esame(tipoEsame) VALUES ("Altra riparazione dentaria");
INSERT INTO esame(tipoEsame) VALUES ("Impianto di dente");
INSERT INTO esame(tipoEsame) VALUES ("Impianto di protesi dentaria");
INSERT INTO esame(tipoEsame) VALUES ("Terapia canalare in monoradicolato");
INSERT INTO esame(tipoEsame) VALUES ("Terapia canalare in pluriradicolato");
INSERT INTO esame(tipoEsame) VALUES ("Apicectomia");
INSERT INTO esame(tipoEsame) VALUES ("Gengivoplastica");
INSERT INTO esame(tipoEsame) VALUES ("Asportazione di tessuto della gengiva");
INSERT INTO esame(tipoEsame) VALUES ("Levigatura delle radici");
INSERT INTO esame(tipoEsame) VALUES ("Intervento chirurgico preprotesico");
INSERT INTO esame(tipoEsame) VALUES ("Asportazione di lesione dentaria della mandibola");
INSERT INTO esame(tipoEsame) VALUES ("Trattamento ortodontico con apparecchi mobili");
INSERT INTO esame(tipoEsame) VALUES ("Trattamento con apparecchi fissi");
INSERT INTO esame(tipoEsame) VALUES ("Trattamento con apparecchi funzionali");
INSERT INTO esame(tipoEsame) VALUES ("Riparazione di apparecchio ortodontico");
INSERT INTO esame(tipoEsame) VALUES ("Radiologia diagnostica");
INSERT INTO esame(tipoEsame) VALUES ("Tomografia computerizzata del rachide");
INSERT INTO esame(tipoEsame) VALUES ("Tomografia computerizzata con contrasto");
INSERT INTO esame(tipoEsame) VALUES ("Tomografia computerizzata dell’arto superiore (solo con patologia traumatica acuta)");
INSERT INTO esame(tipoEsame) VALUES ("Tomografia computerizzata dell’arto superiore senza e con contrasto (patologia o sospetto oncologico)");
INSERT INTO esame(tipoEsame) VALUES ("Tomografia computerizzata dell’arto inferiore (patologia traumatica)");
INSERT INTO esame(tipoEsame) VALUES ("Tomografia computerizzata dell’arto inferiore senza e con contrasto (patologia o sospetto oncologico)");
INSERT INTO esame(tipoEsame) VALUES ("Risonanza magnetica nucleare (RM) della colonna cervicale");
INSERT INTO esame(tipoEsame) VALUES ("Risonanza magnetica nucleare (RM) della colonna senza e con contrasto");
INSERT INTO esame(tipoEsame) VALUES ("Risonanza magnetica nucleare (RM) muscoloscheletrica");
INSERT INTO esame(tipoEsame) VALUES ("Risonanza magnetica nucleare (RM) muscoloscheletrica senza e con contrasto");
INSERT INTO esame(tipoEsame) VALUES ("Densitometria ossera");
INSERT INTO esame(tipoEsame) VALUES ("Deossicortisolo");
INSERT INTO esame(tipoEsame) VALUES ("Acido 5 idrossi 3 indolacetico");
INSERT INTO esame(tipoEsame) VALUES ("Acido delta");
INSERT INTO esame(tipoEsame) VALUES ("Ala deidrasi");
INSERT INTO esame(tipoEsame) VALUES ("Alanina");
INSERT INTO esame(tipoEsame) VALUES ("Albumina");
INSERT INTO esame(tipoEsame) VALUES ("Aldolasi");
INSERT INTO esame(tipoEsame) VALUES ("Alfa amilasi");
INSERT INTO esame(tipoEsame) VALUES ("Alfa amilasi isoenzimi");
INSERT INTO esame(tipoEsame) VALUES ("Androstenediolo");
INSERT INTO esame(tipoEsame) VALUES ("Aspartato aminotrasferiasi");
INSERT INTO esame(tipoEsame) VALUES ("Calcio totale");
INSERT INTO esame(tipoEsame) VALUES ("Colesterolo HDL");
INSERT INTO esame(tipoEsame) VALUES ("Colesterolo LDL");
INSERT INTO esame(tipoEsame) VALUES ("Colesterolo totale");
INSERT INTO esame(tipoEsame) VALUES ("Creatinchinasi");
INSERT INTO esame(tipoEsame) VALUES ("Creatinina");
INSERT INTO esame(tipoEsame) VALUES ("Cromo");
INSERT INTO esame(tipoEsame) VALUES ("Enolasi");
INSERT INTO esame(tipoEsame) VALUES ("Ferro");
INSERT INTO esame(tipoEsame) VALUES ("Fosfatasi acida");
INSERT INTO esame(tipoEsame) VALUES ("Fosfatasi alcalina");
INSERT INTO esame(tipoEsame) VALUES ("Fosfatasi alcalina isoenzima osseo");
INSERT INTO esame(tipoEsame) VALUES ("Fosfato inorganico");
INSERT INTO esame(tipoEsame) VALUES ("Lattato");
INSERT INTO esame(tipoEsame) VALUES ("lipasi");
INSERT INTO esame(tipoEsame) VALUES ("Magnesio");
INSERT INTO esame(tipoEsame) VALUES ("Mioglobina");
INSERT INTO esame(tipoEsame) VALUES ("Potassio");
INSERT INTO esame(tipoEsame) VALUES ("Proteine");
INSERT INTO esame(tipoEsame) VALUES ("Sodio");
INSERT INTO esame(tipoEsame) VALUES ("Sudore");
INSERT INTO esame(tipoEsame) VALUES ("Trigliceridi");
INSERT INTO esame(tipoEsame) VALUES ("Urato Urea");
INSERT INTO esame(tipoEsame) VALUES ("Alfa 2");
INSERT INTO esame(tipoEsame) VALUES ("Anticorpi anti microsomi");
INSERT INTO esame(tipoEsame) VALUES ("Antigene carboidratico 125");
INSERT INTO esame(tipoEsame) VALUES ("Antigene carboidratico15.3");
INSERT INTO esame(tipoEsame) VALUES ("Antigene carboidratico 19.9");
INSERT INTO esame(tipoEsame) VALUES ("Antigene carcino embrionario");
INSERT INTO esame(tipoEsame) VALUES ("Antigeni HLA");
INSERT INTO esame(tipoEsame) VALUES ("beta tromboglobulina");
INSERT INTO esame(tipoEsame) VALUES ("cyfra");
INSERT INTO esame(tipoEsame) VALUES ("Eparina");
INSERT INTO esame(tipoEsame) VALUES ("Fenotipo RH");
INSERT INTO esame(tipoEsame) VALUES ("Glicoproteina");
INSERT INTO esame(tipoEsame) VALUES ("Gruppo sanguigno ABO e RH (D)");
INSERT INTO esame(tipoEsame) VALUES ("Ige specifiche allergologiche");
INSERT INTO esame(tipoEsame) VALUES ("Ige specifiche allergologiche screening qualitativo");
INSERT INTO esame(tipoEsame) VALUES ("Ige specifiche allergologiche");
INSERT INTO esame(tipoEsame) VALUES ("inibitore attivatore del plasminogeno");
INSERT INTO esame(tipoEsame) VALUES ("Tempo di protrombina");
INSERT INTO esame(tipoEsame) VALUES ("Tempo di tromboplastina");
INSERT INTO esame(tipoEsame) VALUES ("Test aggregazione piastrinica");
INSERT INTO esame(tipoEsame) VALUES ("Test resistenza proteina C");
INSERT INTO esame(tipoEsame) VALUES ("Tipizzazione genomica");
INSERT INTO esame(tipoEsame) VALUES ("Tipizzazione genomica HLA – A sequenziamento diretto");
INSERT INTO esame(tipoEsame) VALUES ("Tipizzazione genomica HLA – B");
INSERT INTO esame(tipoEsame) VALUES ("Tipizzazione genomica HLA – B sequenziamento diretto");
INSERT INTO esame(tipoEsame) VALUES ("Tipizzazione genomica HLA – C");
INSERT INTO esame(tipoEsame) VALUES ("Tipizzazione genomica HLA – C seq. diretto");
INSERT INTO esame(tipoEsame) VALUES ("Tipizzazione genomica HLA – DP seq. diretto");
INSERT INTO esame(tipoEsame) VALUES ("Tipizzazione genomica HLA – DP alta risoluzione");
INSERT INTO esame(tipoEsame) VALUES ("Tipizzazione genomica HLA – DQ seq. diretto");
INSERT INTO esame(tipoEsame) VALUES ("Tipizzazione genomica HLA – DQ alta risoluzione");
INSERT INTO esame(tipoEsame) VALUES ("Tipizzazione genomica HLA – DQB1 bassa risoluzione");
INSERT INTO esame(tipoEsame) VALUES ("Tipizzazione genomica HLA – DQB1 alta risoluzione");
INSERT INTO esame(tipoEsame) VALUES ("Tipizzazione genomica HLA – DR seq. diretto");
INSERT INTO esame(tipoEsame) VALUES ("Tipizzazione genomica HLA – DRB bassa risoluzione");
INSERT INTO esame(tipoEsame) VALUES ("Tipizzazione genomica HLA – DRB alta risoluzione");
INSERT INTO esame(tipoEsame) VALUES ("Tipizzazione sierologica HLA classe I");
INSERT INTO esame(tipoEsame) VALUES ("Tipizzazione sierologica HLA classe II");
INSERT INTO esame(tipoEsame) VALUES ("Trombossano B2");
INSERT INTO esame(tipoEsame) VALUES ("Viscosità ematica");
INSERT INTO esame(tipoEsame) VALUES ("Campylobacter antibiogramma");
INSERT INTO esame(tipoEsame) VALUES ("Campylobacter da coltura");
INSERT INTO esame(tipoEsame) VALUES ("Campylobacter esame colturale");
INSERT INTO esame(tipoEsame) VALUES ("Chlamydie ricerca diretta (EIA)");
INSERT INTO esame(tipoEsame) VALUES ("Chlamydie ricerca diretta (IF)");
INSERT INTO esame(tipoEsame) VALUES ("Chlamydie ricerca diretta (ibridazione)");
INSERT INTO esame(tipoEsame) VALUES ("Miceti anticorpi");
INSERT INTO esame(tipoEsame) VALUES ("Miceti lieviti");
INSERT INTO esame(tipoEsame) VALUES ("Salmonelle da coltura");
INSERT INTO esame(tipoEsame) VALUES ("Shigelle");
INSERT INTO esame(tipoEsame) VALUES ("Virus epatite B (HBV) Anticorpi Hbeag");
INSERT INTO esame(tipoEsame) VALUES ("Analisi citogenetica per fragilità cromosomica");
INSERT INTO esame(tipoEsame) VALUES ("Analisi citogenetica per ricerca siti fragili");
INSERT INTO esame(tipoEsame) VALUES ("Analisi citogenetica per scambi di cromatidi");
INSERT INTO esame(tipoEsame) VALUES ("Analisi citogenetica per studio mosaicismo");
INSERT INTO esame(tipoEsame) VALUES ("Analisi citogenetica per studio riarrangiamenti");
INSERT INTO esame(tipoEsame) VALUES ("Analisi DNA e ibridazione con sonda");
INSERT INTO esame(tipoEsame) VALUES ("Analisi DNA per polimorfismo");
INSERT INTO esame(tipoEsame) VALUES ("Analisi mutazione del DNA con reazione polimerasica a catena");
INSERT INTO esame(tipoEsame) VALUES ("Analisi mutazione del DNA con ibridazione sonde non radiomarcate");
INSERT INTO esame(tipoEsame) VALUES ("Analisi mutazione del DNA con ibridazione sonde radiomarcate");
INSERT INTO esame(tipoEsame) VALUES ("Analisi mutazione del DNA con reverse dot blot");
INSERT INTO esame(tipoEsame) VALUES ("Analisi di polimorfismi");
INSERT INTO esame(tipoEsame) VALUES ("Analisi di segmenti di DNA");
INSERT INTO esame(tipoEsame) VALUES ("Cariotipo ad alta risoluzione");
INSERT INTO esame(tipoEsame) VALUES ("Cariotipo da metafasi di fibroblasti");
INSERT INTO esame(tipoEsame) VALUES ("Cariotipo da metafasi linfocitarie");
INSERT INTO esame(tipoEsame) VALUES ("Cariotipo da metafasi spontanee di villi corali");
INSERT INTO esame(tipoEsame) VALUES ("Cariotipo da metafasi di midollo osseo");
INSERT INTO esame(tipoEsame) VALUES ("Colorazione aggiuntiva in bande: Actinomicina D");
INSERT INTO esame(tipoEsame) VALUES ("Colorazione aggiuntiva in bande: bandeggio C");
INSERT INTO esame(tipoEsame) VALUES ("Colorazione aggiuntiva in bande: bandeggio G");
INSERT INTO esame(tipoEsame) VALUES ("Colorazione aggiuntiva in bande: bandeggio G alta ris.");
INSERT INTO esame(tipoEsame) VALUES ("Colorazione aggiuntiva in bande: bandeggio NOR");
INSERT INTO esame(tipoEsame) VALUES ("Colorazione aggiuntiva in bande: bandeggio Q");
INSERT INTO esame(tipoEsame) VALUES ("Colorazione aggiuntiva in bande: bandeggio R");
INSERT INTO esame(tipoEsame) VALUES ("Colorazione aggiuntiva in bande: bandeggio T");
INSERT INTO esame(tipoEsame) VALUES ("Colorazione aggiuntiva in bande: distamicina A");
INSERT INTO esame(tipoEsame) VALUES ("Coltura di amniociti");
INSERT INTO esame(tipoEsame) VALUES ("Coltura di cellule o tessuti");
INSERT INTO esame(tipoEsame) VALUES ("Coltura di fibroblasti");
INSERT INTO esame(tipoEsame) VALUES ("Coltura di linee cellulari stabilizzate con virus");
INSERT INTO esame(tipoEsame) VALUES ("Coltura di linee linfocitarie stabilizzate con virus o interleuchina");
INSERT INTO esame(tipoEsame) VALUES ("Coltura di linfociti fetali");
INSERT INTO esame(tipoEsame) VALUES ("Coltura di linfociti periferici");
INSERT INTO esame(tipoEsame) VALUES ("Coltura di materiale abortivo");
INSERT INTO esame(tipoEsame) VALUES ("Coltura semisolida di cellule emopoietiche");
INSERT INTO esame(tipoEsame) VALUES ("Coltura di villi coriali a breve termine");
INSERT INTO esame(tipoEsame) VALUES ("Coltura di cilli coriali");
INSERT INTO esame(tipoEsame) VALUES ("Coltura per studio del cromosoma X");
INSERT INTO esame(tipoEsame) VALUES ("Conservazione campioni DNA RNA");
INSERT INTO esame(tipoEsame) VALUES ("Crioconservazione in azoto liquido di colture cellulari");
INSERT INTO esame(tipoEsame) VALUES ("Crioconservazione in azoto liquido di cellule e tessuti");
INSERT INTO esame(tipoEsame) VALUES ("Digestione DNA con enzimi");
INSERT INTO esame(tipoEsame) VALUES ("Estrazione DNA o RNA");
INSERT INTO esame(tipoEsame) VALUES ("Ibridazione con sonda molecolare");
INSERT INTO esame(tipoEsame) VALUES ("Ibridazione in SITU (Fish)");
INSERT INTO esame(tipoEsame) VALUES ("Ricerca Mutazione (DGGE)");
INSERT INTO esame(tipoEsame) VALUES ("Ricerca mutazione (SSCP)");
INSERT INTO esame(tipoEsame) VALUES ("Analisi DNA studio citometrico");
INSERT INTO esame(tipoEsame) VALUES ("Orticarie fisiche");
INSERT INTO esame(tipoEsame) VALUES ("Inalanti");
INSERT INTO esame(tipoEsame) VALUES ("Test epicutanei a lettura ritardata");
INSERT INTO esame(tipoEsame) VALUES ("Test a lettura immediata");
INSERT INTO esame(tipoEsame) VALUES ("Tomoscintigrafia miocardica (PET)");
INSERT INTO esame(tipoEsame) VALUES ("Tomoscintigrafia Cerebrale (PET)");
INSERT INTO esame(tipoEsame) VALUES ("Radioterapia stereotassica");
INSERT INTO esame(tipoEsame) VALUES ("Irradiazione cutanea");
INSERT INTO esame(tipoEsame) VALUES ("Terapie e riabilitazioni");
INSERT INTO esame(tipoEsame) VALUES ("Esercizi respiratori per seduta collettiva");
INSERT INTO esame(tipoEsame) VALUES ("Esercizi respiratori per seduta individuale");
INSERT INTO esame(tipoEsame) VALUES ("Tomoscintigrafia globale");
INSERT INTO esame(tipoEsame) VALUES ("Irradiazione cutanea");
INSERT INTO esame(tipoEsame) VALUES ("Terapia del dolore da metastasi ossee");
INSERT INTO esame(tipoEsame) VALUES ("Sigillatura solchi e fossette");
INSERT INTO esame(tipoEsame) VALUES ("Rimozione protesi dentarie");
INSERT INTO esame(tipoEsame) VALUES ("Immunizzazione allergia");
INSERT INTO esame(tipoEsame) VALUES ("Immunizzazione malattia autoimmune");
INSERT INTO esame(tipoEsame) VALUES ("Terapia luce ultravioletta");
INSERT INTO esame(tipoEsame) VALUES ("Splintaggio per gruppi di 4 denti");
INSERT INTO esame(tipoEsame) VALUES ("Trattamento applicazioni protesi semovibili");


-- insert fotografia
insert into foto values(1, "CGLMRC38D08L378S", NULL, "1.png");
insert into foto values(2, "XVRCRL40A01L781R", NULL, "2.png");
insert into foto values(3, "FRRSMN97P21L378I", NULL, "3.png");
insert into foto values(4, "FRRSMN97P21L378I", NULL, "4.png");
insert into foto values(5, "BLKDNL51T21L378R", NULL, "5.png");
insert into foto values(6, "JFFTMS49M12A952L", NULL, "6.png");
insert into foto values(7, "TRKCRS74C21L378D", NULL, "7.png");
insert into foto values(8, "GRLMTT48H16A952G", NULL, "8.png");
insert into foto values(9, "JYCJMS87R10L378T", NULL, "9.png");
insert into foto values(10, "JHNLCU70S51A952T", NULL, "10.png");
insert into foto values(11, "JNKLNE97E47L378R", NULL, "11.png");
insert into foto values(12, "LRCDRD74T08L378I", NULL, "12.png");
insert into foto values(13, "MCCMTH81B20L378V", NULL, "13.png");
insert into foto values(14, "MNDHRY86R23L378H", NULL, "14.png");
insert into foto values(15, "MNNJCB59C18L378B", NULL, "15.png");
insert into foto values(16, "MRTRCR94M17L378L", NULL, "16.png");
insert into foto values(17, "PLASRA09H47A952Q", NULL, "17.png");
insert into foto values(18, "PLLJKA79H19L378I", NULL, "18.png");
insert into foto values(19, "PNKJSS99C02L781D", NULL, "19.png");
insert into foto values(20, "PTRJHN67A31L378J", NULL, "20.png");
insert into foto values(21, "PTRLVR96P05A952M", NULL, "21.png");
insert into foto values(22, "PTRSML59C07L378J", NULL, "22.png");
insert into foto values(23, "PTTNSA78B66L781N", NULL, "23.png");
insert into foto values(24, "PWRRRT66L30L378U", NULL, "24.png");
insert into foto values(25, "RYAMHL65S13L378B", NULL, "25.png");
insert into foto values(26, "SKYLKU37E13A952L", NULL, "26.png");
insert into foto values(27, "SNDNHN29P12L781E", NULL, "27.png");
insert into foto values(28, "SNTFNK60H21L378W", NULL, "28.png");
insert into foto values(29, "STVLCY61S51L781U", NULL, "29.png");
insert into foto values(30, "TRMTHN96L10A952G", NULL, "30.png");
insert into foto values(31, "VLPGNN53D12L781Y", NULL, "31.png");
insert into foto values(32, "VNSRHR92C27A952I", NULL, "32.png");
insert into foto values(33, "WGNLCU53M63L781L", NULL, "33.png");
insert into foto values(34, "WHTMLE03M70A952U", NULL, "34.png");
insert into foto values(35, "WLKFNC05C54A952O", NULL, "35.png");
insert into foto values(36, "WLLDVD87M26A952P", NULL, "36.png");
insert into foto values(37, "WLSTNY50L59L781F", NULL, "37.png");
insert into foto values(38, "BTLLSI69T20L781B", NULL, "38.png");
insert into foto values(39, "BYRPLA43P53L781K", NULL, "39.png");
insert into foto values(40, "CHVFRC90R07L378X", NULL, "40.png");
insert into foto values(41, "CLRLCU99E23L378W", NULL, "41.png");
insert into foto values(42, "CLRSFO75L66A952R", NULL, "42.png");
insert into foto values(43, "CRPMBR98A56L781K", NULL, "43.png");
insert into foto values(44, "CRVBBY88R53L378S", NULL, "44.png");
insert into foto values(45, "CSYLEI45P20L781B", NULL, "45.png");
insert into foto values(46, "DFAWLM53S06L378I", NULL, "46.png");
insert into foto values(47, "DLGJLU01S45L378L", NULL, "47.png");
insert into foto values(48, "DLGMRM60C42A952V", NULL, "48.png");
insert into foto values(49, "DVLKST88M48A952Q", NULL, "49.png");
insert into foto values(50, "FRDHRS64H03L378B", NULL, "50.png");
insert into foto values(51, "FSLGNN97E20L378C", NULL, "51.png");
insert into foto values(52, "FTZSLV73D55L781X", NULL, "52.png");
insert into foto values(53, "GLLDRN93T22A952A", NULL, "53.png");
insert into foto values(54, "HMPSLL55A58L781O", NULL, "54.png");
insert into foto values(55, "HWLMBL66C42L781H", NULL, "55.png");
insert into foto values(56, "HYSLSO90L17A952I", NULL, "56.png");


-- insert medicinali
INSERT INTO medicinale(tipoMedicinale) VALUES ('Aripiprazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Irbesartan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Amlodipina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fluticasone/formoterolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ezetimibe');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fentanil');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Travoprost');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acarbosio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Carvedilolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Filgrastim');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Quinapril');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Quinapril/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Aceclofenac');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Captopril/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Enalapril/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Aciclovir');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sodio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pregabalin');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acqua');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sodio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fluorouracile/acido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Estradiolo/noretisterone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pioglitazone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Insulina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Insulina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cabergolina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Nifedipina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tramadolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tadalafil');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Rizatriptan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Scopolamina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Riociguat');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Febuxostat');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Amitriptilina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Nitroglicerina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Anastrozolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tacrolimus');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Adrenalina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Metilprednisolone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Octocog');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Flunisolide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Desloratadina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Salmeterolo/fluticasone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Metotrexato');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lonoctocog');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Omega');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Dipiridamolo/acido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Levofloxacina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Rasagilina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fattore');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Budesonide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Montelukast');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Isotretinoina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Biperidene');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Rabeprazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Beclometasone/formoterolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ramipril/Amlodipina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lisinopril');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Betametasone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Albumina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Spironolattone/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Spironolattone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Imiquimod');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Metildopa');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Alfuzosina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Buprenorfina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Nimesulide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Etoricoxib');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ibuprofene');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Duloxetina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Melfalan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ketoprofene');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Allopurinolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Flecainide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Salbutamolo/ipratropio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Almotriptan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Nebivololo/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Aloperidolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fluconazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Alfacalcidolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Brimonidina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fattore');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Simvastatina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Eftrenonacog');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Valsartan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Diltiazem');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ciclesonide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lamotrigina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Glimepiride');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Escitalopram');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Amikacina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Aminofillina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Teofillina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Amiodarone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Amisulpride');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sodio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Amoxicillina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ampicillina/sulbactam');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ampicillina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Omeprazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Clomipramina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Anagrelide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Testosterone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ciproterone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Estradiolo/drospirenone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Colecalciferolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Umeclidinio/Vilanterolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Doxofillina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Naltrexone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sucralfato');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tamsulosina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Insulina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pravastatina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ticlopidina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Apomorfina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lacidipina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pantoprazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Darbepoetina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Leflunomide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Mesalazina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Donepezil');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Esomeprazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fondaparinux');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Atorvastatina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Exemestane');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Triesifenidile');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Nabumetone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Valsartan/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Diclofenac/misoprostolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Mometasone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Budesonide/formoterolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Roxitromicina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Finasteride');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ipratropio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Atenololo/clortalidone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Atenololo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Formoterolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Enalapril/lercanidipina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Atropina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Teriflunomide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Frovatriptan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Moxifloxacina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ramipril/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Clopidogrel');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Dutasteride');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Interferone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cetirizina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ceftriaxone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Azitromicina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Azatioprina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Brinzolamide/timololo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Brinzolamide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Bacampicillina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Baclofene');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cefaclor');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Trimetoprim/sulfametoxazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sildenafil');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Balsalazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Entecavir');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tenoxicam');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ciprofloxacina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Doxiciclina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cefotaxima');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Nitrendipina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Beclometasone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Follitropina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Benazepril/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Benazepril');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Nonacog');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ketorolac');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Doxazosina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fattore');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Proteina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fosfomicina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Dorzolamide/timololo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Interferone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Betaxololo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Bezafibrato');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Bicalutamide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Zofenopril');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Zofenopril/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Bimatoprost');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Epoetina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Calcio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Nicardipina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Bisoprololo/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Bisoprololo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Olmesartan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Timololo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Candesartan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Candesartan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Mirtazapina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Bosentan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tiotropio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tobramicina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Letrozolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Aclidinio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Piroxicam');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ticagrelor');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Aclidinio/Formoterolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Vortioxetina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Brivudina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Bromocriptina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Salbutamolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ibuprofene/codeina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Midazolam');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Bupropione');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Exenatide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Calcio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Calcio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Eparina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Calcipotriolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Calcitriolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Calcio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acamprosato');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Candesartan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Capecitabina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Desametasone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Captopril');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Carbamazepina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Litio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lercanidipina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Levocarnitina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Carteololo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Isosorbide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Clonidina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Alprostadil');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cefazolina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cefixima');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cefpodoxima');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ceftazidima');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cefuroxima');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Celecoxib');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Micofenolato');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cefepime');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cefalexina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Eliglustat');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Everolimus');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ketotifene');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lomefloxacina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Prulifloxacina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Rosuvastatina/Ezetimibe');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Citalopram');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ciclosporina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Claritromicina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Loratadina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Beclometasone/salbutamolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ebastina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Enoxaparina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Estradiolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Clindamicina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Isradipina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Clobetasolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Clomifene');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Zuclopentixolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Clorochina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Clozapina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Irbesartan/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Paracetamolo/codeina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Famciclovir');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Rosuvastatina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Colchicina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Idrocortisone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Estriolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Promestriene');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pirantel');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Timololo/brimonidina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Estradiolo/levonorgestrel');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pioglitazone/metformina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Entacapone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Bazedoxifene');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pralidoxima');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Enalapril');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Glatiramer');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ribavirina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Levodopa/carbidopa/entacapone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Celiprololo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ivabradina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Metirapone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cortisone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Bisoprololo/perindopril');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Warfarin');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Perindopril/amlodipina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Perindopril');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pancrelipasi');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Isavuconazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Progesterone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ferroso');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cefprozil');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Betaina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Mercaptamina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Misoprostolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Daclatasvir');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Danazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Levodropropizina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Glibenclamide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Paroxetina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Roflumilast');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Diclofenac');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Triptorelina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Deferoxamina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Metronidazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Delapril');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Delapril/indapamide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Prednisone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Memantina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sodio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Oxicodone/paracetamolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Magnesio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Valpromide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Medrossiprogesterone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Prednicarbato');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Diflucortolone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Gliclazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Metformina/glibenclamide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acetazolamide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Etinilestradiolo/ciproterone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Calcifediolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fluoxetina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cefodizima');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lisinopril/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fenitoina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Potassio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Torasemide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cianocobalamina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Dorzolamide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fenoterolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Calcipotriolo/betametasone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pilocarpina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Latanoprost/timololo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Latanoprost');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Didrogesterone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fluvoxamina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Clopidogrel/acido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Timololo/travoprost');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fenoterolo/ipratropio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Isosorbide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ramipril');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Topiramato');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Reboxetina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Venlafaxina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sitagliptin/metformina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Prasugrel');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Oxicodone/naloxone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Vilanterolo/umeclidinio/fluticasone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Eletriptan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Leuprorelina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Apixaban');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fosinopril');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Efmoroctocog');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Corifollitropina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Desmopressina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ciclofosfamide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sacubitril/Valsartan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Clotiapina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Repaglinide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lattulosio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Eparina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sofosbuvir/Velpatasvir');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Levetiracetam');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Eplerenone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Epoetina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Metilfenidato');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Eritromicina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lansoprazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Manidipina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Metaciclina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ulipristal');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Estramustina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Etambutolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Disulfiram');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Etinilestradiolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Vildagliptin/metformina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Digossina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Amtolmetina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Levotiroxina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Raloxifene');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Rivastigmina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Deferasirox');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Dasabuvir');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ezetimibe/simvastatina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Famotidina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Toremifene');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Prometazina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Benralizumab');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Complesso');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Felodipina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Moexipril/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Estradiolo/didrogesterone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Dexibuprofene');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fenofibrato');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Deferiprone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sodio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ferroso');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fexofenadina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Insulina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Degarelix');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Moxonidina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fluticasone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Flucloxacillina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fludarabina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fluocinolone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Furosemide/triamterene');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Flutamide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fluvastatina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Parnaparina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Teriparatide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Dapagliflozin');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Losartan/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fosinopril/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Urofollitropina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lantanio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Delapril/Manidipina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Flurbiprofene');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Griseofulvina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Furosemide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Perampanel');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Gabapentin');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tiagabina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Magaldrato');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Migalastat');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Galantamina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Vildagliptin');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Timololo/bimatoprost');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fenobarbital');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sodio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Gemfibrozil');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Somatropina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Megestrolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cefditoren');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fingolimod');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ranitidina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Imatinib');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Glucagone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Metformina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Glucosio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Gliquidone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Trandolapril');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Granisetrone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lenograstim');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Polline');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fattore');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Alcinonide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Zolmitriptan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Propranololo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Emicizumab');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Adefovir');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Indacaterolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Insulina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Insulina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Paromomicina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Insulina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Topotecan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Piperacillina/tazobactam');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Olmesartan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Olmesartan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Prednisolone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Albutrepenonacog');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Diidrochinidina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Immunoglobulina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Immunoglobulina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Immunoglobulina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Clortalidone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sumatriptan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Bromperidolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Alogliptin/Pioglitazone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Umeclidinio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Indapamide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Indometacina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cilazapril/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cilazapril');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Rufinamide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ferro');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lipidi');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Neostigmina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Interferone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Paliperidone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Canagliflozin');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Apraclonidina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Josamicina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lanreotide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ceftibuten');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Verapamil');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Mannitolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Itraconazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Terazosina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Bemiparina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sitagliptin');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Empagliflozin');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Metformina/Linagliptin');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tolvaptan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Selegilina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Idromorfone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Potassio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ivacaftor');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sodio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Potassio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Triamcinolone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tamoxifene');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Telitromicina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ketoconazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Saxagliptin/Metformina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fitomenadione');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Gonadorelina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sapropterina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Terbinafina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lamivudina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Mianserina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Clorpromazina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Meflochina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Naproxene');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Furosemide/spironolattone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Losartan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lurasidone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Olanzapina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Idrossiprogesterone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Clorambucile');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Insulina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Vardenafil');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Levosulpiride');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Levocetirizina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Levodopa/benserazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Levodopa/carbidopa');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Vancomicina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Barnidipina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Alizapride');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lincomicina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Linezolid');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Liotironina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tibolone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Edoxaban');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Nebivololo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lomitapide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Octreotide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lipegfilgrastim');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Trifluridina/Tipiracil');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Metoprololo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Miconazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lovastatina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tafluprost/Timololo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Nomegestrolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lutropina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Canrenone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Mitotano');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lixisenatide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Miocamicina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Glecaprevir/Pibrentasvir');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cefoxitina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Meloxicam');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Menotropina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Piridostigmina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Metadone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cefmetazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Metilergometrina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Metoclopramide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Telmisartan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Telmisartan/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Miglustat');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cinacalcet');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Glipizide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Minociclina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ritodrina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pramipexolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Metossipolietilenglicole-epoetina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Mizolastina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Trifluoperazina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Flufenazina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Amiloride/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Rufloxacina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Morfina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Rupatadina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Naloxegol');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Morfina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Dronedarone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Rifabutina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Nistatina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Primidone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Estradiolo/nomegestrolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Procarbazina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Vinorelbina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Idroxocobalamina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Roxatidina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Epoetina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acitretina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sodio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pegfilgrastim');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Periciazina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Rotigotina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Isoniazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ferroso');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Nimesulide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Nitisinone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Nizatidina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Perindopril/indapamide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Norfloxacina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Nortriptilina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Rifaximina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Atenololo/indapamide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Turoctocog');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Posaconazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lornoxicam');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Levomepromazina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Brivaracetam');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Mepolizumab');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Simoctocog');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Idroxicarbamide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ondansetrone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Opicapone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Saxagliptin');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Macitentan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Polline');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pimozide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lumacaftor/Ivacaftor');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Stronzio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Oxibutinina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Oxicodone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ossigeno');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Calcio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Apremilast');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Coriogonadotropina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Oxcarbazepina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Oxitropio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tapentadolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Paracalcitolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Diidrocodeina/acido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Diidrocodeina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Etelcalcetide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pefloxacina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Peginterferone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Peginterferone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Oxacillina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pentamidina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Follitropina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pentaeritritile');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ingenolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Piperacillina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pirazinamide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Idroxiclorochina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Peginterferone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Salbutamolo/flunisolide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lattitolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Dabigatran');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Alirocumab');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Gonadotropina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Letermovir');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ramipril/piretanide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Noretisterone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Diazossido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Denosumab');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Propafenone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Protamina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fattore');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Modafinil');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Proglumetacina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Desossiribonucleasi');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Follitropina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Mercaptopurina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Bismuto');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Quetiapina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Colestiramina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ranolazina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sirolimus');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Aliskiren/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Aliskiren');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Glicerolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Idebenone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cariprazina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Moroctocog');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Follitropina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Metilnaltrexone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fluticasone/Vilanterolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sevelamer');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Evolocumab');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ropinirolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Epoetina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Rifampicina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Isoniazide/pirazinamide/rifampicina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Rifampicina/isoniazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Riluzolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Risperidone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Disopiramide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Clonazepam');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Nonacog');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Interferone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Spiramicina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sotalolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Vigabatrin');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tafluprost');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sulfasalazina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Salmeterolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pizotifene');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Calcio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Telbivudina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acebutololo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Glicopirronio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Nadroparina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tiapride');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sertralina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Carbomer');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Benzilpenicillina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pasireotide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Silodosina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cinnoxicam');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acenocumarolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Melevodopa/carbidopa');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tedizolid');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Dimetilfumarato');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sodio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pegvisomant');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Calcio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Bornaprina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Floroglucinolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pipetanato');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Avanafil');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tiotropio/Olodaterolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Regorafenib');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Atomoxetina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Olodaterolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Buprenorfina/naloxone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Insulina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Buserelin');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Trimipramina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Asenapina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Palivizumab');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Empagliflozin/Metformina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Nisoldipina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Valaciclovir');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Felbamato');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pioglitazone/glimepiride');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tiamazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Teicoplanina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Bexarotene');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tolcapone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Acido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Temozolomide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tetrabenazina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Limeciclina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Eprosartan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tegafur/gimeracil/oteracil');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Eprosartan/idroclorotiazide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Nedocromile');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Oxatomide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tioguanina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tolterodina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Fluocinonide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Linagliptin');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Labetalolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Metixene');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Insulina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Felodipina/ramipril');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Beclometasone/formoterolo/glicopirronio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tinidazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Perindopril/indapamide/amlodipina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Trazodone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Retigabina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Dulaglutide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Indacaterolo/Glicopirronio');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Selexipag');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Cinoxacina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Mesna');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Valganciclovir');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tacalcitolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ossidrossido');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Iloprost');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Etoposide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Te''');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Mebendazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lidocaina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tretinoina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Voriconazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Liraglutide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ombitasvir/Paritaprevir/Ritonavir');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lacosamide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Alogliptin/Metformina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Alogliptin');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Pindololo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Levobunololo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Canagliflozin/Metformina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ambrisentan');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Sofosbuvir/Velpatasvir/Voxilaprevir');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Zinco');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Safinamide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Rivaroxaban');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Dapagliflozin/Metformina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Omalizumab');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Insulina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Niclosamide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Etosuccimide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Metolazone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Idarubicina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Eslicarbazepina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Ziprasidone');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Albendazolo');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Elbasvir/Grazoprevir');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Netilmicina');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Goserelin');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Zonisamide');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Tazarotene');
INSERT INTO medicinale(tipoMedicinale) VALUES ('Lesinurad');









 