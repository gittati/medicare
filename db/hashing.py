##IMPORT
import mysql.connector
import hashlib


##DOC
__author__ = 'MediCare Team'
__description__ = "Genera hash delle password e aggiorna il database"


##VARIABILI
global boolD
boolD = True


##FUNZIONI


def fn_generaconnessione():
    '''
    Funzione che genera e ritorna la connessione a mysql
    '''
    connessione = mysql.connector.connect(user='root', password='admin', host='127.0.0.1', database='ospedale')
    return connessione


def fn_getPasswords():
    '''
    Funzione che carica su vettore tutte le password
    '''
    connessione = fn_generaconnessione()
    cursore = connessione.cursor()

    query = "SELECT id, password FROM login;"
    cursore.execute(query);
    vett = []

    for i in cursore:
        row = str(i).split(",")
        index = row[0]
        index = index.replace("(", "")
        password = row[1]
        password = password.replace(" '", "")
        password = password.replace("')", "")
        vett.insert(int(index)-1, password)
        
    connessione.commit()
    connessione.close()
    return vett


def fn_addSalts(passwords):
    '''
    Funzione che carica su vettore tutti i salts
    '''
    connessione = fn_generaconnessione()
    cursore = connessione.cursor()

    query = "SELECT id, salt FROM login;"
    cursore.execute(query);
    vett = []
    
    for i in cursore:
        row = str(i).split(",")
        index = row[0]
        index = index.replace("(", "")
        salt = row[1]
        salt = salt.replace(" '", "")
        salt = salt.replace("')", "")
        vett.insert(int(index)-1, passwords[int(index)-1] + salt);

    connessione.commit()
    connessione.close()
    return vett



def fn_generaHash(passwordsSalts):
    '''
    Funzione che genera gli hash delle password e le carica su vettore 
    '''
    vett = []

    for i in range(0, len(passwordsSalts)):
        value = passwordsSalts[i]
        result = hashlib.md5(value.encode())
        vett.append(result.hexdigest())

    return vett


def fn_caricaHashedPasswords(hashPasswords):
    '''
    Funzione che carica gli hash delle password su database
    '''
    connessione = fn_generaconnessione()
    cursore = connessione.cursor()
    
    for i in range(0, len(hashPasswords)):
        query = "update login set password = '" + hashPasswords[i] + "' where id = " + str(i+1) + ";"
        cursore.execute(query);

    connessione.commit()
    connessione.close()


##ELABORAZIONE
if __name__ == "__main__":
    if(boolD):
        print("inizio programma")


    passwords = fn_getPasswords()
    passwordsSalts = fn_addSalts(passwords)
    hashPasswords = fn_generaHash(passwordsSalts)
    fn_caricaHashedPasswords(hashPasswords)

        
    if(boolD):
        print("Fine programma")
